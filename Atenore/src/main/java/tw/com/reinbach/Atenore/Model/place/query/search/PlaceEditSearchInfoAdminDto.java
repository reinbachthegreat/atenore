package tw.com.reinbach.Atenore.Model.place.query.search;

import java.time.LocalDateTime;

public class PlaceEditSearchInfoAdminDto {

	private String editID;

	private String account;

	private Boolean inUsed;

	private LocalDateTime insertTime;

	public String getEditID() {
		return editID;
	}

	public void setEditID(String editID) {
		this.editID = editID;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Boolean getInUsed() {
		return inUsed;
	}

	public void setInUsed(Boolean inUsed) {
		this.inUsed = inUsed;
	}

	public LocalDateTime getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(LocalDateTime insertTime) {
		this.insertTime = insertTime;
	}

}
