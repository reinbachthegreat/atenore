package tw.com.reinbach.Atenore.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ViewsController {

	// 前台API測試頁面
	@RequestMapping(value = { "/test" })
	public String memberLogin(HttpServletRequest request) {
		if (request.getRequestURI().equals("/login")) {
			return "redirect:/test";
		}
		return "RestAPI/RestAPI_TEST.html";
	}
	
	@RequestMapping(value = "/privacyPolicy", method = RequestMethod.GET)
	public String getPrivacyPolicy() {
		return "privacy-policy.html";
	}

	@RequestMapping(value = "/clause", method = RequestMethod.GET)
	public String getUserClause() {
		return "user-clause.html";
	}
	
//	後台 view
	@RequestMapping(value = { "/admin/login" })
	public String adminLogin(HttpServletRequest request, Model model, HttpSession session) {

		return "admin/login.html";
	}

//	--admin account
	@RequestMapping(value = { "/admin/", "/admin", "/admin/index" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String adminIndex(HttpServletRequest request, Model model, HttpSession session) {
		return "admin/index.html";
	}

	@RequestMapping(value = { "/admin/account/edit" })
	public String adminEdit(HttpServletRequest request, Model model, HttpSession session) {

		return "admin/accountEdit.html";
	}

	@RequestMapping(value = { "/admin/account/password" })
	public String adminPWEdit(HttpServletRequest request, Model model, HttpSession session) {

		return "admin/accountPW.html";
	}

//	--admin accounts and feature setting
	@RequestMapping(value = { "/admin/admin/account-list" })
	public String adminListpage(HttpServletRequest request, Model model, HttpSession session) {

		return "admin/admin/accountList.html";
	}

	@RequestMapping(value = { "/admin/admin/account-list/new" })
	public String adminListNew(HttpServletRequest request, Model model, HttpSession session) {

		return "admin/admin/accountListNew.html";
	}

	@RequestMapping(value = { "/admin/admin/account-list/edit/{adminID}" })
	public String adminListEdit(HttpServletRequest request, @PathVariable("adminID") String adminID, Model model,
			HttpSession session) {

		return "admin/admin/accountListEdit.html";
	}

	@RequestMapping(value = { "/admin/admin/feature-list" })
	public String featureList(HttpServletRequest request, Model model, HttpSession session) {

		return "admin/admin/featureList.html";
	}

	@RequestMapping(value = { "/admin/admin/feature-list/new" })
	public String featureListNew(HttpServletRequest request, Model model, HttpSession session) {

		return "admin/admin/featureListNew.html";
	}

	@RequestMapping(value = { "/admin/admin/feature-list/edit/{fID}" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String featureListEdit(HttpServletRequest request, Model model, @PathVariable("fID") String featureID,
			HttpSession session) {

		model.addAttribute("fID", featureID);
		return "admin/admin/featureListEdit.html";
	}

	// --user management
	@RequestMapping(value = { "/admin/user/account-list" })
	public String userList(HttpServletRequest request, Model model, HttpSession session) {

		return "admin/user/accountList.html";
	}

	@RequestMapping(value = { "/admin/user/account/new" })
	public String userListNew(HttpServletRequest request, Model model, HttpSession session) {

		return "admin/user/accountNew.html";
	}

	@RequestMapping(value = { "/admin/user/account/edit/{mID}" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String userListEdit(HttpServletRequest request, Model model, @PathVariable("mID") String memberID, HttpSession session) {

		model.addAttribute("mID", memberID);
		return "admin/user/accountEdit.html";
	}

	// --ads
	@RequestMapping(value = { "/admin/ad/ad-list" })
	public String adList(HttpServletRequest request, Model model, HttpSession session) {

		return "admin/ad/adList.html";
	}

	@RequestMapping(value = { "/admin/ad/ad-list/new" })
	public String adListNew(HttpServletRequest request, Model model, HttpSession session) {

		return "admin/ad/adNew.html";
	}

	@RequestMapping(value = { "/admin/ad/ad-list/edit/{aID}" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String adListEdit(HttpServletRequest request, Model model, @PathVariable("aID") String aID, HttpSession session) {

		model.addAttribute("aID", aID);
		return "admin/ad/adEdit.html";
	}

	// 分類管理
	// --活動
	@RequestMapping(value = { "/admin/cate/event/cate-list" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String eventCateList(HttpServletRequest request, HttpSession session) {

		return "admin/event/cate-list.html";
	}

	@RequestMapping(value = { "/admin/cate/event/s-cate-list/{cID}" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String eventSCateList(HttpServletRequest request, HttpSession session, Model model, @PathVariable("cID") String cateID) {

		model.addAttribute("cID", cateID);
		return "admin/event/cate-s-list.html";
	}

	@RequestMapping(value = { "/admin/cate/event/event-list/{cID}" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String cateEventList(HttpServletRequest request, HttpSession session, Model model, @PathVariable("cID") String cateID) {

		model.addAttribute("cID", cateID);
		return "admin/event/cate-event-list.html";
	}

	@RequestMapping(value = { "/admin/cate/event/s/event-list/{cID}" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String cateEventSList(HttpServletRequest request, HttpSession session, Model model, @PathVariable("cID") String cateID) {

		model.addAttribute("cID", cateID);
		return "admin/event/cate-s-event-list.html";
	}

	@RequestMapping(value = { "/admin/cate/event/event-info/{eID}" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String eventDetail(HttpServletRequest request, HttpSession session, Model model, @PathVariable("eID") String eventID) {

		model.addAttribute("eID", eventID);
		return "admin/event/cate-event-detail.html";
	}

	// --場所
	@RequestMapping(value = { "/admin/cate/place/cate-list" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String placeCateList(HttpServletRequest request, HttpSession session) {

		return "admin/place/cate-list.html";
	}

	@RequestMapping(value = { "/admin/cate/place/s-cate-list/{cID}" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String placeSCateList(HttpServletRequest request, HttpSession session, Model model, @PathVariable("cID") String cateID) {

		model.addAttribute("cID", cateID);
		return "admin/place/cate-s-list.html";
	}

	@RequestMapping(value = { "/admin/cate/place/place-list/{cID}" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String catePlaceList(HttpServletRequest request, HttpSession session, Model model, @PathVariable("cID") String cateID) {

		model.addAttribute("cID", cateID);
		return "admin/place/cate-place-list.html";
	}

	@RequestMapping(value = { "/admin/cate/place/s/place-list/{cID}" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String cateSPlaceList(HttpServletRequest request, HttpSession session, Model model, @PathVariable("cID") String cateID) {

		model.addAttribute("cID", cateID);
		return "admin/place/cate-s-place-list.html";
	}

	@RequestMapping(value = { "/admin/cate/place/place-info/{pID}" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String placeDetail(HttpServletRequest request, HttpSession session, Model model, @PathVariable("pID") String placeID) {

		model.addAttribute("pID", placeID);
		return "admin/place/cate-place-detail.html";
	}

	// --達人
	@RequestMapping(value = { "/admin/cate/master/cate-list" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String masterCateList(HttpServletRequest request, HttpSession session) {

		return "admin/master/cate-list.html";
	}

	@RequestMapping(value = { "/admin/cate/master/m-cate-list/{cID}" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String masterMCateList(HttpServletRequest request, HttpSession session, Model model, @PathVariable("cID") String cateID) {

		model.addAttribute("cID", cateID);
		return "admin/master/cate-m-list.html";
	}

	@RequestMapping(value = { "/admin/cate/master/s-cate-list/{cID}" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String masterSCateList(HttpServletRequest request, HttpSession session, Model model, @PathVariable("cID") String cateID) {

		model.addAttribute("cID", cateID);
		return "admin/master/cate-s-list.html";
	}

	@RequestMapping(value = { "/admin/cate/master/master-list/{cID}" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String cateMasterList(HttpServletRequest request, HttpSession session, Model model, @PathVariable("cID") String cateID) {

		model.addAttribute("cID", cateID);
		return "admin/master/cate-master-list.html";
	}

	@RequestMapping(value = { "/admin/cate/master/m/master-list/{cID}" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String cateMasterMList(HttpServletRequest request, HttpSession session, Model model, @PathVariable("cID") String cateID) {

		model.addAttribute("cID", cateID);
		return "admin/master/cate-m-master-list.html";
	}

	@RequestMapping(value = { "/admin/cate/master/s/master-list/{cID}" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String cateMasterSList(HttpServletRequest request, HttpSession session, Model model, @PathVariable("cID") String cateID) {

		model.addAttribute("cID", cateID);
		return "admin/master/cate-s-master-list.html";
	}

	@RequestMapping(value = { "/admin/cate/master/master-info/{mID}" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String masterDetail(HttpServletRequest request, HttpSession session, Model model, @PathVariable("mID") String masterID) {

		model.addAttribute("mID", masterID);
		return "admin/master/cate-master-detail.html";
	}

	// 內容管理
	// --活動
	@RequestMapping(value = { "/admin/content/event-list" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String contentEventList(HttpServletRequest request, HttpSession session) {

		return "admin/event/contentManage-event-list.html";
	}

	@RequestMapping(value = { "/admin/content/event-list/new" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String contentEventNew(HttpServletRequest request, HttpSession session) {

		return "admin/event/event-new.html";
	}

	@RequestMapping(value = { "/admin/content/event-list/edit/{eID}" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String contentEventEdit(HttpServletRequest request, Model model, @PathVariable("eID") String eventID, HttpSession session) {

		model.addAttribute("eID", eventID);
		return "admin/event/event-edit.html";
	}

	@RequestMapping(value = { "/admin/content/event-list/check-list" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String contentCheckEventList(HttpServletRequest request, HttpSession session) {

		return "admin/event/contentManage-check-event-list.html";
	}

	@RequestMapping(value = { "/admin/content/event-list/check-event/{eID}" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String contentCheckEvent(HttpServletRequest request, HttpSession session, Model model,
			@PathVariable("eID") String eventID) {

		model.addAttribute("eID", eventID);
		return "admin/event/contentManage-check-event.html";
	}

	// 場所
	@RequestMapping(value = { "/admin/content/place-list" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String contentPlaceList(HttpServletRequest request, HttpSession session) {

		return "admin/place/contentManage-place-list.html";
	}

	@RequestMapping(value = { "/admin/content/place-list/new" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String contentPlaceNew(HttpServletRequest request, HttpSession session) {

		return "admin/place/place-new.html";
	}

	@RequestMapping(value = { "/admin/content/place-list/edit/{pID}" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String contentPlaceEdit(HttpServletRequest request, Model model, @PathVariable("pID") String placeID, HttpSession session) {

		model.addAttribute("pID", placeID);
		return "admin/place/place-edit.html";
	}

	@RequestMapping(value = { "/admin/content/place/check-history/{pID}" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String contentCheckPlaceList(HttpServletRequest request, Model model, @PathVariable("pID") String placeID,
			HttpSession session) {

		model.addAttribute("pID", placeID);
		return "admin/place/contentManage-check-place-list.html";
	}

	@RequestMapping(value = { "/admin/content/place/check-history/version/{editID}" }, method = { RequestMethod.GET })
	public String contentCheckPlaceEditDetail(HttpServletRequest request, Model model, @PathVariable("editID") String editID,
			HttpSession session) {

		model.addAttribute("editID", editID);
		return "admin/place/contentManage-check-place-detail.html";
	}

	// 達人
	@RequestMapping(value = { "/admin/content/master-list" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String contentMasterList(HttpServletRequest request, HttpSession session) {

		return "admin/master/contentManage-master-list.html";
	}

	@RequestMapping(value = { "/admin/content/master-list/new" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String contentMasterNew(HttpServletRequest request, HttpSession session) {

		return "admin/master/master-new.html";
	}

	@RequestMapping(value = { "/admin/content/master-list/edit/{mID}" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String contentMasterEdit(HttpServletRequest request, Model model, @PathVariable("mID") String masterID,
			HttpSession session) {

		model.addAttribute("mID", masterID);
		return "admin/master/master-edit.html";
	}

	@RequestMapping(value = { "/admin/content/master/check-history/{mID}" }, method = { RequestMethod.GET })
	public String contentCheckMasterList(HttpServletRequest request, Model model, @PathVariable("mID") String masterID,
			HttpSession session) {

		model.addAttribute("mID", masterID);
		return "admin/master/contentManage-check-master-list.html";
	}

	@RequestMapping(value = { "/admin/content/master/check-history/version/{editID}" }, method = { RequestMethod.GET })
	public String contentCheckMasterEditDetail(HttpServletRequest request, Model model, @PathVariable("editID") String editID,
			HttpSession session) {

		model.addAttribute("editID", editID);
		return "admin/master/contentManage-check-master-detail.html";
	}

//	固定資料------------------------------------------------------------------------------------------------
//	台灣縣市列表
	@RequestMapping(value = { "/fixedData/taiwan_county" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String twCountyList(HttpServletRequest request) {

		return "fixedData/taiwan_county.json";
	}

//	活動、場所、達人
	@RequestMapping(value = { "/fixedData/type_category" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String typeCategoryList(HttpServletRequest request) {

		return "fixedData/type_category.json";
	}

//	test json------------------------------------------------------------------------------------------------
	// login admin account info
	@RequestMapping(value = { "/testData/admin_data" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String adminDataJson(HttpServletRequest request) {

		return "test_data/admin_data.json";
	}

	// admin account list
	@RequestMapping(value = { "/testData/admin_accounts" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String adminAccountsJson(HttpServletRequest request) {

		return "test_data/admin_accounts.json";
	}

	// admin feature list
	@RequestMapping(value = { "/testData/admin_feature" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String adminFeatureJson(HttpServletRequest request) {

		return "test_data/admin_feature.json";
	}

	@RequestMapping(value = { "/testData/admin_feature/info" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String adminFeatureDataJson(HttpServletRequest request) {

		return "test_data/admin_feature_info.json";
	}

	// user accounts
	@RequestMapping(value = { "/testData/user_accounts" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String userAccountsJson(HttpServletRequest request) {

		return "test_data/user_accounts.json";
	}

	@RequestMapping(value = { "/testData/user_data" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String userInfoJson(HttpServletRequest request) {

		return "test_data/user_data.json";
	}

	// ad
	@RequestMapping(value = { "/testData/ad_list" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String adListJson(HttpServletRequest request) {

		return "test_data/ad_list.json";
	}

	@RequestMapping(value = { "/testData/ad_info" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String adDataJson(HttpServletRequest request) {

		return "test_data/ad_info.json";
	}

	// cate
	// --event
	@RequestMapping(value = { "/testData/event_cate_list" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String eventCateJson(HttpServletRequest request) {

		return "test_data/event_cates.json";
	}

	@RequestMapping(value = { "/testData/event_list" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String eventListJson(HttpServletRequest request) {

		return "test_data/event_list.json";
	}

	@RequestMapping(value = { "/testData/event_info" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String eventInfoJson(HttpServletRequest request) {

		return "test_data/event_info.json";
	}

	// --place
	@RequestMapping(value = { "/testData/place_list" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String placeListJson(HttpServletRequest request) {

		return "test_data/place_list.json";
	}

	@RequestMapping(value = { "/testData/place_info" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String placeInfoJson(HttpServletRequest request) {

		return "test_data/place_info.json";
	}

	@RequestMapping(value = { "/testData/master_info" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String masterInfoJson(HttpServletRequest request) {

		return "test_data/master_info.json";
	}

//	-----------------------------------------
	@RequestMapping(value = { "/testData/version_list" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String versionJson(HttpServletRequest request) {

		return "test_data/version_list.json";
	}

	/**
	 * 2020.06.19 Merging
	 * 
	 */
	@RequestMapping(value = { "/index", "/" , "" }, method = { RequestMethod.GET })
	public String frountIndex(HttpServletRequest request) {

		return "frount/index.html";
	}

	@RequestMapping(value = {"/activity/activity-detail/{eID}"},method= {RequestMethod.GET})
	public String frountActivityDetail(HttpSession session, Model model, @PathVariable(required = true)String eID) {	
		model.addAttribute("eventID", eID);
		return "frount/activity/activityDetail.html";
	}

	@RequestMapping(value = { "/place/place-detail/{pID}" }, method = { RequestMethod.GET})
	public String frountPlaceDetail(Model model, @PathVariable(required = true)String pID) {
		model.addAttribute("placeID", pID);
		return "frount/place/placeDetail.html";
	}

	@RequestMapping(value = { "/master/master-detail/{mID}" }, method = { RequestMethod.GET})
	public String frountMasterDetail(Model model,@PathVariable(required = true) String mID) {
		model.addAttribute("mID", mID);
		return "frount/master/masterDetail.html";
	}

	@RequestMapping(value = { "/master/master-list" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String frountMasterList(HttpServletRequest request) {

		return "frount/master/masterList.html";
	}

	@RequestMapping(value = { "/master/hero-list" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String frountHeroList(HttpServletRequest request) {

		return "frount/master/heroList.html";
	}
	
	@RequestMapping(value = { "/master/popular-list" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String frountPopularList(HttpServletRequest request) {

		return "frount/master/popularList.html";
	}

	@RequestMapping(value = { "/master/god-list" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String frountGodList(HttpServletRequest request) {

		return "frount/master/godList.html";
	}

	@RequestMapping(value = { "/place/place-map-list" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String frountPlaceMapList(@RequestParam(required = false) String selectedCountry, Model model) {
		
		if(selectedCountry!=null) {
			model.addAttribute("selectedCountry", selectedCountry);
		}
		
		return "frount/place/placeMapList.html";
	}

	@RequestMapping(value = { "/place/hot-place-list" }, method = { RequestMethod.GET })
	public String frountPlaceList() {		

		return "frount/place/placeList.html";
	}

	@RequestMapping(value = { "/activity/activity-map-list" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String frountActivityMapList(HttpServletRequest request) {

		return "frount/activity/activityMapList.html";
	}

	@RequestMapping(value = { "/activity/new-activity-list", "/activity/hot-activity-list" }, method = { RequestMethod.GET,
			RequestMethod.POST })
	public String frountActivityList(HttpServletRequest request) {

		return "frount/activity/activityList.html";
	}

	@RequestMapping(value = { "/member/member-information/{mID}" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String frountMemInfo(Model model,@PathVariable(required = true) String mID) {
		model.addAttribute("mID", mID);

		return "frount/member/memInfo.html";
	}

	@RequestMapping(value = { "/member/member-favorites/{mID}" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String frountMemFavorits(Model model,@PathVariable(required = true) String mID) {
		model.addAttribute("mID", mID);

		return "frount/member/memFavorits.html";
	}
	
	@RequestMapping(value = {"/member/member-apply/{mID}"},method= {RequestMethod.GET, RequestMethod.POST})
	public String frountMemApply(Model model,@PathVariable(required = true) String mID) {
		model.addAttribute("mID", mID);
	
		return "frount/member/memApply.html";
	}
	
	@RequestMapping(value = {"/member/member-edit-history/{mID}"},method= {RequestMethod.GET, RequestMethod.POST})
	public String frountMemEditHistory(Model model,@PathVariable(required = true) String mID) {
		model.addAttribute("mID", mID);
	
		return "frount/member/memEditHistory.html";
	}
	
	@RequestMapping(value = {"/login"},method= {RequestMethod.GET, RequestMethod.POST})

	public String frountLogin(HttpServletRequest request) {

		return "frount/common/login.html";
	}

	@RequestMapping(value = { "/contact" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String frountContactUs(HttpServletRequest request) {

		return "frount/common/contact.html";
	}

	@RequestMapping(value = { "/65a60f90ed0f345d88616923c519faae/{member}" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String accountActive(HttpServletRequest request, Model model, @PathVariable(required = true) String member) {
		model.addAttribute("userID", member);
		return "frount/account_active.html";
	}

	@RequestMapping(value = { "/c180caed7c8855fc34d51d82f4636769/{token}" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String verifyTokenResetPwd(HttpServletRequest request, Model model, @PathVariable(required = true) String token) {
		model.addAttribute("token", token);
		return "frount/reset_password.html";
	}

}
