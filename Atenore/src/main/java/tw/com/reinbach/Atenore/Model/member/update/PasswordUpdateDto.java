package tw.com.reinbach.Atenore.Model.member.update;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PasswordUpdateDto {

	@ApiModelProperty(hidden = true)
	private String memberID;
	
	@ApiModelProperty(required = true)
	private String originPassword;
	
	@ApiModelProperty(required = true)
	private String updatePaswword;
	
	@ApiModelProperty(required = true)
	private String verfiedPasswrod;

	public String getOriginPassword() {
		return originPassword;
	}

	public void setOriginPassword(String originPassword) {
		this.originPassword = originPassword;
	}

	public String getUpdatePaswword() {
		return updatePaswword;
	}

	public void setUpdatePaswword(String updatePaswword) {
		this.updatePaswword = updatePaswword;
	}

	public String getVerfiedPasswrod() {
		return verfiedPasswrod;
	}

	public void setVerfiedPasswrod(String verfiedPasswrod) {
		this.verfiedPasswrod = verfiedPasswrod;
	}

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}
	
	
	
}
