package tw.com.reinbach.Atenore.Model.master.query.search;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;
import tw.com.reinbach.Atenore.Model.master.component.MasterTag;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MasterDeadSearchInfoDto {

	@ApiModelProperty(example = "123e4567-e89b-42d3-a456-556642440000", dataType = "String")
	private String masterID;
	
	@ApiModelProperty(example = "喵英大統領", dataType = "String")
	private String publicName;
	
	@ApiModelProperty(example = "https://upload.wikimedia.org/wikipedia/commons/1/1b/%E8%94%A1%E8%8B%B1%E6%96%87%E5%AE%98%E6%96%B9%E5%85%83%E9%A6%96%E8%82%96%E5%83%8F%E7%85%A7.png", dataType = "String")
	private String img;
	
	@JsonFormat(pattern = "yyyy.MM.dd")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDateTime death;
	
//	@ApiModelProperty(example = "25.154558", dataType = "Double")
//	private BigDecimal lat;
//	
//	@ApiModelProperty(example = "121.39805", dataType = "Double")
//	private BigDecimal lng;

	@ApiModelProperty(notes = "for master is alive.", example = "台灣喵英大統領是也", dataType = "String")
	private String legend;
	
//	@ApiModelProperty(notes = "for master is alive.", example = "台灣喵英大統領是也", dataType = "String")
//	private String intro;
	
	@ApiModelProperty(hidden = true)
	private String tag1;
	
	@ApiModelProperty(hidden = true)
	private String tag2;
	
	@ApiModelProperty(hidden = true)
	private String tag3;
	
	private List<MasterTag> tagList = new ArrayList<MasterTag>();

	public String getMasterID() {
		return masterID;
	}

	public void setMasterID(String masterID) {
		this.masterID = masterID;
	}

	public String getPublicName() {
		return publicName;
	}

	public void setPublicName(String publicName) {
		this.publicName = publicName;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

//	public BigDecimal getLat() {
//		return lat;
//	}
//
//	public void setLat(BigDecimal lat) {
//		this.lat = lat;
//	}
//
//	public BigDecimal getLng() {
//		return lng;
//	}
//
//	public void setLng(BigDecimal lng) {
//		this.lng = lng;
//	}

//	public String getIntro() {
//		return intro;
//	}
//
//	public void setIntro(String intro) {
//		this.intro = intro;
//	}

	public void setTag1(String tag1) {
		this.tag1 = tag1;
	}

	public void setTag2(String tag2) {
		this.tag2 = tag2;
	}

	public void setTag3(String tag3) {
		this.tag3 = tag3;
	}

	public List<MasterTag> getTagList() {
		
		String [] tagArry = {tag1, tag2, tag3};
		
		for(String t : tagArry) {
			if(t==null){
				continue;
			}
			MasterTag tag = new MasterTag();
			tag.setTag(t);
			this.tagList.add(tag);			
		}
		
		return this.tagList;
	}

	public void setTagList(List<MasterTag> tagList) {
		this.tagList = tagList;
	}
	
	public void addTagList(MasterTag tag) {
		this.tagList.add(tag);
	}

	public String getLegend() {
		return legend;
	}

	public void setLegend(String legend) {
		this.legend = legend;
	}

	public LocalDateTime getDeath() {
		return death;
	}

	public void setDeath(LocalDateTime death) {
		this.death = death;
	}
	
	
	
	
}
