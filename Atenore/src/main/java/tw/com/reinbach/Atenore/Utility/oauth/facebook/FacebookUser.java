package tw.com.reinbach.Atenore.Utility.oauth.facebook;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@SuppressWarnings("serial")
public class FacebookUser implements UserDetails {
	
	private final String id;
	private final String email;
	private final String name;
	private final String first_name;
	private final String middle_name;
	private final String last_name;
	private final URI picture;

	public FacebookUser(
			String id, String email, String name, String first_name, String middle_name, String last_name, URI picture) {
		super();
		this.id = id;
		this.email = email;
		this.name = name;
		this.first_name = first_name;
		this.middle_name = middle_name;
		this.last_name = last_name;
		this.picture = picture;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return Collections.emptyList();
	}

	@Override
	public String getPassword() {
		return null;
	}

	@Override
	public String getUsername() {
		return name;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public String getId() {
		return id;
	}

	public String getEmail() {
		return email;
	}
	
	public String getName() {
		return name;
	}

	public String getFirst_Name() {
		return first_name;
	}
	
	public String getMiddle_name() {
		return middle_name;
	}

	public String getLast_Name() {
		return last_name;
	}

	public URI getPicture() {
		return picture;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Facebook [id=");
		builder.append(id);
		builder.append(", email=");
		builder.append(email);
		builder.append(", name=");
		builder.append(name);
		builder.append("]");
		return builder.toString();
	}

	public static FacebookUser fromUserInfoMap(Map<String, Object> map) {
		Map<String, Object> pictureRawObject = (Map<String, Object>)map.get("picture");
		Map<String, Object> pictureRawData = (Map<String, Object>)pictureRawObject.get("data");
		URI picture = getUri(pictureRawData, "url");
		return new FacebookUser(
				(String) (map.containsKey("id") ? map.get("id") : map.get("sub")),
				(String) map.get("email"),
				(String) map.get("name"),
				(String) map.get("first_name"),
				(String) map.get("middle_name"),
				(String) map.get("last_name"),
				picture);
	}

	private static URI getUri(Map<String, Object> map, String key) {
		URI uri = null;
		try {
			if (map.containsKey(key)) {
				uri = new URI((String) map.get(key));
			}
		} catch (URISyntaxException e) {}
		return uri;
	}



}
