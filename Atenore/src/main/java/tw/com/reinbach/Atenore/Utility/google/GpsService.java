package tw.com.reinbach.Atenore.Utility.google;

import java.io.IOException;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.maps.GeoApiContext;
import com.google.maps.GeoApiContext.Builder;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;

import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;

@Service
public class GpsService {

	private GeoApiContext context;

	@Value("${google.geocoding.apikey}")
	private String apiKey;

	@PostConstruct
	public void initiateGoogleGeoApi() {
		Builder build = new Builder();
		this.context = build.apiKey(this.apiKey).build();
		System.out.println("here conetxt");
		System.out.println(context == null);
	}

	/**
	 * 取得LatLng
	 * 
	 * @param inputtedAddress
	 * @return LatLng
	 */
	public LatLng getSimpleLocationFromAddress(String inputtedAddress) {
		try {
			GeocodingResult[] result = GeocodingApi.geocode(this.context, inputtedAddress).await();
			return covertAsLocation(result);
		} catch (ApiException | InterruptedException | IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 取得LatLng
	 * 
	 * @param county
	 * @param address
	 * @return
	 */
	public ResponseModel<LatLng> getSimpleLocationFromAddress(String county, String address) {
		try {
			String inputtedAddress = county + address;
			GeocodingResult[] result = GeocodingApi.geocode(this.context, inputtedAddress).await();
			return covertAsLocation(result, inputtedAddress);
		} catch (ApiException | InterruptedException | IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 取得完整的Location資訊
	 * 
	 * @param inputtedAddress
	 * @return GeocodingResult[]
	 */
	public GeocodingResult[] getLocationFromAddress(String inputtedAddress) {
		System.out.println(this.apiKey);
		System.out.println(inputtedAddress);
		System.out.println(context == null);

		GeocodingResult[] result = null;
		try {
			result = GeocodingApi.geocode(this.context, inputtedAddress).await();
		} catch (ApiException | InterruptedException | IOException e) {

			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 轉換為Lat與Lng
	 * 
	 * @param result
	 * @return LatLng
	 */
	private LatLng covertAsLocation(GeocodingResult[] result) {

		ArrayNode node = new ObjectMapper().valueToTree(result);
		LatLng location = new LatLng();
		Double lat = Double.valueOf(node.get(0).get("geometry").get("location").get("lat").toString());
		Double lng = Double.valueOf(node.get(0).get("geometry").get("location").get("lng").toString());

		System.out.println("Lat:" + lat + ", Lng:" + lng);
		location.lat = lat;
		location.lng = lng;

		return location;
	}

	/**
	 * 轉換為Lat與Lng
	 * 
	 * @param result
	 * @param inputtedAddress
	 * @return
	 */
	private ResponseModel<LatLng> covertAsLocation(GeocodingResult[] result, String inputtedAddress) {

		ResponseModel<LatLng> resultModel = null;
		ArrayNode node = new ObjectMapper().valueToTree(result);
		LatLng location = new LatLng();
		System.out.println("result = " + result.length);
		System.out.println("location gps = " + location);
		System.out.println("node.size() = " + node.size());
//		if (node.size() != 0) {
		if (result.length > 0) {
			Double lat = Double.valueOf(node.get(0).get("geometry").get("location").get("lat").toString());
			Double lng = Double.valueOf(node.get(0).get("geometry").get("location").get("lng").toString());

			System.out.println("Lat:" + lat + ", Lng:" + lng + ", " + inputtedAddress);

			location.lat = lat;
			location.lng = lng;

		} else {
			return new ResponseModel<LatLng>(500, null, "[" + inputtedAddress + "]" + "this address is incorrect.");
		}
		return new ResponseModel<LatLng>(200, location);
	}

}
