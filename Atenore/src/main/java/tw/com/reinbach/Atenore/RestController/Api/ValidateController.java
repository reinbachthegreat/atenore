package tw.com.reinbach.Atenore.RestController.Api;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import tw.com.reinbach.Atenore.Model.admin.query.AdminInfoDto;
import tw.com.reinbach.Atenore.Model.validate.SignInAdminDto;
import tw.com.reinbach.Atenore.Model.validate.SignInDto;
import tw.com.reinbach.Atenore.Model.validate.SignInInfoDto;
import tw.com.reinbach.Atenore.Model.validate.SignInSSODto;
import tw.com.reinbach.Atenore.Model.validate.SignUpDto;
import tw.com.reinbach.Atenore.Model.validate.SignUpInfo;
import tw.com.reinbach.Atenore.Model.validate.ValidateService;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;

@RestController
public class ValidateController {

	@Autowired
	private ValidateService validateSerivce;

	/** Single sign on,SSO
	 *  第三方登入
	 * 
	 * @param id_token 第三方登入token，由前端傳入
	 * @param sso_type 第三方登入型態
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:30-1", value = "sign in by sso")
	@ApiImplicitParam(name = "Authorization", required = false, paramType = "header", dataType = "String", access = "hidden")
	@PostMapping(value = { "/signIn/SSO" })
	public ResponseModel<SignInInfoDto> signInSSO(@Valid SignInSSODto dto) {
		switch (dto.getSso_type()) {
		case "google":
			return validateSerivce.signInByGoogle(dto.getId_token());
		case "facebook":
			return validateSerivce.signInByFacebook(dto.getId_token());
		default:
			return new ResponseModel<SignInInfoDto>(401, ApiError.PARARMETER_NOT_FOUND);
		}
	}
	
	/** Sign in(Email only)
	 *  Email登入
	 *  
	 * @param dto
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:30", value = "sign in")
	@RequestMapping(value = { "/signIn" }, method = { RequestMethod.POST })
	@ApiImplicitParam(name = "Authorization", required = false, paramType = "header", dataType = "String", access = "hidden")
	public ResponseModel<SignInInfoDto> signIn(@Valid SignInDto dto) {
		return validateSerivce.signIn(dto);
	}

	/** Sign up by email
	 *  Email註冊
	 * 
	 * @param dto
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:31", value = "sign up")
	@ApiImplicitParam(name = "Authorization", required = false, paramType = "header", dataType = "String", access = "hidden")
	@RequestMapping(value = { "/signUp" }, method = RequestMethod.POST)
	public ResponseModel<Void> signUp(@Valid SignUpDto dto, MultipartFile file) {
		ResponseModel<SignUpInfo> info = validateSerivce.signUp(dto, file);
		if(info.getCode()==201) {
			return new ResponseModel<>(201, "");
		}else {
			return new ResponseModel<>(info.getCode(), info.getErrMsg());
		}
	}

	/** Account activated(Email only)
	 *  帳號啟用(第三方登入除外)
	 * 
	 * @param memberID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:31-1", value = "account activated")
	@ApiImplicitParam(name = "Authorization", required = false, paramType = "header", dataType = "String", access = "hidden")
	@RequestMapping(value = { "/account/active/{member}" }, method = RequestMethod.GET)
	public ResponseModel<SignInInfoDto> accountActivaed(@PathVariable(value = "member") String memberID) {
		return validateSerivce.activeAccount(memberID);
	}

	/** Forget password apply reset url(Email only)
	 *  忘記密碼發送驗證連結(第三登入除外)
	 * 
	 * @param account
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:32", value = "Forget password apply reset url.")
	@ApiImplicitParam(name = "Authorization", required = false, paramType = "header", dataType = "String", access = "hidden")
	@RequestMapping(value = {"/key/reset/{account}"}, method = {RequestMethod.PUT})
	public ResponseModel<Void> forgetPasswordVerification(@PathVariable(required = true) String account) {
		return validateSerivce.forgetPasswordVerification(account);
	}

	/** Reset password(Email Only)
	 *  重設密碼(第三方登入除外)
	 *  
	 * @param token
	 * @param newPw
	 * @return
	 * @throws Throwable
	 */
	@ApiOperation(notes = "ApiNo:32-1", value = "reset password")
	@ApiImplicitParam(name = "Authorization", required = false, paramType = "header", dataType = "String", access = "hidden")
	@PutMapping(value = "/account/resetPwd/{token}")
	public ResponseModel<Void> resetPassword(@PathVariable(required = true) String token, @RequestParam(required = true) String newPw)
			throws Throwable {
		Map<String, Object> resetResult = validateSerivce.resetPassword(token, newPw);
		if (!Boolean.valueOf(resetResult.get("result").toString())) {
			return new ResponseModel<Void>(409, resetResult.get("errMsg").toString());
		}
		return new ResponseModel<Void>(204, "");
	}

	/** Backstage validation
	 *  後台安全驗證
	 * 
	 * @param signInAdmin
	 * @return
	 * @throws Throwable
	 */
	@ApiOperation(notes = "ApiNo:A0", value = "Backstage validation")
	@ApiImplicitParam(name = "Authorization", required = false, paramType = "header", dataType = "String", access = "hidden")
	@RequestMapping(value = { "/admin/validate" }, method = { RequestMethod.POST })
	public ResponseModel<AdminInfoDto> adminValidation(@Valid SignInAdminDto signInAdmin) {
		return validateSerivce.adminValidation(signInAdmin);
	}

}
