package tw.com.reinbach.Atenore.Model.category.place.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import tw.com.reinbach.Atenore.Model.category.place.dto.component.PlaceTypeAdminDto;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PlaceTypeListSearchByMainTypeInfoDto {

	private String mainTypeName;
	
	private List<PlaceTypeAdminDto> placeTypeList;

	public List<PlaceTypeAdminDto> getPlaceTypeList() {
		return placeTypeList;
	}

	public void setPlaceTypeList(List<PlaceTypeAdminDto> placeTypeList) {
		this.placeTypeList = placeTypeList;
	}

	public String getMainTypeName() {
		return mainTypeName;
	}

	public void setMainTypeName(String mainTypeName) {
		this.mainTypeName = mainTypeName;
	}
}
