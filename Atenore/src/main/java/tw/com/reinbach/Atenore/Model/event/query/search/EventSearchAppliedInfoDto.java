package tw.com.reinbach.Atenore.Model.event.query.search;

import java.time.LocalDateTime;

import org.springframework.format.annotation.DateTimeFormat;

import io.swagger.annotations.ApiModelProperty;

public class EventSearchAppliedInfoDto {

	@ApiModelProperty(value = "活動編號", example = "123e4567-e89b-42d3-a456-556642440000")
	private String eventID;

	@ApiModelProperty(value = "活動名稱", example = "緊急對策")
	private String name;

	@ApiModelProperty(value = "活動主圖片")
	private String img;

	@ApiModelProperty(value = "活動開始時間", example = "2020-01-10 10:00:00")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime startTime;

	@ApiModelProperty(value = "活動結束時間", example = "2020-01-10 10:00:00")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime endTime;

	@ApiModelProperty(value = "活動審核狀態")
	private String apply;

	@ApiModelProperty(value = "活動是否能在列表中被會員取消")
	private boolean cancelable = false;

	@ApiModelProperty(value = "活動申請時間", example = "2020-01-10 10:00:00")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime insertTime;

	public String getEventID() {
		return eventID;
	}

	public void setEventID(String eventID) {
		this.eventID = eventID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}

	public String getApply() {
		return apply;
	}

	public void setApply(String apply) {
		this.apply = apply;
	}

	public boolean isCancelable() {
		return cancelable;
	}

	public void setCancelable(boolean cancelable) {
		this.cancelable = cancelable;
	}

	public LocalDateTime getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(LocalDateTime insertTime) {
		this.insertTime = insertTime;
	}

}
