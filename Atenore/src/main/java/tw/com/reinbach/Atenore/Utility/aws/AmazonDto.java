package tw.com.reinbach.Atenore.Utility.aws;

import org.springframework.stereotype.Component;

@Component
public class AmazonDto {

	private String url;
	
	private String objectKey;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getObjectKey() {
		return objectKey;
	}

	public void setObjectKey(String objectKey) {
		this.objectKey = objectKey;
	}
	
	
	
}
