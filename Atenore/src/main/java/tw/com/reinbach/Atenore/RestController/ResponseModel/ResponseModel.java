package tw.com.reinbach.Atenore.RestController.ResponseModel;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;

@ApiModel
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseModel<T> {

	private Integer code;

	private T data;

	private String errMsg;

	public ResponseModel(Integer code, T data) {
		this.code = code;
		this.data = data;
		this.errMsg = "";
	}

	public ResponseModel(Integer code, T data, String errMsg) {
		this.code = code;
		this.data = data;
		this.errMsg = errMsg;
	}

	public ResponseModel(Integer code, String errMsg) {
		this.code = code;
		this.errMsg = errMsg;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

}
