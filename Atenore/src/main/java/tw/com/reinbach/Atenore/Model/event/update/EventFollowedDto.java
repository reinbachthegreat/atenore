package tw.com.reinbach.Atenore.Model.event.update;

import javax.validation.constraints.NotEmpty;

import io.swagger.annotations.ApiModelProperty;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;

public class EventFollowedDto {

	@NotEmpty(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@ApiModelProperty(required = true)
	private String eventID;

	@NotEmpty(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@ApiModelProperty(required = true)
	private String memberID;

	@ApiModelProperty(hidden = true)
	private Boolean wannaFollow;

	public String getEventID() {
		return eventID;
	}

	public void setEventID(String eventID) {
		this.eventID = eventID;
	}

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public Boolean getWannaFollow() {
		return wannaFollow;
	}

	public void setWannaFollow(Boolean wannaFollow) {
		this.wannaFollow = wannaFollow;
	}

}
