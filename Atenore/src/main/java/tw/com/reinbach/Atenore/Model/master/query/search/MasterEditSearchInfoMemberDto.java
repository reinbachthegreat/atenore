package tw.com.reinbach.Atenore.Model.master.query.search;

import java.time.LocalDateTime;

public class MasterEditSearchInfoMemberDto {

	private String masterID;

	private String editID;

	private String memberID;

	private String publicName;

	private String img;

	private Boolean inUsed;

	private LocalDateTime insertTime;

	public String getMasterID() {
		return masterID;
	}

	public void setMasterID(String masterID) {
		this.masterID = masterID;
	}

	public String getEditID() {
		return editID;
	}

	public void setEditID(String editID) {
		this.editID = editID;
	}

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}


	public String getPublicName() {
		return publicName;
	}

	public void setPublicName(String publicName) {
		this.publicName = publicName;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public Boolean getInUsed() {
		return inUsed;
	}

	public void setInUsed(Boolean inUsed) {
		this.inUsed = inUsed;
	}

	public LocalDateTime getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(LocalDateTime insertTime) {
		this.insertTime = insertTime;
	}

}
