package tw.com.reinbach.Atenore.Model.member;

import java.util.List;

import tw.com.reinbach.Atenore.Model.member.component.HotspotDto;
import tw.com.reinbach.Atenore.Model.member.create.ContactCreateDto;
import tw.com.reinbach.Atenore.Model.member.create.HotspotCreateDto;
import tw.com.reinbach.Atenore.Model.member.create.MemberCreateDto;
import tw.com.reinbach.Atenore.Model.member.query.HotspotInfoDto;
import tw.com.reinbach.Atenore.Model.member.query.MemberInfoAdminDto;
import tw.com.reinbach.Atenore.Model.member.query.MemberInfoDto;
import tw.com.reinbach.Atenore.Model.member.query.search.EventFollowedSearchArgsDto;
import tw.com.reinbach.Atenore.Model.member.query.search.EventFollwedSearchInfoDto;
import tw.com.reinbach.Atenore.Model.member.query.search.MasterFollowedSearchArgsDto;
import tw.com.reinbach.Atenore.Model.member.query.search.MasterFollowedSearchInfoDto;
import tw.com.reinbach.Atenore.Model.member.query.search.MemberSearchInfoAdminDto;
import tw.com.reinbach.Atenore.Model.member.query.search.PlaceFollowedSearchArgsDto;
import tw.com.reinbach.Atenore.Model.member.query.search.PlaceFollowedSearchInfoDto;
import tw.com.reinbach.Atenore.Model.member.update.HotspotUpdateDto;
import tw.com.reinbach.Atenore.Model.member.update.MemberSatusUpdateDto;
import tw.com.reinbach.Atenore.Model.member.update.MemberUpdateDto;
import tw.com.reinbach.Atenore.Model.member.update.PasswordUpdateDto;
import tw.com.reinbach.Atenore.Model.validate.AccountStatusInfoDto;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;

public interface MemberDAO {

	public List<MemberSearchInfoAdminDto> getMemberListForAdminByFilter(String fuzzyFilter);

	public ResponseModel<Void> setAccountByAdmin(MemberCreateDto member);

	public ResponseModel<Void> setMemberDetailByAdmin(MemberCreateDto member);

	public ResponseModel<Void> updateMemberDetailByMemberID(MemberUpdateDto dto);

	public ResponseModel<Void> updateAccountStatusByMemberID(MemberSatusUpdateDto status);

	public ResponseModel<Void> deleteAccountByMemberID(String memberID);

	public ResponseModel<Void> deleteMemberDetailByMemberID(String memberID);

	public ResponseModel<Void> deleteEventFollowedByMemberID(String memberID);

	public ResponseModel<Void> deletePlaceFollowedByMemberID(String memberID);

	public ResponseModel<Void> deleteMasterFollwedByMemberID(String memberID);

	public ResponseModel<Void> deleteHotspotByMemberID(String memberID);

	public ResponseModel<Void> deletePinEventTypeByMemberID(String memberID);

	public ResponseModel<Void> deletePinPlaceTypeByMemberID(String memberID);

	public MemberInfoDto getMemberSimpleByMemberID(String memberID);

	public MemberInfoDto getMemberDetailByMemberID(String memberID);

	public MemberInfoAdminDto getMemberWithDetailForAdminByMemberID(String memberID);

	public MemberUpdateDto getMemberDetailForUpdateByMemberID(String memberID);

	public AccountStatusInfoDto getAccontStatus(String memberID);

	public ResponseModel<Void> updatePassword(PasswordUpdateDto dto);

	public ResponseModel<Void> setHotspot(HotspotCreateDto dto);

	public List<HotspotDto> getHotspotListByMemberID(String memberID);

	public HotspotInfoDto getHotspotByHotspotID(String hotspotID);

	public ResponseModel<Void> updateHotspotByHotspotID(HotspotUpdateDto dto);

	public ResponseModel<Void> deleteHotspotByHotspotID(String hotspotID);

	public List<EventFollwedSearchInfoDto> getEventListFollwedByMemberIDWithFilter(EventFollowedSearchArgsDto argsDto);

	public List<MasterFollowedSearchInfoDto> getMasterListFollowedByMemberIDWithFilter(MasterFollowedSearchArgsDto argsDto);

	public List<PlaceFollowedSearchInfoDto> getPlaceListFollowedByMemberIDWithFilter(PlaceFollowedSearchArgsDto argsDto);

	public ResponseModel<Void> setContact(ContactCreateDto dto);
}
