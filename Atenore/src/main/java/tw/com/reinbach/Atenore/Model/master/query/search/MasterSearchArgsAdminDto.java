package tw.com.reinbach.Atenore.Model.master.query.search;

import io.swagger.annotations.ApiModelProperty;

public class MasterSearchArgsAdminDto {
	
	private String lastName;
	
	private String firstName;

	@ApiModelProperty(example = "1")
	private Integer mainID;
	
	@ApiModelProperty(example = "true", dataType = "Boolean")
	private Boolean alive;

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public Boolean getAlive() {
		return alive;
	}

	public void setAlive(Boolean alive) {
		this.alive = alive;
	}
	
}
