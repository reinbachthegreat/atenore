package tw.com.reinbach.Atenore.Model.search;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SearchPreferDto {
	
	@ApiModelProperty(example = "0")
	private Integer searchPreferSN;
	
	@ApiModelProperty(example = "最新")
	private String searchPreferName;
	
	private List<SearchOrderDto> searchOrderList;

	public Integer getSearchPreferSN() {
		return searchPreferSN;
	}

	public void setSearchPreferSN(Integer searchPreferSN) {
		this.searchPreferSN = searchPreferSN;
	}

	public String getSearchPreferName() {
		return searchPreferName;
	}

	public void setSearchPreferName(String searchPreferName) {
		this.searchPreferName = searchPreferName;
	}

	public List<SearchOrderDto> getSearchOrderList() {
		return searchOrderList;
	}

	public void setSearchOrderList(List<SearchOrderDto> searchOrderList) {
		this.searchOrderList = searchOrderList;
	}
	
	

}
