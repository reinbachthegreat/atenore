package tw.com.reinbach.Atenore.Model.category.place.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CapacityTypeDto {

	@ApiModelProperty(value = "查詢收容人數條件編號")
	private Integer capacitySN;

	@ApiModelProperty(value = "查詢收容人數條件名稱")
	private String capacityName;

	public Integer getCapacitySN() {
		return capacitySN;
	}

	public void setCapacitySN(Integer capacitySN) {
		this.capacitySN = capacitySN;
	}

	public String getCapacityName() {
		return capacityName;
	}

	public void setCapacityName(String capacityName) {
		this.capacityName = capacityName;
	}

}
