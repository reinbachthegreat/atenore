package tw.com.reinbach.Atenore.Model.event.update;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import io.swagger.annotations.ApiModelProperty;

public class EventUpdateDto {

	@ApiModelProperty(hidden = true, name = "eventID", value = "活動編號", example = "123e4567-e89b-42d3-a456-556642440000", dataType = "String")
	private String eventID;
	
	/* 分類屬性
	 * 
	 */
	@ApiModelProperty(name = "typeID", value = "活動分類編號", example = "1", dataType = "Integer")
	private Integer typeID;

	@ApiModelProperty(name = "mainID", value = "活動大分類編號", example = "1", dataType = "Integer")
	private Integer mainID;
	
	@ApiModelProperty(name = "name", value = "活動名稱", example = "武漢肺炎緊急對策", dataType = "String")
	private String name;
	
	@ApiModelProperty(name = "startTime", value = "活動開始時間", example = "2020-01-10 10:00:00", dataType = "LocalDateTime")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime startTime;
	
	@ApiModelProperty(name = "endTime", value = "活動結束時間", example = "2020-01-10 10:00:00", dataType = "LocalDateTime")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime endTime;
	
	@ApiModelProperty(name = "place", value = "活動場所", example = "台北榮總", dataType = "String")
	private String place;
	
	@ApiModelProperty(name = "county", value = "活動場所縣市", example = "新北市", dataType = "String")
	private String county;
	
	@ApiModelProperty(name = "address", value = "活動場所地址(排除縣市)", example = "板橋區縣民大道55號2巷158號6樓", dataType = "String")
	private String address;
	
	@ApiModelProperty(name = "host", value = "活動舉辦單位", example = "行政院腿輔會", dataType = "String")
	private String host;
	
//	@ApiModelProperty(name = "fee", value = "活動費用", example = "3000", dataType = "Integer")
//	private Integer fee;

	@ApiModelProperty(name = "fee", value = "活動費用", example = "3000", dataType = "String")
	private String fee;
	
	@ApiModelProperty(name = "intro", value = "活動簡介", example = "防堵小熊維尼病毒入侵", dataType = "String")
	private String intro;
	
	@ApiModelProperty(name = "status", value = "活動舉辦狀態", example = "正在舉行", dataType = "String")
	private String status;
	
	@ApiModelProperty(name = "visibilitySN", value = "活動開放狀態編號", example = "0", dataType = "Integer")
	private Integer visibilitySN;
	
	@ApiModelProperty(name = "signupUrl", value = "活動報名連結", example = "https://google.com", dataType = "String")
	private String signupUrl;
	
	@ApiModelProperty(name = "liveUrl", value = "活動直播連結", example = "https://google.com", dataType = "String")
	private String liveUrl;
	
	@ApiModelProperty(hidden = true, name = "lat", value = "GPS, Lat", example = "25.05788", dataType = "BigDecimal")
	private BigDecimal lat;
	
	@ApiModelProperty(hidden = true, name = "lng", value = "GPS, Lng", example = "24.5585", dataType = "BigDecimal")
	private BigDecimal lng;
	
	@ApiModelProperty(hidden = true, name = "apply", value = "活動申請狀態", example = "通過/待審核/拒絕上架", dataType = "String")
	private String apply;
	
	@ApiModelProperty(hidden = true, name = "memberID", value = "活動創立者編號", example = "我高興咬我啊", dataType = "String")
	private String memberID;
	
	@ApiModelProperty(name = "insertTime", value = "資訊創立時間", example = "2020-01-10 10:00:00", dataType = "LocalDateTime")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")	
	private LocalDateTime insertTime;
	
	private List<String> tagList;
	
	private List<String> masterIDList;

	public String getEventID() {
		return eventID;
	}

	public void setEventID(String eventID) {
		this.eventID = eventID;
	}

	public Integer getTypeID() {
		return typeID;
	}

	public void setTypeID(Integer typeID) {
		this.typeID = typeID;
	}

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	//	public Integer getFee() {
	//	return fee;
	//}
	//
	//public void setFee(Integer fee) {
	//	this.fee = fee;
	//}
	
	public String getFee() {
		return fee;
	}
	
	public void setFee(String fee) {
		this.fee = fee;
	}

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getVisibilitySN() {
		return visibilitySN;
	}

	public void setVisibilitySN(Integer visibilitySN) {
		this.visibilitySN = visibilitySN;
	}

	public String getSignupUrl() {
		return signupUrl;
	}

	public void setSignupUrl(String signupUrl) {
		this.signupUrl = signupUrl;
	}

	public String getLiveUrl() {
		return liveUrl;
	}

	public void setLiveUrl(String liveUrl) {
		this.liveUrl = liveUrl;
	}

	public BigDecimal getLat() {
		return lat;
	}

	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}

	public BigDecimal getLng() {
		return lng;
	}

	public void setLng(BigDecimal lng) {
		this.lng = lng;
	}

	public String getApply() {
		return apply;
	}

	public void setApply(String apply) {
		this.apply = apply;
	}

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public LocalDateTime getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(LocalDateTime insertTime) {
		this.insertTime = insertTime;
	}

	public List<String> getTagList() {
		return tagList;
	}

	public void setTagList(List<String> tagList) {
		this.tagList = tagList;
	}

	public List<String> getMasterIDList() {
		return masterIDList;
	}

	public void setMasterIDList(List<String> masterIDList) {
		this.masterIDList = masterIDList;
	}
	
}
