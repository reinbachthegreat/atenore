package tw.com.reinbach.Atenore.Model.event;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import tw.com.reinbach.Atenore.Model.event.component.EventAdminDto;
import tw.com.reinbach.Atenore.Model.event.component.EventDto;
import tw.com.reinbach.Atenore.Model.event.component.EventImageDto;
import tw.com.reinbach.Atenore.Model.event.component.EventRecommend;
import tw.com.reinbach.Atenore.Model.event.component.EventTag;
import tw.com.reinbach.Atenore.Model.event.component.PinEventTypeInfoDto;
import tw.com.reinbach.Atenore.Model.event.create.EventCreateDto;
import tw.com.reinbach.Atenore.Model.event.create.EventImageItemCreateDto;
import tw.com.reinbach.Atenore.Model.event.query.EventNotReviewCountDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventNearBySearchArgsDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchAppliedInfoDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchArgsAdminDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchArgsDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchInfoAdminDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchInfoDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchReviewArgsAdminDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchReviewInfoAdminDto;
import tw.com.reinbach.Atenore.Model.event.update.EventApplyDto;
import tw.com.reinbach.Atenore.Model.event.update.EventFollowedDto;
import tw.com.reinbach.Atenore.Model.event.update.EventUpdateDto;
import tw.com.reinbach.Atenore.Model.event.update.PinEventTypeUpdateDto;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;

@Repository
public class EventDAOJdbcTemplate implements EventDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private NamedParameterJdbcTemplate namedJdbcTemplate;

	@Override
	public List<EventDto> getEventList() {
		try {
			String sql = "select e.*, et.name as typeName, et.mainID, emt.name as mainTypeName from atenore.event e "
					+ " left join eventtype et on e.typeID = et.typeID "
					+ " left join eventmaintype emt on et.mainID = emt.mainID order by e.startTime desc";
			return jdbcTemplate.query(sql, new BeanPropertyRowMapper<EventDto>(EventDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<EventSearchInfoDto> getEventListWithFilter(EventSearchArgsDto argsDto) {
		try {
//			StringBuilder sql = new StringBuilder("select e.*, ei.uuid as imageID, ei.objectKey, ei.img, ei.isMain,"
//					+ " et.name as typeName, et.mainID, emt.name as mainTypeName from atenore.event e "
//					+ " left join eventtype et on e.typeID = et.typeID "
//					+ " left join eventmaintype emt on et.mainID = emt.mainID "
//					+ " right join eventimg as ei on ei.eventID = e.eventID where 1=1 ");
//			sql.append(" and  ei.isMain = 'true' ");

			StringBuilder sql = new StringBuilder(
					"select e.*, (select ei.img from eventimg ei where ei.eventID = e.eventID and ei.isMain=true) as img, (select ei.uuid from eventimg ei where ei.eventID = e.eventID and ei.isMain=true) as imageID, "
							+ " (select ei.objectKey from eventimg ei where ei.eventID = e.eventID and ei.isMain=true) as objectKey, (select ei.isMain from eventimg ei where ei.eventID = e.eventID and ei.isMain= true) as isMain,  "
							+ " et.name as typeName, et.mainID, emt.name as mainTypeName from atenore.event e "
							+ " left join eventtype et on e.typeID = et.typeID "
							+ " left join eventmaintype emt on et.mainID = emt.mainID where 1 = 1 ");

			switch (argsDto.getSearchPreferArg().getSearchPreferSN()) {
			case 1:
				sql.append(" and e.insertTime between date_sub(now(), interval 10 day) and now() "
						+ " and e.endTime not between date_sub(now(), interval 10 day) and now() ");
				break;

			case 2:
				sql.append(" and e.click is not null and e.follow is not null ");
				break;

			case 3:
				sql.append(" and e.insertTime between date_sub(now(), interval 10 day) and now() "
						+ " and e.click is not null and e.follow is not null ");
				break;

			default:
				break;
			}
			
			if(argsDto.getCounty()!=null) {
				sql.append(" and e.county = :county");
			}

			if (argsDto.getTodayOnly()!=null &&argsDto.getTodayOnly() == true ) {
				sql.append(" and date(e.startTime) = CURDATE()");
			} else if(argsDto.getTodayOnly()!=null && argsDto.getTodayOnly()==false){
				sql.append(" and date(e.startTime) > CURDATE()");
			}

			if (argsDto.getFuzzyFilter() != null) {
				sql.append(
						" and ( e.name like '%' :fuzzyFilter '%'  or e.county like '%' :fuzzyFilter '%' or e.address like '%' :fuzzyFilter '%' )");
			}

			if (argsDto.getMainID() != null) {
				sql.append(" and et.mainID = :mainID ");
				if (argsDto.getTypeID() != null) {
					sql.append(" and e.typeID = :typeID ");
				}
			}

			if (argsDto.getStartTime() != null) {
				sql.append(" and e.startTime = :startTime ");
			}

			if (argsDto.getEndTime() != null) {
				sql.append(" and e.endTime = :endTime ");
			}

			if (argsDto.getApply() != null) {
				sql.append(" and e.apply = :apply ");
			}

			SqlParameterSource ps = new BeanPropertySqlParameterSource(argsDto);
			return namedJdbcTemplate.query(sql.toString(), ps,
					new BeanPropertyRowMapper<EventSearchInfoDto>(EventSearchInfoDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}
	
	@Override
	public List<EventSearchInfoDto> getEventListNearBy(EventNearBySearchArgsDto argsDto) {
		try {
			StringBuilder sql = new StringBuilder(
					"select e.*, (select ei.img from eventimg ei where ei.eventID = e.eventID and ei.isMain=true) as img, (select ei.uuid from eventimg ei where ei.eventID = e.eventID and ei.isMain=true) as imageID, "
							+ " (select ei.objectKey from eventimg ei where ei.eventID = e.eventID and ei.isMain=true) as objectKey, (select ei.isMain from eventimg ei where ei.eventID = e.eventID and ei.isMain= true) as isMain,  "
							+ " et.name as typeName, et.mainID, emt.name as mainTypeName from atenore.event e "
							+ " left join eventtype et on e.typeID = et.typeID "
							+ " left join eventmaintype emt on et.mainID = emt.mainID where 1 = 1 ");

			switch (argsDto.getSearchPreferArg().getSearchPreferSN()) {
			case 1:
				sql.append(" and e.insertTime between date_sub(now(), interval 10 day) and now() ");
				break;

			case 2:
				sql.append(" and e.click is not null and e.follow is not null ");
				break;

			case 3:
				sql.append(" and e.insertTime between date_sub(now(), interval 10 day) and now() "
						+ " and e.click is not null and e.follow is not null ");
				break;

			default:
				break;
			}
			
			sql.append(" and e.lat between :minLat and :maxLat and e.lnt between :minLng and :maxLat ");

			if (argsDto.getTodayOnly() == true) {
				sql.append(" and date(e.startTime) = CURDATE()");
			} else {
				sql.append(" and date(e.startTime) > CURDATE()");
			}
//
//			if (argsDto.getStartTime() != null) {
//				sql.append(" and e.startTime = :startTime ");
//			}
//
//			if (argsDto.getEndTime() != null) {
//				sql.append(" and e.endTime = :endTime ");
//			}
//
//			if (argsDto.getApply() != null) {
//				sql.append(" and e.apply = :apply ");
//			}

			SqlParameterSource ps = new BeanPropertySqlParameterSource(argsDto);
			return namedJdbcTemplate.query(sql.toString(), ps,
					new BeanPropertyRowMapper<EventSearchInfoDto>(EventSearchInfoDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

//	@Override
//	public List<EventDto> getEventListWithFilter(String eventFilter, LocalDateTime startTime, LocalDateTime endTime) {
//		try {
//			StringBuilder sql = new StringBuilder("select e.*, et.name as typeName, et.mainID, emt.name as mainTypeName from atenore.event e "
//					+ " left join eventtype et on e.typeID = et.typeID " + " left join eventmaintype emt on et.mainID = emt.mainID ");
//
//			if (eventFilter == null && startTime == null && endTime == null) {
//				return this.getEventList();
//			}
//
//			if (eventFilter != null && startTime != null && endTime != null) {
//				String query = new StringBuilder("%").append(eventFilter).append("%").toString();
//				sql.append("where e.name like ? and e.startTime >= ? and e.endTime <= ? ").append("order by e.startTime desc");
//				return jdbcTemplate.query(sql.toString(), new Object[] { query, startTime, endTime },
//						new BeanPropertyRowMapper<EventDto>(EventDto.class));
//			}
//
//			if (eventFilter != null && startTime == null && endTime != null) {
//				String query = new StringBuilder("%").append(eventFilter).append("%").toString();
//				sql.append("where e.name like ? and e.endTime <= ? ").append("order by e.startTime desc");
//				return jdbcTemplate.query(sql.toString(), new Object[] { query, endTime },
//						new BeanPropertyRowMapper<EventDto>(EventDto.class));
//			}
//
//			if (eventFilter != null && startTime != null && endTime == null) {
//				String query = new StringBuilder("%").append(eventFilter).append("%").toString();
//				sql.append("where e.name like ? and e.startTime >= ?").append("order by e.startTime desc");
//				return jdbcTemplate.query(sql.toString(), new Object[] { query, startTime },
//						new BeanPropertyRowMapper<EventDto>(EventDto.class));
//			}
//
//			if (eventFilter != null && startTime == null && endTime == null) {
//				String query = new StringBuilder("%").append(eventFilter).append("%").toString();
//				sql.append("where e.name like ? ").append("order by e.startTime desc");
//				return jdbcTemplate.query(sql.toString(), new Object[] { query }, new BeanPropertyRowMapper<EventDto>(EventDto.class));
//			}
//
//			if (eventFilter == null && startTime != null && endTime != null) {
//				sql.append("where e.startTime >= ? and e.endTime <= ? ").append("order by e.startTime desc");
//				return jdbcTemplate.query(sql.toString(), new Object[] { startTime, endTime },
//						new BeanPropertyRowMapper<EventDto>(EventDto.class));
//			}
//
//			if (eventFilter == null && startTime == null && endTime != null) {
//				sql.append("where  e.endTime <= ? ").append("order by e.startTime desc");
//				return jdbcTemplate.query(sql.toString(), new Object[] { endTime },
//						new BeanPropertyRowMapper<EventDto>(EventDto.class));
//			}
//
//			if (eventFilter == null && startTime != null && endTime == null) {
//				sql.append("where e.startTime >= ?").append("order by e.startTime desc");
//				return jdbcTemplate.query(sql.toString(), new Object[] { startTime },
//						new BeanPropertyRowMapper<EventDto>(EventDto.class));
//			}
//
//			return null;
//		} catch (DataAccessException e) {
//			e.printStackTrace();
//			return null;
//		}
//	}

	@Override
	public EventDto getEvent(String eventID) {
		try {
			String sql = "select e.*, et.name as typeName, et.mainID, emt.name as mainTypeName from atenore.event e "
					+ " left join eventtype et on e.typeID = et.typeID left join eventmaintype emt on et.mainID = emt.mainID "
					+ " where e.eventID = ? ";
			return jdbcTemplate.queryForObject(sql, new Object[] { eventID }, new BeanPropertyRowMapper<EventDto>(EventDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public EventUpdateDto getEventForUpdate(String eventID) {
		try {
			String sql = "select e.*, et.name as typeName, et.mainID, emt.name as mainTypeName from atenore.event e "
					+ " left join eventtype et on e.typeID = et.typeID left join eventmaintype emt on et.mainID = emt.mainID "
					+ " where e.eventID = ? ";

			return jdbcTemplate.queryForObject(sql, new Object[] { eventID },
					new BeanPropertyRowMapper<EventUpdateDto>(EventUpdateDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public EventAdminDto getEventAll(String eventID) {
		try {
			String sql = "select e.*, et.name as typeName, et.mainID, emt.name as mainTypeName from atenore.event e "
					+ " left join eventtype et on e.typeID = et.typeID left join eventmaintype emt on et.mainID = emt.mainID "
					+ " where e.eventID = ? ";

			return jdbcTemplate.queryForObject(sql, new Object[] { eventID },
					new BeanPropertyRowMapper<EventAdminDto>(EventAdminDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public EventDto getEventWithMemberID(String eventID, String memberID) {
		try {
			String sql = "select e.*, et.name as typeName, et.mainID, emt.name as mainTypeName, "
					+ " (select count(*) from memberevent where memberID = ? and eventID = ?) as isFollowed "
					+ " from atenore.event e left join eventtype et on e.typeID = et.typeID left join eventmaintype emt on et.mainID = emt.mainID "
					+ " where e.eventID = ? ";
			return jdbcTemplate.queryForObject(sql, new Object[] { memberID, eventID, eventID },
					new BeanPropertyRowMapper<EventDto>(EventDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<EventTag> getEventTag(String eventID) {
		try {
			String sql = "select e.uuid as tagID, e.tag, e.eventID from eventtag e where eventID = ? ";

			return jdbcTemplate.query(sql, new Object[] { eventID }, new BeanPropertyRowMapper<EventTag>(EventTag.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<EventTag> getEventTagWithoutEventID(String eventID) {
		try {
			String sql = "select e.uuid as tagID, e.tag from eventtag e where eventID = ? ";

			return jdbcTemplate.query(sql, new Object[] { eventID }, new BeanPropertyRowMapper<EventTag>(EventTag.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<EventRecommend> getEventRecommend(String eventID) {
		try {
			String sql = "select e.uuid as 'recommendID', e.eventID, m.masterID, m.img, m.publicName as masterName from eventrecommend e "
					+ " left join master m on e.masterID = m.masterID " + " where e.eventID = ? " + " order by e.insertTime desc ";
			return jdbcTemplate.query(sql, new Object[] { eventID }, new BeanPropertyRowMapper<EventRecommend>(EventRecommend.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<EventRecommend> getEventRecommendWithoutEventID(String eventID) {
		try {
			String sql = "select m.masterID, m.img, m.publicName as masterName from eventrecommend e "
					+ " left join master m on e.masterID = m.masterID " + " where e.eventID = ? " + " order by e.insertTime desc "
					+ " limit 0,5";
			return jdbcTemplate.query(sql, new Object[] { eventID }, new BeanPropertyRowMapper<EventRecommend>(EventRecommend.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<EventImageDto> getEventImage(String eventID) {
		try {
			String sql = "select e.uuid as 'imageID', e.eventID, e.img, e.objectKey, e.isMain from eventimg e where e.eventID = ? order by e.isMain desc ";
			return jdbcTemplate.query(sql, new Object[] { eventID }, new BeanPropertyRowMapper<EventImageDto>(EventImageDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<EventImageDto> getEventImage(List<String> uuidList) {
		try {
			String sql = "select * from eventimg where uuid in (:uuidList)";
			Map<String, List<String>> photoParam = Collections.singletonMap("uuidList", uuidList);
			return namedJdbcTemplate.query(sql, photoParam, new BeanPropertyRowMapper<EventImageDto>(EventImageDto.class));
		} catch (DataAccessException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public EventImageDto getEventImageByImageID(String imageID) {
		try {
			String sql = "select e.uuid as 'imageID', e.eventID, e.img, e.objectKey, e.isMain from eventimg e where e.uuid = ? ";
			return jdbcTemplate.queryForObject(sql, new Object[] { imageID },
					new BeanPropertyRowMapper<EventImageDto>(EventImageDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<String> getEventImageObjectkey(String eventID) {
		try {
			String sql = "select objectkey from eventimg where eventID = ? ";
			return jdbcTemplate.queryForList(sql, new Object[] { eventID }, String.class);
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public ResponseModel<Void> setEvent(EventCreateDto event) {
		try {
			String sql = "insert into atenore.event(eventID, typeID, name, startTime, endTime, place, county, address"
					+ " , host, fee, intro, visibilitySN, signupUrl, liveUrl, lat, lng, apply, insertTime, memberID) "
					+ " values(:eventID, :typeID, :name, :startTime, :endTime, :place, :county, :address"
					+ " , :host, :fee, :intro, :visibilitySN, :signupUrl, :liveUrl, :lat, :lng, :apply, now(), :memberID)";
			SqlParameterSource ps = new BeanPropertySqlParameterSource(event);
			int count = namedJdbcTemplate.update(sql, ps);
			if (count != 0) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<Void>(500, ApiError.EVENT_INFO_CREATION_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> setEvent(List<EventCreateDto> list) {
		try {
			String sql = "insert into atenore.event(eventID, typeID, name, startTime, endTime, place, county, address"
					+ " , host, fee, intro, visibilitySN, signupUrl, liveUrl, lat, lng, apply, insertTime, memberID) "
					+ " values(:eventID, :typeID, :name, :startTime, :endTime, :place, :county, :address"
					+ " , :host, :fee, :intro, :visibilitySN, :signupUrl, :liveUrl, :lat, :lng, :apply, now(), :memberID)";

			List<SqlParameterSource> psList = new ArrayList<SqlParameterSource>();
			for (EventCreateDto bean : list) {
				psList.add(new BeanPropertySqlParameterSource(bean));
			}
			SqlParameterSource[] psArry = new SqlParameterSource[psList.size()];
			psList.toArray(psArry);
			int[] countList = namedJdbcTemplate.batchUpdate(sql, psArry);
			if (this.resultIfBatchUpdateSuccess(countList)) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<Void>(201, "Some of " + ApiError.EVENT_INFO_CREATION_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> setEventTag(List<EventTag> list) {
		try {
			String sql = "insert into eventtag (uuid, eventID, tag) values(:tagID, :eventID, :tag)";

			List<SqlParameterSource> psList = new ArrayList<SqlParameterSource>();
			for (EventTag bean : list) {
				psList.add(new BeanPropertySqlParameterSource(bean));
			}
			SqlParameterSource[] psArry = new SqlParameterSource[psList.size()];
			psList.toArray(psArry);
			int[] countList = namedJdbcTemplate.batchUpdate(sql, psArry);
			if (this.resultIfBatchUpdateSuccess(countList)) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<Void>(201, "Some of " + ApiError.EVENT_TAG_CREATION_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public ResponseModel<Void> setEventRecommend(List<EventRecommend> list) {
		try {
			String sql = "insert into eventrecommend (uuid, eventID, masterID, insertTime) values(:recommendID, :eventID, :masterID, now())";

			List<SqlParameterSource> psList = new ArrayList<SqlParameterSource>();
			for (EventRecommend bean : list) {
				psList.add(new BeanPropertySqlParameterSource(bean));
			}
			SqlParameterSource[] psArry = new SqlParameterSource[psList.size()];
			psList.toArray(psArry);
			int[] countList = namedJdbcTemplate.batchUpdate(sql, psArry);
			if (this.resultIfBatchUpdateSuccess(countList)) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<Void>(201, "Some of " + ApiError.EVENT_RECOMMEND_CREATION_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> setEventImage(List<EventImageDto> list) {
		try {
			String sql = "insert into eventimg (uuid, eventID, objectKey, img, isMain) values(:imageID, :eventID, :objectKey, :img, :isMain)";

			List<SqlParameterSource> psList = new ArrayList<SqlParameterSource>();
			for (EventImageDto bean : list) {
				psList.add(new BeanPropertySqlParameterSource(bean));
			}
			SqlParameterSource[] psArry = new SqlParameterSource[psList.size()];
			psList.toArray(psArry);
			int[] countList = namedJdbcTemplate.batchUpdate(sql, psArry);
			if (this.resultIfBatchUpdateSuccess(countList)) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<Void>(201, "Some of " + ApiError.EVENT_IMAGE_CREATION_FAILED);
			}
		} catch (DataAccessException e) {
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

//	@Override
//	public ResponseModel<Void> updateEvent(EventDto event) {
//		try {
//			String sql = "update atenore.event set typeID=:typeID, name=:name, startTime=:startTime, endTime=:endTime, place=:place, county=:county, address=:address, "
//					+ " host=:host, fee=:fee, intro=:intro, status=:status, visibilitySN=:visibilitySN, click=:click, follow=:follow, liveUrl=:liveUrl, signupUrl=:signupUrl, lat=:lat, lng=:lng, apply=:apply, refuse=:refuse, insertTime=:insertTime, memberID=:memberID "
//					+ " where eventID = :eventID ";
//			SqlParameterSource ps = new BeanPropertySqlParameterSource(event);
//			int count = namedJdbcTemplate.update(sql, ps);
//			if (count != 0) {
//				return new ResponseModel<Void>(201, "");
//			} else {
//				return new ResponseModel<>(500, ApiError.EVENT_INFO_UPDATE_FAILED);
//			}
//		} catch (DataAccessException e) {
//			e.printStackTrace();
//			return new ResponseModel<Void>(500, e.getMessage());
//		}
//	}

	@Override
	public ResponseModel<Void> updateEvent(EventUpdateDto event) {
		try {
			String sql = "update atenore.event set typeID = :typeID, name = :name, startTime = :startTime, endTime = :endTime, "
					+ " place = :place, county = :county, address = :address, host = :host, fee = :fee, intro = :intro, visibilitySN = :visibilitySN, "
					+ " liveUrl = :liveUrl, signupUrl = :signupUrl, lat = :lat, lng = :lng, apply = :apply, insertTime = :insertTime "
					+ " where eventID = :eventID ";
			SqlParameterSource ps = new BeanPropertySqlParameterSource(event);
			int count = namedJdbcTemplate.update(sql, ps);
			if (count != 0) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<>(500, ApiError.EVENT_INFO_UPDATE_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deleteEventImage(List<String> uuidList) {
		try {
			String sql = "delete from eventimg where uuid in (:uuidList)";
			Map<String, List<String>> deleteUuidParam = Collections.singletonMap("uuidList", uuidList);
			int count = namedJdbcTemplate.update(sql, deleteUuidParam);
			if (count != 0) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<Void>(500, ApiError.EVENT_IMAGE_DELETION_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public Boolean getEventIfMemberFollowed(EventFollowedDto follow) {
		try {
			String sql = "select count(*) from memberevent where eventID = :eventID and memberID= :memberID ";
			SqlParameterSource ps = new BeanPropertySqlParameterSource(follow);
			Integer count = namedJdbcTemplate.queryForObject(sql, ps, Integer.class);
			if (count == 0) {
				return false;
			} else {
				return true;
			}
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public ResponseModel<Void> setEventFollowed(EventFollowedDto follow) {
		try {
			String sql = "insert into memberevent (eventID, memberID) values(:eventID, :memberID)";
			SqlParameterSource ps = new BeanPropertySqlParameterSource(follow);
			int count = namedJdbcTemplate.update(sql, ps);
			if (count != 0) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<>(500, ApiError.EVENT_FOLLOWED_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deleteEventFollowed(EventFollowedDto follow) {
		try {
			String sql = "delete from memberevent where eventID = :eventID and memberID = :memberID ";
			SqlParameterSource ps = new BeanPropertySqlParameterSource(follow);
			int count = namedJdbcTemplate.update(sql, ps);
			if (count != 0) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<>(500, ApiError.EVENT_DISFOLLOWED_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> updateEventFollowed(String eventID, Boolean wannaFollow) {
		try {
			String sql = "";
			if (wannaFollow == true) {
				sql = "update atenore.event set follow = IFNULL(follow, 0) + 1 where eventID = ? ";
			} else {
				sql = "update atenore.event set follow = IF(follow>1,IFNULL(follow, 0) - 1 ,0) where eventID = ? ";
			}
			int count = jdbcTemplate.update(sql, new Object[] { eventID });
			if (count != 0) {
				return new ResponseModel<Void>(204, "");
			} else {
				return new ResponseModel<>(409, ApiError.EVENT_FOLLOWED_UPDATE_FAILED);
			}
		} catch (DataAccessException e) {
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public List<PinEventTypeInfoDto> getPinEventTypeListByMemberID(String memberID) {
		try {
			String sql = "select pe.typeID, et.name as 'typeName', pe.mainID, emt.name as 'mainTypeName' from pineventtype pe "
					+ " left join eventtype et on pe.typeID = et.typeID" + " left join eventmaintype emt on pe.mainID = emt.mainID "
					+ " where pe.memberID = ? ";
			return jdbcTemplate.query(sql, new Object[] { memberID },
					new BeanPropertyRowMapper<PinEventTypeInfoDto>(PinEventTypeInfoDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<String> getPinEventTypeUUIDListByMemberID(String memberID) {
		try {
			String sql = "select uuid from pineventtype where memberID = ? ";
			return jdbcTemplate.queryForList(sql, new Object[] { memberID }, String.class);
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public ResponseModel<Void> deletePinEventTypeList(List<String> uuidList) {
		try {
			String sql = "delete from pineventtype where uuid in (:uuidList)";
			Map<String, List<String>> deleteUuidParam = Collections.singletonMap("uuidList", uuidList);
			int count = namedJdbcTemplate.update(sql, deleteUuidParam);
			if (count != 0) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<Void>(500, ApiError.PINNED_EVENT_TYPE_DELETE_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> setPinEventTypeList(List<PinEventTypeUpdateDto> list) {
		try {
			String sql = "insert into pineventtype (uuid, memberID, mainID, typeID, insertTime) values(:uuid, :memberID, :mainID, :typeID, now())";

			List<SqlParameterSource> psList = new ArrayList<SqlParameterSource>();
			for (PinEventTypeUpdateDto dto : list) {
				psList.add(new BeanPropertySqlParameterSource(dto));
			}
			SqlParameterSource[] psArry = new SqlParameterSource[psList.size()];
			psList.toArray(psArry);
			int[] countList = namedJdbcTemplate.batchUpdate(sql, psArry);
			if (this.resultIfBatchUpdateSuccess(countList)) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<Void>(201, "Some of " + ApiError.PINNED_EVENT_TYPE_UPDATE_FAILED);
			}
		} catch (DataAccessException e) {
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> updateEventImageAllNotMain(String eventID) {
		try {
			String sql = "update eventimg set ismain = 'fasle' where eventID = ?";

			int count = jdbcTemplate.update(sql, new Object[] { eventID });
			if (count != 0) {
				return new ResponseModel<>(204, "");
			} else {
				return new ResponseModel<>(409, ApiError.EVENT_IMAGE_UPDATE_FAILED);
			}
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> updateEventImageAllNotMain(List<String> eventIDList) {
		try {
			String sql = "update eventimg set ismain = 'false' where eventID in (:eventIDList)";
			Map<String, List<String>> deleteUuidParam = Collections.singletonMap("eventIDList", eventIDList);
			int count = namedJdbcTemplate.update(sql, deleteUuidParam);
			return new ResponseModel<Void>(204, "");
//			if (count != 0) {
//				
//			} else {
//				return new ResponseModel<Void>(500, ApiError.EVENT_IMAGE_UPDATE_FAILED);
//			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deleteEventFollowedByEventID(String eventID) {
		try {
			String sql = "delete from memberevent where eventID = ?";

			jdbcTemplate.update(sql, new Object[] { eventID });
			return new ResponseModel<>(204, "");
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deleteEventTagByEventID(String eventID) {
		try {
			String sql = "delete from eventtag where eventID = ?";

			jdbcTemplate.update(sql, new Object[] { eventID });
			return new ResponseModel<Void>(204, "");
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deleteEventRecommendByEventID(String eventID) {
		try {
			String sql = "delete from eventrecommend where eventID = ?";

			jdbcTemplate.update(sql, new Object[] { eventID });
			return new ResponseModel<Void>(204, "");
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deleteEventImgByEventID(String eventID) {
		try {
			String sql = "delete from eventimg where eventID = ?";

			jdbcTemplate.update(sql, new Object[] { eventID });
			return new ResponseModel<>(204, "");
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deleteEventByEventID(String eventID) {
		try {
			String sql = "delete from atenore.event where eventID = ?";
			int count = jdbcTemplate.update(sql, new Object[] { eventID });
			if (count != 0) {
				return new ResponseModel<>(204, "");
			} else {
				return new ResponseModel<>(409, ApiError.EVENT_DELETION_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public List<EventSearchAppliedInfoDto> getAppliedEventListByMemberID(String memberID) {
		try {
			String sql = "select e.eventID, e.name, e.startTime, e.endTime, e.insertTime, e.apply, "
					+ " (select ei.img from atenore.eventimg ei where ei.eventID = e.eventID and ei.isMain = 'true') as img"
					+ " from atenore.event e where e.memberID = ? ";
			return jdbcTemplate.query(sql, new Object[] { memberID },
					new BeanPropertyRowMapper<EventSearchAppliedInfoDto>(EventSearchAppliedInfoDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public EventSearchAppliedInfoDto getEventApplyStatus(String eventID, String memberID) {
		try {
			String sql = "select eventID, startTime, endTime, apply from atenore.event where eventID = ? and memberID = ? ";
			return jdbcTemplate.queryForObject(sql, new Object[] { eventID, memberID },
					new BeanPropertyRowMapper<EventSearchAppliedInfoDto>(EventSearchAppliedInfoDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public ResponseModel<Void> setEventCancelByMemberID(String eventID, String memberID) {
		try {
			String sql = "update atenore.event set apply = ? where eventID = ? and memberID = ? ";
			int count = jdbcTemplate.update(sql, new Object[] { new EventService().getApplyStatusList().get(3), eventID, memberID });
			if (count != 0) {
				return new ResponseModel<Void>(204, "");
			} else {
				return new ResponseModel<>(409, ApiError.EVENT_APPLY_CANCLE_FAILED);
			}
		} catch (DataAccessException e) {
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> updateEventClick(String eventID) {
		try {
			String sql = "update atenore.event set click = IFNULL(click, 0) + 1 where eventID = ? ";
			int count = jdbcTemplate.update(sql, new Object[] { eventID });
			if (count != 0) {
				return new ResponseModel<Void>(204, "");
			} else {
				return new ResponseModel<>(409, ApiError.EVENT_CLICK_UPDATE_FAILED);
			}
		} catch (DataAccessException e) {
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	/* ADMIN */

	@Override
	public List<EventSearchInfoAdminDto> getEventListWithFilter(EventSearchArgsAdminDto argsDto) {
		try {
			StringBuilder sql = new StringBuilder().append("select e.eventID, e.name, e.startTime, e.endTime, e.memberID,"
					+ " case when now() >= e.startTime and now()<=e.endTime then '正在舉行' when  e.endTime < now() then '已結束' else '尚未開始' end as 'status' "
					+ " from atenore.event e where 1 = 1 ");

			if (argsDto.getFuzzyFilter() != null && argsDto.getFuzzyFilter().trim().length() != 0) {
				sql.append(" and e.name like '%' :fuzzyFilter '%' ");
			}

			if (argsDto.getStartTime() != null) {
				sql.append(" and e.startTime >= :startTime ");
			}

			if (argsDto.getEndTime() != null) {
				sql.append(" and e.endTime <=  :endTime ");
			}

			sql.append(" order by e.insertTime desc ;");

			SqlParameterSource ps = new BeanPropertySqlParameterSource(argsDto);
			return namedJdbcTemplate.query(sql.toString(), ps,
					new BeanPropertyRowMapper<EventSearchInfoAdminDto>(EventSearchInfoAdminDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public ResponseModel<Void> updateEventApplyStatus(EventApplyDto argsDto) {
		try {
			StringBuilder sql = new StringBuilder(" update atenore.event set apply = :applyStatus ");
			sql.append(" , refuse =:refuseReason  ");
//			if (argsDto.getRefuseReason() != null && argsDto.getRefuseReason().length() != 0) {
//				
//			}

			sql.append(" where eventID = :eventID ");

			SqlParameterSource ps = new BeanPropertySqlParameterSource(argsDto);
			int count = namedJdbcTemplate.update(sql.toString(), ps);

			if (count != 0) {
				return new ResponseModel<Void>(204, "");
			} else {
				return new ResponseModel<Void>(409, ApiError.EVENT_APPLY_STATUS_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public EventNotReviewCountDto getEventListNotReviewCount(EventSearchReviewArgsAdminDto argsDto) {
		try {
			StringBuilder sql = new StringBuilder(
					"select count(1) as 'notReviewCount' from atenore.event e where e.apply!=:hasReviewed ");

			SqlParameterSource ps = new BeanPropertySqlParameterSource(argsDto);
			return namedJdbcTemplate.queryForObject(sql.toString(), ps,
					new BeanPropertyRowMapper<EventNotReviewCountDto>(EventNotReviewCountDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<EventSearchReviewInfoAdminDto> getEventListNotReview(EventSearchReviewArgsAdminDto argsDto) {
		try {
			StringBuilder sql = new StringBuilder(
					"select e.eventID, e.name, e.startTime, e.endTime, e.apply, e.memberID from atenore.event e where e.apply!=:hasReviewed ");

			if (argsDto.getFuzzyFilter() != null) {
				sql.append(" and e.name like '%' :fuzzyFilter '%' ");
			}

			if (argsDto.getStartTime() != null) {
				sql.append(" and e.startTime >= :startTime ");
			}

			if (argsDto.getEndTime() != null) {
				sql.append(" and e.endTime <= :endTime ");
			}

			sql.append(" order by e.insertTime desc ;");

			SqlParameterSource ps = new BeanPropertySqlParameterSource(argsDto);
			return namedJdbcTemplate.query(sql.toString(), ps,
					new BeanPropertyRowMapper<EventSearchReviewInfoAdminDto>(EventSearchReviewInfoAdminDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public ResponseModel<Void> deleteEventImgByImageID(String imageID) {
		try {
			String sql = "delete from atenore.eventimg where uuid = ?";

			int count = jdbcTemplate.update(sql, new Object[] { imageID });
			if (count != 0) {
				return new ResponseModel<>(204, "");
			} else {
				return new ResponseModel<>(409, "");
			}
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> setEventImageByEventID(EventImageItemCreateDto image) {
		try {
			String sql = "insert into atenore.eventimg (uuid, eventID, img, objectKey, isMain) "
					+ " values(:imageID, :eventID, :img, :objectKey, false) ";
			SqlParameterSource ps = new BeanPropertySqlParameterSource(image);
			int count = namedJdbcTemplate.update(sql, ps);
			if (count != 0) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<>(500, "");
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> setEventImageAllAsNotMainByEventID(String eventID) {
		try {
			String sql = "update atenore.eventimg set isMain = false where eventID = ? ";

			jdbcTemplate.update(sql, new Object[] { eventID });
			return new ResponseModel<>(204, "");
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> setEventImageAsMainByImageID(String imageID) {
		try {
			String sql = "update atenore.eventimg set isMain = true where uuid = ?";

			int count = jdbcTemplate.update(sql, new Object[] { imageID });
			if (count != 0) {
				return new ResponseModel<>(204, "");
			} else {
				return new ResponseModel<>(409, "");
			}
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public String getEventIDByImageID(String imageID) {
		try {
			String sql = "select eventID from atenore.eventimg where uuid = ? ";
			return jdbcTemplate.queryForObject(sql, new Object[] { imageID }, String.class);
		} catch (DataAccessException e) {
			return null;
		}
	}

	private Boolean resultIfBatchUpdateSuccess(int[] countList) {
		Boolean flag = true;
		for (int count : countList) {
			if (count == 0) {
				flag = false;
				break;
			}
		}
		return flag;
	}

}
