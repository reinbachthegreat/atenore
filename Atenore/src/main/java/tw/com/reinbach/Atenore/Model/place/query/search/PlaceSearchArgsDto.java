package tw.com.reinbach.Atenore.Model.place.query.search;

import java.math.BigDecimal;

import javax.validation.constraints.NotEmpty;

import io.swagger.annotations.ApiModelProperty;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;

public class PlaceSearchArgsDto {

	@ApiModelProperty(hidden = true)
	private String memberID;

	@ApiModelProperty(value = "user's lat", example = "25.0639368", required = true)
	private BigDecimal lat;

	@ApiModelProperty(value = "user's lng", example = "121.5471534", required = true)
	private BigDecimal lng;

	@ApiModelProperty(value = "場所縣市", example = "新北市")
	private String county;

//	@ApiModelProperty(value = "場所地址", example = "新北市板橋區縣民大道55號2巷158號6樓")
//	private String address;

	@ApiModelProperty(value = "alias keywords filter")
	private String fuzzyFilter;

	@NotEmpty(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@ApiModelProperty(required = true)
	private PlaceSearchPreferArgDto searchPreferArg;

//	@NotEmpty(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
//	@ApiModelProperty(value = "sn of search order", required = true)
//	private Integer searchOrderSN;

	/*
	 * 分類屬性
	 * 
	 */
	@ApiModelProperty(value = "minor type")
	private Integer typeID;

	@ApiModelProperty(value = "main type")
	private Integer mainID;

	@ApiModelProperty(value = "capacity limit type")
	private Integer capacitySN;

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public BigDecimal getLat() {
		return lat;
	}

	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}

	public BigDecimal getLng() {
		return lng;
	}

	public void setLng(BigDecimal lng) {
		this.lng = lng;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

//	public String getAddress() {
//		return address;
//	}
//
//	public void setAddress(String address) {
//		this.address = address;
//	}

	public String getFuzzyFilter() {
		return fuzzyFilter;
	}

	public void setFuzzyFilter(String fuzzyFilter) {
		this.fuzzyFilter = fuzzyFilter;
	}

	public PlaceSearchPreferArgDto getSearchPreferArg() {
		return searchPreferArg;
	}

	public void setSearchPreferArg(PlaceSearchPreferArgDto searchPreferArg) {
		this.searchPreferArg = searchPreferArg;
	}

//	public Integer getSearchOrderSN() {
//		return searchOrderSN;
//	}
//
//	public void setSearchOrderSN(Integer searchOrderSN) {
//		this.searchOrderSN = searchOrderSN;
//	}

	public Integer getTypeID() {
		return typeID;
	}

	public void setTypeID(Integer typeID) {
		this.typeID = typeID;
	}

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public Integer getCapacitySN() {
		return capacitySN;
	}

	public void setCapacitySN(Integer capacitySN) {
		this.capacitySN = capacitySN;
	}

}
