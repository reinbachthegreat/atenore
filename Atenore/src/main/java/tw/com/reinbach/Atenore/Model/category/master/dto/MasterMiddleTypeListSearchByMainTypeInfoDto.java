package tw.com.reinbach.Atenore.Model.category.master.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import tw.com.reinbach.Atenore.Model.category.master.dto.component.MasterMiddleTypeAdminDto;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MasterMiddleTypeListSearchByMainTypeInfoDto {
	
	private String mainTypeName;

	private List<MasterMiddleTypeAdminDto> masterMiddleTypeList;

	public List<MasterMiddleTypeAdminDto> getMasterMiddleTypeList() {
		return masterMiddleTypeList;
	}

	public void setMasterMiddleTypeList(List<MasterMiddleTypeAdminDto> masterMiddleTypeList) {
		this.masterMiddleTypeList = masterMiddleTypeList;
	}

	public String getMainTypeName() {
		return mainTypeName;
	}

	public void setMainTypeName(String mainTypeName) {
		this.mainTypeName = mainTypeName;
	}
	
}
