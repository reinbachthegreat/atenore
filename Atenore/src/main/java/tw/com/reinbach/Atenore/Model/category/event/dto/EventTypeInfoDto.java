package tw.com.reinbach.Atenore.Model.category.event.dto;

import java.util.List;

public class EventTypeInfoDto {

	private List<EventTypeCascadeDto> eventTypeCascadeList;

	public List<EventTypeCascadeDto> getEventTypeCascadeList() {
		return eventTypeCascadeList;
	}

	public void setEventTypeCascadeList(List<EventTypeCascadeDto> eventTypeCascadeList) {
		this.eventTypeCascadeList = eventTypeCascadeList;
	}
	
}
