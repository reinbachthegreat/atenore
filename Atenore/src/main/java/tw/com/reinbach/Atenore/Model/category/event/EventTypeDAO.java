package tw.com.reinbach.Atenore.Model.category.event;

import java.time.LocalDateTime;
import java.util.List;

import tw.com.reinbach.Atenore.Model.category.event.dto.EventListSearchByMainTypeArgsDto;
import tw.com.reinbach.Atenore.Model.category.event.dto.EventListSearchByTypeArgsDto;
import tw.com.reinbach.Atenore.Model.category.event.dto.EventSearchByMainTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.event.dto.EventSearchByTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.event.dto.EventTypeDto;
import tw.com.reinbach.Atenore.Model.category.event.dto.component.EventMainTypeAdminDto;
import tw.com.reinbach.Atenore.Model.category.event.dto.component.EventTypeAdminDto;
import tw.com.reinbach.Atenore.Model.event.component.EventDto;

public interface EventTypeDAO {
	
//	public List<EventMainTypeDto> getEventMainTypeList();
	
	public List<EventMainTypeAdminDto> getEventMainTypeListWithFilter(String filter);
	
//	public List<EventDto> getEventListByMainType(Integer mainID);
	
	public List<EventSearchByMainTypeInfoDto> getEventListByMainTypeWithFilter(EventListSearchByMainTypeArgsDto argsDto);
	
	public List<EventTypeAdminDto> getEventTypeListByMainTypeWithFilter(Integer mainID, String fuzzyFilter);
	
	public List<EventSearchByTypeInfoDto> getEventListByTypeWithFilter(EventListSearchByTypeArgsDto argsDto);
	
	public List<EventTypeDto> getEventTypeList();
	
	public List<EventTypeAdminDto> getEventTypeListWithStatics();
	
	public List<EventTypeAdminDto> getEventTypeListWithFilter(String filter);
	
	public List<EventDto> getEventListByType(Integer typeID);
	
	public List<EventDto> getEventListByTypeWithFilter(Integer typeID, String eventFilter, LocalDateTime startTime, LocalDateTime endTime);
}
