package tw.com.reinbach.Atenore.Model.event.component;

import io.swagger.annotations.ApiModelProperty;

public class EventVisibilityDto {

	@ApiModelProperty(example = "1")
	private Integer visibilitySN;
	
	@ApiModelProperty(example = "開放 a.「歡迎參加」:不限資格、不限費用")
	private String visibility;

	public Integer getVisibilitySN() {
		return visibilitySN;
	}

	public void setVisibilitySN(Integer sn) {
		this.visibilitySN = sn;
	}

	public String getVisibility() {
		return visibility;
	}

	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}
	
	
	
}
