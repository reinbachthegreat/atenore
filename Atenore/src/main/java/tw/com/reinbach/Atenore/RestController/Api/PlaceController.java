package tw.com.reinbach.Atenore.RestController.Api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import tw.com.reinbach.Atenore.Model.category.CategoryService;
import tw.com.reinbach.Atenore.Model.category.place.dto.PlaceTypeCascadeDto;
import tw.com.reinbach.Atenore.Model.place.PlaceService;
import tw.com.reinbach.Atenore.Model.place.component.PinPlaceTypeInfoDto;
import tw.com.reinbach.Atenore.Model.place.component.PlaceDto;
import tw.com.reinbach.Atenore.Model.place.component.PlaceImageDto;
import tw.com.reinbach.Atenore.Model.place.component.PlaceTag;
import tw.com.reinbach.Atenore.Model.place.query.PinPlaceTypeListInfoDto;
import tw.com.reinbach.Atenore.Model.place.query.PlaceInfoDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceNearBySearchArgsDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceSearchArgsDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceSearchInfoDto;
import tw.com.reinbach.Atenore.Model.place.update.PinPlaceTypeUpdateDto;
import tw.com.reinbach.Atenore.Model.place.update.PlaceFollowedDto;
import tw.com.reinbach.Atenore.Model.search.SearchPreferDto;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;

@RestController
public class PlaceController {

	@Autowired
	private PlaceService placeService;

	@Autowired
	private CategoryService categoryService;

	/**
	 * Get place searching prefer list
	 * 
	 * @return
	 */
	@ApiImplicitParam(name = "Authorization", required = false, paramType = "header", dataType = "String", access = "hidden")
	@ApiOperation(notes = "ApiNo:18-1", value = "Get place searching prefer list")
	@GetMapping(value = { "/web/place/search/prefer", "/app/place/search/prefer" })
	public ResponseModel<List<SearchPreferDto>> getPlaceSearcingPreferList() {
		return new ResponseModel<List<SearchPreferDto>>(200, placeService.getPlaceSearcingPreferList());
	}

	/**
	 * Get place list by searching filter
	 * 
	 * @param searchArgs (PlaceSearchArgsDto)
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:18", value = "Get place list by searching filter")
	@GetMapping(value = { "/web/place/list", "/app/place/list" })
	@ApiImplicitParam(name = "Authorization", required = false, paramType = "header", dataType = "String", access = "hidden")
	public ResponseModel<List<PlaceSearchInfoDto>> getPlaceList(PlaceSearchArgsDto searchArgs) {
		List<PlaceSearchInfoDto> list = placeService.getPlaceListWithFilter(searchArgs);
		if (list == null || list.size() == 0) {
			return new ResponseModel<List<PlaceSearchInfoDto>>(404, ApiError.DATA_NOT_FOUND);
		}
		return new ResponseModel<List<PlaceSearchInfoDto>>(200, list);
	}

	/**
	 * Get place type cascade list
	 * 
	 * @return
	 */
	@ApiImplicitParam(name = "Authorization", required = false, paramType = "header", dataType = "String", access = "hidden")
	@ApiOperation(notes = "ApiNo:19", value = "Get place type cascade list")
	@GetMapping(value = { "/web/place/type", "/app/place/type" })
	public ResponseModel<List<PlaceTypeCascadeDto>> getPlaceTypeCascadeWithMainType() {
		List<PlaceTypeCascadeDto> list = categoryService.getPlaceTypeCascadeWithMainType();
		if (list == null || list.size() == 0) {
			return new ResponseModel<List<PlaceTypeCascadeDto>>(404, ApiError.DATA_NOT_FOUND);
		}
		return new ResponseModel<List<PlaceTypeCascadeDto>>(200, list);
	}

	/**
	 * Get member pinned place type list
	 * 
	 * @param memberID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:20", value = "Get member pinned place type list")
	@GetMapping(value = { "/web/placetype/pin/{memberID}", "/app/placetype/pin/{memberID}" })
	public ResponseModel<PinPlaceTypeListInfoDto> getPinPlaceTypeList(@PathVariable(required = true) String memberID) {

		List<PinPlaceTypeInfoDto> list = placeService.getPinPlaceTypeListByMemberID(memberID);
		if (list == null || list.size() == 0) {
			return new ResponseModel<PinPlaceTypeListInfoDto>(404, ApiError.DATA_NOT_FOUND);
		}

		PinPlaceTypeListInfoDto dto = new PinPlaceTypeListInfoDto();
		dto.setMemberID(memberID);
		dto.setPinPlaceTypeList(list);

		return new ResponseModel<PinPlaceTypeListInfoDto>(200, dto);
	}

	/**
	 * Update member pinned place type list
	 * 
	 * @param memberID
	 * @param list
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:21", value = "Update member pinned place type list")
	@PutMapping(value = { "/web/placetype/pin/{memberID}", "/app/placetype/pin/{memberID}" })
	public ResponseModel<PinPlaceTypeListInfoDto> updatePinPlaceTypeList(@PathVariable(required = true) String memberID,
			@RequestBody List<PinPlaceTypeUpdateDto> list) {

		ResponseModel<Void> result = placeService.updatePinPlaceTypeList(memberID, list);
		if (result.getCode() != 201) {
			return new ResponseModel<PinPlaceTypeListInfoDto>(result.getCode(), result.getErrMsg());
		}

		ResponseModel<PinPlaceTypeListInfoDto> resultModel = getPinPlaceTypeList(memberID);
		if (resultModel.getCode() == 200) {
			resultModel.setCode(204);
		}

		return resultModel;
	}

	/**
	 * Get place information detail
	 * 
	 * @param placeID
	 * @param memberID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:22", value = "Get place information detail.")
	@GetMapping(value = { "/web/place/info/{placeID}", "/app/place/info/{placeID}" })
	@ApiImplicitParam(name = "Authorization", required = false, paramType = "header", dataType = "String", access = "hidden")
	public ResponseModel<PlaceInfoDto> getPlaceInfoDetail(
			@ApiParam(example = "e5a55b02-b65e-446f-928f-1121ca14b060", required = true) @PathVariable(value = "placeID", required = true) String placeID,
			@ApiParam(example = "75c6538b-facd-461b-b143-919c0913028c", required = false) @RequestParam(required = false) String memberID) {

		PlaceDto placeInfo = placeService.getPlace(placeID, memberID);

		if (placeInfo == null) {
			return new ResponseModel<PlaceInfoDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			List<PlaceTag> placeTagList = placeService.getPlaceTagWithoutPlaceID(placeID);
			List<PlaceImageDto> placeImgList = placeService.getPlaceImage(placeID);
			PlaceInfoDto place = new PlaceInfoDto();
			place.setPlaceInfo(placeInfo);
			place.setPlaceTagList(placeTagList);
			place.setPlaceImgList(placeImgList);
			return new ResponseModel<PlaceInfoDto>(200, place);
		}
	}

	/**
	 * Place followed by Member
	 * 
	 * @param follow
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:23", value = "Set place followed by member.")
	@PostMapping(value = { "/web/place/follow", "/app/place/follow" })
	public ResponseModel<Void> setPlaceFollowed(PlaceFollowedDto follow) {
		follow.setWannaFollow(true);
		ResponseModel<Void> resultModel = placeService.updatePlaceFollowByMemberID(follow);
		if (resultModel.getCode() == 201) {
			return new ResponseModel<Void>(204, "Place followed success.");
		} else {
			return resultModel;
		}
	}

	/**
	 * Place cancel followed by Member
	 * 
	 * @param follow
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:24", value = "Delete place followed by member.")
	@DeleteMapping(value = { "/web/place/follow", "/app/place/follow" })
	public ResponseModel<Void> deletePlaceFollowed(PlaceFollowedDto follow) {
		follow.setWannaFollow(false);
		ResponseModel<Void> resultModel = placeService.updatePlaceFollowByMemberID(follow);
		if (resultModel.getCode() == 201) {
			return new ResponseModel<Void>(204, "Place cancel followed success.");
		} else {
			return resultModel;
		}
	}
	
	/** Get place list at GIS location near by
	 * 
	 * @param searchArgs
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:W18", value = "Get place list at GIS location near by")
	@GetMapping(value = { "/web/place/nearby" })
	public ResponseModel<List<PlaceSearchInfoDto>> getPlaceListNearBy(PlaceNearBySearchArgsDto searchArgs) {
		List<PlaceSearchInfoDto> list = placeService.getPlaceListNearBy(searchArgs);
		if (list == null || list.size() == 0) {
			return new ResponseModel<List<PlaceSearchInfoDto>>(404, ApiError.DATA_NOT_FOUND);
		}
		return new ResponseModel<List<PlaceSearchInfoDto>>(200, list);
	}
	

//	/**Preparation of creating event
//	 * 
//	 * @return
//	 */
//	@ApiOperation(value = "Preparation of creating event")
//	@RequestMapping(value = {"/admin/event/info/setting", "/web/event/info/setting"}, method = {RequestMethod.GET})
//	public ResponseModel<EventCreatePreparationDto> getEventSettingData() {
//
//		EventCreatePreparationDto dto = new EventCreatePreparationDto();
//		dto.setEventTypeCascadeList(categoryService.getEventTypeCascadeWithMainType());
//		dto.setEventVisibilityList(placeService.getVisibilityList());
//		return new ResponseModel<EventCreatePreparationDto>(200, dto);
//	}
//	
//	/**Event creation
//	 * 
//	 * @param event
//	 * @param tagList
//	 * @param masterIDList
//	 * @param files
//	 * @param request
//	 * @return
//	 * @throws NoSuchAlgorithmException
//	 * @throws UnsupportedEncodingException
//	 */
//	@ApiOperation(value = "Event creation")
//	@RequestMapping(value = {"/web/event/info/set", "/admin/event/info/set"}, method = {RequestMethod.POST})
//	public ResponseModel<Void> setEvent(
//		EventCreateDto event, 
//		@ApiParam(required = false)
//		@RequestParam(required = false) List<String> tagList, 
//		@ApiParam(required = false)
//		@RequestParam(required = false) List<String> masterIDList,
//		@ApiParam(required = false)
//		@RequestParam(required = false) List<MultipartFile> files, 
//		HttpServletRequest request) throws NoSuchAlgorithmException, UnsupportedEncodingException {
//		
//		if(request.getRequestURI().equalsIgnoreCase("/admin/event/info/set")) {
//			event.setMemberID("後台新增");
//			event.setApply(placeService.getApplyStatusList().get(1));
//		}
//		
//		try {
//			return placeService.setEvent(event, tagList, masterIDList, files);
//		} catch (Exception e) {
//			e.printStackTrace();
//			return new ResponseModel<Void>(500, e.getMessage());
//		}
//	}
//	
//	/**Event batch creation by import from formal excel
//	 * 
//	 * @param reapExcelDataFile
//	 * @return
//	 * @throws IOException
//	 */
//	@ApiOperation(value = "Event batch creation by import from formal excel")
//	@RequestMapping(value = {"/admin/event/info/import"}, method = {RequestMethod.POST})
//	public ResponseModel<Void> eventImportExcelDatatoDB(@RequestParam("file") MultipartFile reapExcelDataFile) throws IOException {
//	    try {
//	    	ResponseModel<Void> resultModel = placeService.setEventByImportExcel(reapExcelDataFile);
//	    	return resultModel;
//	    }catch(Exception e) {
//	    	e.printStackTrace();
//	    	return new ResponseModel<Void>(500, e.getMessage());
//	    }
//	}
//	
//	/**Event list need to verify by administrator, with filter selection
//	 * 
//	 * @param nameFuzzyFilter
//	 * @param startTime
//	 * @param endTime
//	 * @return
//	 */
//	@ApiOperation(value = "Event list need to verify by administrator, with filter selection")
//	@RequestMapping(value = {"/admin/event/verify"}, method = {RequestMethod.GET})
//	public ResponseModel<List<EventDto>> getEventListNotReviewedWithFilter(
//			@ApiParam(value = "event name fuzzy filter", required = false, example = "醫") @RequestParam(required = false) String nameFuzzyFilter, 
//			@ApiParam(value = "event start time filter", required = false, example = "2020-01-01 10:00:00") @RequestParam(required = false) LocalDateTime startTime, 
//			@ApiParam(value = "event end time filter", required = false, example = "2020-01-04 10:00:00") @RequestParam(required = false) LocalDateTime endTime) {
//		
//		// 檢核篩選器是否存在
//		if(nameFuzzyFilter==null || nameFuzzyFilter!=null && nameFuzzyFilter.trim().length()==0) {
//			nameFuzzyFilter= null;
//		}
//		
//		List<EventDto> eventList = placeService.getEventListNotReview(nameFuzzyFilter, startTime, endTime);
//		if(eventList==null || eventList!=null && eventList.size()==0){
//			return new ResponseModel<List<EventDto>>(404, ApiError.DATA_NOT_FOUND);
//		}else {
//			return new ResponseModel<List<EventDto>>(200, eventList);
//		}
//	}
//	
//	/**Event verified and passed by administrator
//	 * 
//	 * @param eventID
//	 * @return
//	 */
//	@ApiOperation(value = "Event verified and passed by administrator")
//	@RequestMapping(value = {"/admin/event/verify/pass/{eventID}"}, method = {RequestMethod.POST})
//	public ResponseModel<Void> setEventPassed(
//			@ApiParam(required = true)
//			@PathVariable(required = true)String eventID) {
//		
//		try {
//			return placeService.setEventPassed(eventID);
//		}catch(Exception e) {
//			e.printStackTrace();
//			return new ResponseModel<>(500, e.getMessage());
//		}
//	}
//	
//	/**Event verified and denied by administrator
//	 * 
//	 * @param eventID
//	 * @param refuse
//	 * @return
//	 */
//	@ApiOperation(value = "Event verified and denied by administrator")
//	@RequestMapping(value = {"/admin/verify/denied/{eventID}"}, method = {RequestMethod.POST})
//	public ResponseModel<Void> setEventDenied(
//			@ApiParam(required = true)
//			@PathVariable(value = "eventID", required = true)String eventID, 
//			@ApiParam(required = false, example = "防疫期間不可以大型集會ok~") 
//			@RequestParam(required = false) String refuse) {
//		
//		try {
//			return placeService.setEventDenied(eventID);
//		}catch(Exception e) {
//			e.printStackTrace();
//			return new ResponseModel<>(500, e.getMessage());
//			
//		}
//	}

//	// Not use. Not fully Deprecated.
//	/**Get event list by searching filter
//	 * 
//	 * @param nameFuzzyFilter
//	 * @param startTime
//	 * @param endTime
//	 * @return
//	 */
//	@ApiOperation(value = "Get event list by searching filter")
//	@RequestMapping(value = {"/web/event/list/{memberID}", "/app/event/list/{memberID}"}, method = {RequestMethod.GET})
//	public ResponseModel<List<EventDto>> getEventList(
//			@PathVariable(required = true)String memberID,
//			@ApiParam(value = "event name fuzzy filter", required = false, example = "醫") @RequestParam(required = false) String nameFuzzyFilter, 
//			@ApiParam(value = "start time Filter", required = false, example = "2020-01-01 10:00:00") @RequestParam(required = false) LocalDateTime startTime, 
//			@ApiParam(value = "end time filter", required = false, example = "2020-01-04 10:00:00") @RequestParam(required = false) LocalDateTime endTime) {
//		
//		// 檢核篩選器是否存在
//		if(nameFuzzyFilter==null || nameFuzzyFilter!=null && nameFuzzyFilter.trim().length()==0) {
//			nameFuzzyFilter= null;
//		}
//		
//		List<EventDto> eventList = placeService.getEventListWithFilter(nameFuzzyFilter, startTime, endTime);
//		if(eventList==null || eventList!=null && eventList.size()==0) {
//			return new ResponseModel<>(404, ApiError.DATA_NOT_FOUND);
//		}else {
//			return new ResponseModel<List<EventDto>>(200, eventList);
//		}
//	}

	/**
	 * Get capacity list of place
	 * 
	 * @return
	 */
	@GetMapping(value = "/web/capacity")
	public List<String> getCountyList() {
		return placeService.getCapacity();
	}

}
