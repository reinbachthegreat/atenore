package tw.com.reinbach.Atenore.RestController.Api.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import tw.com.reinbach.Atenore.Model.admin.AdminService;
import tw.com.reinbach.Atenore.Model.admin.create.AdminCreateDto;
import tw.com.reinbach.Atenore.Model.admin.create.FeatureCreateDto;
import tw.com.reinbach.Atenore.Model.admin.query.AdminInfoDto;
import tw.com.reinbach.Atenore.Model.admin.query.FeatureInfoDto;
import tw.com.reinbach.Atenore.Model.admin.query.FeatureListInfoDto;
import tw.com.reinbach.Atenore.Model.admin.query.LimitListInfoDto;
import tw.com.reinbach.Atenore.Model.admin.query.search.AdminSearchListInfoDto;
import tw.com.reinbach.Atenore.Model.admin.update.AdminUpdateDto;
import tw.com.reinbach.Atenore.Model.admin.update.FeatureUpdateDto;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;

@RestController
public class AdminManageController {

	@Autowired
	private AdminService adminService;
	
	/** Get ADMIN list by featureID filter
	 * 
	 * @param featureID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A58", value = "Get ADMIN list by featureID filter")
	@RequestMapping(value = {"/admin/manager"}, method = {RequestMethod.GET})
	public ResponseModel<AdminSearchListInfoDto> getAdminByFeatureID(
			@RequestParam(required = false) String featureID) {
		AdminSearchListInfoDto dto = adminService.getAdminByFeatureID(featureID);
		if(dto==null) {
			return new ResponseModel<AdminSearchListInfoDto>(404, ApiError.DATA_NOT_FOUND);
		}else {
			return new ResponseModel<AdminSearchListInfoDto>(200, dto);
		}
	}

	/** Get ADMIN info by adminID
	 * 
	 * @param adminID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A59", value = "Get ADMIN info by adminID")
	@RequestMapping(value = {"/admin/manager/{adminID}"}, method = {RequestMethod.GET})
	public ResponseModel<AdminInfoDto> getAdminByAdminID(
			@PathVariable(required = true)String adminID) {
		
		AdminInfoDto dto = adminService.getAdminByAdminID(adminID);
		if(dto==null) {
			return new ResponseModel<AdminInfoDto>(404, ApiError.DATA_NOT_FOUND);
		}else {
			return new ResponseModel<AdminInfoDto>(200, dto);
		}
	}
	
	/** Create ADMIN info
	 * 
	 * @param admin
	 * @return
	 * @throws Throwable 
	 */
	@ApiOperation(notes = "ApiNo:A60", value = "Create ADMIN info")
	@RequestMapping(value = {"/admin/manager"}, method = {RequestMethod.POST})
	public ResponseModel<Void> createAdmin(AdminCreateDto admin) throws Throwable{
		return adminService.createAdmin(admin);
	}
	
	/** Update ADMIN info
	 * 
	 * @param adminID
	 * @param formAdmin
	 * @return
	 * @throws Throwable 
	 */
	@ApiOperation(notes = "ApiNo:A61", value = "Update ADMIN info")
	@RequestMapping(value = {"/admin/manager/{adminID}"}, method = {RequestMethod.PUT})	
	public ResponseModel<Void> updateAdmin(
			@PathVariable(required = true) String adminID, AdminUpdateDto formAdmin) throws Throwable{
		return adminService.updateAdmin(adminID, formAdmin);
	}

	/** Delete ADMIN by adminID
	 * 
	 * @param adminID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A62", value = "Delete ADMIN by adminID")
	@RequestMapping(value = {"/admin/manager/{adminID}"}, method = {RequestMethod.DELETE})	
	public ResponseModel<Void> deleteAdminByAdminID(@PathVariable(required = true)String adminID){
		return adminService.deleteAdminByAdminID(adminID);
	}

	/** Get ADMIN feature list
	 * 
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A63", value = "Get ADMIN feature list")
	@RequestMapping(value = {"/admin/feature"}, method = {RequestMethod.GET})	
	public ResponseModel<FeatureListInfoDto> getFeatureList() {
		FeatureListInfoDto dto =  adminService.getFeatureList();
		if(dto!=null) {
			return new ResponseModel<FeatureListInfoDto>(200, dto);
		}else {
			return new ResponseModel<FeatureListInfoDto>(404, ApiError.DATA_NOT_FOUND);
		}
	}
	
	/** Get ADMIN feature with limit list by featurID
	 * 
	 * @param featureID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A64", value = "Get ADMIN feature with limit list by featurID")
	@RequestMapping(value = {"/admin/feature/{featureID}"}, method = {RequestMethod.GET})		
	public ResponseModel<FeatureInfoDto> getFeatureWithLimitListByFeaturID(
			@PathVariable(required = true)String featureID) {
		FeatureInfoDto dto = adminService.getFeatureWithLimitListByFeaturID(featureID);
		if(dto!=null) {
			return new ResponseModel<FeatureInfoDto>(200, dto);
		}else {
			return new ResponseModel<FeatureInfoDto>(404, ApiError.DATA_NOT_FOUND);
		}
	}
	
	/** Create ADMIN feature with limit list
	 * 
	 * @param feature
	 * @param limitList
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A65", value = "Create ADMIN feature with limit list")
	@RequestMapping(value = {"/admin/feature"}, method = {RequestMethod.POST})	
	public ResponseModel<Void> createFeatureWithLimitList(FeatureCreateDto feature){
		return adminService.createFeatureWithLimitList(feature);
	}
	
	/** Get ADMIN limit list
	 * 
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A66", value = "Get ADMIN limit list")
	@RequestMapping(value = {"/admin/limit"}, method = {RequestMethod.GET})	
	public ResponseModel<LimitListInfoDto> getLimitList() {
		LimitListInfoDto dto = adminService.getLimitList();
		if(dto!=null) {
			return new ResponseModel<LimitListInfoDto>(200, dto);
		}else {
			return new ResponseModel<LimitListInfoDto>(404, ApiError.DATA_NOT_FOUND);
		}
	}

	/** Update ADMIN feature with limit list by featureID
	 * 
	 * @param featureID
	 * @param formFeature
	 * @param limitList
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A67", value = "Update ADMIN feature with limit list by featureID")
	@RequestMapping(value = {"/admin/feature/{featureID}"}, method = {RequestMethod.PUT})
	public ResponseModel<Void> updateFeatureByFeatureID(
			@PathVariable(required = true)String featureID, FeatureUpdateDto formFeature){
		System.out.println(formFeature.getLimitIDList().get(1));
		return adminService.updateFeatureByFeatureID(featureID, formFeature);
	}
	
	/** Delete ADMIN feature by featureID
	 * 
	 * @param featureID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A68", value = "Delete ADMIN feature by featureID")
	@RequestMapping(value = {"/admin/feature/{featureID}"}, method = {RequestMethod.DELETE})	
	public ResponseModel<Void> deleteFeatureByFeatureID(@PathVariable(required = true) String featureID){
		return adminService.deleteFeatureByFeatureID(featureID);
	}
	
}
