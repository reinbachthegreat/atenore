package tw.com.reinbach.Atenore.Model.category.master.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MasterListSearchByMainTypeInfoDto {
	
	private Integer mainID;
	
	private String mainTypeName;
	
	private String typeName;
	
	private List<MasterSearchByMainTypeInfoDto> masterList;

	public List<MasterSearchByMainTypeInfoDto> getMasterList() {
		return masterList;
	}

	public void setMasterList(List<MasterSearchByMainTypeInfoDto> masterList) {
		this.masterList = masterList;
	}

	public String getMainTypeName() {
		return mainTypeName;
	}

	public void setMainTypeName(String mainTypeName) {
		this.mainTypeName = mainTypeName;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}
	
}
