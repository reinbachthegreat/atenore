package tw.com.reinbach.Atenore.Model.master.query.search;

import io.swagger.annotations.ApiModelProperty;

public class MasterSearchArgsMemberEditedDto {

	@ApiModelProperty(value = "會員編號", hidden = true)
	private String memberID;

	@ApiModelProperty(value = "區域縣市")
	private String county;

	@ApiModelProperty(value = "master type(minor)", example = "1")
	private Integer typeID;

	@ApiModelProperty(value = "master middle type(middle)", example = "1")
	private Integer midID;

	@ApiModelProperty(value = "master main type(main)", example = "1")
	private Integer mainID;

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public Integer getTypeID() {
		return typeID;
	}

	public void setTypeID(Integer typeID) {
		this.typeID = typeID;
	}

	public Integer getMidID() {
		return midID;
	}

	public void setMidID(Integer midID) {
		this.midID = midID;
	}

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

}
