package tw.com.reinbach.Atenore.Model.category.master.dto.component;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MasterTypeAdminDto {

	@ApiModelProperty(example = "1")
	private Integer typeID;

	@ApiModelProperty(hidden = true)
	private Integer mainID;
	
	@ApiModelProperty(hidden = true)
	private Integer midID;
	
	@ApiModelProperty(example = "醫院")
	private String name;
	
	private String mainTypeName;
	
	private String middleTypeName;
	
	@ApiModelProperty(name = "aliveQty", value = "大分類下梁山伯數量", example = "100", dataType = "Integer")
	private Integer aliveQty;
	
	@ApiModelProperty(name = "deadQty", value = "大分類下封神榜數量數量", example = "100", dataType = "Integer")
	private Integer deadQty;
	
	@ApiModelProperty(name = "totalQty", value = "大分類下所有達人數量", example = "131", dataType = "Integer")
	private Integer totalQty;

	public Integer getTypeID() {
		return typeID;
	}

	public void setTypeID(Integer typeID) {
		this.typeID = typeID;
	}

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public Integer getMidID() {
		return midID;
	}

	public void setMidID(Integer midID) {
		this.midID = midID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMainTypeName() {
		return mainTypeName;
	}

	public void setMainTypeName(String mainTypeName) {
		this.mainTypeName = mainTypeName;
	}

	public String getMiddleTypeName() {
		return middleTypeName;
	}

	public void setMiddleTypeName(String middleTypeName) {
		this.middleTypeName = middleTypeName;
	}

	public Integer getAliveQty() {
		return aliveQty;
	}

	public void setAliveQty(Integer aliveQty) {
		this.aliveQty = aliveQty;
	}

	public Integer getDeadQty() {
		return deadQty;
	}

	public void setDeadQty(Integer deadQty) {
		this.deadQty = deadQty;
	}

	public Integer getTotalQty() {
		return totalQty;
	}

	public void setTotalQty(Integer totalQty) {
		this.totalQty = totalQty;
	}
	
}
