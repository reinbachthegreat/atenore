package tw.com.reinbach.Atenore.Model.category.master.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MasterListSearchByMiddleTypeInfoDto {
	
	private Integer mainID;
	
	private Integer middleID;
	
	private String middleTypeName;
	
	private String mainTypeName;
	
	private List<MasterSearchByMiddleTypeInfoDto> masterList;

	public List<MasterSearchByMiddleTypeInfoDto> getMasterList() {
		return masterList;
	}

	public void setMasterList(List<MasterSearchByMiddleTypeInfoDto> masterList) {
		this.masterList = masterList;
	}

	public String getMiddleTypeName() {
		return middleTypeName;
	}

	public void setMiddleTypeName(String middleTypeName) {
		this.middleTypeName = middleTypeName;
	}

	public String getMainTypeName() {
		return mainTypeName;
	}

	public void setMainTypeName(String mainTypeName) {
		this.mainTypeName = mainTypeName;
	}

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public Integer getMiddleID() {
		return middleID;
	}

	public void setMiddleID(Integer middleID) {
		this.middleID = middleID;
	}
}
