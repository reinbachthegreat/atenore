package tw.com.reinbach.Atenore.Model.admin.query.search;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AdminSearchListInfoDto {

	private List<AdminSearchInfoDto> adminList;

	public List<AdminSearchInfoDto> getAdminList() {
		return adminList;
	}

	public void setAdminList(List<AdminSearchInfoDto> adminList) {
		this.adminList = adminList;
	}
	
}
