package tw.com.reinbach.Atenore.Model.event.query.search;

import java.time.LocalDateTime;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;

public class EventSearchReviewArgsAdminDto {

	@ApiModelProperty(value = "fuzzy searching by name keywords", required = false, example = "醫")
	private String fuzzyFilter;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(required = false, example = "2020-01-01 10:00:00")
	private LocalDateTime startTime;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(required = false, example = "2020-01-04 10:00:00")
	private LocalDateTime endTime;
	
	@ApiModelProperty(hidden = true)
	private String hasReviewed;

	public String getFuzzyFilter() {
		return fuzzyFilter;
	}

	public void setFuzzyFilter(String fuzzyFilter) {
		this.fuzzyFilter = fuzzyFilter;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}

	public String getHasReviewed() {
		return hasReviewed;
	}

	public void setHasReviewed(String hasReviewed) {
		this.hasReviewed = hasReviewed;
	}
	
}
