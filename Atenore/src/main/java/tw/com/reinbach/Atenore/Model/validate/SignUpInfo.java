package tw.com.reinbach.Atenore.Model.validate;

public class SignUpInfo {
	
	private String memberID;

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

}
