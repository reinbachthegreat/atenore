package tw.com.reinbach.Atenore.Utility.oauth.facebook;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public class FBGraph {
	
	private String accessToken;

	public FBGraph(String accessToken) {
		this.accessToken = accessToken;
	}
	
	public ResponseEntity<FacebookUserInfo> getFBGraph() {
		RestTemplate template = new RestTemplate();
		MultiValueMap<String, String> parametersMap = new LinkedMultiValueMap<String, String>();
		parametersMap.set("access_token", accessToken);
		parametersMap.set("fields", "id,email,name");
		return template.postForEntity("https://graph.facebook.com/v9.0/me", parametersMap, FacebookUserInfo.class);
	}

	public Map<String, String> getGraphData(String fbGraph) {
		Map<String, String> fbProfile = new HashMap<String, String>();
		try {
			JSONObject json = new JSONObject(fbGraph);
			System.out.println(json);
			fbProfile.put("id", json.getString("id"));
			fbProfile.put("name", json.getString("name"));
			if (json.has("email"))
				fbProfile.put("email", json.getString("email"));
		} catch (JSONException e) {
			e.printStackTrace();
			throw new RuntimeException("ERROR in parsing FB graph data. " + e);
		}
		return fbProfile;
	}
}
