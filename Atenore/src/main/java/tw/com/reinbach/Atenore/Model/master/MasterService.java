package tw.com.reinbach.Atenore.Model.master;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.multipart.MultipartFile;

import com.google.maps.model.LatLng;

import tw.com.reinbach.Atenore.Model.master.component.MasterDto;
import tw.com.reinbach.Atenore.Model.master.component.MasterEditDto;
import tw.com.reinbach.Atenore.Model.master.component.MasterRecommend;
import tw.com.reinbach.Atenore.Model.master.component.MasterTag;
import tw.com.reinbach.Atenore.Model.master.component.MasterTagEditDto;
import tw.com.reinbach.Atenore.Model.master.create.MasterCreateBatchResultInfoDto;
import tw.com.reinbach.Atenore.Model.master.create.MasterCreateDto;
import tw.com.reinbach.Atenore.Model.master.query.MasterEditInfoDto;
import tw.com.reinbach.Atenore.Model.master.query.MasterInfoDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterAliveSearchArgsDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterAliveSearchInfoDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterDeadSearchArgsDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterDeadSearchInfoDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterEditSearchArgsAdminDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterEditSearchInfoAdminDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterEditSearchInfoMemberDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterEditSearchListInfoAdminDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterHitSearchArgsDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterHitSearchInfoDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterSearchArgsAdminDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterSearchArgsDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterSearchArgsMemberEditedDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterSearchInfoDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterSearchListInfoAdminDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterSearchSimpleInfoDto;
import tw.com.reinbach.Atenore.Model.master.update.MasterFollowedDto;
import tw.com.reinbach.Atenore.Model.master.update.MasterUpdateDto;
import tw.com.reinbach.Atenore.Model.search.SearchPreferDto;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;
import tw.com.reinbach.Atenore.Utility.UUIDGenerator;
import tw.com.reinbach.Atenore.Utility.Utility;
import tw.com.reinbach.Atenore.Utility.aws.AmazonDto;
import tw.com.reinbach.Atenore.Utility.aws.AmazonService;
import tw.com.reinbach.Atenore.Utility.google.GpsService;

@Service
public class MasterService {

	@Autowired
	private MasterDAO masterDAO;

	@Autowired
	private AmazonService amazonService;

	@Autowired
	private Utility utility;

	@Autowired
	private GpsService gpsService;

	@Autowired
	private PlatformTransactionManager transactionManager;

	@Value("${master.update.excel.template.from}")
	private String updateExcelTemplateUrl;

	@Value("${master.update.excel.export.aws.to}")
	private String updateExcelFolder;

	private String[] searchPrefer = { "收藏排行", "點閱排行" };

	/**
	 * Get master list with MasterSearchArgsDto
	 * 
	 * @param argsDto
	 * @return
	 */
	public List<MasterSearchInfoDto> getMasterListWithFilter(MasterSearchArgsDto argsDto) {

		List<MasterSearchInfoDto> list = masterDAO.getMasterListWithFilter(argsDto);
		if (list != null) {
			int index = 0;
			for (MasterSearchInfoDto master : list) {

				if (master.getLat() != null && master.getLng() != null) {
					master = this.calculator(argsDto.getLat(), argsDto.getLng(), master);
					list.set(index, master);
				}
				index++;
			}

			Collections.sort(list,
					Comparator.comparing(MasterSearchInfoDto::getDistance, Comparator.nullsLast(Comparator.reverseOrder())));

		}
		return list;
	}

	/**
	 * Get master list with MasterSearchArgsAdminDto
	 * 
	 * @param argsDto
	 * @return
	 */
	public MasterSearchListInfoAdminDto getMasterListWithFilter(MasterSearchArgsAdminDto argsDto) {
		MasterSearchListInfoAdminDto dto = new MasterSearchListInfoAdminDto();
		dto.setMasterList(masterDAO.getMasterListWithFilter(argsDto));
		return dto;
	}

	/**
	 * Get master hit searching prefer list
	 * 
	 * @return
	 */
	public List<SearchPreferDto> getMasterHitSearcingPreferList() {
		int preferIndex = 0;
		List<SearchPreferDto> preferList = new ArrayList<SearchPreferDto>();
		for (String p : this.searchPrefer) {
			SearchPreferDto prefer = new SearchPreferDto();
			prefer.setSearchPreferSN(preferIndex);
			prefer.setSearchPreferName(p);
			preferList.add(prefer);
			preferIndex++;
		}
		return preferList;
	}

	/**
	 * Get master hit list by hit prefer SN order by distance between user and
	 * master
	 * 
	 * @param searchPreferSN
	 * @return
	 */
	public List<MasterHitSearchInfoDto> getMasterListByHitSearchPreferSN(MasterHitSearchArgsDto argsDto) {
		List<MasterHitSearchInfoDto> list = masterDAO.getMasterListByHitSearchPreferSN(argsDto.getSearchPreferSN());
		if (list != null) {
			int index = 0;
			for (MasterHitSearchInfoDto master : list) {

				if (master.getLat() != null && master.getLng() != null) {
					master = this.calculator(argsDto.getLat(), argsDto.getLng(), master);
					list.set(index, master);
				}
				index++;
			}

			Collections.sort(list,
					Comparator.comparing(MasterHitSearchInfoDto::getDistance, Comparator.nullsLast(Comparator.reverseOrder())));

		}

		return list;
	}

	/**
	 * Get master alive list by MasterAliveSearchArgsDto
	 * 
	 * @param argsDto
	 * @return
	 */
	public List<MasterAliveSearchInfoDto> getMasterAliveListWithFilter(MasterAliveSearchArgsDto argsDto) {
		List<MasterAliveSearchInfoDto> list = masterDAO.getMasterAliveListWithFilter(argsDto);
		return list;
	}

	/**
	 * Get master dead list by MasterDeadSearchArgsDto
	 * 
	 * @param argsDto
	 * @return
	 */
	public List<MasterDeadSearchInfoDto> getMasterDeadListWithFilter(MasterDeadSearchArgsDto argsDto) {
		List<MasterDeadSearchInfoDto> list = masterDAO.getMasterDeadListWithFilter(argsDto);
		return list;
	}

	/**
	 * Get master list simple information by publicName
	 * 
	 * @param name
	 * @return
	 */
	public List<MasterSearchSimpleInfoDto> getMasterSimpleByPublicName(String name, Integer alive) {
		List<MasterSearchSimpleInfoDto> master = masterDAO.getMasterSimpleByPublicName(name.trim(), alive);
		if (master != null) {
			return master;
		} else {
			return null;
		}
	}

	/** Get master fully information by masterID(Admin)
	 * 
	 * @param masterID
	 * @return
	 */
	public MasterInfoDto getMasterFullyByMasterID(String masterID) {
		 MasterDto master = masterDAO.getMasterByMasterID(masterID);			 
		if (master != null) {
			List<MasterTag> tagList = masterDAO.getMasterTagListByMasterID(masterID);
			MasterInfoDto dto = new MasterInfoDto();
			dto.setMaster(master);
			dto.setTagList(tagList);
			return dto;
		} else {
			return null;
		}
	}
	
	/** Get master fully information by masterID
	 * 
	 * @param masterID
	 * @param memberID
	 * @return
	 */
	public MasterInfoDto getMasterFullyByMasterID(String masterID, String memberID) {
		MasterDto master;
		if(memberID != null && memberID.trim().length() != 0) {
			master = masterDAO.getMasterByMasterIDWithMemberID(masterID, memberID);
		}else {
			master = masterDAO.getMasterByMasterID(masterID);	
		}
		masterDAO.updateMasterClick(masterID);
		if (master != null) {
			List<MasterTag> tagList = masterDAO.getMasterTagListByMasterID(masterID);
			MasterInfoDto dto = new MasterInfoDto();
			dto.setMaster(master);
			dto.setTagList(tagList);
			return dto;
		} else {
			return null;
		}
	}

	/**
	 * Get master information
	 * 
	 * @param masterID, required.
	 * @param isAilve,  required.
	 * @param memberID
	 * @return
	 */
	public MasterDto getMaster(String masterID, Boolean isAilve, String memberID) {

		masterDAO.updateMasterClick(masterID);

		if (isAilve) {
			if (memberID != null && memberID.trim().length() != 0) {
				return masterDAO.getMasterAliveWithMemberID(masterID, memberID);
			} else {
				return masterDAO.getMasterAlive(masterID);
			}
		} else {
			if (memberID != null && memberID.trim().length() != 0) {
				return masterDAO.getMasterDeadWithMemberID(masterID, memberID);
			} else {
				return masterDAO.getMasterDead(masterID);
			}
		}

	}

	public List<MasterTag> getMasterTagListByMasterID(String masterID) {
		return masterDAO.getMasterTagListByMasterID(masterID);
	}

	public List<MasterRecommend> getMasterRecommendListByMasterID(String masterID) {
		return masterDAO.getMasterRecommendListByMasterID(masterID);
	}

	/**
	 * Update master followed by member
	 * 
	 * @param follow
	 * @return Message of Doing result;
	 */
	public ResponseModel<Void> updateMasterFollowByMemberID(MasterFollowedDto follow) {

		Boolean ifFollowed = masterDAO.getMasterIfMemberFollowed(follow);

		if (ifFollowed == true && follow.getWannaFollow() == true) {
			return new ResponseModel<Void>(500, ApiError.MASTER_HAS_FOLLOWED);
		} else if (ifFollowed == false && follow.getWannaFollow() == false) {
			return new ResponseModel<Void>(500, ApiError.MASTER_HAS_NOT_FOLLOWED);
		} else if (ifFollowed == null) {
			return new ResponseModel<Void>(500, ApiError.DATA_ACCESS_ERROR);
		}

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> resultModel = null;

		try {

			resultModel = masterDAO.updateMasterFollowed(follow.getMasterID(), follow.getWannaFollow());
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}
			
			if (follow.getWannaFollow() == true) {
				resultModel = masterDAO.setMasterFollowed(follow);
				if (resultModel.getCode() != 201) {
					throw new Exception();
				}
			} else {
				resultModel = masterDAO.deleteMasterFollowed(follow);
				if (resultModel.getCode() != 201) {
					throw new Exception();
				}
			}
			transactionManager.commit(status);
			return new ResponseModel<Void>(201, "");
		} catch (Exception e) {
			transactionManager.rollback(status);
			return resultModel;
		}
	}

	/**
	 * Master creation with image uploaded. In the meanwhile, will create a version
	 * of master edit.
	 * 
	 * @param master
	 * @return
	 */
	public ResponseModel<Void> setMaster(MasterCreateDto master) {

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> resultModel = null;

		List<String> tagList = master.getTagList();
		MultipartFile file = master.getFile();

		// Set masterID by UUID, insert master info
		String masterID = UUIDGenerator.generateType4UUID().toString();
		String editID = UUIDGenerator.generateType4UUID().toString();

		if (file != null && file.getSize() != 0) {
			amazonService.setFolderName(new StringBuilder("Master").append("/").append(masterID).toString());
			try {
				AmazonDto awsDto = amazonService.uploadFile(file);
				master.setImg(awsDto.getUrl());
				master.setObjectKey(awsDto.getObjectKey());
			} catch (IOException e) {
				e.printStackTrace();
				return new ResponseModel<>(500, ApiError.AWS_FILE_UPLOAD_FAILED + "Due to: " + e.getMessage());
			}
		}

		try {
			master.setMasterID(masterID);
			master.setEditID(editID);

			// Google Map回傳所在LAT, LNG
			if (master.getCounty() != null && master.getAddress() != null) {
				LatLng location = gpsService.getSimpleLocationFromAddress(master.getCounty() + master.getAddress());
				if (location != null) {
					master.setLat(BigDecimal.valueOf(location.lat));
					master.setLng(BigDecimal.valueOf(location.lng));
				}
			}

			resultModel = masterDAO.setMasterEdit(master);
			if (resultModel.getCode() != 201) {
				throw new Exception();
			}

			resultModel = masterDAO.setMaster(master);
			if (resultModel.getCode() != 201) {
				throw new Exception();
			}

			if (tagList != null && tagList.size() != 0) {
				// Set Master tag list and insert.
				List<MasterTag> tagInsertList = new ArrayList<MasterTag>();
				for (String t : tagList) {
					MasterTag tag = new MasterTag();
					tag.setMasterID(masterID);
					tag.setTag(t);
					tag.setTagID(UUIDGenerator.generateType4UUID().toString());
					tagInsertList.add(tag);
				}

				// Set Master tag edit list and insert.
				List<MasterTagEditDto> tageditInsertList = new ArrayList<MasterTagEditDto>();
				for (String t : tagList) {
					MasterTagEditDto tag = new MasterTagEditDto();
					tag.setUuid(UUIDGenerator.generateType4UUID().toString());
					tag.setTag(t);
					tag.setEditID(editID);
					tageditInsertList.add(tag);
				}

				resultModel = masterDAO.setMasterTag(tagInsertList);
				if (resultModel.getCode() != 201) {
					throw new Exception();
				}

				resultModel = masterDAO.setMasterTagEdit(tageditInsertList);
				if (resultModel.getCode() != 201) {
					throw new Exception();
				}
			}

			transactionManager.commit(status);
			return new ResponseModel<Void>(201, "");
		} catch (Exception e) {
			e.printStackTrace();
			transactionManager.rollback(status);
			return resultModel;
		}
	}

	/**
	 * Master batch creation
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public ResponseModel<MasterCreateBatchResultInfoDto> setMasterByImportExcel(MultipartFile file) throws IOException {

		List<MasterCreateDto> masterImportList = new ArrayList<MasterCreateDto>();
		XSSFWorkbook workbook = null;
		// Output file read from Template
		XSSFWorkbook outputBook = null;
		File f = null;
		try {
			workbook = new XSSFWorkbook(file.getInputStream());
			URL url = new URL(updateExcelTemplateUrl);
			InputStream in = url.openStream();
			Files.copy(in, Paths.get("master_template.xlsx"), StandardCopyOption.REPLACE_EXISTING);
			f = new File("master_template.xlsx");
			InputStream input = new FileInputStream(f);
			outputBook = new XSSFWorkbook(input);
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseModel<MasterCreateBatchResultInfoDto>(500, e.getMessage());
		}
		XSSFSheet worksheet = workbook.getSheetAt(0);

		for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {

			MasterCreateDto master = new MasterCreateDto();
			XSSFRow row = worksheet.getRow(i);

			String masterID = UUIDGenerator.generateType4UUID().toString();
			String editID = UUIDGenerator.generateType4UUID().toString();
			master.setMasterID(masterID);
			master.setEditID(editID);
//			DataFormatter formatter = new DataFormatter();
			master.setTypeID((int) row.getCell(0).getNumericCellValue());
			if (row.getCell(1) != null && row.getCell(1).getStringCellValue().length() != 0) {
				master.setPublicName(row.getCell(1).getStringCellValue());
			}

			CellStyle cellStyle = outputBook.createCellStyle();
			XSSFFont font = outputBook.createFont();
			font.setFontName("Microsoft JhengHei");
			font.setFontHeight(12);
			cellStyle.setWrapText(true);
			cellStyle.setFont(font);
			cellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

			CellStyle cellStyle2 = outputBook.createCellStyle();
			XSSFFont font2 = outputBook.createFont();
			font.setFontName("Microsoft JhengHei");
			font.setFontHeight(8);
			cellStyle2.setWrapText(true);
			cellStyle2.setFont(font2);
			cellStyle2.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

			XSSFRow row1 = outputBook.getSheetAt(0).createRow(i);
			XSSFCell cell1 = row1.createCell(0);
			XSSFCell cell2 = row1.createCell(1);
			XSSFCell cell3 = row1.createCell(2);

			cell1.setCellValue(masterID);
			cell2.setCellValue(editID);
			cell3.setCellValue(master.getPublicName());
			cell1.setCellStyle(cellStyle);
			cell2.setCellStyle(cellStyle2);
			cell3.setCellStyle(cellStyle);

			for (int k = 2; k <= 17; k++) {
				if (row.getCell(k) != null) {
					row.getCell(k).setCellType(Cell.CELL_TYPE_STRING);
				}
			}

			if (row.getCell(2) != null && row.getCell(2).getStringCellValue().length() != 0) {
				master.setLastName(row.getCell(2).getStringCellValue());
			}

			if (row.getCell(3) != null && row.getCell(3).getStringCellValue().length() != 0) {
				master.setFirstName(row.getCell(3).getStringCellValue());
			}

			if (row.getCell(4) != null && row.getCell(4).getStringCellValue().length() != 0) {
				master.setImg(row.getCell(4).getStringCellValue());
			}

			if (row.getCell(5) != null && row.getCell(5).getStringCellValue().length() != 0) {
				master.setObjectKey(row.getCell(5).getStringCellValue());
			}

			if (row.getCell(6) != null && row.getCell(6).getStringCellValue().length() != 0) {
				master.setLineID(row.getCell(6).getStringCellValue());
			}

			if (row.getCell(7) != null && row.getCell(7).getStringCellValue().length() != 0) {
				master.setFacebook(row.getCell(7).getStringCellValue());
			}

			if (row.getCell(8) != null && row.getCell(8).getStringCellValue().length() != 0) {
				master.setWebsite(row.getCell(8).getStringCellValue());
			}

			if (row.getCell(9) != null && row.getCell(9).getStringCellValue().length() != 0) {
				master.setMail(row.getCell(9).getStringCellValue());
			}

			row.getCell(10).setCellType(Cell.CELL_TYPE_STRING);
			if (row.getCell(10) != null && row.getCell(10).getStringCellValue().length() != 0) {
				master.setPhone(row.getCell(10).getStringCellValue());
			}

			if (row.getCell(11) != null && row.getCell(11).getStringCellValue().length() != 0) {
				master.setCounty(row.getCell(11).getStringCellValue());
			}

			if (row.getCell(12) != null && row.getCell(12).getStringCellValue().length() != 0) {
				master.setAddress(row.getCell(12).getStringCellValue());
			}

			if (row.getCell(13) != null && row.getCell(13).getStringCellValue().length() != 0) {
				master.setCompany(row.getCell(13).getStringCellValue());
			}

			if (row.getCell(14) != null && row.getCell(14).getStringCellValue().length() != 0) {
				String aliveString = row.getCell(14).getStringCellValue();
				if (aliveString.equalsIgnoreCase("y") || aliveString.equalsIgnoreCase("yes") || aliveString.equalsIgnoreCase("true")) {
					master.setAlive(true);
				} else if (aliveString.equalsIgnoreCase("n") || aliveString.equalsIgnoreCase("no")
						|| aliveString.equalsIgnoreCase("false")) {
					master.setAlive(false);
				}
			}

			if (row.getCell(15) != null && row.getCell(15).getStringCellValue().length() != 0) {
				master.setIntro(row.getCell(15).getStringCellValue());
			}

			if (row.getCell(16) != null && row.getCell(16).getStringCellValue().length() != 0) {
				StringBuilder datetime = new StringBuilder(row.getCell(16).getStringCellValue()).append(" 00:00:00");
				master.setDeath(LocalDateTime.parse(datetime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
			}

			if (row.getCell(17) != null && row.getCell(17).getStringCellValue().length() != 0) {
				master.setLegend(row.getCell(17).getStringCellValue());
			}

			master.setMemberID("後台新增");

			// Google Map回傳所在LAT, LNG
			if (master.getCounty() != null && master.getAddress() != null) {
				LatLng location = gpsService.getSimpleLocationFromAddress(master.getCounty() + master.getAddress());
				if (location != null) {
					master.setLat(BigDecimal.valueOf(location.lat));
					master.setLng(BigDecimal.valueOf(location.lng));
				}
			}
			masterImportList.add(master);
		}

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> tempModel = masterDAO.setMaster(masterImportList);
		ResponseModel<Void> tempModel2 = masterDAO.setMasterEdit(masterImportList);

		if (tempModel.getCode() == 201 && tempModel2.getCode() == 201) {

			MasterCreateBatchResultInfoDto dto = new MasterCreateBatchResultInfoDto();
			try {
				String outputTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss"));
				amazonService.setFolderName(this.updateExcelFolder);
				File outputFile = new File("MasterBatchUpdate_" + outputTime + ".xlsx");
				outputFile.createNewFile();
				OutputStream o = new FileOutputStream(outputFile);
				outputBook.write(o);
				o.flush();
				o.close();
				outputBook.close();
				workbook.close();
				AmazonDto bean = amazonService.uploadFileNotEncrypted(outputFile);
				dto.setBatchUpdateUrl(bean.getUrl());
				dto.setBatchUpdateObjectKey(bean.getObjectKey());
				f.delete();
				outputFile.delete();
			} catch (IOException e) {
				e.printStackTrace();
				transactionManager.rollback(status);
				return new ResponseModel<MasterCreateBatchResultInfoDto>(500, e.getMessage());
			}
			transactionManager.commit(status);
			return new ResponseModel<MasterCreateBatchResultInfoDto>(201, dto);
		} else {
			outputBook.close();
			workbook.close();
			transactionManager.rollback(status);
			return new ResponseModel<MasterCreateBatchResultInfoDto>(tempModel.getCode(), tempModel.getErrMsg());
		}
	}

	/**
	 * Master batch update from previous creation result template
	 * 
	 * @param file
	 * @return
	 */
	public ResponseModel<Void> updateMasterTagByImportExcel(MultipartFile file) {

		XSSFWorkbook workbook = null;
		try {
			workbook = new XSSFWorkbook(file.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseModel<>(500, e.getMessage());
		}

		XSSFSheet tagSheet = workbook.getSheetAt(0);

		List<String> masterIDList = new ArrayList<String>();
		List<MasterTag> tagList = new ArrayList<MasterTag>();
		for (int i = 1; i < tagSheet.getPhysicalNumberOfRows(); i++) {
			XSSFRow row = tagSheet.getRow(i);
			for (int c = 3; c < row.getPhysicalNumberOfCells(); c++) {
				if (row.getCell(c) != null && row.getCell(c).getStringCellValue().length() != 0) {
					MasterTag tag = new MasterTag();
					tag.setTagID(UUIDGenerator.generateType4UUID().toString());
					masterIDList.add(row.getCell(0).getStringCellValue());
					tag.setMasterID(row.getCell(0).getStringCellValue());
					tag.setTag(row.getCell(c).getStringCellValue());
					tagList.add(tag);
				} else {
					continue;
				}
			}
		}

		List<MasterTagEditDto> tagEditList = new ArrayList<MasterTagEditDto>();
		for (int i = 1; i < tagSheet.getPhysicalNumberOfRows(); i++) {
			XSSFRow row = tagSheet.getRow(i);
			for (int c = 3; c < row.getPhysicalNumberOfCells(); c++) {
				if (row.getCell(c) != null && row.getCell(c).getStringCellValue().length() != 0) {
					MasterTagEditDto tag = new MasterTagEditDto();
					tag.setUuid(UUIDGenerator.generateType4UUID().toString());
					tag.setEditID(row.getCell(1).getStringCellValue());
					tag.setTag(row.getCell(c).getStringCellValue());
					tagEditList.add(tag);
				} else {
					continue;
				}
			}
		}

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> resultModel = null;

		try {
			// Delete current tag list, no check
			masterDAO.deleteMasterTagByMasterID(masterIDList);

			// Create new version tag list
			resultModel = masterDAO.setMasterTag(tagList);
			if (resultModel.getCode() != 201) {
				workbook.close();
				throw new Exception();
			}

			// Create new version to tag edit list
			resultModel = masterDAO.setMasterTagEdit(tagEditList);
			if (resultModel.getCode() != 201) {
				workbook.close();
				throw new Exception();
			}

			transactionManager.commit(status);
			workbook.close();
			return new ResponseModel<Void>(201, "");
		} catch (Exception e) {
			transactionManager.rollback(status);
			return resultModel;
		}
	}

	/**
	 * Master update. On the sequence, to create a new version of edit record then
	 * update current information by using the latest edit version. In short, this
	 * mechanism let everyone to edit, information correct or not depends on 'speech
	 * market' theory.
	 * 
	 * @param masterID
	 * @param formMaster
	 * @return
	 */
	public ResponseModel<Void> updateMaster(String masterID, MasterUpdateDto formMaster) {

		List<String> tagList = formMaster.getTagList();
		MultipartFile file = formMaster.getFile();

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> resultModel = null;

		MasterUpdateDto originalMaster = masterDAO.getMasterOriginalByMasterID(masterID);
		utility.updateByDto(formMaster, originalMaster);

		// if file is not null then upload and change current image.
		if (file != null && file.getSize() != 0) {
			amazonService.setFolderName(new StringBuilder("Master").append("/").append(masterID).toString());
			try {
				AmazonDto awsDto = amazonService.uploadFile(file);
				originalMaster.setImg(awsDto.getUrl());
				originalMaster.setObjectKey(awsDto.getObjectKey());
			} catch (IOException e) {
				e.printStackTrace();
				return new ResponseModel<>(500, ApiError.AWS_FILE_UPLOAD_FAILED + "Due to: " + e.getMessage());
			}
		}

		try {
			// Set all edit version to not used.
			resultModel = masterDAO.updateMasterEditAllNotInUsed(masterID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}
			// Create a version of master edit
			String editID = UUIDGenerator.generateType4UUID().toString();
			originalMaster.setEditID(editID);
			resultModel = masterDAO.setMasterEdit(originalMaster);
			if (resultModel.getCode() != 201) {
				throw new Exception();
			}

			// Update current master information
			resultModel = masterDAO.updateMaster(originalMaster);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			// Delete current tag list
			resultModel = masterDAO.deleteMasterTagByMasterID(masterID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			if (tagList != null && tagList.size() != 0) {
				// Assemble tag list as DTO list
				List<MasterTag> tagDtoList = new ArrayList<MasterTag>();
				for (String t : tagList) {
					MasterTag tag = new MasterTag();
					tag.setTagID(UUIDGenerator.generateType4UUID().toString());
					tag.setMasterID(masterID);
					tag.setTag(t);
					tagDtoList.add(tag);
				}

				List<MasterTagEditDto> tagEditDtoList = new ArrayList<MasterTagEditDto>();
				for (String t : tagList) {
					MasterTagEditDto tag = new MasterTagEditDto();
					tag.setUuid(UUIDGenerator.generateType4UUID().toString());
					tag.setEditID(editID);
					tag.setTag(t);
					tagEditDtoList.add(tag);
				}

				resultModel = masterDAO.setMasterTag(tagDtoList);
				if (resultModel.getCode() != 201) {
					throw new Exception();
				}

				resultModel = masterDAO.setMasterTagEdit(tagEditDtoList);
				if (resultModel.getCode() != 201) {
					throw new Exception();
				}
			}

			transactionManager.commit(status);
			return new ResponseModel<Void>(204, "");
		} catch (Exception e) {
			e.printStackTrace();
			transactionManager.rollback(status);
			return resultModel;
		}
	}

	/**
	 * Get master edit list
	 * 
	 * @param masterID
	 * @param argsDto
	 * @return
	 */
	public MasterEditSearchListInfoAdminDto getMaserEditListByMasterID(String masterID, MasterEditSearchArgsAdminDto argsDto) {
		argsDto.setMasterID(masterID);
		List<MasterEditSearchInfoAdminDto> list = masterDAO.getMaserEditListByMasterID(argsDto);
		if (list != null && list.size() != 0) {
			MasterEditSearchListInfoAdminDto dto = new MasterEditSearchListInfoAdminDto();
			dto.setMasterEditList(list);
			return dto;
		} else {
			return null;
		}
	}

	/**
	 * Get member edited master list by filters
	 * 
	 * @param argsDto
	 * @return
	 */
	public List<MasterEditSearchInfoMemberDto> getPlaceMemberEditedListWithFilter(MasterSearchArgsMemberEditedDto argsDto) {
		List<MasterEditSearchInfoMemberDto> list = masterDAO.getMasterMemberEditedListWithFilter(argsDto);
		if (list != null) {
			return list;
		} else {
			return null;
		}
	}

	/**
	 * Update master edit version by editID in used.
	 * 
	 * @param editID
	 * @return
	 */
	public ResponseModel<Void> updateMasterEditApplyInUsed(String editID) {

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> resultModel = null;

		MasterEditDto edit = masterDAO.getMasterEditByEditID(editID);
		if (edit == null) {
			return new ResponseModel<>(404, ApiError.DATA_NOT_FOUND);
		}

		try {
			resultModel = masterDAO.updateMaster(edit);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			resultModel = masterDAO.updateMasterEditAllNotInUsed(edit.getMasterID());
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			resultModel = masterDAO.updateMasterEditInUsed(editID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			transactionManager.commit(status);
			return new ResponseModel<Void>(204, "");
		} catch (Exception e) {
			transactionManager.rollback(status);
			return resultModel;
		}

	}

	/**
	 * Get master edit single recored
	 * 
	 * @param editID
	 * @return
	 */
	public MasterEditInfoDto getMasterEditByEditID(String editID) {
		MasterEditDto edit = masterDAO.getMasterEditByEditID(editID);
		if (edit != null) {
			List<MasterTagEditDto> tagEditList = masterDAO.getMasterTagEditListByEditID(editID);
			MasterEditInfoDto dto = new MasterEditInfoDto();
			dto.setMasterEdit(edit);
			dto.setTagEditList(tagEditList);
			return dto;
		} else {
			return null;
		}
	}

	/**
	 * Get master member edit single recored
	 * 
	 * @param editID
	 * @param memberID
	 * @return
	 */
	public MasterEditInfoDto getMasterEditByEditID(String editID, String memberID) {
		MasterEditDto edit = masterDAO.getMasterEditByEditID(editID, memberID);
		if (edit != null) {
			List<MasterTagEditDto> tagEditList = masterDAO.getMasterTagEditListByEditID(editID);
			MasterEditInfoDto dto = new MasterEditInfoDto();
			dto.setMasterEdit(edit);
			dto.setTagEditList(tagEditList);
			return dto;
		} else {
			return null;
		}
	}

	/**
	 * Delete master cascade with other related data by masterID
	 * 
	 * @param masterID
	 * @return
	 */
	public ResponseModel<Void> deleteMasterByMasterID(String masterID) {

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> resultModel = null;

		try {
			List<String> editIDList = masterDAO.getMasterEditIDListByMasterID(masterID);

			if (editIDList != null && editIDList.size() != 0) {
				masterDAO.deleteMasterTagEditByEditIDList(editIDList);
			}

			resultModel = masterDAO.deleteMasterTagByMasterID(masterID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			resultModel = masterDAO.deleteMasterEditByMasterID(masterID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			resultModel = masterDAO.deleteMasterMemberFollowedByMasterID(masterID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			resultModel = masterDAO.deleteEventRecommendByMasterID(masterID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			resultModel = masterDAO.deletePlaceRecommendByMasterID(masterID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			resultModel = masterDAO.deletePlaceRecommendEditByMasterID(masterID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			resultModel = masterDAO.deleteMasterByMasterID(masterID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			transactionManager.commit(status);
			return new ResponseModel<>(204, "");
		} catch (Exception e) {
			transactionManager.rollback(status);
			return resultModel;
		}
	}

	/**
	 * Delete master edit version cascade with other related data by editID
	 * 
	 * @param editID
	 * @return
	 */
	public ResponseModel<Void> deleteMasterEditByEditID(String editID) {
		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> resultModel = null;

		try {
			resultModel = masterDAO.deleteMasterTagEditByEditID(editID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			resultModel = masterDAO.deleteMasterEditByEditID(editID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			transactionManager.commit(status);
			return new ResponseModel<>(204, "");
		} catch (Exception e) {
			transactionManager.rollback(status);
			return resultModel;
		}
	}

	private MasterSearchInfoDto calculator(BigDecimal user_lat, BigDecimal user_lng, MasterSearchInfoDto master) {
		Double distance = utility.calculateDistance(user_lat.doubleValue(), user_lng.doubleValue(), master.getLat().doubleValue(),
				master.getLng().doubleValue());
		master.setDistance(distance);
		return master;
	}

	private MasterHitSearchInfoDto calculator(BigDecimal user_lat, BigDecimal user_lng, MasterHitSearchInfoDto master) {
		Double distance = utility.calculateDistance(user_lat.doubleValue(), user_lng.doubleValue(), master.getLat().doubleValue(),
				master.getLng().doubleValue());
		master.setDistance(distance);
		return master;
	}

}
