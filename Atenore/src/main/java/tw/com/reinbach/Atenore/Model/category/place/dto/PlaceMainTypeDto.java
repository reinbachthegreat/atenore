package tw.com.reinbach.Atenore.Model.category.place.dto;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class PlaceMainTypeDto {

	@ApiModelProperty(value = "大分類編號")
	private Integer mainID;

	@ApiModelProperty(value = "大分類名稱")
	private String name;

//	@ApiModelProperty(value = "大分類下所有場所數量")
//	private Integer totalQty;
//
//	@ApiModelProperty(value = "場所清單列表")
//	private List<PlaceDto> placeList;

	@ApiModelProperty(value = "大分類下的分類列表")
	private List<PlaceTypeDto> placeTypeList;

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

//	public Integer getTotalQty() {
//		return totalQty;
//	}
//
//	public void setTotalQty(Integer totalQty) {
//		this.totalQty = totalQty;
//	}
//
//	public List<PlaceDto> getPlaceList() {
//		return placeList;
//	}
//
//	public void setPlaceList(List<PlaceDto> placeList) {
//		this.placeList = placeList;
//	}

	public List<PlaceTypeDto> getPlaceTypeList() {
		return placeTypeList;
	}

	public void setPlaceTypeList(List<PlaceTypeDto> placeTypeList) {
		this.placeTypeList = placeTypeList;
	}

}
