package tw.com.reinbach.Atenore.Model.validate;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SignInAdminDto {

	@NotEmpty(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@ApiModelProperty(required = true)
	private String account;
	
	@NotEmpty(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@ApiModelProperty(required = true)
	private String pwd;

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
}
