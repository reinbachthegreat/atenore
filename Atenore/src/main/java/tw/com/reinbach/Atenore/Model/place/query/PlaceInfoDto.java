package tw.com.reinbach.Atenore.Model.place.query;

import java.util.List;

import tw.com.reinbach.Atenore.Model.place.component.PlaceDto;
import tw.com.reinbach.Atenore.Model.place.component.PlaceImageDto;
import tw.com.reinbach.Atenore.Model.place.component.PlaceTag;

public class PlaceInfoDto {

	private PlaceDto placeInfo;

	private List<PlaceTag> placeTagList;

	private List<PlaceImageDto> placeImgList;

	public PlaceDto getPlaceInfo() {
		return placeInfo;
	}

	public void setPlaceInfo(PlaceDto placeInfo) {
		this.placeInfo = placeInfo;
	}

	public List<PlaceTag> getPlaceTagList() {
		return placeTagList;
	}

	public void setPlaceTagList(List<PlaceTag> placeTagList) {
		this.placeTagList = placeTagList;
	}

	public List<PlaceImageDto> getPlaceImgList() {
		return placeImgList;
	}

	public void setPlaceImgList(List<PlaceImageDto> placeImgList) {
		this.placeImgList = placeImgList;
	}

}
