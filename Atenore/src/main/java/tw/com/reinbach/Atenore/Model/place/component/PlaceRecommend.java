package tw.com.reinbach.Atenore.Model.place.component;

import org.springframework.stereotype.Component;

@Component
public class PlaceRecommend {

	private String recommendID;

	private String placeID;

	private String masterID;

	private String masterName;

	/*
	 * Extend attribute, for Place Edit Table. By controlling version.
	 * 
	 */
	private String editID;

	public String getRecommendID() {
		return recommendID;
	}

	public void setRecommendID(String recommendID) {
		this.recommendID = recommendID;
	}

	public String getPlaceID() {
		return placeID;
	}

	public void setPlaceID(String placeID) {
		this.placeID = placeID;
	}

	public String getMasterID() {
		return masterID;
	}

	public void setMasterID(String masterID) {
		this.masterID = masterID;
	}

	public String getMasterName() {
		return masterName;
	}

	public void setMasterName(String masterName) {
		this.masterName = masterName;
	}

	public String getEditID() {
		return editID;
	}

	public void setEditID(String editID) {
		this.editID = editID;
	}

}
