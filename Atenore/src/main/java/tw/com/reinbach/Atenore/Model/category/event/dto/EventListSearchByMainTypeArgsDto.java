package tw.com.reinbach.Atenore.Model.category.event.dto;

import java.time.LocalDateTime;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;

public class EventListSearchByMainTypeArgsDto {

	@ApiModelProperty(hidden = true)
	private Integer mainID;
	
	@ApiModelProperty(value = "fuzzy searching by name keywords", required = false, example = "醫")
	private String fuzzyFilter;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(required = false, example = "2020-01-01 10:00:00")
	private LocalDateTime startTime;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(required = false, example = "2020-01-04 10:00:00")
	private LocalDateTime endTime;

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public String getFuzzyFilter() {
		return fuzzyFilter;
	}

	public void setFuzzyFilter(String fuzzyFilter) {
		this.fuzzyFilter = fuzzyFilter;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}
	
	
	
}
