package tw.com.reinbach.Atenore.Model.member.component;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MemberDto {
	
	@ApiModelProperty(example = "75c6538b-facd-461b-b143-919c0913028c")
	private String memberID;
	
	@ApiModelProperty(example = "reinbachrddev@gmail.com")
	private String account;
	
	@ApiModelProperty(value = "Data from social media", example = "reinbachrddev@gmail.com")
	private String email;
	
	@ApiModelProperty(example = "Sora醬")
	private String publicName;
	
	@ApiModelProperty(example = "Lee")
	private String lastName;
	
	@ApiModelProperty(example = "Anita")
	private String firstName;
	
	@ApiModelProperty(value = "URL of member's headshot", example = "https://mosaicstorage.s3-ap-northeast-1.amazonaws.com/photo/MemberHead/bf194ce20d2f94a6226b3cd7677623ea.jpg")
	private String img;
	
	@ApiModelProperty(value = "AWS ObjectKey of member's headshot", example = "photo/MemberHead/bf194ce20d2f94a6226b3cd7677623ea.jpg")
	private String objectKey;
	
	@ApiModelProperty(example = "0229887744")
	private String phone;
	
	@ApiModelProperty(example = "0922558774")
	private String cell;
	
	@ApiModelProperty(example = "新北市")
	private String county;
	
	@ApiModelProperty(value = "According to client's requirement, it ends with road description", example = "板橋區縣民大道55號")
	private String address1;

	@ApiModelProperty(example = "2巷158號6樓")
	private String address2;
	
	@ApiModelProperty(value = "For app push service.", example = "@#$#$^$%^$^$%^")
	private String deviceToken;
	
	@ApiModelProperty(value = "verfication code", example = "FGHGHHHJJUFdddfie")
	private String token;
	
	@ApiModelProperty(name = "lat", value = "GPS, Lat", example = "25.05788", dataType = "BigDecimal")
	private BigDecimal lat;
	
	@ApiModelProperty(name = "lng", value = "GPS, Lng", example = "24.5585", dataType = "BigDecimal")
	private BigDecimal lng;
	
	@ApiModelProperty(value = "Member's account status.", example = "1")
	private Integer status;

	@ApiModelProperty(example = "2020-09-30 09:30:45", dataType = "LocalDateTime")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime lastLogin;
	
	@ApiModelProperty(value = "If this account is deleted", example = "true")
	private Boolean display;
	
	@ApiModelProperty(value = "Member's account established time.", example = "2020-07-10 10:00:00", dataType = "LocalDateTime")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime insertTime;

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPublicName() {
		return publicName;
	}

	public void setPublicName(String publicName) {
		this.publicName = publicName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getObjectKey() {
		return objectKey;
	}

	public void setObjectKey(String objectKey) {
		this.objectKey = objectKey;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCell() {
		return cell;
	}

	public void setCell(String cell) {
		this.cell = cell;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public BigDecimal getLat() {
		return lat;
	}

	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}

	public BigDecimal getLng() {
		return lng;
	}

	public void setLng(BigDecimal lng) {
		this.lng = lng;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public LocalDateTime getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(LocalDateTime lastLogin) {
		this.lastLogin = lastLogin;
	}

	public Boolean getDisplay() {
		return display;
	}

	public void setDisplay(Boolean display) {
		this.display = display;
	}

	public LocalDateTime getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(LocalDateTime insertTime) {
		this.insertTime = insertTime;
	}

}
