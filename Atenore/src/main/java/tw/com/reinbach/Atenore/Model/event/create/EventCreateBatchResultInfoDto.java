package tw.com.reinbach.Atenore.Model.event.create;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class EventCreateBatchResultInfoDto {

	private String batchUpdateUrl;
	
	private String batchUpdateObjectKey;

	public String getBatchUpdateUrl() {
		return batchUpdateUrl;
	}

	public void setBatchUpdateUrl(String batchUpdateUrl) {
		this.batchUpdateUrl = batchUpdateUrl;
	}

	public String getBatchUpdateObjectKey() {
		return batchUpdateObjectKey;
	}

	public void setBatchUpdateObjectKey(String batchUpdateObjectKey) {
		this.batchUpdateObjectKey = batchUpdateObjectKey;
	}
	
	
	
}
