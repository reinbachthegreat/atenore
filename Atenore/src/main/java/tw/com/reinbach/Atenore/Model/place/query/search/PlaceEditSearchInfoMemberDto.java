package tw.com.reinbach.Atenore.Model.place.query.search;

import java.time.LocalDateTime;

public class PlaceEditSearchInfoMemberDto {

	private String placeID;
	
	private String editID;
	
	private String memberID;
	
	private String name;
	
	private String img;

	private Boolean inUsed;

	private LocalDateTime insertTime;

	public String getPlaceID() {
		return placeID;
	}

	public void setPlaceID(String placeID) {
		this.placeID = placeID;
	}

	public String getEditID() {
		return editID;
	}

	public void setEditID(String editID) {
		this.editID = editID;
	}

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public Boolean getInUsed() {
		return inUsed;
	}

	public void setInUsed(Boolean inUsed) {
		this.inUsed = inUsed;
	}

	public LocalDateTime getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(LocalDateTime insertTime) {
		this.insertTime = insertTime;
	}


}
