package tw.com.reinbach.Atenore.Model.category.place;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import tw.com.reinbach.Atenore.Model.category.place.dto.PlaceListSearchByMainTypeArgsDto;
import tw.com.reinbach.Atenore.Model.category.place.dto.PlaceListSearchByTypeArgsDto;
import tw.com.reinbach.Atenore.Model.category.place.dto.PlaceMainTypeDto;
import tw.com.reinbach.Atenore.Model.category.place.dto.PlaceSearchByMainTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.place.dto.PlaceSearchByTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.place.dto.PlaceTypeDto;
import tw.com.reinbach.Atenore.Model.category.place.dto.component.PlaceMainTypeAdminDto;
import tw.com.reinbach.Atenore.Model.category.place.dto.component.PlaceTypeAdminDto;
import tw.com.reinbach.Atenore.Model.place.component.PlaceDto;

@Repository
public class PlaceTypeDAOJdbcTemplate implements PlaceTypeDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private NamedParameterJdbcTemplate namedJdbcTemplate;

	@Override
	public List<PlaceMainTypeDto> getPlaceMainTypeList() {
		try {
			String sql = "select * from placemaintype; ";
			return jdbcTemplate.query(sql, new BeanPropertyRowMapper<PlaceMainTypeDto>(PlaceMainTypeDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<PlaceMainTypeAdminDto> getPlaceMainTypeListWithFilter(String filter) {
		try {
			StringBuilder sql = new StringBuilder("select pmmt.*, ifnull((select count(p.placeID) as totalQty from place p "
					+ " left join placetype pt on pt.typeID = p.typeID left join placemaintype pmt on pt.mainID = pmt.mainID "
					+ " where pmt.mainID = pmmt.mainID group by pmt.mainID order by pmt.mainID asc),0) as 'totalQty' ");

			if (filter != null) {
				sql.append(" from placemaintype pmmt where pmmt.name like ? ; ");
				return jdbcTemplate.query(sql.toString(), new Object[] { "%" + filter + "%" },
						new BeanPropertyRowMapper<PlaceMainTypeAdminDto>(PlaceMainTypeAdminDto.class));
			} else {
				sql.append(" from placemaintype pmmt; ");
				return jdbcTemplate.query(sql.toString(),
						new BeanPropertyRowMapper<PlaceMainTypeAdminDto>(PlaceMainTypeAdminDto.class));
			}
		} catch (DataAccessException e) {
			return null;
		}
	}

//	@Override
//	public List<PlaceDto> getPlaceListByMainType(Integer mainID) {
//		try {
//			String sql = "select p.* from place p left join placetype pt on pt.typeID = p.typeID "
//					+ "left join placemaintype pmt on pt.mainID = pmt.mainID where pmt.mainID = ? order by p.placeID asc ";
//			return jdbcTemplate.query(sql, new Object[] { mainID }, new BeanPropertyRowMapper<PlaceDto>(PlaceDto.class));
//		} catch (DataAccessException e) {
//			return null;
//		}
//	}

	@Override
	public List<PlaceSearchByMainTypeInfoDto> getPlaceListByMainTypeWithFilter(PlaceListSearchByMainTypeArgsDto argsDto) {
		try {
			StringBuilder sql = new StringBuilder().append(
					"select p.placeID, p.name, l.name as mainContactor, l.phone as mainContactPhone, l.isMain, p.memberID, p.insertTime, "
							+ " p.typeID, pt.name as typeName, pmt.mainID, pmt.name as mainTypeName from place p "
							+ " left join placetype pt on pt.typeID = p.typeID "
							+ " left join placemaintype pmt on pt.mainID = pmt.mainID"
							+ " left join liaison l on l.placeID = p.placeID where pmt.mainID = :mainID ");

			if (argsDto.getFuzzyFilter() != null && argsDto.getFuzzyFilter().trim().length() != 0) {
				sql.append(" and p.name like '%' :fuzzyFilter '%' ");
			}

			if (argsDto.getCounty() != null && argsDto.getCounty().trim().length() != 0) {
				sql.append(" and p.county = :county ");
			}

			sql.append(" order by p.placeID asc; ");

			SqlParameterSource ps = new BeanPropertySqlParameterSource(argsDto);
			return namedJdbcTemplate.query(sql.toString(), ps,
					new BeanPropertyRowMapper<PlaceSearchByMainTypeInfoDto>(PlaceSearchByMainTypeInfoDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<PlaceTypeDto> getPlaceTypeList() {
		try {
			String sql = "select * from placetype; ";
//			String sql = "select pt.*, count(p.placeID) as 'totalQty' from place p "
//					+ "left join placetype pt on pt.typeID = p.typeID group by pt.typeID order by pt.typeID asc;";
			return jdbcTemplate.query(sql, new BeanPropertyRowMapper<PlaceTypeDto>(PlaceTypeDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<PlaceTypeAdminDto> getPlaceTypeListByMainTypeWithFilter(Integer mainID, String filter) {
		try {
			StringBuilder sql = new StringBuilder(
					" select pt.typeID, pt.name, pmt.mainID, pmt.name as mainTypeName, ifnull(ptall.totalQty,0) as totalQty "
							+ " from placetype pt left join "
							+ " (select pt.*, count(p.placeID) as totalQty from place p left join placetype pt on pt.typeID = p.typeID "
							+ "	group by pt.typeID) as ptall on ptall.typeID = pt.typeID left join placemaintype as pmt on pt.mainID = pmt.mainID "
							+ " where pt.mainID = ? ");

			if (filter != null) {
				sql.append(" and pt.name like ? order by pt.typeID asc ");
				return jdbcTemplate.query(sql.toString(), new Object[] { mainID, "%" + filter + "%" },
						new BeanPropertyRowMapper<PlaceTypeAdminDto>(PlaceTypeAdminDto.class));
			} else {
				sql.append(" order by pt.typeID asc ");
				return jdbcTemplate.query(sql.toString(), new Object[] { mainID },
						new BeanPropertyRowMapper<PlaceTypeAdminDto>(PlaceTypeAdminDto.class));
			}
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<PlaceDto> getPlaceListByType(Integer typeID) {
		try {
			String sql = "select p.* from place p left join placetype pt on pt.typeID = p.typeID "
					+ " where pt.typeID = ? order by p.placeID asc ";
			return jdbcTemplate.query(sql, new Object[] { typeID }, new BeanPropertyRowMapper<PlaceDto>(PlaceDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<PlaceSearchByTypeInfoDto> getPlaceListByTypeWithFilter(PlaceListSearchByTypeArgsDto argsDto) {
		try {
			StringBuilder sql = new StringBuilder().append(
					"select p.placeID, p.name, l.name as 'mainContactor', l.phone as 'mainContactPhone', l.isMain, p.memberID, p.insertTime, "
							+ " p.typeID, pt.name as 'typeName', pmt.mainID, pmt.name as 'mainTypeName' from place p "
							+ " left join placetype pt on pt.typeID = p.typeID "
							+ " left join placemaintype pmt on pt.mainID = pmt.mainID"
							+ " left join liaison l on l.placeID = p.placeID where p.typeID = :typeID ");
			if (argsDto.getFuzzyFilter() != null && argsDto.getFuzzyFilter().trim().length() != 0) {
				sql.append(" and p.name like '%' :fuzzyFilter '%' ");
			}

			if (argsDto.getCounty() != null && argsDto.getCounty().trim().length() != 0) {
				sql.append(" and p.county=:county ");
			}

			sql.append(" order by p.placeID asc; ");

			SqlParameterSource ps = new BeanPropertySqlParameterSource(argsDto);
			return namedJdbcTemplate.query(sql.toString(), ps,
					new BeanPropertyRowMapper<PlaceSearchByTypeInfoDto>(PlaceSearchByTypeInfoDto.class));

		} catch (DataAccessException e) {
			return null;
		}
	}

}
