package tw.com.reinbach.Atenore.Model.category.place.dto;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class PlaceTypeCascadeDto {

	@ApiModelProperty(value = "大分類編號")
	private Integer mainID;

	@ApiModelProperty(value = "大分類名稱")
	private String name;

	private List<PlaceTypeDto> minorTypeList;

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<PlaceTypeDto> getMinorTypeList() {
		return minorTypeList;
	}

	public void setMinorTypeList(List<PlaceTypeDto> minorTypeList) {
		this.minorTypeList = minorTypeList;
	}

	public void addMinorTypeList(PlaceTypeDto placeTypeDto) {
		this.minorTypeList.add(placeTypeDto);
	}

}
