package tw.com.reinbach.Atenore.Model.master.query.search;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;
import tw.com.reinbach.Atenore.Model.master.component.MasterTag;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MasterHitSearchInfoDto {
	
	@ApiModelProperty(example = "123e4567-e89b-42d3-a456-556642440000", dataType = "String")
	private String masterID;
	
	@ApiModelProperty(example = "喵英大統領", dataType = "String")
	private String publicName;
	
	@ApiModelProperty(example = "https://upload.wikimedia.org/wikipedia/commons/1/1b/%E8%94%A1%E8%8B%B1%E6%96%87%E5%AE%98%E6%96%B9%E5%85%83%E9%A6%96%E8%82%96%E5%83%8F%E7%85%A7.png", dataType = "String")
	private String img;
	
	@ApiModelProperty(example = "25.154558", dataType = "Double")
	private BigDecimal lat;
	
	@ApiModelProperty(example = "121.39805", dataType = "Double")
	private BigDecimal lng;
	
	@ApiModelProperty(notes = "if choose hit by click, shown",example = "122255", dataType = "Integer")
	private Integer click;
	
	@ApiModelProperty(notes=" if choose hit by follow, shown", example = "12105", dataType = "Integer")
	private Integer follow;
	
	@ApiModelProperty(value = "calculate between current user and this event, use 'meter' as unit.", example = "913.13")
	private Double distance;
	
	@ApiModelProperty(hidden = true)
	private String tag1;
	
	@ApiModelProperty(hidden = true)
	private String tag2;
	
	@ApiModelProperty(hidden = true)
	private String tag3;
	
	@ApiModelProperty(hidden = true)
	private String tag4;
	
	@ApiModelProperty(hidden = true)
	private String tag5;
	
	@ApiModelProperty(hidden = true)
	private String tag6;
	
	@ApiModelProperty(hidden = true)
	private String tag7;
	
	@ApiModelProperty(hidden = true)
	private String tag8;
	
	private List<MasterTag> tagList = new ArrayList<MasterTag>();

	public String getMasterID() {
		return masterID;
	}

	public void setMasterID(String masterID) {
		this.masterID = masterID;
	}

	public String getPublicName() {
		return publicName;
	}

	public void setPublicName(String publicName) {
		this.publicName = publicName;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public BigDecimal getLat() {
		return lat;
	}

	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}

	public BigDecimal getLng() {
		return lng;
	}

	public void setLng(BigDecimal lng) {
		this.lng = lng;
	}
	
	public Integer getClick() {
		return click;
	}

	public void setClick(Integer click) {
		this.click = click;
	}

	public Integer getFollow() {
		return follow;
	}

	public void setFollow(Integer follow) {
		this.follow = follow;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	public List<MasterTag> getTagList() {

		String [] tagArry = {tag1, tag2, tag3, tag4, tag5, tag6, tag7, tag8	};
		
		for(String t : tagArry) {
			if(t==null){
				continue;
			}
			MasterTag tag = new MasterTag();
			tag.setTag(t);
			this.tagList.add(tag);			
		}
		return this.tagList;
	}

	public void setTagList(List<MasterTag> tagList) {
		this.tagList = tagList;
	}
	
	public void addTagList(MasterTag tag) {
		this.tagList.add(tag);
	}

	public void setTag1(String tag1) {
		this.tag1 = tag1;
	}

	public void setTag2(String tag2) {
		this.tag2 = tag2;
	}

	public void setTag3(String tag3) {
		this.tag3 = tag3;
	}

	public void setTag4(String tag4) {
		this.tag4 = tag4;
	}

	public void setTag5(String tag5) {
		this.tag5 = tag5;
	}

	public void setTag6(String tag6) {
		this.tag6 = tag6;
	}

	public void setTag7(String tag7) {
		this.tag7 = tag7;
	}

	public void setTag8(String tag8) {
		this.tag8 = tag8;
	}	
	
	

}
