package tw.com.reinbach.Atenore.Model.event.query;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import tw.com.reinbach.Atenore.Model.event.component.PinEventTypeInfoDto;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PinEventTypeListInfoDto {

	private String memberID;
	
	private List<PinEventTypeInfoDto> pinEventTypeList;

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public List<PinEventTypeInfoDto> getPinEventTypeList() {
		return pinEventTypeList;
	}

	public void setPinEventTypeList(List<PinEventTypeInfoDto> pinEventTypeList) {
		this.pinEventTypeList = pinEventTypeList;
	}
	
}
