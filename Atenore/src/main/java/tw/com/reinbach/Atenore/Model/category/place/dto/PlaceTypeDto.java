package tw.com.reinbach.Atenore.Model.category.place.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;
import tw.com.reinbach.Atenore.Model.place.component.PlaceDto;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PlaceTypeDto {

	@ApiModelProperty(value = "小分類編號")
	private Integer typeID;

	@ApiModelProperty(value = "大分類編號")
	private Integer mainID;

	@ApiModelProperty(value = "小分類名稱")
	private String name;

	private List<CapacityTypeDto> capacityList;

//	@ApiModelProperty(value = "分類下所有場所數量")
//	private Integer totalQty;
//
//	@ApiModelProperty(value = "場所清單列表")
//	private List<PlaceDto> placeList;

	public Integer getTypeID() {
		return typeID;
	}

	public void setTypeID(Integer typeID) {
		this.typeID = typeID;
	}

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<CapacityTypeDto> getCapacityList() {
		return capacityList;
	}

	public void setCapacityList(List<CapacityTypeDto> capacityList) {
		this.capacityList = capacityList;
	}

//	public Integer getTotalQty() {
//		return totalQty;
//	}
//
//	public void setTotalQty(Integer totalQty) {
//		this.totalQty = totalQty;
//	}
//
//	public List<PlaceDto> getPlaceList() {
//		return placeList;
//	}
//
//	public void setPlaceList(List<PlaceDto> placeList) {
//		this.placeList = placeList;
//	}

}
