package tw.com.reinbach.Atenore.Utility;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class IDGenerator {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public String getIDGenerator(String tablePkName, String tableName, int fromEndIndex) {
		String newID = "";

		String sql = "select substring(num." + tablePkName + "," + fromEndIndex + ") " + tablePkName + " from "
				+ tableName + " as num order by " + tablePkName + " desc LIMIT 1";
		List<Map<String, Object>> lastCurrentID = jdbcTemplate.queryForList(sql);
		System.out.println("lastCurrentID=" + lastCurrentID);
		if (lastCurrentID.size() != 0) {
			String strlastID = (String) lastCurrentID.get(0).get(tablePkName);
			System.out.println("strlastID=" + strlastID);
			int intlastID = Integer.parseInt(strlastID) + 1;
			System.out.println("intlastID=" + intlastID);
			newID = "000000000" + Integer.toString(intlastID);
		} else {
			newID = "000000001";
		}
		return newID;
	}

}
