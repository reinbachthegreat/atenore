package tw.com.reinbach.Atenore.Model.member.update;

import javax.validation.constraints.NotEmpty;

import io.swagger.annotations.ApiModelProperty;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;

public class MemberUpdateDto {

	@ApiModelProperty(hidden = true, example = "75c6538b-facd-461b-b143-919c0913028c")
	private String memberID;
	
	@NotEmpty(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@ApiModelProperty(required = true, example = "Sora醬")
	private String publicName;
	
	@NotEmpty(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@ApiModelProperty(required = true, example = "Lee")
	private String lastName;
	
	@NotEmpty(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@ApiModelProperty(required = true, example = "Anita")
	private String firstName;
	
	@ApiModelProperty(value = "URL of member's headshot", example = "https://mosaicstorage.s3-ap-northeast-1.amazonaws.com/photo/MemberHead/bf194ce20d2f94a6226b3cd7677623ea.jpg")
	private String img;
	
	@ApiModelProperty(value = "AWS ObjectKey of member's headshot", example = "photo/MemberHead/bf194ce20d2f94a6226b3cd7677623ea.jpg")
	private String objectKey;
	
	@ApiModelProperty(example = "0229887744")
	private String phone;
	
	@NotEmpty(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@ApiModelProperty(required = true, example = "0922558774")
	private String cell;
	
	@ApiModelProperty(example = "新北市")
	private String county;
	
	@ApiModelProperty(value = "According to client's requirement, it ends with road description", example = "板橋區縣民大道55號")
	private String address1;

	@ApiModelProperty(example = "2巷158號6樓")
	private String address2;

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public String getPublicName() {
		return publicName;
	}

	public void setPublicName(String publicName) {
		this.publicName = publicName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getObjectKey() {
		return objectKey;
	}

	public void setObjectKey(String objectKey) {
		this.objectKey = objectKey;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCell() {
		return cell;
	}

	public void setCell(String cell) {
		this.cell = cell;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	
}
