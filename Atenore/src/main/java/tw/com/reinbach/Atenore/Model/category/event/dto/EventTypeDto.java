package tw.com.reinbach.Atenore.Model.category.event.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class EventTypeDto {

	@ApiModelProperty(example = "1")
	private Integer typeID;

	@ApiModelProperty(hidden = true)
	private Integer mainID;

	@ApiModelProperty(example = "醫療院所")
	private String name;

	public Integer getTypeID() {
		return typeID;
	}

	public void setTypeID(Integer typeID) {
		this.typeID = typeID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

}
