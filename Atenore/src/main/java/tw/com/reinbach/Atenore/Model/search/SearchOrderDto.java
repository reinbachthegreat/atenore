package tw.com.reinbach.Atenore.Model.search;

import io.swagger.annotations.ApiModelProperty;

public class SearchOrderDto {

	@ApiModelProperty(example = "1")
	private Integer searchOrderSN;

	@ApiModelProperty(example = "以點閱率高到低")
	private String searchOrderName;

	public Integer getSearchOrderSN() {
		return searchOrderSN;
	}

	public void setSearchOrderSN(Integer searchOrderSN) {
		this.searchOrderSN = searchOrderSN;
	}

	public String getSearchOrderName() {
		return searchOrderName;
	}

	public void setSearchOrderName(String searchOrderName) {
		this.searchOrderName = searchOrderName;
	}

}
