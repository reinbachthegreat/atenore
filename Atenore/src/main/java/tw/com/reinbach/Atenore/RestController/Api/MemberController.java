package tw.com.reinbach.Atenore.RestController.Api;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import tw.com.reinbach.Atenore.Model.event.EventService;
import tw.com.reinbach.Atenore.Model.event.create.EventCreateDto;
import tw.com.reinbach.Atenore.Model.event.create.EventImageItemCreateDto;
import tw.com.reinbach.Atenore.Model.event.query.EventInfoAllDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchAppliedInfoDto;
import tw.com.reinbach.Atenore.Model.event.update.EventUpdateDto;
import tw.com.reinbach.Atenore.Model.master.MasterService;
import tw.com.reinbach.Atenore.Model.master.query.MasterEditInfoDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterEditSearchInfoMemberDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterSearchArgsMemberEditedDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterSearchSimpleInfoDto;
import tw.com.reinbach.Atenore.Model.master.update.MasterUpdateDto;
import tw.com.reinbach.Atenore.Model.member.MemberService;
import tw.com.reinbach.Atenore.Model.member.create.ContactCreateDto;
import tw.com.reinbach.Atenore.Model.member.create.HotspotCreateDto;
import tw.com.reinbach.Atenore.Model.member.query.HotspotInfoDto;
import tw.com.reinbach.Atenore.Model.member.query.HotspotListInfoDto;
import tw.com.reinbach.Atenore.Model.member.query.MemberInfoDto;
import tw.com.reinbach.Atenore.Model.member.query.search.EventFollowedSearchArgsDto;
import tw.com.reinbach.Atenore.Model.member.query.search.EventFollwedSearchInfoDto;
import tw.com.reinbach.Atenore.Model.member.query.search.MasterFollowedSearchArgsDto;
import tw.com.reinbach.Atenore.Model.member.query.search.MasterFollowedSearchInfoDto;
import tw.com.reinbach.Atenore.Model.member.query.search.PlaceFollowedSearchArgsDto;
import tw.com.reinbach.Atenore.Model.member.query.search.PlaceFollowedSearchInfoDto;
import tw.com.reinbach.Atenore.Model.member.update.HotspotUpdateDto;
import tw.com.reinbach.Atenore.Model.member.update.MemberUpdateDto;
import tw.com.reinbach.Atenore.Model.member.update.PasswordUpdateDto;
import tw.com.reinbach.Atenore.Model.place.PlaceService;
import tw.com.reinbach.Atenore.Model.place.query.PlaceInfoDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceEditSearchInfoMemberDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceSearchArgsMemberEditedDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceSearchSimpleInfoDto;
import tw.com.reinbach.Atenore.Model.place.update.PlaceUpdateDto;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;

@Validated
@RestController
public class MemberController {

	@Autowired
	private EventService eventService;

	@Autowired
	private MasterService masterService;

	@Autowired
	private MemberService memberService;

	@Autowired
	private PlaceService placeService;

	@InitBinder
	public void initBinder(WebDataBinder webDataBinder) {
		webDataBinder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}

	/**
	 * Get Taiwan's county list
	 * 
	 * @return
	 */
	@ApiImplicitParam(name = "Authorization", required = false, paramType = "header", dataType = "String", access = "hidden")
	@ApiOperation(notes = "ApiNo:25", value = "Get Taiwan's county list.")
	@RequestMapping(value = { "/web/county", "/app/county" }, method = RequestMethod.GET)
	public ResponseModel<List<String>> getCountyList() {
		return new ResponseModel<List<String>>(200, memberService.getTaiwanCounty());
	}

	/**
	 * Changing password by memberID
	 * 
	 * @param memberID
	 * @param dto      PasswordUpdateDto
	 * @return
	 * @throws Throwable
	 */
	@ApiOperation(notes = "ApiNo:33", value = "Changing password by memberID.")
	@RequestMapping(value = { "/web/member/password/{memberID}", "/app/member/password/{memberID}" }, method = { RequestMethod.PUT })
	public ResponseModel<Void> updatePassword(@PathVariable(required = true) String memberID, PasswordUpdateDto dto) throws Throwable {

		if (dto.getUpdatePaswword().equalsIgnoreCase(dto.getOriginPassword())) {
			return new ResponseModel<Void>(406, "New password can not equals old one.");
		}
		return memberService.updatePassword(memberID, dto);
	}

	/**
	 * Get hotspot list by memberID
	 * 
	 * @param memberID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:26", value = "Get hotspot list by memberID")
	@RequestMapping(value = { "/web/hotspot/{memberID}", "/app/hotspot/{memberID}" }, method = RequestMethod.GET)
	public ResponseModel<HotspotListInfoDto> getHotspotListByMemberID(@PathVariable(required = true) String memberID) {

		HotspotListInfoDto dto = memberService.getHotspotListByMemberID(memberID);

		if (dto.getHotspotList() == null) {
			return new ResponseModel<HotspotListInfoDto>(404, ApiError.DATA_NOT_FOUND);
		}

		return new ResponseModel<HotspotListInfoDto>(200, dto);
	}

	/**
	 * Get hotspot info by hotspotID
	 * 
	 * @param hotspotID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:26-1", value = "Get hotspot info by hotspotID")
	@RequestMapping(value = { "/web/hotspot/info/{hotspotID}", "/app/hotspot/info/{hotspotID}" }, method = RequestMethod.GET)
	public ResponseModel<HotspotInfoDto> getHotspotByHotspotID(@PathVariable(required = true) String hotspotID) {
		HotspotInfoDto dto = memberService.getHotspotByHotspotID(hotspotID);
		if (dto == null) {
			return new ResponseModel<HotspotInfoDto>(404, ApiError.DATA_NOT_FOUND);
		}

		return new ResponseModel<HotspotInfoDto>(200, dto);
	}

	/**
	 * Update hotspot info by hotspotID
	 * 
	 * @param hotspotID
	 * @param dto
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:27", value = "Update hotspot info by hotspotID")
	@RequestMapping(value = { "/web/hotspot/info/{hotspotID}", "/app/hotspot/info/{hotspotID}" }, method = RequestMethod.PUT)
	public ResponseModel<Void> updateHotspotByHotspotID(@PathVariable(required = true) String hotspotID, HotspotUpdateDto dto) {
		return memberService.updateHotspotByHotspotID(dto, hotspotID);
	}

	@ApiOperation(notes = "ApiNo:28", value = "Delete hotspot info by hotspotID")
	@RequestMapping(value = { "/web/hotspot/info/{hotspotID}", "/app/hotspot/info/{hotspotID}" }, method = RequestMethod.DELETE)
	public ResponseModel<Void> deleteHotspotByHotspotID(@PathVariable(required = true) String hotspotID) {
		return memberService.deleteHotspotByHotspotID(hotspotID);
	}

	/**
	 * Create hotspot by memberID
	 * 
	 * @param dto
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:29", value = "Create hotspot by memberID")
	@RequestMapping(value = { "/web/hotspot", "/app/hotspot" }, method = RequestMethod.POST)
	public ResponseModel<Void> setHotspot(@Valid HotspotCreateDto dto) {
		return memberService.setHotspot(dto);
	}

	/**
	 * Get event list followed by memberID
	 * 
	 * @param memberID
	 * @param searchArgs
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:34", value = "Get event list followed by memberID")
	@RequestMapping(value = { "/web/member/event/{memberID}", "/app/member/event/{memberID}" }, method = RequestMethod.GET)
	public ResponseModel<List<EventFollwedSearchInfoDto>> getEventListFollwedByMemberIDWithFilter(
			@PathVariable(required = true) String memberID, EventFollowedSearchArgsDto searchArgs) {
		searchArgs.setApply(eventService.getApplyStatusList().get(1));
		List<EventFollwedSearchInfoDto> list = memberService.getEventListFollwedByMemberIDWithFilter(searchArgs, memberID);
		if (list == null || list.size() == 0) {
			return new ResponseModel<List<EventFollwedSearchInfoDto>>(404, ApiError.DATA_NOT_FOUND);
		}
		return new ResponseModel<List<EventFollwedSearchInfoDto>>(200, list);
	}

	/**
	 * Get master list followed by memberID
	 * 
	 * @param memberID
	 * @param searchArgs
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:35", value = "Get master list followed by memberID")
	@RequestMapping(value = { "/web/member/master/{memberID}", "/app/member/master/{memberID}" }, method = RequestMethod.GET)
	public ResponseModel<List<MasterFollowedSearchInfoDto>> getMasterListFollowedByMemberIDWithFilter(
			@PathVariable(required = true) String memberID, MasterFollowedSearchArgsDto searchArgs) {
		List<MasterFollowedSearchInfoDto> list = memberService.getMasterListFollowedByMemberIDWithFilter(searchArgs, memberID);
		if (list == null || list.size() == 0) {
			return new ResponseModel<List<MasterFollowedSearchInfoDto>>(404, ApiError.DATA_NOT_FOUND);
		}
		return new ResponseModel<List<MasterFollowedSearchInfoDto>>(200, list);
	}

	/**
	 * Get place list followed by memberID
	 * 
	 * @param memberID
	 * @param searchArgs
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:36", value = "Get place list followed by memberID")
	@GetMapping(value = { "/web/member/place/{memberID}", "/app/member/place/{memberID}" })
	public ResponseModel<List<PlaceFollowedSearchInfoDto>> getPlaceListFollwedByMemberIDWithFilter(
			@PathVariable(required = true) String memberID, PlaceFollowedSearchArgsDto searchArgs) {
		List<PlaceFollowedSearchInfoDto> list = memberService.getPlaceListFollowedByMemberIDWithFilter(searchArgs, memberID);
		if (list == null || list.size() == 0) {
			return new ResponseModel<List<PlaceFollowedSearchInfoDto>>(404, ApiError.DATA_NOT_FOUND);
		}
		return new ResponseModel<List<PlaceFollowedSearchInfoDto>>(200, list);
	}

	/**
	 * Get simple data of member
	 * 
	 * @param memberID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:W3", value = "Get simple data of member")
	@GetMapping(value = { "/web/member/simple/{memberID}", "/app/member/simple/{memberID}" })
	public ResponseModel<MemberInfoDto> getMemberSimpleByMemberID(@PathVariable(required = true) String memberID) {
		MemberInfoDto dto = memberService.getMemberSimpleByMemberID(memberID);
		if (dto == null) {
			return new ResponseModel<MemberInfoDto>(404, ApiError.ACCONUT_NOT_FOUND);
		}
		return new ResponseModel<MemberInfoDto>(200, dto);
	}

	/**
	 * Get detail data of member
	 * 
	 * @param memberID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:37", value = "Get detail data of member")
	@RequestMapping(value = { "/web/member/detail/{memberID}", "/app/member/detail/{memberID}" }, method = RequestMethod.GET)
	public ResponseModel<MemberInfoDto> getMemberDetailByMemberID(@PathVariable(required = true) String memberID) {
		MemberInfoDto dto = memberService.getMemberDetailByMemberID(memberID);
		if (dto == null) {
			return new ResponseModel<MemberInfoDto>(404, ApiError.ACCONUT_NOT_FOUND);
		}
		return new ResponseModel<MemberInfoDto>(200, dto);
	}

	/**
	 * Update detail data of member
	 * 
	 * @param memberID
	 * @param dto
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:38", value = "Update detail data of member")
	@RequestMapping(value = { "/web/member/detail/{memberID}", "/app/member/detail/{memberID}" }, method = RequestMethod.PUT)
	public ResponseModel<Void> updateMemberDetailByMemberID(@PathVariable(required = true) String memberID, @Valid MemberUpdateDto dto,
			MultipartFile file) {
		if (file != null && !file.isEmpty()) {
			return memberService.updateMemberDataByMemberID(dto, memberID, file);
		} else {
			return memberService.updateMemberDataByMemberID(dto, memberID);
		}
	}

//	/** 取得單一會員資料
//	 * @param memberID
//	 * @return
//	 */
//	@ApiOperation(value = "RestAPI：單一會員資料", notes = "**單一會員資料**<br/>" + "說明：輸出所有會員資料")
//	@ApiResponses(value = { @ApiResponse(code = 200, message = "success") })
//	@RequestMapping(value = "/info/{memberID}", method = RequestMethod.GET)
//	public ResponseModel<MemberInfo> getMemberInfo(
//			@ApiParam(value = "**會員編號**", required = true) @PathVariable(value = "memberID", required = true) String memberID) {
//
//		if(memberID==null || memberID.isEmpty() || memberID.trim().length()==0) {
//			return new ResponseModel<MemberInfo>(401, ApiError.PARARMETER_NOT_FOUND);
//		}
//		
//		MemberInfo member = memberService.getMemberByMemberID(memberID, false);
//		
//		if(member==null) {
//			
//			return new ResponseModel<MemberInfo>(404, ApiError.DATA_NOT_FOUND);
//		}else {
//			ArrayNode dataArry = mapper.createArrayNode();
//			dataArry.add(mapper.createObjectNode().set("memberInfo",mapper.valueToTree(member)));			
//			 return new ResponseModel<MemberInfo>(200, member);
//		}
//	}

	@ApiOperation(notes = "ApiNo:W4", value = "Create contact")
	@PostMapping(value = "/web/contact")
	public ResponseModel<Void> setContact(@Valid ContactCreateDto dto) {
		return memberService.setContact(dto);
	}

	/**
	 * Get list applied for event by memberID
	 * 
	 * @param memberID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:W5", value = "Get list applied for event by memberID")
	@GetMapping(value = "/web/member/event/apply/{memberID}")
	public ResponseModel<List<EventSearchAppliedInfoDto>> getAppliedEventListByMemberID(
			@PathVariable(required = true) String memberID) {
		List<EventSearchAppliedInfoDto> dto = eventService.getAppliedEventListByMemberID(memberID);
		if (dto == null) {
			return new ResponseModel<List<EventSearchAppliedInfoDto>>(404, ApiError.DATA_NOT_FOUND);
		}
		return new ResponseModel<List<EventSearchAppliedInfoDto>>(200, dto);
	}

	/**
	 * Cancel applied event by memberID
	 * 
	 * @param memberID
	 * @param eventID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:W6", value = "Cancel applied event by memberID")
	@PutMapping(value = "/web/member/event/cancel/{eventID}")
	public ResponseModel<Void> setEventCancelByMemberID(@PathVariable(required = true) String eventID, @RequestParam String memberID) {
		EventSearchAppliedInfoDto dto = eventService.getEventApplyStatus(eventID, memberID);
		if (dto != null && (dto.getApply().equals(eventService.getApplyStatusList().get(0))
				|| dto.getApply().equals(eventService.getApplyStatusList().get(1)))) {
			return eventService.setEventCancelByMemberID(eventID, memberID);
		} else {
			return new ResponseModel<Void>(409, ApiError.EVENT_APPLY_STATUS_FAILED);
		}
	}

	/**
	 * Event creation by member
	 * 
	 * @param event
	 * @param memberID
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws UnsupportedEncodingException
	 */
	@ApiOperation(notes = "ApiNo:W7", value = "Event creation by member")
	@PostMapping(value = "/web/member/event")
	public ResponseModel<Void> setEventByAdmin(EventCreateDto event, @RequestParam String memberID)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		event.setMemberID(memberID);
		event.setApply(eventService.getApplyStatusList().get(0));
		return eventService.setEvent(event);
	}

	/**
	 * Update event without images by eventID and memberID
	 * 
	 * @param eventID
	 * @param event
	 * @param memberID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:W8", value = "Update event without images by eventID and memberID")
	@PutMapping(value = "/web/member/event/info/{eventID}")
	public ResponseModel<Void> setEventUpdate(@PathVariable(required = true) String eventID, EventUpdateDto event,
			@RequestParam String memberID) {
		event.setMemberID(memberID);
		return eventService.updateEvent(eventID, event);
	}

	/**
	 * Set event image as main photo by imageID
	 * 
	 * @param imageID
	 * @param memberID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:W8-1", value = "Set event image as main photo by imageID")
	@PutMapping(value = "/web/member/event/image/setMain/{imageID}")
	public ResponseModel<Void> setEventImageAsMainByImageID(@PathVariable(value = "imageID", required = true) String imageID,
			@RequestParam String memberID) {
		return eventService.setEventImageAsMainByImageID(imageID);
	}

	/**
	 * Upload single event image by eventID
	 * 
	 * @param eventID
	 * @param image
	 * @param memberID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:W8-2", value = "Upload single event image by eventID")
	@PostMapping(value = "/web/member/event/image/item/{eventID}")
	public ResponseModel<Void> setEventImageByEventID(@PathVariable(value = "eventID", required = true) String eventID,
			EventImageItemCreateDto image, @RequestParam String memberID) {
		System.out.println("?????????");
		return eventService.setEventImageByEventID(eventID, image);
	}

	/**
	 * Delete event image by imageID
	 * 
	 * @param imageID
	 * @param memberID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:W8-3", value = "Delete event image by imageID")
	@DeleteMapping(value = "/web/member/event/image/item/{imageID}")
	public ResponseModel<Void> deleteEventImageByImageID(@PathVariable(required = true) String imageID,
			@RequestParam String memberID) {
		return eventService.deleteEventImgByImageID(imageID);
	}

	/**
	 * Get event fully information by eventID and memberID
	 * 
	 * @param eventID
	 * @param memberID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:W9", value = "Get event fully information by eventID and memberID")
	@GetMapping(value = "/web/member/event/info/{eventID}")
	public ResponseModel<EventInfoAllDto> getEventFullyByEventID(@PathVariable(required = true) String eventID,
			@RequestParam String memberID) {
		EventInfoAllDto dto = eventService.getEventFullyByEventID(eventID);
		if (dto == null) {
			return new ResponseModel<EventInfoAllDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<EventInfoAllDto>(200, dto);
		}
	}

	/**
	 * Get member edited place list by filters
	 * 
	 * @param argsDto
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:W10", value = "Get place edit list by filters")
	@GetMapping(value = "/web/member/edited/place")
	public ResponseModel<List<PlaceEditSearchInfoMemberDto>> getPlaceMemberEditedListWithFilter(PlaceSearchArgsMemberEditedDto argsDto,
			@RequestParam String memberID) {
		List<PlaceEditSearchInfoMemberDto> dto = placeService.getPlaceMemberEditedListWithFilter(argsDto);
		if (dto == null) {
			return new ResponseModel<List<PlaceEditSearchInfoMemberDto>>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<List<PlaceEditSearchInfoMemberDto>>(200, dto);
		}
	}

	/**
	 * Get place edit single recored by memberID
	 * 
	 * @param editID
	 * @param memberID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:W11", value = "Get place edit single recored")
	@GetMapping(value = "/web/member/edited/place/{editID}")
	public ResponseModel<PlaceInfoDto> getPlaceEditByEditID(@PathVariable(required = true) String editID,
			@RequestParam String memberID) {
		PlaceInfoDto dto = placeService.getPlaceMemberEditByEditID(editID, memberID);
		if (dto == null) {
			return new ResponseModel<PlaceInfoDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<PlaceInfoDto>(200, dto);
		}
	}

	/**
	 * Update place by placeID
	 * 
	 * @param placeID
	 * @param place
	 * @param tagIDList
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:W12", value = "Update place placeID")
	@PutMapping(value = "/web/member/edited/place/{editID}")
	public ResponseModel<Void> setPlaceEditUpdate(@PathVariable(required = true) String editID, PlaceUpdateDto place,
			@RequestParam(required = false) List<String> imageIDList, @RequestParam String placeID, @RequestParam String memberID)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		place.setMemberID(memberID);
		return placeService.updatePlace(placeID, place, imageIDList);
	}

	/**
	 * Get member edited place list by filters
	 * 
	 * @param argsDto
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:W13", value = "Get master edit list by filters")
	@GetMapping(value = "/web/member/edited/master")
	public ResponseModel<List<MasterEditSearchInfoMemberDto>> getMasterMemberEditedListWithFilter(
			MasterSearchArgsMemberEditedDto argsDto, @RequestParam String memberID) {
		List<MasterEditSearchInfoMemberDto> dto = masterService.getPlaceMemberEditedListWithFilter(argsDto);
		if (dto == null) {
			return new ResponseModel<List<MasterEditSearchInfoMemberDto>>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<List<MasterEditSearchInfoMemberDto>>(200, dto);
		}
	}

	/**
	 * Get place edit single recored by memberID
	 * 
	 * @param editID
	 * @param memberID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:W14", value = "Get master edit single recored")
	@GetMapping(value = "/web/member/edited/master/{editID}")
	public ResponseModel<MasterEditInfoDto> getMasterEditByEditID(@PathVariable(required = true) String editID,
			@RequestParam String memberID) {
		MasterEditInfoDto dto = masterService.getMasterEditByEditID(editID, memberID);
		if (dto == null) {
			return new ResponseModel<MasterEditInfoDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<MasterEditInfoDto>(200, dto);
		}
	}

	/**
	 * Update master by masterID
	 * 
	 * @param placeID
	 * @param place
	 * @param tagIDList
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:W15", value = "Update master masterID")
	@PutMapping(value = "/web/member/edited/master/{editID}")
	public ResponseModel<Void> setMasterEditUpdate(@PathVariable(required = true) String editID, MasterUpdateDto master,
			@RequestParam String masterID, @RequestParam String memberID)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		master.setMemberID(memberID);
		return masterService.updateMaster(masterID, master);
	}

	/**
	 * Get master simple information list by master publicName
	 * 
	 * @param name
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:W16", value = "Get master simple information list by master publicName")
	@GetMapping(value = "/web/member/search/simple/master")
	public ResponseModel<List<MasterSearchSimpleInfoDto>> getMasterSimpleByPublicName(
			@NotBlank(message = "name must not be blank") @RequestParam String name,
			@ApiParam(value = "歿[0]/存[1]/全選[不送]") @RequestParam(required = false) Integer alive) {
		List<MasterSearchSimpleInfoDto> dto = masterService.getMasterSimpleByPublicName(name, alive);
		if (dto == null) {
			return new ResponseModel<List<MasterSearchSimpleInfoDto>>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<List<MasterSearchSimpleInfoDto>>(200, dto);
		}
	}

	/**
	 * Get place simple information list by place name
	 * 
	 * @param name
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:W17", value = "Get place simple information list by place name")
	@GetMapping(value = "/web/member/search/simple/place")
	public ResponseModel<List<PlaceSearchSimpleInfoDto>> getPlaceSimpleByName(
			@NotBlank(message = "name must not be blank") @RequestParam String name) {
		List<PlaceSearchSimpleInfoDto> dto = placeService.getPlaceSimpleByName(name);
		if (dto == null) {
			return new ResponseModel<List<PlaceSearchSimpleInfoDto>>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<List<PlaceSearchSimpleInfoDto>>(200, dto);
		}
	}

	@RequestMapping(value = { "/web/member/test", "/app/member/test" }, method = { RequestMethod.GET })
	public String testOAuth2WithApi() {
		return "See, it works!";
	}

}
