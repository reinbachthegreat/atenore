package tw.com.reinbach.Atenore.Model.place.query.search;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModelProperty;

public class PlaceSearchInfoDto {

	@ApiModelProperty(value = "場所編號")
	private String placeID;

	@ApiModelProperty(value = "場所大分類編號 [場地編號][29]")
	private Integer mainID;

//	private PlaceImageDto coverImage = new PlaceImageDto();

	private String img;

//	分類屬性
	@ApiModelProperty(value = "場所名稱")
	private String name;

	@ApiModelProperty(value = "場所最大容留人數")
	private Integer max;

	@ApiModelProperty(value = "場所最大容留人數名稱")
	private String maxName;

	@ApiModelProperty(value = "場所面積", example = "50.5坪")
	private String area;

	@ApiModelProperty(value = "場所縣市", example = "新北市")
	private String county;

	@ApiModelProperty(value = "場所地址(排除縣市)", example = "板橋區縣民大道55號2巷158號6樓")
	private String address;

	@ApiModelProperty(value = "聯絡電話")
	private String phone;

//	private PlaceLiaisonDto liaison = new PlaceLiaisonDto();
//
//	@ApiModelProperty(value = "場所相關單位")
//	private String unit;
//
//	@ApiModelProperty(value = "場所email", example = "aaa@gmail.com")
//	private String mail;
//
//	@ApiModelProperty(value = "場所FB", example = "https://www.facebook.com/tsaiingwen/")
//	private String facebook;
//
//	@ApiModelProperty(value = "場所服務項目", example = "會議討論、設備租借")
//	private String service;
//
//	@ApiModelProperty(value = "場所網站URL")
//	private String website;

	@ApiModelProperty(value = "場所簡介")
	private String intro;

	@ApiModelProperty(value = "場所累計點閱數")
	private Integer click;

	@ApiModelProperty(value = "場所累計追蹤數")
	private Integer follow;

	@ApiModelProperty(value = "GPS, Lat", example = "25.05788")
	private BigDecimal lat;

	@ApiModelProperty(value = "GPS, Lng", example = "24.55855")
	private BigDecimal lng;

//	@ApiModelProperty(value = "資訊創立時間", example = "2020-01-10 10:00:00")
//	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//	private LocalDateTime insertTime;
//
//	@ApiModelProperty(value = "場所資訊是否開放編輯", example = "true/false")
//	private String editable;
//
//	@ApiModelProperty(value = "場所發布者編號(現行版本或各歷史編輯版本)")
//	private String memberID;

	@ApiModelProperty(value = "calculate between current user and this place, use 'meter' as unit.", example = "913.13")
	private Double distance;

	public String getPlaceID() {
		return placeID;
	}

	public void setPlaceID(String placeID) {
		this.placeID = placeID;
	}

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

//	public PlaceImageDto getCoverImage() {
//		return coverImage;
//	}
//
//	public void setCoverImage(PlaceImageDto coverImage) {
//		this.coverImage = coverImage;
//	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getMax() {
		return max;
	}

	public void setMax(Integer max) {
		this.max = max;
	}

	public String getMaxName() {
		return maxName;
	}

	public void setMaxName(String maxName) {
		this.maxName = maxName;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

//	public PlaceLiaisonDto getLiaison() {
//		return liaison;
//	}
//
//	public void setLiaison(PlaceLiaisonDto liaison) {
//		this.liaison = liaison;
//	}
//
//	public String getUnit() {
//		return unit;
//	}
//
//	public void setUnit(String unit) {
//		this.unit = unit;
//	}
//
//	public String getMail() {
//		return mail;
//	}
//
//	public void setMail(String mail) {
//		this.mail = mail;
//	}
//
//	public String getFacebook() {
//		return facebook;
//	}
//
//	public void setFacebook(String facebook) {
//		this.facebook = facebook;
//	}
//
//	public String getService() {
//		return service;
//	}
//
//	public void setService(String service) {
//		this.service = service;
//	}
//
//	public String getWebsite() {
//		return website;
//	}
//
//	public void setWebsite(String website) {
//		this.website = website;
//	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public Integer getClick() {
		return click;
	}

	public void setClick(Integer click) {
		this.click = click;
	}

	public Integer getFollow() {
		return follow;
	}

	public void setFollow(Integer follow) {
		this.follow = follow;
	}

	public BigDecimal getLat() {
		return lat;
	}

	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}

	public BigDecimal getLng() {
		return lng;
	}

	public void setLng(BigDecimal lng) {
		this.lng = lng;
	}

//	public LocalDateTime getInsertTime() {
//		return insertTime;
//	}
//
//	public void setInsertTime(LocalDateTime insertTime) {
//		this.insertTime = insertTime;
//	}
//
//	public String getEditable() {
//		return editable;
//	}
//
//	public void setEditable(String editable) {
//		this.editable = editable;
//	}
//
//	public String getMemberID() {
//		return memberID;
//	}
//
//	public void setMemberID(String memberID) {
//		this.memberID = memberID;
//	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

}
