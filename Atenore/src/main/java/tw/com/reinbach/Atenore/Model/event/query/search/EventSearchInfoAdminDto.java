package tw.com.reinbach.Atenore.Model.event.query.search;

import java.time.LocalDateTime;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class EventSearchInfoAdminDto {

	@ApiModelProperty(name = "eventID", value = "活動編號", example = "123e4567-e89b-42d3-a456-556642440000", dataType = "String")
	private String eventID;
	
	/* 分類屬性
	 * 
	 */
	@ApiModelProperty(name = "typeID", value = "活動分類編號", example = "1", dataType = "Integer")
	private Integer typeID;
	@ApiModelProperty(name = "typeName", value = "活動分類名稱", example = "醫療院所", dataType = "String")
	private String typeName;
	@ApiModelProperty(name = "mainID", value = "活動大分類編號", example = "1", dataType = "Integer")
	private Integer mainID;
	@ApiModelProperty(name = "mainTypeName", value = "活動大分類名稱)", example = "醫療", dataType = "String")
	private String mainTypeName;
	
	@ApiModelProperty(name = "name", value = "活動名稱", example = "武漢肺炎緊急對策", dataType = "String")
	private String name;
	
	@ApiModelProperty(name = "startTime", value = "活動開始時間", example = "2020-01-10 10:00:00", dataType = "LocalDateTime")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private LocalDateTime startTime;
	
	@ApiModelProperty(name = "endTime", value = "活動結束時間", example = "2020-01-10 10:00:00", dataType = "LocalDateTime")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private LocalDateTime endTime;
	
	@ApiModelProperty(name = "status", value = "活動舉辦狀態", example = "正在舉行", dataType = "String")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String status;
	
	@ApiModelProperty(name = "memberID", value = "活動創立者編號", example = "我高興咬我啊", dataType = "String")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String memberID;
	
	@ApiModelProperty(hidden = true, name = "insertTime", value = "資訊創立時間", example = "2020-01-10 10:00:00", dataType = "LocalDateTime")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private LocalDateTime insertTime;

	public String getEventID() {
		return eventID;
	}

	public void setEventID(String eventID) {
		this.eventID = eventID;
	}

	public Integer getTypeID() {
		return typeID;
	}

	public void setTypeID(Integer typeID) {
		this.typeID = typeID;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public String getMainTypeName() {
		return mainTypeName;
	}

	public void setMainTypeName(String mainTypeName) {
		this.mainTypeName = mainTypeName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public LocalDateTime getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(LocalDateTime insertTime) {
		this.insertTime = insertTime;
	}
	
	
	
	
}
