package tw.com.reinbach.Atenore.Model.place.update;

import javax.validation.constraints.NotEmpty;

import io.swagger.annotations.ApiModelProperty;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;

public class PlaceFollowedDto {

	@NotEmpty(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@ApiModelProperty(required = true)
	private String placeID;

	@NotEmpty(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@ApiModelProperty(required = true)
	private String memberID;

	@ApiModelProperty(hidden = true)
	private Boolean wannaFollow;

	public String getPlaceID() {
		return placeID;
	}

	public void setPlaceID(String placeID) {
		this.placeID = placeID;
	}

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public Boolean getWannaFollow() {
		return wannaFollow;
	}

	public void setWannaFollow(Boolean wannaFollow) {
		this.wannaFollow = wannaFollow;
	}

}
