package tw.com.reinbach.Atenore.Utility.aws;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.HttpMethod;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.transfer.MultipleFileDownload;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;

import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;
import tw.com.reinbach.Atenore.Utility.CipherUtility;

@Service
public class AmazonService {

	private AmazonS3 s3client;
	
	private TransferManager tran;

	/**
	 * 連接點
	 * 在application.properties中設定
	 */
	@Value("${amazonProperties.endpointUrl}")
	private String endpointUrl;
	/**
	 * S3內的儲存容器名稱
	 * 在application.properties中設定
	 */
	@Value("${amazonProperties.bucketName}")
	private String bucketName;
	/**
	 * 儲存容器的進入ID
	 * 在application.properties中設定
	 */
	@Value("${amazonProperties.accessKey}")
	private String accessKey;
	/**
	 * 儲存容器的進入密碼
	 * 在application.properties中設定
	 */
	@Value("${amazonProperties.secretKey}")
	private String secretKey;
	/**
	 * 檔案所在的資料夾名稱
	 * "可"在application.properties中設定
	 * 由於儲存資料夾可能變動因此建議用setter設定
	 * 預設值為null，null的情形下表示不指定資料夾，會將檔案置放於儲存容器的root下
	 */
	private String folderName = null;
	/**
	 * 檔案的objectKey是AWS S3的名稱，由"資料夾" + "檔名" + "副檔名"組成
	 */
	private String objectKey;

	@PostConstruct
	private void initializeAmazon() {
		AWSCredentials credentials = new BasicAWSCredentials(this.accessKey, this.secretKey);
		this.s3client = AmazonS3ClientBuilder
						.standard()
						 .withRegion(Regions.AP_NORTHEAST_1)
						.withCredentials(new AWSStaticCredentialsProvider(credentials))
						.build();
		this.tran = TransferManagerBuilder
						.standard()
						.withS3Client(this.s3client)
						.build();
	}
	
	/**設定檔案上傳的目標資料夾，可含子資料夾
	 * Ex："/photo/subfolder1/subfolder2"
	 * @param folderName
	 */
	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}
	
	/**設定檔案的ObjectKey：由資料夾+檔名+副檔名組成\
	 * 
	 * Ex:"/photo/12345678.png"
	 * @param file
	 */
	private void setObjectKey(MultipartFile file) {
		StringBuilder buildObjectKey = new StringBuilder();
		// 如果沒有指定資料夾，物件會上傳至S3 Root下
		if(this.folderName!=null) {
			buildObjectKey.append(this.folderName).append("/");
		}
		buildObjectKey.append(CipherUtility.getMD5Encoding(CipherUtility.createPassWord()))
					  .append(".")
					  .append(FilenameUtils.getExtension(file.getOriginalFilename()));
		this.objectKey = buildObjectKey.toString();
	}

	private void setObjectKey(File file) {
		StringBuilder buildObjectKey = new StringBuilder();
		// 如果沒有指定資料夾，物件會上傳至S3 Root下
		if(this.folderName!=null) {
			buildObjectKey.append(this.folderName).append("/");
		}
		buildObjectKey.append(CipherUtility.getMD5Encoding(CipherUtility.createPassWord()))
					  .append(".")
					  .append(FilenameUtils.getExtension(file.getName()));
		this.objectKey = buildObjectKey.toString();
	}
	
	private void setObjectKeyNotEncrypted(File file) {
		StringBuilder buildObjectKey = new StringBuilder();
		// 如果沒有指定資料夾，物件會上傳至S3 Root下
		if(this.folderName!=null) {
			buildObjectKey.append(this.folderName).append("/");
		}
		buildObjectKey.append(file.getName());
		this.objectKey = buildObjectKey.toString();
	}
	
	/**取得檔案所在Url
	 * 
	 * @param bucketName
	 * @param objectKey
	 * @return 回傳檔案所在Url String
	 */
	public String getObjectUrl(String bucketName, String objectKey) {
		return this.s3client.getUrl(bucketName, objectKey).toString();
	}

	/**取得檔案所在Url，給予Url時效限制
	 * 
	 * @param bucketName
	 * @param objectKey
	 * @return
	 */
	public String getObjectPresignedUrl(String bucketName, String objectKey){
		// Generate the presigned URL.
		return new GeneratePresignedUrlRequest(bucketName, objectKey)
                .withMethod(HttpMethod.GET).toString(); 
	}

	public AmazonDto uploadFileNotEncrypted(File file) throws IOException {
		this.setObjectKeyNotEncrypted(file);
		File uploadFile = file;
		System.out.println(this.objectKey);
		PutObjectRequest request = 
				new PutObjectRequest(this.bucketName, 
									this.objectKey, 
									uploadFile); 
		this.s3client.putObject(request);
		
		AmazonDto amazonDto =new AmazonDto();
		amazonDto.setUrl(this.getObjectUrl(this.bucketName, this.objectKey));
		amazonDto.setObjectKey(this.objectKey);
		uploadFile.delete();
		return amazonDto;
	}
	
	public AmazonDto uploadFile(File file) throws IOException {
		this.setObjectKey(file);
		File uploadFile = file;
		PutObjectRequest request = 
				new PutObjectRequest(this.bucketName, 
									this.objectKey, 
									uploadFile); 
		this.s3client.putObject(request);
		
		AmazonDto amazonDto =new AmazonDto();
		amazonDto.setUrl(this.getObjectUrl(this.bucketName, this.objectKey));
		amazonDto.setObjectKey(this.objectKey);
		uploadFile.delete();
		return amazonDto;
	}
	
	/**上傳到AWS S3並取回存取路徑
	 * 
	 * @param Multipart File
	 * @return 回傳上傳檔案的Url與檔案物件金鑰(AmazonBean)
	 * @throws IOException
	 */
	public AmazonDto uploadFile(MultipartFile file) throws IOException {
		
		this.setObjectKey(file);
		File uploadFile = this.convertMultipartFileAsFile(file);
		PutObjectRequest request = 
				new PutObjectRequest(this.bucketName, 
									this.objectKey, 
									uploadFile); 
		this.s3client.putObject(request);
		
		AmazonDto amazonDto =new AmazonDto();
		amazonDto.setUrl(this.getObjectUrl(this.bucketName, this.objectKey));
		amazonDto.setObjectKey(this.objectKey);
		uploadFile.delete();
		return amazonDto;
	}

	/**上傳到AWS S3並取回存取路徑
	 * 
	 * @param Multipart File
	 * @return ResponseModel : 回傳上傳檔案的Url與檔案物件金鑰(AmazonBean)
	 * 
	 */	
	public ResponseModel<AmazonDto> uploadFileReturnResponse(MultipartFile file)  {
		
		this.setObjectKey(file);
		File uploadFile = null;
		try {
			uploadFile = this.convertMultipartFileAsFile(file);
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseModel<AmazonDto>(500, e.getMessage());
		}
		PutObjectRequest request = 
				new PutObjectRequest(this.bucketName, 
									this.objectKey, 
									uploadFile); 
		try {
			this.s3client.putObject(request);	
		}catch(Exception e) {
			return new ResponseModel<AmazonDto>(409, e.getMessage());
		}
		
		AmazonDto amazonDto =new AmazonDto();
		amazonDto.setUrl(this.getObjectUrl(this.bucketName, this.objectKey));
		amazonDto.setObjectKey(this.objectKey);
		uploadFile.delete();
		
		return new ResponseModel<AmazonDto>(201, amazonDto);
	}
	
	/**上傳檔案到AWS S3並取回存取路徑
	 * 
	 * @param Multipart File Array
	 * @return ArrayList：回傳上傳檔案的Url與檔案物件金鑰 List<AmazonBean>
	 * @throws IOException
	 */
	public List<AmazonDto> uploadFile(List<MultipartFile> files) throws IOException {
		
		List<AmazonDto> list = new ArrayList<AmazonDto>();
		
		for(MultipartFile file : files) {
			this.setObjectKey(file);
			File uploadFile = this.convertMultipartFileAsFile(file);
			PutObjectRequest request = 
					new PutObjectRequest(this.bucketName, 
										this.objectKey, 
										uploadFile); 
			this.s3client.putObject(request);	
			AmazonDto amazonDto = new AmazonDto(); 
			amazonDto.setUrl(this.getObjectUrl(this.bucketName, this.objectKey));
			amazonDto.setObjectKey(this.objectKey);
			list.add(amazonDto);
			uploadFile.delete();
		}
		return list;
	}
	/**刪除單一檔案
	 * 
	 * @param objectKey
	 * @return String：結果訊息
	 */
	public String deleteFile(String objectKey) {
	    try {
			s3client.deleteObject(new DeleteObjectRequest(this.bucketName, objectKey));
		} catch (AmazonServiceException e) {
			e.printStackTrace();
			return "檔案刪除失敗：AWS Service";
		} catch (SdkClientException e) {
			e.printStackTrace();
			return "檔案刪除失敗：SDK";
		}
	    return "檔案刪除成功。";
	}

	/** Delete single file by ObjectKey and return response model
	 * 
	 * @param objectKey
	 * @return
	 */
	public ResponseModel<Void> deleteFileReturnResponse(String objectKey) {
	    try {
			s3client.deleteObject(new DeleteObjectRequest(this.bucketName, objectKey));
		} catch (AmazonServiceException e) {
			return new ResponseModel<Void>(409, e.getMessage());
		} catch (SdkClientException e) {
			return new ResponseModel<Void>(409, e.getMessage());
		}
	    return new ResponseModel<Void>(204, "");
	}
	
	/**刪除多個檔案
	 * 
	 * @param objectKeyList：將多個檔案的objectKey包成一個List
	 * @return String：結果訊息
	 */
	public String deleteFile(List<String> objectKeyList) {
		
		List<String> list =  new ArrayList<String>();
		StringBuilder returnMessage = new StringBuilder();
		
		for(String key : objectKeyList) {
		    try {
				s3client.deleteObject(new DeleteObjectRequest(this.bucketName, key));
			} catch (AmazonServiceException e) {
				e.printStackTrace();
				list.add(key);
				continue;
			} catch (SdkClientException e) {
				e.printStackTrace();
				list.add(key);
				continue;
			}			
		}
		if(list.size()!=0) {
			returnMessage.append("有").append(list.size()).append("個檔案無法刪除：").append(list.toString());
		}else {
			returnMessage.append("檔案刪除成功。"); 
		}

	    return returnMessage.toString();
	}
	
	/**
	 * 
	 * @param folderPath
	 * @return
	 */
	public Boolean deleteFileByFolder(String folderPath) {
		
		Boolean result = false;
		ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
	                .withBucketName(this.bucketName)
	                .withPrefix(folderPath);

        ObjectListing objectListing = s3client.listObjects(listObjectsRequest);
        while (true) {
            for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
                s3client.deleteObject(this.bucketName, objectSummary.getKey());
            }
            if (objectListing.isTruncated()) {
                objectListing = s3client.listNextBatchOfObjects(objectListing);
            } else {
            	result = true;
                break;
            }
        }
		return result;
	}
	
	/**
	 * 
	 * @param folderPath
	 * @return
	 */
	public Boolean deleteFileByFolder(List<String> folderPathList) {
		
		Boolean result = false;
		for(String folderpath : folderPathList) {
			 ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
		                .withBucketName(this.bucketName)
		                .withPrefix(folderpath);

	        ObjectListing objectListing = s3client.listObjects(listObjectsRequest);
	        while (true) {
	            for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
	                s3client.deleteObject(this.bucketName, objectSummary.getKey());
	            }
	            if (objectListing.isTruncated()) {
	                objectListing = s3client.listNextBatchOfObjects(objectListing);
	            } else {
	            	result = true;
	                break;
	            }
	        }			
		}
		return result;
	}

	/** 資料夾下載：使用者指定下載位置路徑
	 * 
	 * @param folderPath 設定下載資料夾的相對路徑
	 * @param fileDestination 指定下載位置路徑
	 * @return
	 */
	public Boolean downloadFilesByFolder(String folderPath, String fileDestination) {
		MultipleFileDownload download = tran.downloadDirectory(
				this.bucketName, folderPath, 
				new File(fileDestination), 
				true);
		try {
			download.waitForCompletion();
			return  true;
		} catch (AmazonClientException | InterruptedException e) {
			e.printStackTrace();
			return  false;
		}
	}
	
	/** 資料夾下載：未指定下載路徑(預設為使用者桌面)
	 * 
	 * @param folderPath
	 * @return
	 */
	public Boolean downloadFilesByFolder(String folderPath) {
		MultipleFileDownload download = tran.downloadDirectory(
				this.bucketName, folderPath, 
				new File(System.getProperty("user.home") + "\\Desktop"), 
				true);
		try {
			download.waitForCompletion();
			return  true;
		} catch (AmazonClientException | InterruptedException e) {
			e.printStackTrace();
			return  false;
		}
	}
	
	/**將Multipart File轉成File
	 * 
	 * @param file
	 * @return File
	 * @throws IOException
	 */
	private File convertMultipartFileAsFile(MultipartFile file) throws IOException {
		File covertFile = File.createTempFile(
				file.getOriginalFilename(), 
				FilenameUtils.getExtension(file.getOriginalFilename()));
		FileOutputStream fos = new FileOutputStream(covertFile);
		fos.write(file.getBytes());
		fos.close();
		return covertFile;
	}
}
