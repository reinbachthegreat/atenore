package tw.com.reinbach.Atenore.Model.master.query.search;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModelProperty;

public class MasterSearchArgsDto {
	
	@ApiModelProperty(hidden = true)
	private String memberID;

	@ApiModelProperty(value = "user's lat", example = "25.0639368", required = true)
	private BigDecimal lat;
	
	@ApiModelProperty(value = "user's lng", example = "121.5471534", required = true)
	private BigDecimal lng;
	
	@ApiModelProperty(value = "alias keywords filter")
	private String fuzzyFilter;
	
	/* 分類屬性
	 * 
	 */
	@ApiModelProperty(name = "typeID", value = "master type(minor)", example = "1", dataType = "Integer")
	private Integer typeID;

	@ApiModelProperty(name = "midID", value = "master middle type(middle)", example = "1", dataType = "Integer")
	private Integer midID;
	
	@ApiModelProperty(name = "mainID", value = "master main type(main)", example = "1", dataType = "Integer")
	private Integer mainID;
	
	@ApiModelProperty(value = "alias area, base on Taiwan's county.", example = "新北市")
	private String county;

	public BigDecimal getLat() {
		return lat;
	}

	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}

	public BigDecimal getLng() {
		return lng;
	}

	public void setLng(BigDecimal lng) {
		this.lng = lng;
	}

	public String getFuzzyFilter() {
		return fuzzyFilter;
	}

	public void setFuzzyFilter(String fuzzyFilter) {
		this.fuzzyFilter = fuzzyFilter;
	}

	public Integer getTypeID() {
		return typeID;
	}

	public void setTypeID(Integer typeID) {
		this.typeID = typeID;
	}

	public Integer getMidID() {
		return midID;
	}

	public void setMidID(Integer midID) {
		this.midID = midID;
	}

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}
	
	
	
}
