package tw.com.reinbach.Atenore.Model.validate;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;

@ApiModel
public class SignUpDto {
	
	@ApiModelProperty(hidden = true)
	private String memberID;
	
	@NotEmpty(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@Email(message = ApiError.INPUT_TYPE_MISMATCHED)
	@ApiModelProperty(required = true)
	private String account;

	@NotEmpty(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@Size(min=8, max=16, message = ApiError.INPUT_LENGTH_MISMATCHED)
	@ApiModelProperty(required = true)
	private String pwd;
	
	@NotEmpty(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@ApiModelProperty(required = true)
	private String publicName;
	
	@NotEmpty(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@ApiModelProperty(required = true)
	private String lastName;
	
	@NotEmpty(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@ApiModelProperty(required = true)
	private String firstName;
	
	private String phone;
	
	@NotEmpty(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@ApiModelProperty(required = true)
	private String cell;
	
	private String county;
	
	private String address1;
	
	private String address2;
	
	private String img;
	
	private String objectKey;
	
	private String deviceToken;
	
	@ApiModelProperty(hidden = true)
	private String accountType;
	
	@ApiModelProperty(hidden = true)
	private Integer status;
	
	@ApiModelProperty(hidden = true, notes = "for sso")
	private String email;

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getPublicName() {
		return publicName;
	}

	public void setPublicName(String publicName) {
		this.publicName = publicName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCell() {
		return cell;
	}

	public void setCell(String cell) {
		this.cell = cell;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}
	
	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getObjectKey() {
		return objectKey;
	}

	public void setObjectKey(String objectKey) {
		this.objectKey = objectKey;
	}

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
}
