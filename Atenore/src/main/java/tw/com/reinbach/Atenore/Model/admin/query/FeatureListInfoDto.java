package tw.com.reinbach.Atenore.Model.admin.query;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class FeatureListInfoDto {

	private List<FeatureInfoDto> featureList;

	public List<FeatureInfoDto> getFeatureList() {
		return featureList;
	}

	public void setFeatureList(List<FeatureInfoDto> featureList) {
		this.featureList = featureList;
	}
}
