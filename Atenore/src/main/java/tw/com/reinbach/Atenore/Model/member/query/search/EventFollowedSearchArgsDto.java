package tw.com.reinbach.Atenore.Model.member.query.search;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.validation.constraints.NotEmpty;

import org.springframework.format.annotation.DateTimeFormat;

import io.swagger.annotations.ApiModelProperty;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchPreferArgDto;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;

public class EventFollowedSearchArgsDto {

	@ApiModelProperty(hidden = true)
	private String memberID;

	@ApiModelProperty(value = "user's lat", example = "25.0639368", required = true)
	private BigDecimal lat;

	@ApiModelProperty(value = "user's lng", example = "121.5471534", required = true)
	private BigDecimal lng;

	@ApiModelProperty(value = "alias keywords filter")
	private String fuzzyFilter;

	@ApiModelProperty(value = "data filter by today only or not, default is false", required = true, example = "false")
	private Boolean todayOnly = false;

	@NotEmpty(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@ApiModelProperty(required = true)
	private EventSearchPreferArgDto searchPreferArg;

	@ApiModelProperty(value = "data filter by expired or not, default is false", example = "false")
	private Boolean expired = false;

	/*
	 * 分類屬性
	 * 
	 */
	@ApiModelProperty(name = "typeID", value = "event type(minor)", example = "1", dataType = "Integer")
	private Integer typeID;

	@ApiModelProperty(name = "mainID", value = "event main type(main)", example = "1", dataType = "Integer")
	private Integer mainID;

	@ApiModelProperty(name = "startTime", example = "2020-01-10 10:00:00", dataType = "LocalDateTime")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime startTime;

	@ApiModelProperty(name = "endTime", example = "2020-01-10 10:00:00", dataType = "LocalDateTime")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime endTime;

	@ApiModelProperty(hidden = true)
	private String apply;

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public BigDecimal getLat() {
		return lat;
	}

	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}

	public BigDecimal getLng() {
		return lng;
	}

	public void setLng(BigDecimal lng) {
		this.lng = lng;
	}

	public String getFuzzyFilter() {
		return fuzzyFilter;
	}

	public void setFuzzyFilter(String fuzzyFilter) {
		this.fuzzyFilter = fuzzyFilter;
	}

	public EventSearchPreferArgDto getSearchPreferArg() {
		return searchPreferArg;
	}

	public void setSearchPreferArg(EventSearchPreferArgDto searchPreferArg) {
		this.searchPreferArg = searchPreferArg;
	}

	public Integer getTypeID() {
		return typeID;
	}

	public void setTypeID(Integer typeID) {
		this.typeID = typeID;
	}

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}

	public String getApply() {
		return apply;
	}

	public void setApply(String apply) {
		this.apply = apply;
	}

	public Boolean getTodayOnly() {
		return todayOnly;
	}

	public void setTodayOnly(Boolean todayOnly) {
		this.todayOnly = todayOnly;
	}

	public Boolean getExpired() {
		return expired;
	}

	public void setExpired(Boolean expired) {
		this.expired = expired;
	}

}
