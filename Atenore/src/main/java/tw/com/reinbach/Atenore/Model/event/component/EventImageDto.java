package tw.com.reinbach.Atenore.Model.event.component;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class EventImageDto {

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String imageID;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String eventID;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String objectKey;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String img;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Boolean isMain;

	public String getImageID() {
		return imageID;
	}

	public void setImageID(String imageID) {
		this.imageID = imageID;
	}

	public String getEventID() {
		return eventID;
	}

	public void setEventID(String eventID) {
		this.eventID = eventID;
	}

	public String getObjectKey() {
		return objectKey;
	}

	public void setObjectKey(String objectKey) {
		this.objectKey = objectKey;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public Boolean getIsMain() {
		return isMain;
	}

	public void setIsMain(Boolean isMain) {
		this.isMain = isMain;
	}
	
}
