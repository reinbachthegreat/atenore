package tw.com.reinbach.Atenore.Model.admin.create;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class FeatureCreateDto {

	@ApiModelProperty(hidden = true)
	private String featureID;
	
	private String name;
	
	private List<String> limitIDList;
	
	private Boolean topFeature;

	public String getFeatureID() {
		return featureID;
	}

	public void setFeatureID(String featureID) {
		this.featureID = featureID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getLimitIDList() {
		return limitIDList;
	}

	public void setLimitIDList(List<String> limitIDList) {
		this.limitIDList = limitIDList;
	}

	public Boolean getTopFeature() {
		return topFeature;
	}

	public void setTopFeature(Boolean topFeature) {
		this.topFeature = topFeature;
	}

}
