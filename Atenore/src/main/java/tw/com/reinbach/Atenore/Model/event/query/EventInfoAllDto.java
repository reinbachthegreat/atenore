package tw.com.reinbach.Atenore.Model.event.query;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import tw.com.reinbach.Atenore.Model.event.component.EventAdminDto;
import tw.com.reinbach.Atenore.Model.event.component.EventImageDto;
import tw.com.reinbach.Atenore.Model.event.component.EventRecommend;
import tw.com.reinbach.Atenore.Model.event.component.EventTag;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class EventInfoAllDto {

	private EventAdminDto eventInfo;

	private List<EventTag> eventTagList;

	private List<EventRecommend> eventRecommendList;

	private List<EventImageDto> eventImgList;

	public EventAdminDto getEventInfo() {
		return eventInfo;
	}

	public void setEventInfo(EventAdminDto eventInfo) {
		this.eventInfo = eventInfo;
	}

	public List<EventTag> getEventTagList() {
		return eventTagList;
	}

	public void setEventTagList(List<EventTag> eventTagList) {
		this.eventTagList = eventTagList;
	}

	public List<EventRecommend> getEventRecommendList() {
		return eventRecommendList;
	}

	public void setEventRecommendList(List<EventRecommend> eventRecommendList) {
		this.eventRecommendList = eventRecommendList;
	}

	public List<EventImageDto> getEventImgList() {
		return eventImgList;
	}

	public void setEventImgList(List<EventImageDto> eventImgList) {
		this.eventImgList = eventImgList;
	}

}
