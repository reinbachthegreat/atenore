package tw.com.reinbach.Atenore.Model.category.master.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MasterListSearchByTypeInfoDto {
	
	private Integer typeID;
	
	private Integer middleID;
	
	private Integer mainID;
	
	private String typeName;
	
	private String middleTypeName;
	
	private String mainTypeName;
	
	private List<MasterSearchByTypeInfoDto> masterList;

	public List<MasterSearchByTypeInfoDto> getMasterList() {
		return masterList;
	}

	public void setMasterList(List<MasterSearchByTypeInfoDto> masterList) {
		this.masterList = masterList;
	}

	public Integer getTypeID() {
		return typeID;
	}

	public void setTypeID(Integer typeID) {
		this.typeID = typeID;
	}

	public Integer getMiddleID() {
		return middleID;
	}

	public void setMiddleID(Integer middleID) {
		this.middleID = middleID;
	}

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getMiddleTypeName() {
		return middleTypeName;
	}

	public void setMiddleTypeName(String middleTypeName) {
		this.middleTypeName = middleTypeName;
	}

	public String getMainTypeName() {
		return mainTypeName;
	}

	public void setMainTypeName(String mainTypeName) {
		this.mainTypeName = mainTypeName;
	}
}
