package tw.com.reinbach.Atenore.Model.place.query.search;

import io.swagger.annotations.ApiModelProperty;

public class PlaceSearchPreferArgDto {

	@ApiModelProperty(value = "sn of search prefer", example = "0", required = true)
	private Integer searchPreferSN;

	@ApiModelProperty(value = "sn of search order", example = "1", required = true)
	private Integer searchOrderSN;

	@ApiModelProperty(hidden = true)
	private String searchOrderSql;

	public Integer getSearchPreferSN() {
		return searchPreferSN;
	}

	public void setSearchPreferSN(Integer searchPreferSN) {
		this.searchPreferSN = searchPreferSN;
	}

	public Integer getSearchOrderSN() {
		return searchOrderSN;
	}

	public void setSearchOrderSN(Integer searchOrderSN) {
		this.searchOrderSN = searchOrderSN;
	}

	public String getSearchOrderSql() {
		return searchOrderSql;
	}

	public void setSearchOrderSql(String searchOrderSql) {
		this.searchOrderSql = searchOrderSql;
	}

}
