package tw.com.reinbach.Atenore.Model.place.create;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import javax.validation.constraints.NotBlank;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiModelProperty;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;

public class PlaceCreateDto {

	@ApiModelProperty(value = "場所編號", hidden = true)
	private String placeID;

	@NotBlank(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@ApiModelProperty(value = "場所名稱", required = true)
	private String name;

	@NotBlank(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@ApiModelProperty(value = "場所縣市", example = "新北市", required = true)
	private String county;
	@ApiModelProperty(value = "場所大分類編號")
	private Integer mainID;
	@ApiModelProperty(value = "場所分類編號")
	private Integer typeID;

	@ApiModelProperty(value = "場所地址(排除縣市)", example = "板橋區縣民大道55號2巷158號6樓")
	private String address;

	@ApiModelProperty(value = "電子郵件", example = "aaa@gmail.com")
	private String mail;

	@ApiModelProperty(value = "官方網站URL")
	private String website;

	@ApiModelProperty(value = "場所Facebook", example = "https://www.facebook.com/tsaiingwen/")
	private String facebook;

	@ApiModelProperty(value = "場所服務項目", example = "會議討論、設備租借")
	private String service;

	@ApiModelProperty(value = "場所簡介")
	private String intro;

//	場地
	@ApiModelProperty(value = "場所最大容納人數")
	private Integer max;

	@ApiModelProperty(value = "場所最大容納人數名稱")
	private String maxName;

	@ApiModelProperty(value = "場所面積", example = "50.5坪")
	private String area;

	@ApiModelProperty(value = "管理單位")
	private String unit;

	@ApiModelProperty(value = "GPS, Lat", example = "25.05788", hidden = true)
	private BigDecimal lat;

	@ApiModelProperty(value = "GPS, Lng", example = "24.5585", hidden = true)
	private BigDecimal lng;

	@ApiModelProperty(name = "memberID", value = "活動創立者編號", example = "123e4567-e89b-42d3-a456-556642440000", dataType = "String")
	private String memberID;

	/*
	 * Extend attribute, for Place Edit Table. By controlling version.
	 * 
	 */
	@ApiModelProperty(value = "場所資訊是否開放編輯", example = "true/false", hidden = true)
	private String editable;

	@ApiModelProperty(value = "場所資訊歷史編輯編號", hidden = true)
	private String editID;

	@ApiModelProperty(value = "場所資訊歷史編輯版本是否使用中", example = "true/false", hidden = true)
	private String inUsed;

	@ApiModelProperty(name = "insertTime", value = "資訊創立時間", hidden = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime insertTime;

	@ApiModelProperty(value = "place liaison list")
	private List<PlaceLiaisonCreateDto> placeLiaisonList;
	@ApiModelProperty(value = "place tag list")
	private List<String> tagList;
	@ApiModelProperty(value = "place img files list")
	private List<MultipartFile> files;

	public String getPlaceID() {
		return placeID;
	}

	public void setPlaceID(String placeID) {
		this.placeID = placeID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public Integer getTypeID() {
		return typeID;
	}

	public void setTypeID(Integer typeID) {
		this.typeID = typeID;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public Integer getMax() {
		return max;
	}

	public void setMax(Integer max) {
		this.max = max;
	}

	public String getMaxName() {
		return maxName;
	}

	public void setMaxName(String maxName) {
		this.maxName = maxName;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public BigDecimal getLat() {
		return lat;
	}

	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}

	public BigDecimal getLng() {
		return lng;
	}

	public void setLng(BigDecimal lng) {
		this.lng = lng;
	}

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public String getEditable() {
		return editable;
	}

	public void setEditable(String editable) {
		this.editable = editable;
	}

	public String getEditID() {
		return editID;
	}

	public void setEditID(String editID) {
		this.editID = editID;
	}

	public String getInUsed() {
		return inUsed;
	}

	public void setInUsed(String inUsed) {
		this.inUsed = inUsed;
	}

	public LocalDateTime getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(LocalDateTime insertTime) {
		this.insertTime = insertTime;
	}

	public List<PlaceLiaisonCreateDto> getPlaceLiaisonList() {
		return placeLiaisonList;
	}

	public void setPlaceLiaisonList(List<PlaceLiaisonCreateDto> placeLiaisonList) {
		this.placeLiaisonList = placeLiaisonList;
	}

	public List<String> getTagList() {
		return tagList;
	}

	public void setTagList(List<String> tagList) {
		this.tagList = tagList;
	}

	public List<MultipartFile> getFiles() {
		return files;
	}

	public void setFiles(List<MultipartFile> files) {
		this.files = files;
	}

}
