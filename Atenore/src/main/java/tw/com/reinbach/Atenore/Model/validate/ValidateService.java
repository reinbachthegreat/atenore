package tw.com.reinbach.Atenore.Model.validate;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;

import de.dentrassi.crypto.pem.PemKeyStoreProvider;
import tw.com.reinbach.Atenore.Model.admin.AdminDAO;
import tw.com.reinbach.Atenore.Model.admin.query.AdminInfoDto;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;
import tw.com.reinbach.Atenore.Utility.CipherUtility;
import tw.com.reinbach.Atenore.Utility.SendMail;
import tw.com.reinbach.Atenore.Utility.UUIDGenerator;
import tw.com.reinbach.Atenore.Utility.aws.AmazonDto;
import tw.com.reinbach.Atenore.Utility.aws.AmazonService;
import tw.com.reinbach.Atenore.Utility.oauth.facebook.FBGraph;
import tw.com.reinbach.Atenore.Utility.oauth.facebook.FacebookUserInfo;

@Service
public class ValidateService {

	@Autowired
	private ValidateDAO validateDAO;

	@Autowired
	private AdminDAO adminDAO;

	@Autowired
	private PlatformTransactionManager transactionManager;

	@Value("${server.url}")
	private String serverUrl;

	@Value("${spring.security.oauth2.client.registration.google.client-id}")
	private String googleClientId;
	
	@Value("${spring.security.oauth2.client.registration.client-mode.client-id}")
	private String clientID;

	@Value("${spring.security.oauth2.client.registration.client-mode.client-secret}")
	private String clientSecret;
	
	@Value("${spring.security.oauth2.client.provider.local.token-uri}")
	private String local_token_uri;

	private String local_grant_type = "client_credentials";

	@Value("${spring.security.oauth2.client.registration.google.redirect-uri}")
	private String googleOauth2FilterCallbackPath;

	@Value("${spring.security.oauth2.client.provider.faceook.user-info-uri}")
	private String facebookUserInfoUri;

	@Autowired
	private SendMail sendMail;
	
	@Autowired
	private AmazonService amazonService;

	public enum MemberStatus {
		INACTIVE, ACTIVE, BANNED;
	};

	/**
	 * Sign in 前台安全驗證
	 * 
	 * @param dto
	 * @return
	 */
	public ResponseModel<SignInInfoDto> signIn(SignInDto dto) {

		Map<String, Object> memberInfo = null;

		/**
		 * SSO Login Sequence 第三方登入
		 */
		if (dto.getPwd() == null) {
			String ssoAccount;
			try {
				ssoAccount = CipherUtility.getMD5Encoding(CipherUtility.encryptStringPasswordOnly(dto.getAccount()));
			} catch (Throwable e) {
				e.printStackTrace();
				return new ResponseModel<SignInInfoDto>(500, ApiError.ENCRYPT_EXCEPTION);
			}
			dto.setAccount(ssoAccount);
			memberInfo = validateDAO.signInSSO(dto);

			// No Member exist, sign up.
			String memberID = null;
			if (memberInfo == null) {

				SignUpDto signUpDto = new SignUpDto();
				signUpDto.setAccount(dto.getAccount());
				signUpDto.setEmail(dto.getEmail());
				signUpDto.setPublicName(dto.getPublicName());
				signUpDto.setImg(dto.getImg());
				signUpDto.setAccountType(dto.getAccountType());
				signUpDto.setStatus(dto.getStatus());

				ResponseModel<SignUpInfo> info = signUp(signUpDto, null);
				if(info.getCode()!=201) {
					return new ResponseModel<SignInInfoDto>(500, ApiError.ACCONUT_CREATION_FAILED);
				}
				memberID = info.getData().getMemberID();
			} else {

				// Get Member status code
				Integer statusCode = Integer.valueOf(memberInfo.get("status").toString());
				MemberStatus status = MemberStatus.values()[statusCode];
				switch (status) {
				case INACTIVE:
					return new ResponseModel<SignInInfoDto>(403, "Member account is not activated.");
				case ACTIVE:
					break;
				case BANNED:
					return new ResponseModel<SignInInfoDto>(403, "Member account has been banned.");
				}
				memberID = memberInfo.get("memberID").toString();
			}
			
			// If account is active get token
			OAuth2AccessToken token = this.getOAuth2AccessToken();
			ResponseModel<Void> tokenSetToMember = setAccountTokenByMemberID(memberID, token);
			if (tokenSetToMember.getCode() != 204) {
				return new ResponseModel<>(tokenSetToMember.getCode(), tokenSetToMember.getErrMsg());
			}

			SignInInfoDto signInfo = new SignInInfoDto();
			signInfo.setMemberID(memberID);
			signInfo.setOauth2AccessToken(token);
			return new ResponseModel<SignInInfoDto>(200, signInfo);

		}
		/**
		 * Ordinary Login Sequence 一般登入
		 */
		else {

			String encryptedPwd;
			try {
				encryptedPwd = CipherUtility.encryptStringCommonKey(dto.getAccount(), dto.getPwd());
			} catch (Throwable e) {
				e.printStackTrace();
				return new ResponseModel<SignInInfoDto>(500, ApiError.ENCRYPT_EXCEPTION);
			}
			dto.setPwd(encryptedPwd);
			memberInfo = validateDAO.signIn(dto);

			// Validate if member existing.
			if (memberInfo == null) {
				return new ResponseModel<SignInInfoDto>(404, ApiError.ACCONUT_NOT_FOUND);
			} else {

				String memberID = memberInfo.get("memberID").toString();

				// Get Member status code
				Integer statusCode = Integer.valueOf(memberInfo.get("status").toString());
				MemberStatus status = MemberStatus.values()[statusCode];
				switch (status) {
				case INACTIVE:
					return new ResponseModel<SignInInfoDto>(403, "Member account is not activated.");
				case ACTIVE:
					break;
				case BANNED:
					return new ResponseModel<SignInInfoDto>(403, "Member account has been banned.");
				}

				// If account is active get token
				OAuth2AccessToken token = this.getOAuth2AccessToken();
				ResponseModel<Void> tokenSetToMember = setAccountTokenByMemberID(memberID, token);
				if (tokenSetToMember.getCode() != 204) {
					return new ResponseModel<>(tokenSetToMember.getCode(), tokenSetToMember.getErrMsg());
				}

				SignInInfoDto signInfo = new SignInInfoDto();
				signInfo.setMemberID(memberID);
				signInfo.setOauth2AccessToken(token);

				return new ResponseModel<SignInInfoDto>(200, signInfo);
			}

		}

	}

	/**
	 * Sign up 前台註冊
	 * 
	 * @param dto
	 * @return
	 */
	public ResponseModel<SignUpInfo> signUp(SignUpDto dto, MultipartFile file) {
		
		/** If file not null then upload to AWS
		 *  若檔案不為NULL上傳AWS
		 * 
		 */
		if(file!=null) {
			AmazonDto awsBean = new AmazonDto();
			if (file != null && !file.isEmpty()) {
				try {
					amazonService.setFolderName(new StringBuilder().append("photo").append("/").append("MemberHead").toString());
					awsBean = amazonService.uploadFile(file);
					dto.setImg(awsBean.getUrl());
					dto.setObjectKey(awsBean.getObjectKey());
				} catch (IOException e) {
					// 失敗的話直接跳回
					return new ResponseModel<SignUpInfo>(500, ApiError.AWS_FILE_UPLOAD_FAILED);
				}
			}			
		}

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);

		String memberID = UUIDGenerator.generateType4UUID().toString();
		dto.setMemberID(memberID);

		/** Sign up by SSO
		 * 
		 */
		if (dto.getPwd() == null) {
			String ssoAccount;
			try {
				ssoAccount = CipherUtility.getMD5Encoding(CipherUtility.encryptStringPasswordOnly(dto.getAccount()));
			} catch (Throwable e) {
				e.printStackTrace();
				return null;
			}
			dto.setAccount(ssoAccount);
			
			try {

				int result_account = validateDAO.signUp(dto);
				if (result_account == 0) {
					throw new Exception();
				}

				int result_detail = validateDAO.setMemmberDetail(dto);

				if (result_detail == 0) {
					throw new Exception();
				}

				transactionManager.commit(status);
				SignUpInfo signUpinfo = new SignUpInfo();
				signUpinfo.setMemberID(memberID);
				return new ResponseModel<SignUpInfo>(201, signUpinfo);
			} catch (Exception e) {
				transactionManager.rollback(status);
				return new ResponseModel<SignUpInfo>(500, e.getMessage());
			}
		}
		/** Sign up by ordinary sequence
		 * 
		 */
		else {
			
			String encryptedPwd;
			try {
				encryptedPwd = CipherUtility.encryptStringCommonKey(dto.getAccount(), dto.getPwd());
			} catch (Throwable e) {
				e.printStackTrace();
				return null;
			}
			dto.setPwd(encryptedPwd);
			dto.setAccountType("Email");
			dto.setStatus(MemberStatus.INACTIVE.ordinal());
			
			try {

				int result_account = validateDAO.signUp(dto);
				if (result_account == 0) {
					throw new Exception();
				}

				int result_detail = validateDAO.setMemmberDetail(dto);

				if (result_detail == 0) {
					throw new Exception();
				}

				transactionManager.commit(status);
			} catch (Exception e) {
				transactionManager.rollback(status);
				return new ResponseModel<SignUpInfo>(500, e.getMessage());
			}
			
			if (memberID != null) {
				String mailSubject = "Atenore：驗證碼發送通知";
				String mailContent = new StringBuilder((dto.getPublicName()==null? "寢愛的會員" : dto.getPublicName())).append("，您好：<br/><h2 style='font-weight:bold'>")
						.append(this.serverUrl).append("/65a60f90ed0f345d88616923c519faae/").append(memberID)
						.append("</h2><br/>請點選上列連結完成帳號啟用，感謝您。").toString();

				try {
					sendMail.SendMailToUserByHTML(dto.getAccount(), mailSubject, mailContent);
					return new ResponseModel<SignUpInfo>(201, "");
				} catch (UnsupportedEncodingException | MessagingException e) {
					return new ResponseModel<SignUpInfo>(500, ApiError.MAIL_SEND_FAILED);
				}
			} else {
				return new ResponseModel<SignUpInfo>(500, "acoount creation failed.");
			}
		}
	}

	/**
	 * Activate account 啟用帳號
	 * 
	 * @param memberID
	 * @return
	 */
	public ResponseModel<SignInInfoDto> activeAccount(String memberID) {

		Integer statusCode = validateDAO.getMemberStatusByMemberID(memberID);

		if (statusCode == -1) {
			return new ResponseModel<SignInInfoDto>(404, "No such Member.");
		}

		MemberStatus memberStatus = MemberStatus.values()[statusCode];
		switch (memberStatus) {
		case ACTIVE:
			return new ResponseModel<SignInInfoDto>(409, "Member account has been activated.");
		case BANNED:
			return new ResponseModel<SignInInfoDto>(409, "Member account has been banned. Contact administrator.");
		default:
			break;
		}

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);

		try {
			int result = validateDAO.updateMemberStatus(memberID, MemberStatus.ACTIVE.ordinal());
			if (result == 0) {
				throw new Exception();
			}

			transactionManager.commit(status);
		} catch (Exception e) {
			transactionManager.rollback(status);
			return new ResponseModel<SignInInfoDto>(409, ApiError.ACCONUT_ACTIVE_FAILED);
		}

		OAuth2AccessToken token = this.getOAuth2AccessToken();

		ResponseModel<Void> tokenSetToMember = setAccountTokenByMemberID(memberID, token);
		if (tokenSetToMember.getCode() != 204) {
			return new ResponseModel<>(tokenSetToMember.getCode(), tokenSetToMember.getErrMsg());
		}

		SignInInfoDto signInfo = new SignInInfoDto();
		signInfo.setMemberID(memberID);
		signInfo.setOauth2AccessToken(token);

		return new ResponseModel<SignInInfoDto>(200, signInfo);
	}

	/**
	 * Setting OAtuh token string to single member by memberID 設定單一會員Token
	 * 
	 * @param memberID
	 * @param token
	 * @return
	 */
	public ResponseModel<Void> setAccountTokenByMemberID(String memberID, OAuth2AccessToken token) {
		String tokenString = token.getValue();
		return validateDAO.updateVerifyTokenByMember(memberID, tokenString);
	}

	public ResponseModel<Void> forgetPasswordVerification(String account) {

		AccountStatusInfoDto statusDto = validateDAO.getAccontStatus(account);
		ResponseModel<Void> resultModel = null;

		if (statusDto.getErrMsg() != null) {

			resultModel = new ResponseModel<Void>(500, statusDto.getErrMsg());

		} else if (statusDto.getHasAccount() == false) {

			resultModel = new ResponseModel<Void>(404, ApiError.ACCONUT_NOT_FOUND);

		} else if (statusDto.getHasAccount() == true && !statusDto.getAccountType().equalsIgnoreCase("Email")) {
			resultModel = new ResponseModel<Void>(406, ApiError.ACCOUNT_CAN_NOT_MODIFY_PASSWORD);
		} else {

			Integer statusCode = statusDto.getStatus();

			MemberStatus memberStatus = MemberStatus.values()[statusCode];
			switch (memberStatus) {
			case INACTIVE:
				resultModel = new ResponseModel<Void>(409, ApiError.ACCOUNT_EXISTED_NOT_ACTVATED);
				return resultModel;
			default:
				break;
			}

			resultModel = this.generateVerifyTokenAndSendMail(statusDto);
		}

		return resultModel;
	}

	/**
	 * Reset Password 重設密碼
	 * 
	 * @param token
	 * @param pwd
	 * @return
	 * @throws Throwable
	 */
	public Map<String, Object> resetPassword(String token, String pwd) throws Throwable {

		Map<String, Object> resetResult = new HashMap<String, Object>();
		String account = validateDAO.getAccountByToken(token);
		pwd = CipherUtility.encryptStringCommonKey(account, pwd);
		Integer statusCode = validateDAO.resetPwdByToken(token, pwd);
		if (statusCode != 1) {
			resetResult.put("result", false);
			resetResult.put("errMsg", "Reset Password Failed.");
		} else {
			resetResult.put("result", true);
		}

		return resetResult;
	}

	/**
	 * Check if account has registered 確認帳號是否註冊
	 * 
	 * @param account
	 * @return
	 */
	public Integer checkRegistration(String account) {
		return validateDAO.getMemberCountByAccount(account);
	}

	/**
	 * ADMIN validation 後台安全驗證
	 * 
	 * @param signInAdmin
	 * @return
	 */
	public ResponseModel<AdminInfoDto> adminValidation(SignInAdminDto signInAdmin) {

		AdminInfoDto admin = adminDAO.getAdminByAccount(signInAdmin.getAccount());
		if (admin == null) {
			return new ResponseModel<AdminInfoDto>(404, ApiError.ACCONUT_NOT_FOUND);
		}
		String inputPwd;
		try {
			inputPwd = CipherUtility.encryptStringCommonKey(signInAdmin.getAccount(), signInAdmin.getPwd());
		} catch (Throwable e) {
			e.printStackTrace();
			return new ResponseModel<AdminInfoDto>(500, ApiError.ENCRYPT_EXCEPTION);
		}
		if (!inputPwd.equals(admin.getPwd())) {
			return new ResponseModel<AdminInfoDto>(404, ApiError.VALIDATION_FAILED_PASSWORD);
		} else {
			admin.setPwd(null);
			return new ResponseModel<AdminInfoDto>(200, admin);
		}
	}

	/**
	 * Reset password verification and email notification 重設密碼與Email發送驗證信
	 * 
	 * @param statusDto
	 * @return
	 */
	private ResponseModel<Void> generateVerifyTokenAndSendMail(AccountStatusInfoDto statusDto) {

		ResponseModel<Void> resultModel = null;
		String verifyToken = null;

		try {
			verifyToken = CipherUtility.getMD5Encoding(CipherUtility.encryptStringPasswordOnly(UUIDGenerator.generateType4UUID().toString()));
		} catch (Throwable e) {
			e.printStackTrace();
			verifyToken = CipherUtility.getMD5Encoding(statusDto.getMemberID());
		}

		resultModel = validateDAO.updateVerifyTokenByMember(statusDto.getMemberID(), verifyToken);
		if (resultModel.getCode() != 204) {
			return resultModel;
		}

		String mailSubject = "Atenore：重設密碼通知";
		String mailContent = new StringBuilder("親愛的會員您好：<br/><h2 style='font-weight:bold'>")
				.append(this.serverUrl).append("/c180caed7c8855fc34d51d82f4636769/").append(verifyToken)
				.append("</h2><br/>請點選上列連結前往修改密碼。若您未註冊或未提出密碼修改申請，請忽略。").toString();
		try {
			sendMail.SendMailToUserByHTML(statusDto.getAccount(), mailSubject, mailContent);
			return new ResponseModel<Void>(204, "");
		} catch (UnsupportedEncodingException | MessagingException e) {
			return new ResponseModel<Void>(500, ApiError.MAIL_SEND_FAILED);
		}

	}

//	@Value("${server.ssl.key-store}")
//	private String keystorepath;
//
//	@Value("${server.ssl.key-store-password}")
//	private String keyPwd;
//

	
	/**
	 * Get OAuth2 access token 取得OAuth2 token
	 * 
	 * @return OAuth2AccessToken
	 */
	private OAuth2AccessToken getOAuth2AccessToken() {

		MultiValueMap<String, String> parametersMap = new LinkedMultiValueMap<String, String>();
		parametersMap.set("grant_type", this.local_grant_type);
		parametersMap.set("client_id", this.clientID);
		parametersMap.set("client_secret", this.clientSecret);

		/**
		 * This block request API with SSL 本區段具備SSL憑證狀態
		 */
//		KeyStore keyStore = null;
//		try {
////			keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
//			keyStore = KeyStore.getInstance("PEM");
//		} catch (KeyStoreException e) {
//			e.printStackTrace();
//			return null;
//		}
//		try {
//			keyStore.load(new FileInputStream(new File(this.keystorepath)), this.keyPwd.toCharArray());
//		} catch (NoSuchAlgorithmException | CertificateException | IOException e) {
//			e.printStackTrace();
//			return null;
//		}
//
//		SSLConnectionSocketFactory socketFactory = null;
//		try {
//			socketFactory = new SSLConnectionSocketFactory(new SSLContextBuilder().loadTrustMaterial(null, new TrustSelfSignedStrategy()).loadKeyMaterial(keyStore, this.keyPwd.toCharArray()).build(), NoopHostnameVerifier.INSTANCE);
//		} catch (KeyManagementException | UnrecoverableKeyException | NoSuchAlgorithmException | KeyStoreException e) {
//			e.printStackTrace();
//			return null;
//		}
//		HttpClient httpClient = HttpClients.custom().setSSLSocketFactory(socketFactory).build();
//		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
//		requestFactory.setHttpClient(httpClient);
//		RestTemplate restTemplate = new RestTemplate(requestFactory);

		/**
		 * This block request API without SSL 本區段不具備SSL憑證狀態
		 */
		RestTemplate restTemplate = new RestTemplate();

		OAuth2AccessToken token = restTemplate.postForObject(this.local_token_uri, parametersMap, OAuth2AccessToken.class);

		return token;
	}
	
	/** Sign in by Google
	 *  Google第三方登入
	 * 
	 * @param id_token
	 * @return
	 */
	public ResponseModel<SignInInfoDto> signInByGoogle(String id_token){
		GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(), new JacksonFactory()).setAudience(Collections.singletonList(this.googleClientId)).build();

		GoogleIdToken idToken;
		SignInDto dto = new SignInDto();
		try {
			idToken = verifier.verify(id_token);
		} catch (GeneralSecurityException | IOException e) {
			e.printStackTrace();
			return new ResponseModel<SignInInfoDto>(500, e.getMessage());
		}
		if (idToken != null) {
			Payload payload = idToken.getPayload();

			// Print user identifier
			String userId = payload.getSubject();

			// Get profile information from payload
			String email = payload.getEmail();
			boolean emailVerified = Boolean.valueOf(payload.getEmailVerified());
			String name = (String) payload.get("name");
			String pictureUrl = (String) payload.get("picture");
			String locale = (String) payload.get("locale");
			String familyName = (String) payload.get("family_name");
			String givenName = (String) payload.get("given_name");

			dto.setAccount(userId);
			dto.setPublicName(name);
			dto.setFirst_name(givenName);
			dto.setLast_name(familyName);
			dto.setEmail(email);
			dto.setImg(pictureUrl);
			dto.setAccountType("Google");
			return signIn(dto);
		} else {
			return new ResponseModel<SignInInfoDto>(403, "Invalid ID token.");
		}
	}
	
	/** Sign in by facebook
	 *  Facebook第三方登入
	 * 
	 * @param id_token
	 * @return
	 */
	public ResponseModel<SignInInfoDto> signInByFacebook(String id_token) {
		
		FBGraph graph = new FBGraph(id_token);
		ResponseEntity<FacebookUserInfo> user = graph.getFBGraph();
		System.out.println(user.getBody().getId());
		SignInDto dto = new SignInDto();
		
		if (user.getStatusCodeValue() == 200) {
			FacebookUserInfo info = user.getBody();
			dto.setAccount(info.getId());
			dto.setPublicName(info.getName());
			dto.setFirst_name(info.getFirst_name());
			dto.setLast_name(info.getLast_name());
			dto.setEmail(info.getEmail());
			if (info.getPicture() != null) {
				Map<String, Object> picture = (Map<String, Object>) info.getPicture();
				Map<String, Object> data = (Map<String, Object>) picture.get("data");
				dto.setImg(data.get("url").toString());
			}
			dto.setAccountType("Facebook");
			
			return signIn(dto);

		} else {
			return new ResponseModel<SignInInfoDto>(user.getStatusCodeValue(), ApiError.ACCONUT_CREATION_FAILED);
		}
	}

}
