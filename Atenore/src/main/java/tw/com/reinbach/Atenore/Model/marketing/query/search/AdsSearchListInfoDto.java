package tw.com.reinbach.Atenore.Model.marketing.query.search;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import tw.com.reinbach.Atenore.Model.marketing.query.AdsInfoDto;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AdsSearchListInfoDto {

	private List<AdsSearchInfoDto> adsList;

	public List<AdsSearchInfoDto> getAdsList() {
		return adsList;
	}

	public void setAdsList(List<AdsSearchInfoDto> adsList) {
		this.adsList = adsList;
	}

}
