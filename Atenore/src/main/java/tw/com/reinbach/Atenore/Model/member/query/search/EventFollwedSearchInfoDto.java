package tw.com.reinbach.Atenore.Model.member.query.search;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;
import tw.com.reinbach.Atenore.Model.event.component.EventImageDto;

public class EventFollwedSearchInfoDto {

	@ApiModelProperty(name = "eventID", value = "活動編號", example = "123e4567-e89b-42d3-a456-556642440000", dataType = "String")
	private String eventID;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private EventImageDto coverImage = new EventImageDto();

	/*
	 * 分類屬性
	 * 
	 */
	@ApiModelProperty(name = "name", value = "活動名稱", example = "武漢肺炎緊急對策", dataType = "String")
	private String name;

	@ApiModelProperty(name = "startTime", value = "活動開始時間", example = "2020-01-10 10:00:00", dataType = "LocalDateTime")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private LocalDateTime startTime;

	@ApiModelProperty(name = "endTime", value = "活動結束時間", example = "2020-01-10 10:00:00", dataType = "LocalDateTime")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private LocalDateTime endTime;

	@ApiModelProperty(name = "place", value = "活動場所", example = "台北榮總", dataType = "String")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String place;

	@ApiModelProperty(name = "county", value = "活動場所縣市", example = "新北市", dataType = "String")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String county;

	@ApiModelProperty(name = "address", value = "活動場所地址(排除縣市)", example = "板橋區縣民大道55號2巷158號6樓", dataType = "String")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String address;

	@ApiModelProperty(name = "host", value = "活動舉辦單位", example = "行政院腿輔會", dataType = "String")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String host;

	@ApiModelProperty(name = "click", value = "累計點擊數", example = "1098", dataType = "Integer")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Integer click;

	@ApiModelProperty(name = "follow", value = "累計追蹤數", example = "109876", dataType = "Integer")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Integer follow;

	@ApiModelProperty(name = "lat", value = "GPS, event's Lat", example = "25.05788", dataType = "BigDecimal")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private BigDecimal lat;

	@ApiModelProperty(name = "lng", value = "GPS, event's Lng", example = "24.5585", dataType = "BigDecimal")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private BigDecimal lng;

	@ApiModelProperty(name = "insertTime", value = "資訊創立時間", example = "2020-01-10 10:00:00", dataType = "LocalDateTime")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private LocalDateTime insertTime;

	@ApiModelProperty(value = "calculate between current user and this event, use 'meter' as unit.", example = "913.13")
	private Double distance;

	@ApiModelProperty(value = "活動是否過期")
	private String expired;
	
	private Boolean isExpired;

	public String getEventID() {
		return eventID;
	}

	public void setEventID(String eventID) {
		this.eventID = eventID;
	}

	public EventImageDto getCoverImage() {
		return coverImage;
	}

	public void setCoverImage(EventImageDto coverImage) {
		this.coverImage = coverImage;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Integer getClick() {
		return click;
	}

	public void setClick(Integer click) {
		this.click = click;
	}

	public Integer getFollow() {
		return follow;
	}

	public void setFollow(Integer follow) {
		this.follow = follow;
	}

	public BigDecimal getLat() {
		return lat;
	}

	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}

	public BigDecimal getLng() {
		return lng;
	}

	public void setLng(BigDecimal lng) {
		this.lng = lng;
	}

	public LocalDateTime getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(LocalDateTime insertTime) {
		this.insertTime = insertTime;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	public void setImageID(String imageID) {
		this.coverImage.setImageID(imageID);
	}

	public void setObjectKey(String objectKey) {
		this.coverImage.setObjectKey(objectKey);
	}

	public void setImg(String img) {
		this.coverImage.setImg(img);
	}

	public void setIsMain(Boolean isMain) {
		this.coverImage.setIsMain(isMain);
	}

	public String getExpired() {
		return expired;
	}

	public void setExpired(String expired) {
		this.expired = expired;
	}

	public Boolean getIsExpired() {
		return isExpired;
	}

	public void setIsExpired(Boolean isExpired) {
		this.isExpired = isExpired;
	}

}
