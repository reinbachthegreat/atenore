package tw.com.reinbach.Atenore.RestController.Api.admin;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiOperation;
import tw.com.reinbach.Atenore.Model.member.MemberService;
import tw.com.reinbach.Atenore.Model.member.component.AccountStatusListInfoDto;
import tw.com.reinbach.Atenore.Model.member.create.MemberCreateDto;
import tw.com.reinbach.Atenore.Model.member.query.MemberInfoAdminDto;
import tw.com.reinbach.Atenore.Model.member.query.search.MemberSearchListInfoAdminDto;
import tw.com.reinbach.Atenore.Model.member.update.MemberSatusUpdateDto;
import tw.com.reinbach.Atenore.Model.member.update.MemberUpdateDto;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;

@RestController
public class MemberManageController {
	
	@Autowired
	private MemberService memberService;
	
	@InitBinder
	public void initBinder(WebDataBinder webDataBinder) {
		webDataBinder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}
	
	/** Get member list for ADMIN by fuzzyFilter
	 * 
	 * @param fuzzyFilter
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A46", value = "Get member list for ADMIN by fuzzyFilter")
	@RequestMapping(value = {"/admin/member"}, method = {RequestMethod.GET})
	public ResponseModel<MemberSearchListInfoAdminDto> getMemberListForAdminByFilter(
			@RequestParam(required = false)String fuzzyFilter) {
		System.out.println(fuzzyFilter);
		MemberSearchListInfoAdminDto dto = memberService.getMemberListForAdminByFilter(fuzzyFilter);
		if(dto!=null) {
			return new ResponseModel<MemberSearchListInfoAdminDto>(200, dto);
		}else {
			return new ResponseModel<MemberSearchListInfoAdminDto>(404, ApiError.DATA_NOT_FOUND);
		}
	}
	
	/** Create account and set member detail by ADMIN
	 * 
	 * @param member
	 * @return
	 * @throws Throwable 
	 */
	@ApiOperation(notes = "ApiNo:A47", value = "Create account and set member detail by ADMIN")
	@RequestMapping(value = {"/admin/member"}, method = {RequestMethod.POST})	
	public ResponseModel<Void> createAccountByAdmin(
			@Valid MemberCreateDto member, @RequestParam(required = false)MultipartFile file) throws Throwable{
		return memberService.createAccountByAdmin(member, file);
	}
	
	/**
	 * 
	 * @param memberID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A47-1", value = "Get account and member detail for ADMIN by memberID")
	@RequestMapping(value = {"/admin/member/{memberID}"}, method = RequestMethod.GET)
	public ResponseModel<MemberInfoAdminDto> getMemberWithDetailForAdminByMemberID(
			@PathVariable(required = true)String memberID) {
		MemberInfoAdminDto dto = memberService.getMemberWithDetailForAdminByMemberID(memberID);
		if(dto!=null) {
			return new ResponseModel<MemberInfoAdminDto>(200, dto);
		}else {
			return new ResponseModel<MemberInfoAdminDto>(404, ApiError.DATA_NOT_FOUND);
		}
		
	}
	
	/** Update detail data of member
	 * 
	 * @param memberID
	 * @param dto
	 * @param file
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:48", value = "Update detail data of member")
	@RequestMapping(value = {"/admin/member/{memberID}"}, method = RequestMethod.PUT)
	public ResponseModel<Void> updateMemberDetailByMemberID(
			@PathVariable(required = true) String memberID, @Valid MemberUpdateDto dto, @RequestParam(required = false)MultipartFile file){
		if(file != null && !file.isEmpty()) {
			return memberService.updateMemberDataByMemberID(dto, memberID, file);
		}else {
			return memberService.updateMemberDataByMemberID(dto, memberID);
		}
	}

	/** Update authority status code of member
	 * 
	 * @param memberID
	 * @param code
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:49", value = "Update authority status code of member")
	@RequestMapping(value = {"/admin/member/status/{memberID}"}, method = RequestMethod.PUT)	
	public ResponseModel<Void> updateAccountStatusByMemberID(
			@PathVariable(required = true) String memberID, @RequestBody(required = true)MemberSatusUpdateDto status){
		return memberService.updateAccountStatusByMemberID(memberID, status);
	}
	
	/** Get account status list
	 * 
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:50", value = "Get account status list")
	@RequestMapping(value = "/admin/member/status", method = RequestMethod.GET)
	public ResponseModel<AccountStatusListInfoDto> getAccountStatusList() {
		return new ResponseModel<AccountStatusListInfoDto>(200, memberService.getAccountStatusList());
	}
	
	/** Delete account by memberID
	 * 
	 * @param memberID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:51", value = "Delete account by memberID")
	@RequestMapping(value = {"/admin/member/{memberID}"}, method = RequestMethod.DELETE)
	public ResponseModel<Void> deleteAccountByMemberID(@PathVariable(required = true)String memberID){
		return memberService.deleteAccountByMemberID(memberID);
	}

}
