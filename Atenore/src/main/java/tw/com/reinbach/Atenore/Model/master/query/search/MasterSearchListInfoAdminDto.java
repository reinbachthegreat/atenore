package tw.com.reinbach.Atenore.Model.master.query.search;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MasterSearchListInfoAdminDto {
	
	private List<MasterSearchInfoAdminDto> masterList;

	public List<MasterSearchInfoAdminDto> getMasterList() {
		return masterList;
	}

	public void setMasterList(List<MasterSearchInfoAdminDto> masterList) {
		this.masterList = masterList;
	}
}
