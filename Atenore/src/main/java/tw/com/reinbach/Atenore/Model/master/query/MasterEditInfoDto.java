package tw.com.reinbach.Atenore.Model.master.query;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import tw.com.reinbach.Atenore.Model.master.component.MasterEditDto;
import tw.com.reinbach.Atenore.Model.master.component.MasterTagEditDto;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MasterEditInfoDto {

	private MasterEditDto masterEdit;
	
	private List<MasterTagEditDto> tagEditList;

	public MasterEditDto getMasterEdit() {
		return masterEdit;
	}

	public void setMasterEdit(MasterEditDto masterEdit) {
		this.masterEdit = masterEdit;
	}

	public List<MasterTagEditDto> getTagEditList() {
		return tagEditList;
	}

	public void setTagEditList(List<MasterTagEditDto> tagEditList) {
		this.tagEditList = tagEditList;
	}
	
}
