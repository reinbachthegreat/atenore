package tw.com.reinbach.Atenore.Model.category.event;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import tw.com.reinbach.Atenore.Model.category.event.dto.EventListSearchByMainTypeArgsDto;
import tw.com.reinbach.Atenore.Model.category.event.dto.EventListSearchByTypeArgsDto;
import tw.com.reinbach.Atenore.Model.category.event.dto.EventSearchByMainTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.event.dto.EventSearchByTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.event.dto.EventTypeDto;
import tw.com.reinbach.Atenore.Model.category.event.dto.component.EventMainTypeAdminDto;
import tw.com.reinbach.Atenore.Model.category.event.dto.component.EventTypeAdminDto;
import tw.com.reinbach.Atenore.Model.event.component.EventDto;

@Repository
public class EventTypeDAOJdbcTemplate implements EventTypeDAO {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private NamedParameterJdbcTemplate namedJdbcTemplate;

//	@Override
//	public List<EventMainTypeDto> getEventMainTypeList() {
//		try {
//			String sql = "select emt.*, ifnull(emtin.inprogressQty,0) as 'inprogressQty', ifnull(emtex.expiredQty,0) as 'expiredQty', ifnull(emtall.totalQty,0) as 'totalQty' from eventmaintype emt left join " + 
//					"(select emt.*, count(e.eventID) as 'expiredQty' from atenore.event e left join eventtype et on et.typeID = e.typeID left join eventmaintype emt on emt.mainID = et.mainID " + 
//					"where e.endTime < now() " + 
//					"group by emt.mainID)  as emtex " + 
//					"on emt.mainID = emtex.mainID left join " + 
//					"(select emt.*, count(e.eventID) as 'inprogressQty' from atenore.event e left join eventtype et on et.typeID = e.typeID left join eventmaintype emt on emt.mainID = et.mainID " + 
//					"where e.startTime <= now() and e.endTime >= now() " + 
//					"group by emt.mainID) as emtin " + 
//					"on emt.mainID = emtin.mainID left join " + 
//					"(select emt.*, count(e.eventID) as 'totalQty' from atenore.event e left join eventtype et on et.typeID = e.typeID left join eventmaintype emt on emt.mainID = et.mainID " + 
//					"group by emt.mainID) as emtall on emtall.mainID = emt.mainID order by emt.mainID asc;";			
//			
//			return jdbcTemplate.query(sql, new BeanPropertyRowMapper<EventMainTypeDto>(EventMainTypeDto.class));
//		}catch(DataAccessException e) {
//			return null;	
//		}
//	}
	
	@Override
	public List<EventMainTypeAdminDto> getEventMainTypeListWithFilter(String filter){
		try {
			StringBuilder sql = new StringBuilder("select emt.*, ifnull(emtin.inprogressQty,0) as 'inprogressQty', ifnull(emtex.expiredQty,0) as 'expiredQty', ifnull(emtall.totalQty,0) as 'totalQty' from eventmaintype emt left join \r\n" + 
					"(select emt.*, count(e.eventID) as 'expiredQty' from atenore.event e left join eventtype et on et.typeID = e.typeID left join eventmaintype emt on emt.mainID = et.mainID\r\n" + 
					"where e.endTime < now()\r\n" + 
					"group by emt.mainID)  as emtex\r\n" + 
					"on emt.mainID = emtex.mainID left join\r\n" + 
					"(select emt.*, count(e.eventID) as 'inprogressQty' from atenore.event e left join eventtype et on et.typeID = e.typeID left join eventmaintype emt on emt.mainID = et.mainID \r\n" + 
					"where e.startTime <= now() and e.endTime >= now()\r\n" + 
					"group by emt.mainID) as emtin\r\n" + 
					"on emt.mainID = emtin.mainID left join\r\n" + 
					"(select emt.*, count(e.eventID) as 'totalQty' from atenore.event e left join eventtype et on et.typeID = e.typeID left join eventmaintype emt on emt.mainID = et.mainID \r\n" + 
					"group by emt.mainID) as emtall on emtall.mainID = emt.mainID ");
			
			if(filter!=null) {
				String query = new StringBuilder().append("%").append(filter).append("%").toString();
				sql.append(" where emt.name like ? order by emt.mainID asc ; ");
				return jdbcTemplate.query(sql.toString(), new Object[] {query},new BeanPropertyRowMapper<EventMainTypeAdminDto>(EventMainTypeAdminDto.class));
			}else {
				sql.append(" order by emt.mainID asc ; ");
				return jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper<EventMainTypeAdminDto>(EventMainTypeAdminDto.class));
			}
		}catch(DataAccessException e) {
			return null;	
		}		
	}

//	@Override
//	public List<EventDto> getEventListByMainType(Integer mainID) {
//		try {
//			String sql = "select e.*, et.name as 'typeName', emt.name as 'mainTypeName' from atenore.event e \r\n" + 
//					"left join eventtype et on et.typeID = e.typeID \r\n" + 
//					"left join eventmaintype emt on emt.mainID = et.mainID \r\n" + 
//					"where emt.mainID = ? ";
//			return jdbcTemplate.query(sql, new Object[] {mainID}, new BeanPropertyRowMapper<EventDto>(EventDto.class));
//		}catch(DataAccessException e) {
//			return null;
//		}
//	}
	
	@Override
	public List<EventSearchByMainTypeInfoDto> getEventListByMainTypeWithFilter(EventListSearchByMainTypeArgsDto argsDto){
		try {
			StringBuilder sql = new StringBuilder().append("select e.eventID, e.typeID, et.name as 'typeName',  emt.mainID, emt.name as 'mainTypeName', e.name, e.startTime, e.endTime, e.memberID,"
					+ " case when e.endTime >= now() >= e.startTime then '正在舉行' when  e.endTime < now() then '已結束' else '尚未開始' end as 'status' "
					+ ", et.name as 'typeName', emt.name as 'mainTypeName' from atenore.event e \r\n" + 
					"left join eventtype et on et.typeID = e.typeID \r\n" + 
					"left join eventmaintype emt on emt.mainID = et.mainID \r\n" + 
					"where emt.mainID = :mainID ");
			
			if(argsDto.getFuzzyFilter() != null && argsDto.getFuzzyFilter().trim().length() != 0) {
				sql.append(" and e.name like '%' :fuzzyFilter '%' ");
			}
			
			if(argsDto.getStartTime() != null) {
				sql.append(" and e.startTime >= :startTime ");
			}
			
			if(argsDto.getEndTime() != null) {
				sql.append(" and e.endTime <=  :endTime ");
			}
			
			sql.append(" order by e.eventID asc ;");
			
			SqlParameterSource ps = new BeanPropertySqlParameterSource(argsDto);
			return namedJdbcTemplate.query(sql.toString(), ps, new BeanPropertyRowMapper<EventSearchByMainTypeInfoDto>(EventSearchByMainTypeInfoDto.class));
		}catch(DataAccessException e) {
			return null;
		}		
	}
	
	@Override
	public List<EventTypeAdminDto> getEventTypeListByMainTypeWithFilter(Integer mainID, String fuzzyFilter){
		try {
			StringBuilder sql = new StringBuilder("select et.typeID, et.name, evmt.mainID, evmt.name as 'mainType', ifnull(etin.inprogressQty,0) as 'inprogressQty', ifnull(etex.expiredQty,0) as 'expiredQty', ifnull(etall.totalQty,0) as 'totalQty' from eventtype et left join \r\n" + 
					"					(select et.*, count(e.eventID) as 'expiredQty' from atenore.event e left join eventtype et on et.typeID = e.typeID\r\n" + 
					"					where e.endTime < now() \r\n" + 
					"					group by et.typeID)  as etex \r\n" + 
					"					on et.typeID = etex.typeID left join \r\n" + 
					"					(select et.*, count(e.eventID) as 'inprogressQty' from atenore.event e left join eventtype et on et.typeID = e.typeID \r\n" + 
					"					where e.startTime <= now() and e.endTime >= now()\r\n" + 
					"					group by et.typeID) as etin \r\n" + 
					"					on et.typeID = etin.typeID left join \r\n" + 
					"					(select et.*, count(e.eventID) as 'totalQty' from atenore.event e left join eventtype et on et.typeID = e.typeID \r\n" + 
					"					group by et.typeID) as etall on etall.typeID = et.typeID left join eventmaintype as evmt on et.mainID = evmt.mainID \r\n"
					+ "					where  et.mainID = ? ");
			
			if(fuzzyFilter!=null) {
				sql.append(" and et.name like '%' ? '%' order by et.typeID asc ");
				return jdbcTemplate.query(sql.toString(), new Object[] {mainID, fuzzyFilter}, new BeanPropertyRowMapper<EventTypeAdminDto>(EventTypeAdminDto.class));
			}else {
				sql.append(" order by et.typeID asc ");
				return jdbcTemplate.query(sql.toString(), new Object[] {mainID}, new BeanPropertyRowMapper<EventTypeAdminDto>(EventTypeAdminDto.class));
			}
		}catch(DataAccessException e) {
			return null;
		}
	}
	
	@Override
	public List<EventSearchByTypeInfoDto> getEventListByTypeWithFilter(EventListSearchByTypeArgsDto argsDto){
		try {
			StringBuilder sql = new StringBuilder().append("select e.eventID, e.typeID, et.name as 'typeName', emt.mainID, emt.name as 'mainTypeName', e.name, e.startTime, e.endTime, e.memberID,"
					+ " case when e.endTime >= now() >= e.startTime then '正在舉行' when e.endTime < now() then '已結束' else '尚未開始' end as 'status' "
					+ ", et.name as 'typeName', emt.name as 'mainTypeName' from atenore.event e \r\n" + 
					"left join eventtype et on et.typeID = e.typeID \r\n" + 
					"left join eventmaintype emt on emt.mainID = et.mainID \r\n" + 
					"where et.typeID = :typeID ");
			
			if(argsDto.getFuzzyFilter() != null && argsDto.getFuzzyFilter().trim().length() != 0) {
				sql.append(" and e.name like '%' :fuzzyFilter '%' ");
			}
			
			if(argsDto.getStartTime() != null) {
				sql.append(" and e.startTime <= :startTime ");
			}
			
			if(argsDto.getEndTime() != null) {
				sql.append(" and e.endTime >=  :endTime ");
			}
			
			sql.append(" order by e.eventID asc ;");
			
			SqlParameterSource ps = new BeanPropertySqlParameterSource(argsDto);
			return namedJdbcTemplate.query(sql.toString(), ps, new BeanPropertyRowMapper<EventSearchByTypeInfoDto>(EventSearchByTypeInfoDto.class));
		}catch(DataAccessException e) {
			return null;
		}		
	}
	
	@Override
	public List<EventTypeDto> getEventTypeList(){
		try {
			String sql = "select * from eventtype";
			return jdbcTemplate.query(sql, new BeanPropertyRowMapper<EventTypeDto>(EventTypeDto.class));
		}catch(DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<EventTypeAdminDto> getEventTypeListWithStatics() {
		try {
			String sql="select et.typeID, et.name, evmt.mainID, evmt.name as 'mainType', etin.inprogressQty, etex.expiredQty, etall.totalQty from eventtype et left join \r\n" + 
					"(select et.*, count(e.eventID) as 'expiredQty' from atenore.event e left join eventtype et on et.typeID = e.typeID\r\n" + 
					"where e.endTime < now()\r\n" + 
					"group by et.typeID)  as etex\r\n" + 
					"on et.typeID = etex.typeID left join\r\n" + 
					"(select et.*, count(e.eventID) as 'inprogressQty' from atenore.event e left join eventtype et on et.typeID = e.typeID \r\n" + 
					"where e.startTime <= now() and e.endTime >= now()\r\n" + 
					"group by et.typeID) as etin\r\n" + 
					"on et.typeID = etin.typeID left join\r\n" + 
					"(select et.*, count(e.eventID) as 'totalQty' from atenore.event e left join eventtype et on et.typeID = e.typeID \r\n" + 
					"group by et.typeID) as etall on etall.typeID = et.typeID left join eventmaintype as evmt on et.mainID = evmt.mainID order by et.typeID asc;";
			
			return jdbcTemplate.query(sql, new BeanPropertyRowMapper<EventTypeAdminDto>(EventTypeAdminDto.class));
		}catch(DataAccessException e) {
			return null;
		}
	}
	
	@Override
	public List<EventTypeAdminDto> getEventTypeListWithFilter(String filter){
		String query = new StringBuilder().append("%").append(filter).append("%").toString();
		try {
			String sql="select et.typeID, et.name, evmt.mainID, evmt.name as 'mainType', etin.inprogressQty, etex.expiredQty, etall.totalQty from eventtype et left join \r\n" + 
					"(select et.*, count(e.eventID) as 'expiredQty' from atenore.event e left join eventtype et on et.typeID = e.typeID\r\n" + 
					"where e.endTime < now()\r\n" + 
					"group by et.typeID)  as etex\r\n" + 
					"on et.typeID = etex.typeID left join\r\n" + 
					"(select et.*, count(e.eventID) as 'inprogressQty' from atenore.event e left join eventtype et on et.typeID = e.typeID \r\n" + 
					"where e.startTime <= now() and e.endTime >= now()\r\n" + 
					"group by et.typeID) as etin\r\n" + 
					"on et.typeID = etin.typeID left join\r\n" + 
					"(select et.*, count(e.eventID) as 'totalQty' from atenore.event e left join eventtype et on et.typeID = e.typeID \r\n" + 
					"group by et.typeID) as etall on etall.typeID = et.typeID left join eventmaintype as evmt on et.mainID = evmt.mainID where et.name like ? order by et.typeID asc;";
			
			return jdbcTemplate.query(sql, new Object[] {query}, new BeanPropertyRowMapper<EventTypeAdminDto>(EventTypeAdminDto.class));
		}catch(DataAccessException e) {
			return null;
		}		
	}

	@Override
	public List<EventDto> getEventListByType(Integer typeID) {
		try {
			String sql = "select e.*, et.name as 'typeName' from atenore.event e \r\n" + 
					"left join eventtype et on et.typeID = e.typeID \r\n" + 
					"where et.typeID = ? ;";
			return jdbcTemplate.query(sql, new Object[] {typeID}, new BeanPropertyRowMapper<EventDto>(EventDto.class));
		}catch(DataAccessException e) {
			return null;
		}
	}
	
	@Override
	public List<EventDto> getEventListByTypeWithFilter(Integer typeID, String eventFilter, LocalDateTime startTime, LocalDateTime endTime){
		try {
			StringBuilder sql = new StringBuilder().append("select e.*, et.name as 'typeName', emt.name as 'mainTypeName' from atenore.event e \r\n" + 
					"left join eventtype et on et.typeID = e.typeID \r\n" + 
					"where et.typeID = ? ");

			String query = null;
			
			if(eventFilter != null && eventFilter.trim().length() != 0) {
				query = new StringBuilder().append("%").append(eventFilter).append("%").toString();
				sql.append(" and e.name like ? ");
			}
			
			if(startTime != null) {
				sql.append(" and e.startTime <= ? ");
			}
			
			if(endTime != null) {
				sql.append(" and e.endTime >= ? ");
			}
			
			sql.append(" order by e.eventID asc ;");
			
			return jdbcTemplate.query(sql.toString(), new Object[] {typeID, query, startTime, endTime}, new BeanPropertyRowMapper<EventDto>(EventDto.class));
		}catch(DataAccessException e) {
			return null;
		}		
	}

}
