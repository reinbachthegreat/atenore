package tw.com.reinbach.Atenore.Model.validate;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;

@Repository
public class ValidateDAOJdbcTemplate implements ValidateDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private NamedParameterJdbcTemplate namedJdbcTemplate;

	@Override
	public Map<String, Object> signIn(SignInDto dto) {
		try {
			String sql = "select m.memberID, m.status from atenore.member m where m.account = :account and m.pwd = :pwd";
			SqlParameterSource ps = new BeanPropertySqlParameterSource(dto);
			return namedJdbcTemplate.queryForMap(sql, ps);
		} catch (EmptyResultDataAccessException e) {
			return null;
		} catch (Exception e) {
			System.out.println("Query exception happend: signIn");
			return null;
		}
	}

	@Override
	public Map<String, Object> signInSSO(SignInDto dto) {
		try {
			String sql = "select m.memberID, m.status from atenore.member m where m.account = :account ";
			SqlParameterSource ps = new BeanPropertySqlParameterSource(dto);
			return namedJdbcTemplate.queryForMap(sql, ps);
		} catch (Exception e) {
			System.out.println("no such user.");
			return null;
		}
	}

	@Override
	public int signUp(SignUpDto dto) {
		try {

			String sql = "insert into atenore.member (memberID, account, pwd, accountType, status)"
					+ " values(:memberID, :account, :pwd, :accountType, :status)";
			SqlParameterSource ps = new BeanPropertySqlParameterSource(dto);
			return namedJdbcTemplate.update(sql, ps);
		} catch (Exception e) {
			System.out.println("sign up failed.");
			return 0;
		}
	}

	@Override
	public int setMemmberDetail(SignUpDto dto) {
		try {
			String sql = "insert into atenore.memberdetail (memberID, email, publicName, lastName, firstName, img, objectKey, phone, cell, county, address1, address2, deviceToken, insertTime)"
					+ "values(:memberID, :email, :publicName, :lastName, :firstName, :img, :objectKey, :phone, :cell, :county, :address1, :address2, :deviceToken, now())";
			SqlParameterSource ps = new BeanPropertySqlParameterSource(dto);
			return namedJdbcTemplate.update(sql, ps);
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public Integer getMemberStatusByMemberID(String memberID) {
		try {
			String sql = "select m.status from atenore.member m where m.memberID = ? ";
			return jdbcTemplate.queryForObject(sql, new Object[] { memberID }, Integer.class);
		} catch (Exception e) {
			return -1;
		}
	}

	@Override
	public int updateMemberStatus(String memberID, Integer statusCode) {
		try {
			String sql = "update atenore.member set status = ? where memberID = ?";
			return jdbcTemplate.update(sql, new Object[] { statusCode, memberID });
		} catch (Exception e) {
			return 0;
		}
	}

	@Override
	public Integer getMemberCountByAccount(String account) {
		try {
			String sql = "select count(*) from member m where m.account = ?";
			return jdbcTemplate.queryForObject(sql, new Object[] { account }, Integer.class);
		} catch (Exception e) {
			return -1;
		}
	}

	@Override
	public AccountStatusInfoDto getAccontStatus(String account) {
		AccountStatusInfoDto dto;
		try {
			String sql = "select m.memberID, m.account, m.accountType, m.status from atenore.member m where m.account = ? ";
			dto = jdbcTemplate.queryForObject(sql, new Object[] { account },
					new BeanPropertyRowMapper<AccountStatusInfoDto>(AccountStatusInfoDto.class));
			if (dto == null) {
				dto = new AccountStatusInfoDto();
				dto.setHasAccount(false);
			} else {
				dto.setHasAccount(true);
			}
			return dto;
		} catch (EmptyResultDataAccessException e) {
			dto = new AccountStatusInfoDto();
			dto.setErrMsg(ApiError.ACCONUT_NOT_FOUND);
			return dto;
		} catch (DataAccessException e) {
			dto = new AccountStatusInfoDto();
			dto.setErrMsg(e.getMessage());
			return dto;
		}
	}

	@Override
	public ResponseModel<Void> updateVerifyTokenByMember(String memberID, String verifyToken) {
		try {
			String sql = "update atenore.member set token = ? where memberID = ?";
			int count = jdbcTemplate.update(sql, new Object[] { verifyToken, memberID });
			if (count != 0) {
				return new ResponseModel<Void>(204, "");
			} else {
				return new ResponseModel<Void>(409, "verification store failed.");
			}
		} catch (DataAccessException e) {
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public String getAccountByToken(String token) {
		try {
			String sql = "select account from atenore.member where token = ?";
			return jdbcTemplate.queryForObject(sql, new Object[] { token }, String.class);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public Integer resetPwdByToken(String token, String pwd) {
		try {
			String sql = "update atenore.member set pwd = ?, token = null where token = ?";
			return jdbcTemplate.update(sql, new Object[] { pwd, token });
		} catch (Exception e) {
			return 0;
		}
	}

}
