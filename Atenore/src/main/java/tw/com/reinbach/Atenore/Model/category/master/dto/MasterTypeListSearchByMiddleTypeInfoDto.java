package tw.com.reinbach.Atenore.Model.category.master.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import tw.com.reinbach.Atenore.Model.category.master.dto.component.MasterMiddleTypeAdminDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.component.MasterTypeAdminDto;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MasterTypeListSearchByMiddleTypeInfoDto {
	
	private String mainTypeName;
	
	private String middleTypeName;

	private List<MasterTypeAdminDto> masterTypeList;

	public List<MasterTypeAdminDto> getMasterTypeList() {
		return masterTypeList;
	}

	public void setMasterTypeList(List<MasterTypeAdminDto> masterTypeList) {
		this.masterTypeList = masterTypeList;
	}

	public String getMainTypeName() {
		return mainTypeName;
	}

	public void setMainTypeName(String mainTypeName) {
		this.mainTypeName = mainTypeName;
	}

	public String getMiddleTypeName() {
		return middleTypeName;
	}

	public void setMiddleTypeName(String middleTypeName) {
		this.middleTypeName = middleTypeName;
	}
}
