package tw.com.reinbach.Atenore.Model.event.query;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;
import tw.com.reinbach.Atenore.Model.event.component.EventDto;
import tw.com.reinbach.Atenore.Model.event.component.EventImageDto;
import tw.com.reinbach.Atenore.Model.event.component.EventRecommend;
import tw.com.reinbach.Atenore.Model.event.component.EventTag;

public class EventInfoDto {

	private EventDto eventInfo;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<EventTag> eventTagList;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@ApiModelProperty(hidden = true)
	private List<EventRecommend> eventRecommendList;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<EventImageDto> eventImgList;

	public EventDto getEventInfo() {
		return eventInfo;
	}

	public void setEventInfo(EventDto eventInfo) {
		this.eventInfo = eventInfo;
	}

	public List<EventTag> getEventTagList() {
		return eventTagList;
	}

	public void setEventTagList(List<EventTag> eventTagList) {
		this.eventTagList = eventTagList;
	}

	public List<EventRecommend> getEventRecommendList() {
		return eventRecommendList;
	}

	public void setEventRecommendList(List<EventRecommend> eventRecommendList) {
		this.eventRecommendList = eventRecommendList;
	}

	public List<EventImageDto> getEventImgList() {
		return eventImgList;
	}

	public void setEventImgList(List<EventImageDto> eventImgList) {
		this.eventImgList = eventImgList;
	}

}
