package tw.com.reinbach.Atenore.Model.master;

import java.util.List;

import tw.com.reinbach.Atenore.Model.master.component.MasterDto;
import tw.com.reinbach.Atenore.Model.master.component.MasterEditDto;
import tw.com.reinbach.Atenore.Model.master.component.MasterRecommend;
import tw.com.reinbach.Atenore.Model.master.component.MasterTag;
import tw.com.reinbach.Atenore.Model.master.component.MasterTagEditDto;
import tw.com.reinbach.Atenore.Model.master.create.MasterCreateDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterAliveSearchArgsDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterAliveSearchInfoDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterDeadSearchArgsDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterDeadSearchInfoDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterEditSearchArgsAdminDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterEditSearchInfoAdminDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterEditSearchInfoMemberDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterHitSearchInfoDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterSearchArgsAdminDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterSearchArgsDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterSearchArgsMemberEditedDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterSearchInfoAdminDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterSearchInfoDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterSearchSimpleInfoDto;
import tw.com.reinbach.Atenore.Model.master.update.MasterFollowedDto;
import tw.com.reinbach.Atenore.Model.master.update.MasterUpdateDto;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;

public interface MasterDAO {

	public List<MasterSearchInfoDto> getMasterListWithFilter(MasterSearchArgsDto argsDto);

	public List<MasterHitSearchInfoDto> getMasterListByHitSearchPreferSN(Integer searchPreferSN);

	public List<MasterAliveSearchInfoDto> getMasterAliveListWithFilter(MasterAliveSearchArgsDto argsDto);

	public List<MasterDeadSearchInfoDto> getMasterDeadListWithFilter(MasterDeadSearchArgsDto argsDto);

	public List<MasterSearchSimpleInfoDto> getMasterSimpleByPublicName(String name, Integer alive);

	public MasterDto getMasterByMasterIDWithMemberID(String masterID, String memberID);
	
	public MasterDto getMasterByMasterID(String masterID);

	public MasterDto getMasterAliveWithMemberID(String masterID, String memberID);

	public MasterDto getMasterAlive(String masterID);

	public MasterDto getMasterDeadWithMemberID(String masterID, String memberID);

	public MasterDto getMasterDead(String masterID);

	public List<MasterTag> getMasterTagListByMasterID(String masterID);

	public List<MasterRecommend> getMasterRecommendListByMasterID(String masterID);

	public Boolean getMasterIfMemberFollowed(MasterFollowedDto follow);

	public ResponseModel<Void> setMasterFollowed(MasterFollowedDto follow);

	public ResponseModel<Void> deleteMasterFollowed(MasterFollowedDto follow);
	
	public ResponseModel<Void> updateMasterFollowed(String masterID, Boolean wannaFollow);

	public ResponseModel<Void> setMaster(MasterCreateDto master);

	public ResponseModel<Void> setMaster(List<MasterCreateDto> list);

	public ResponseModel<Void> setMasterTag(List<MasterTag> list);

	public ResponseModel<Void> setMasterTagEdit(List<MasterTagEditDto> list);

	public ResponseModel<Void> setMasterEdit(MasterCreateDto master);

	public ResponseModel<Void> setMasterEdit(List<MasterCreateDto> list);

	public ResponseModel<Void> setMasterEdit(MasterUpdateDto master);

	public ResponseModel<Void> updateMaster(MasterUpdateDto master);

	public ResponseModel<Void> updateMaster(MasterEditDto master);

	public MasterUpdateDto getMasterOriginalByMasterID(String masterID);

	public List<MasterEditSearchInfoAdminDto> getMaserEditListByMasterID(MasterEditSearchArgsAdminDto argsDto);

	public List<MasterEditSearchInfoMemberDto> getMasterMemberEditedListWithFilter(MasterSearchArgsMemberEditedDto argsDto);

	public MasterEditDto getMasterEditByEditID(String editID);

	public MasterEditDto getMasterEditByEditID(String editID, String memberID);

	public List<MasterTagEditDto> getMasterTagEditListByEditID(String editID);

	public ResponseModel<Void> updateMasterEditAllNotInUsed(String masterID);

	public ResponseModel<Void> updateMasterEditAllNotInUsed(List<String> masterIDList);

	public ResponseModel<Void> updateMasterEditInUsed(String editID);

	public ResponseModel<Void> deleteMasterTagByMasterID(String masterID);

	public ResponseModel<Void> deleteMasterTagByMasterID(List<String> masterIDList);

	public List<String> getMasterEditIDListByMasterID(String masterID);

	public ResponseModel<Void> deleteMasterTagEditByEditIDList(List<String> editIDList);

	public ResponseModel<Void> deleteMasterEditByMasterID(String masterID);

	public ResponseModel<Void> deleteMasterMemberFollowedByMasterID(String masterID);

	public ResponseModel<Void> deleteEventRecommendByMasterID(String masterID);

	public ResponseModel<Void> deletePlaceRecommendByMasterID(String masterID);

	public ResponseModel<Void> deletePlaceRecommendEditByMasterID(String masterID);

	public ResponseModel<Void> deleteMasterByMasterID(String masterID);

	public ResponseModel<Void> deleteMasterEditByEditID(String editID);

	public ResponseModel<Void> deleteMasterTagEditByEditID(String editID);
	
	public ResponseModel<Void> updateMasterClick(String masterID);

	/* ADMIN */
	public List<MasterSearchInfoAdminDto> getMasterListWithFilter(MasterSearchArgsAdminDto argsDto);
}
