package tw.com.reinbach.Atenore.Model.category.master.dto;

import io.swagger.annotations.ApiModelProperty;

public class MasterListSearchByTypeArgsDto {

	@ApiModelProperty(hidden = true)
	private Integer typeID;
	
	private String lastName;
	
	private String firstName;
	
	public Integer getTypeID() {
		return typeID;
	}

	public void setTypeID(Integer typeID) {
		this.typeID = typeID;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
}
