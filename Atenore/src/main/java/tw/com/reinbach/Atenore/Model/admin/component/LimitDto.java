package tw.com.reinbach.Atenore.Model.admin.component;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class LimitDto {

	private String limitID;
	
	private String name;
	
	private String limitGroup;
	
	private String action;
	
	private Boolean limitValue;

	public String getLimitID() {
		return limitID;
	}

	public void setLimitID(String limitID) {
		this.limitID = limitID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLimitGroup() {
		return limitGroup;
	}

	public void setLimitGroup(String limitGroup) {
		this.limitGroup = limitGroup;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Boolean getLimitValue() {
		return limitValue;
	}

	public void setLimitValue(Boolean limitValue) {
		this.limitValue = limitValue;
	}
}
