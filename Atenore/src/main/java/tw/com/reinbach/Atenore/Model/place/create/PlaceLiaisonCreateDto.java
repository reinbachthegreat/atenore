package tw.com.reinbach.Atenore.Model.place.create;

import io.swagger.annotations.ApiModelProperty;

public class PlaceLiaisonCreateDto {

	@ApiModelProperty(hidden = true)
	private String liaisonID;

	@ApiModelProperty(hidden = true)
	private String placeID;

	@ApiModelProperty(value = "聯絡人", example = "聯絡人真名")
	private String name;

	@ApiModelProperty(value = "聯絡電話", example = "0911111111")
	private String phone;

	@ApiModelProperty(hidden = true)
	private String isMain;
	
	/*
	 * Extend attribute, for PlaceLiaison Edit Table. By controlling version.
	 * 
	 */
	@ApiModelProperty(value = "場所聯絡資訊歷史編輯編號", hidden = true)
	private String editID;


	public String getLiaisonID() {
		return liaisonID;
	}

	public void setLiaisonID(String liaisonID) {
		this.liaisonID = liaisonID;
	}

	public String getPlaceID() {
		return placeID;
	}

	public void setPlaceID(String placeID) {
		this.placeID = placeID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getIsMain() {
		return isMain;
	}

	public void setIsMain(String isMain) {
		this.isMain = isMain;
	}

	public String getEditID() {
		return editID;
	}

	public void setEditID(String editID) {
		this.editID = editID;
	}

}
