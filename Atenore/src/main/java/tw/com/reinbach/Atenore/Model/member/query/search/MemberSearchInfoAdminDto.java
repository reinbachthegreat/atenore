package tw.com.reinbach.Atenore.Model.member.query.search;

import java.time.LocalDateTime;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MemberSearchInfoAdminDto {

	@ApiModelProperty(example = "75c6538b-facd-461b-b143-919c0913028c")
	private String memberID;
	
	@ApiModelProperty(example = "reinbachrddev@gmail.com")
	private String account;
	
	@ApiModelProperty(example = "Sora醬")
	private String publicName;
	
	@ApiModelProperty(example = "Lee")
	private String lastName;
	
	@ApiModelProperty(example = "Anita")
	private String firstName;
	
	@ApiModelProperty(value = "Member's account status.", example = "1")
	private Integer status;

	@ApiModelProperty(example = "2020-09-30 09:30:45", dataType = "LocalDateTime")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime lastLogin;
	
	@ApiModelProperty(hidden = true, value = "Member's account established time.", example = "2020-07-10 10:00:00", dataType = "LocalDateTime")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime insertTime;

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPublicName() {
		return publicName;
	}

	public void setPublicName(String publicName) {
		this.publicName = publicName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public LocalDateTime getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(LocalDateTime lastLogin) {
		this.lastLogin = lastLogin;
	}

	public LocalDateTime getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(LocalDateTime insertTime) {
		this.insertTime = insertTime;
	}
}
