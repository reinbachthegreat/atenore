package tw.com.reinbach.Atenore.Model.category.master.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import tw.com.reinbach.Atenore.Model.category.master.dto.component.MasterMainTypeAdminDto;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MasterMainTypeListInfoDto {

	private List<MasterMainTypeAdminDto> masterMainTypeList;

	public List<MasterMainTypeAdminDto> getMasterMainTypeList() {
		return masterMainTypeList;
	}

	public void setMasterMainTypeList(List<MasterMainTypeAdminDto> masterMainTypeList) {
		this.masterMainTypeList = masterMainTypeList;
	}
	
}
