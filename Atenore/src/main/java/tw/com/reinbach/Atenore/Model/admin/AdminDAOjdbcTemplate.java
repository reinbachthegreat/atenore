package tw.com.reinbach.Atenore.Model.admin;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import tw.com.reinbach.Atenore.Model.admin.component.FeatureLimitDto;
import tw.com.reinbach.Atenore.Model.admin.component.LimitDto;
import tw.com.reinbach.Atenore.Model.admin.create.AdminCreateDto;
import tw.com.reinbach.Atenore.Model.admin.create.FeatureCreateDto;
import tw.com.reinbach.Atenore.Model.admin.query.AdminInfoDto;
import tw.com.reinbach.Atenore.Model.admin.query.FeatureInfoDto;
import tw.com.reinbach.Atenore.Model.admin.query.search.AdminSearchInfoDto;
import tw.com.reinbach.Atenore.Model.admin.update.AdminUpdateDto;
import tw.com.reinbach.Atenore.Model.admin.update.FeatureUpdateDto;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;

@Repository
public class AdminDAOjdbcTemplate implements AdminDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private NamedParameterJdbcTemplate namedJdbcTemplate;
	
	@Override
	public List<AdminSearchInfoDto> getAdminByFeatureID(String featureID){
		try {
			StringBuilder sql =  new StringBuilder("select a.adminID, a.featureID, f.name as 'featureName', a.account, a.name, a.mail from admin a  "
					+ " left join feature f on a.featureID = f.featureID "
					+ " where 1=1  ");
			if(featureID!=null && featureID.length()!=0) {
				sql.append(" and a.featureID = ?  order by a.insertTime desc ");
				return jdbcTemplate.query(sql.toString(), new Object[]{featureID}, new BeanPropertyRowMapper<AdminSearchInfoDto>(AdminSearchInfoDto.class));
			}else {
				sql.append(" order by a.insertTime desc ");
				return jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper<AdminSearchInfoDto>(AdminSearchInfoDto.class));
			}
			
		}catch(DataAccessException e) {
			return null;
		}
	}
	
	@Override
	public AdminInfoDto getAdminByAdminID(String adminID) {
		try {
			String sql = "select a.adminID, a.featureID, f.name as 'featureName', a.account, a.name, a.mail from admin a  "
					+ " left join feature f on a.featureID = f.featureID "
					+ " where a.adminID = ?  ";
			return jdbcTemplate.queryForObject(sql.toString(), new Object[]{adminID}, new BeanPropertyRowMapper<AdminInfoDto>(AdminInfoDto.class));
		}catch(DataAccessException e) {
			return null;
		}		
	}
	
	@Override
	public ResponseModel<Void> setAdmin(AdminCreateDto admin){
		try {
			String sql = "insert into admin (adminID, featureID, account, pwd, name, mail, insertTime) "
					+ " values(:adminID, :featureID, :account, :pwd, :name, :mail, now()) ";
			
			SqlParameterSource ps = new BeanPropertySqlParameterSource(admin);
			int count = namedJdbcTemplate.update(sql, ps);
			
			if(count!=0) {
				return new ResponseModel<>(201, "");
			}else {
				return new ResponseModel<>(409, "");
			}
		}catch(DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}
	
	@Override
	public ResponseModel<Void> updateAdmin(AdminUpdateDto admin){
		try {
			String sql = "update admin set featureID = :featureID, account = :account, pwd = :pwd, name = :name, mail = :mail "
					+ " where adminID = :adminID";
			
			SqlParameterSource ps = new BeanPropertySqlParameterSource(admin);
			int count = namedJdbcTemplate.update(sql, ps);
			
			if(count!=0) {
				return new ResponseModel<>(204, "");
			}else {
				return new ResponseModel<>(409, "");
			}
		}catch(DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}
	
	@Override
	public AdminUpdateDto getAdminForUpdateByAdminID(String adminID) {
		try {
			String sql = "select a.adminID, a.pwd,a.featureID, a.account, a.name, a.mail from admin a  "
					+ " where a.adminID = ?  ";
			return jdbcTemplate.queryForObject(sql.toString(), new Object[]{adminID}, new BeanPropertyRowMapper<AdminUpdateDto>(AdminUpdateDto.class));
		}catch(DataAccessException e) {
			return null;
		}
	}
	
	@Override
	public ResponseModel<Void> deleteAdminByAdminID(String adminID){
		try {
			String sql = "delete from admin where adminID = ?";
			jdbcTemplate.update(sql, new Object[] {adminID});
			return new ResponseModel<>(204, "");
		}catch(DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}
	
	@Override
	public List<FeatureInfoDto> getFeatureList(){
		try {
			String sql =  "select * from feature ";
				return jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper<FeatureInfoDto>(FeatureInfoDto.class));
		}catch(DataAccessException e) {
			return null;
		}
	}
	
	@Override
	public FeatureInfoDto getFeatureByFeaturID(String featureID) {
		try {
			String sql =  "select * from feature where featureID = ?";
				return jdbcTemplate.queryForObject(sql.toString(), new Object[] {featureID},new BeanPropertyRowMapper<FeatureInfoDto>(FeatureInfoDto.class));
		}catch(DataAccessException e) {
			return null;
		}		
	}
	
	@Override
	public FeatureUpdateDto getFeatureForUpdateByFeaturID(String featureID) {
		try {
			String sql =  "select * from feature where featureID = ?";
				return jdbcTemplate.queryForObject(sql.toString(), new Object[] {featureID},new BeanPropertyRowMapper<FeatureUpdateDto>(FeatureUpdateDto.class));
		}catch(DataAccessException e) {
			return null;
		}		
	}
	
	@Override
	public List<LimitDto> getLimitListByFeatureID(String featureID){
		try {
			String sql =  "select l.*, f.limitValue from featurelimit f"
					+ " left join limited l on l.limitID = f.limitID "
					+ " where f.featureID = ? ";
				return jdbcTemplate.query(sql, new Object[] {featureID}, new BeanPropertyRowMapper<LimitDto>(LimitDto.class));
		}catch(DataAccessException e) {
			return null;
		}
	}
	
	@Override
	public ResponseModel<Void> setFeature(FeatureCreateDto feature){
		try {
			String sql = "insert into feature (featureID, name, updateTime) "
					+ " values(:featureID, :name, now())";
			
			SqlParameterSource ps = new BeanPropertySqlParameterSource(feature);
			int count = namedJdbcTemplate.update(sql, ps);
			
			if(count!=0) {
				return new ResponseModel<>(201, "");
			}else {
				return new ResponseModel<>(409, "");
			}
		}catch(DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}
	
	@Override
	public ResponseModel<Void> setFeatureLimitList(List<FeatureLimitDto> limitList){
		try {
			String sql = "insert into featurelimit (featureID, limitID, limitValue) values(:featureID, :limitID, :limitValue)";

			List<SqlParameterSource> psList = new ArrayList<SqlParameterSource>();
			for (FeatureLimitDto dto : limitList) {
				psList.add(new BeanPropertySqlParameterSource(dto));
			}
			SqlParameterSource[] psArry = new SqlParameterSource[psList.size()];
			psList.toArray(psArry);
			int[] countList = namedJdbcTemplate.batchUpdate(sql, psArry);
			if(this.resultIfBatchUpdateSuccess(countList)) {
				return new ResponseModel<Void>(201, "");	
			}else {
				return new ResponseModel<Void>(201, "Some of " + "");
			}
		} catch (DataAccessException e) {
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}
	
	@Override
	public ResponseModel<Void> deleteLimitListByFeatureID(String featureID){
		try {
			String sql = "delete from featurelimit where featureID = ?";
			jdbcTemplate.update(sql, new Object[] {featureID});
			return new ResponseModel<>(204, "");
		}catch(DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}
	
	@Override
	public List<LimitDto> getLimitList(){
		try {
			String sql =  "select * from limited ";
				return jdbcTemplate.query(sql, new BeanPropertyRowMapper<LimitDto>(LimitDto.class));
		}catch(DataAccessException e) {
			return null;
		}
	}
	
	@Override
	public ResponseModel<Void> updateFeature(FeatureUpdateDto feature){
		try {
			String sql = "update feature set name = :name, updateTime = now()"
					+ " where featureID = :featureID";
			
			SqlParameterSource ps = new BeanPropertySqlParameterSource(feature);
			int count = namedJdbcTemplate.update(sql, ps);
			
			if(count!=0) {
				return new ResponseModel<>(204, "");
			}else {
				return new ResponseModel<>(409, "");
			}
		}catch(DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}
	
	@Override
	public ResponseModel<Void> deleteFeatureByFeatureID(String featureID){
		try {
			String sql = "delete from feature where featureID = ?";
			jdbcTemplate.update(sql, new Object[] {featureID});
			return new ResponseModel<>(204, "");
		}catch(DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}
	
	private Boolean resultIfBatchUpdateSuccess(int[] countList) {
		Boolean flag = true;
		for(int count : countList) {
			if(count==0) {
				flag = false;
				break;
			}
		}
		return flag;
	}
	
	@Override
	public AdminInfoDto getAdminByAccount(String account) {
		try {
//			String sql = "select a.adminID, a.pwd, a.featureID, f.name as 'featureName', a.account, a.name from admin a  "
//					+ " left join feature f on a.featureID = f.featureID "
//					+ " where a.account = ? ";
			String sql = "select a.adminID, a.pwd, a.featureID, a.name from admin a  "
					+ " left join feature f on a.featureID = f.featureID "
					+ " where a.account = ? ";
			return jdbcTemplate.queryForObject(sql.toString(), new Object[]{account}, new BeanPropertyRowMapper<AdminInfoDto>(AdminInfoDto.class));
		}catch(DataAccessException e) {
			return null;
		}
	}

//	@Override
//	public Map<String, Object> getUserData(String account) {
//
//		try {
//			String sqltext = "select adminID, f.name as featureName, account, pwd, a.name as userName, email, display, insertTime "
//					+ "from admin as a left join feature as f on f.featureID = a.featureID where account = ? ";
//			Map<String, Object> userDataMap = jdbcTemplate.queryForMap(sqltext, account);
//			return userDataMap;
//		} catch (DataAccessException e) {
//			e.printStackTrace();
//			return null;
//		}
//	}
//
//	@Override
//	public List<Map<String, Object>> checkAccountAndPwd(AdminDto bean) {
//		// 核對帳號密碼並取得帳號資料 OK
//		try {
//			String sqltext = "select * from admin where Account = ? and Pwd = ? and Display = 'true' ";
//			List<Map<String, Object>> list = jdbcTemplate.queryForList(sqltext, bean.getAccount(), bean.getPwd());
//			return list;
//		} catch (DataAccessException e) {
//			e.printStackTrace();
//			return null;
//		}
//	}
//
//	@Override
//	public List<Map<String, Object>> findByID(String adminID) {
//		// 查詢單筆帳號by adminID OK
//		try {
//			String sqltext = "select * from admin where AdminID = ? ";
//			List<Map<String, Object>> list = jdbcTemplate.queryForList(sqltext, adminID);
//			return list;
//		} catch (DataAccessException e) {
//			e.printStackTrace();
//			return null;
//		}
//	}
//
//	@Override
//	public Map<String, Object> findByAccountAll(String account) {
////	查詢單筆帳號by account
//		try {
//			String sqltext = "select * from admin where Account = ? ";
//			return jdbcTemplate.queryForMap(sqltext, account);
//		} catch (DataAccessException e) {
//			e.printStackTrace();
//			return null;
//		}
//	}
//
//	@Override
//	public List<Map<String, Object>> findByAccount(String account) {
//		// 查詢是否有重複帳號by account OK
//		try {
//			String sqltext = "select AdminID, Account, Name, Email from admin where Account = ? ";
//			List<Map<String, Object>> list = jdbcTemplate.queryForList(sqltext, account);
//			return list;
//		} catch (DataAccessException e) {
//			e.printStackTrace();
//			return null;
//		}
//	}
//
//	@Override
//	public List<Map<String, Object>> getALLAdminAcount(int begin, int end, String featureID, String order) {
//		// 顯示管理者帳號清單 OK
//		try {
//			String sqltext = null;
//			List<Map<String, Object>> list = null;
//			if (featureID.equalsIgnoreCase("All")) {
//				sqltext = "select ad.AdminID, ad.FeatureID, ad.Account, ad.Name as UserName, ad.Email, ad.Display, f.Name as FeatureName, "
//						+ "(select count(*) from admin ad left join feature f on ad.FeatureID = f.FeatureID) count "
//						+ "from admin ad left join feature f on ad.FeatureID = f.FeatureID where ad.Display = 'true' "
//						+ "order by ad.AdminID " + order + " Limit " + (begin - 1) + ", " + end;
//				list = jdbcTemplate.queryForList(sqltext);
//			} else {
//				sqltext = "select ad.AdminID, ad.FeatureID, ad.Account, ad.Name as UserName, ad.Email, ad.Display, f.Name as FeatureName, "
//						+ "(select count(*) from admin ad left join feature f on ad.FeatureID = f.FeatureID where ad.FeatureID = ?) count "
//						+ "from admin ad left join feature f on ad.FeatureID = f.FeatureID where ad.FeatureID = ? and ad.Display = 'true' "
//						+ "order by ad.AdminID " + order + " Limit " + (begin - 1) + ", " + end;
//				list = jdbcTemplate.queryForList(sqltext, featureID, featureID);
//			}
//			return list;
//		} catch (DataAccessException e) {
//			e.printStackTrace();
//			return null;
//		}
//	}
//
//	@Override
//	public int deleteAdmin(String adminID) {
//		// 刪除管理者 OK
//		try {
//			String sqltext = "update admin set Display = 'false' where AdminID = ? ";
//			int count = jdbcTemplate.update(sqltext, adminID);
//			return count;
//		} catch (DataAccessException e) {
//			e.printStackTrace();
//			return 0;
//		}
//	}
//
//	@Override
//	public int updatePwd(String account, String pwd) {
//		// 變更密碼 OK
//		try {
//			String sqltext = "update admin set Pwd = ? where Account = ? ";
//			int count = jdbcTemplate.update(sqltext, pwd, account);
//			return count;
//		} catch (DataAccessException e) {
//			e.printStackTrace();
//			return 0;
//		}
//	}
//
//	@Override
//	public int insertAdmin(AdminDto bean, String adminID) {
//		// 新增管理者 OK
//		try {
//			String sqltext = "insert into admin (AdminID, FeatureID, Account, Pwd, Name, Email, Display, InsertTime) values (?,?,?,?,?,?, 'true', now())";
//			int count = jdbcTemplate.update(sqltext, adminID, bean.getFeatureID(), bean.getAccount(), bean.getPwd(),
//					bean.getName(), bean.getEmail());
//			return count;
//		} catch (DataAccessException e) {
//			e.printStackTrace();
//			return 0;
//		}
//	}
//
//	@Override
//	public int updateAdminInfo(AdminDto bean) {
//		// 變更管理者資料
//		try {
//			String sqltext = "update admin set Name = ?, Email = ? where AdminID = ? ";
//			int count = jdbcTemplate.update(sqltext, bean.getName(), bean.getEmail(), bean.getAdminID());
//			return count;
//		} catch (DataAccessException e) {
//			e.printStackTrace();
//			return 0;
//		}
//	}
//
//	@Override
//	public int updateAdminLimit(String adminID, String featureID) {
//		// 變更管理者權限
//		try {
//			String sqltext = "update admin set FeatureID = ? where AdminID = ? ";
//			int count = jdbcTemplate.update(sqltext, featureID, adminID);
//			return count;
//		} catch (DataAccessException e) {
//			e.printStackTrace();
//			return 0;
//		}
//	}
//
//	/********************************************************************************/
//
//	@Override
//	public List<Map<String, Object>> getALLFeature() {
//		// 顯示所有管理者身分別
//		try {
//			String sqltext = "select * from feature";
//			List<Map<String, Object>> list = jdbcTemplate.queryForList(sqltext);
//			return list;
//		} catch (DataAccessException e) {
//			e.printStackTrace();
//			return null;
//		}
//	}
//
//	@Override
//	public List<Map<String, Object>> getALLFeature(int begin, int end, String order) {
//		// 顯示所有管理者身分別 (有排序)
//		try {
//			String sqltext = "select *, (select count(*) from feature) count from feature order by FeatureID " + order
//					+ " limit " + (begin - 1) + "," + end;
//			List<Map<String, Object>> list = jdbcTemplate.queryForList(sqltext);
//			return list;
//		} catch (DataAccessException e) {
//			e.printStackTrace();
//			return null;
//		}
//	}
//
//	@Override
//	public int insertFeature(String featureID, String name) {
//		// 新增管理者身分別
//		try {
//			String sqltext = "insert into feature (FeatureID, Name, UpdateTime) values (?, ?, now())";
//			int count = jdbcTemplate.update(sqltext, featureID, name);
//			return count;
//		} catch (DataAccessException e) {
//			e.printStackTrace();
//			return 0;
//		}
//	}
//
//	@Override
//	public int updateFeature(String featureID, String name) {
//		// 編輯管理者身分別
//		try {
//			String sqltext = "update feature set Name = ?, UpdateTime = now() where FeatureID = ? ";
//			int count = jdbcTemplate.update(sqltext, name, featureID);
//			return count;
//		} catch (DataAccessException e) {
//			e.printStackTrace();
//			return 0;
//		}
//	}
//
//	@Override
//	public int deleteFeature(String featureID) {
//		// 刪除管理者身分別
//		try {
//			String sqltext = "delete from feature where FeatureID = ? ";
//			int count = jdbcTemplate.update(sqltext, featureID);
//			return count;
//		} catch (DataAccessException e) {
//			e.printStackTrace();
//			return 0;
//		}
//	}
//
//	@Override
//	public List<Map<String, Object>> findLimitByFeatureID(String featureID) {
//		// 查詢身分別權限
//		try {
//			String sqltext = "select * from featurelimit fl join feature f on fl.FeatureID = f.FeatureID where fl.FeatureID = ? ";
//			List<Map<String, Object>> list = jdbcTemplate.queryForList(sqltext, featureID);
//			return list;
//		} catch (DataAccessException e) {
//			e.printStackTrace();
//			return null;
//		}
//	}
//
//	@Override
//	public int[] insertFeatureLimit(String featureID, List<Map<String, Object>> limitIDList) {
//		// 新增身分別權限
//		try {
//			int[] count = jdbcTemplate.batchUpdate(
//					"insert into featurelimit (FeatureID, LimitID, LimitValue) VALUES (?,?,?);",
//					new BatchPreparedStatementSetter() {
//
//						@Override
//						public void setValues(PreparedStatement ps, int i) throws SQLException {
//							String limitID = limitIDList.get(i).get("limitID").toString();
//							String limitValue = limitIDList.get(i).get("limitValue").toString();
//							ps.setString(1, featureID);
//							ps.setString(2, limitID);
//							ps.setString(3, limitValue);
//						}
//
//						@Override
//						public int getBatchSize() {
//							return limitIDList.size();
//						}
//					});
//			return count;
//		} catch (DataAccessException e) {
//			e.printStackTrace();
//			return null;
//		}
//	}
//
//	@Override
//	public int deleteFeatureLimitByFeatureID(String featureID) {
//		// 刪除身分別權限
//		try {
//			String sqltext = "delete from featurelimit where FeatureID = ? ";
//			int count = jdbcTemplate.update(sqltext, featureID);
//			return count;
//		} catch (DataAccessException e) {
//			e.printStackTrace();
//			return 0;
//		}
//	}
//
//	/**********************************************************************************/
//
//	@Override
//	public List<Map<String, Object>> getFeaturelimitByID(String featureID) {
//		// 查詢管理者權限
//		try {
//			String sqltext = "select * from featurelimit where FeatureID = ?  and LimitValue = 'true' ";
//			List<Map<String, Object>> list = jdbcTemplate.queryForList(sqltext, featureID);
//			return list;
//		} catch (DataAccessException e) {
//			e.printStackTrace();
//			return null;
//		}
//	}
//
//	@Override
//	public List<Map<String, Object>> getAllLimitID() {
//		// 查詢所有權限
//		try {
//			String sqltext = "select LimitID from limited";
//			List<Map<String, Object>> list = jdbcTemplate.queryForList(sqltext);
//			return list;
//		} catch (DataAccessException e) {
//			e.printStackTrace();
//			return null;
//		}
//	}
//
//	@Override
//	public List<Map<String, Object>> getLimitValue(String limitName) {
//		try {
//			String sqltext = "select fl.LimitID, fl.LimitValue from featurelimit fl join feature f on fl.FeatureID = f.FeatureID where f.Name = ?";
//			List<Map<String, Object>> list = jdbcTemplate.queryForList(sqltext, limitName);
//			return list;
//		} catch (DataAccessException e) {
//			e.printStackTrace();
//			return null;
//		}
//	}

}
