package tw.com.reinbach.Atenore.Model.validate;

import java.util.Map;

import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;

public interface ValidateDAO {

	public Map<String, Object> signIn(SignInDto dto);

	public Map<String, Object> signInSSO(SignInDto dto);

	public int signUp(SignUpDto dto);

	public int setMemmberDetail(SignUpDto dto);

	public Integer getMemberStatusByMemberID(String memberID);

	public int updateMemberStatus(String memberID, Integer statusCode);

	public Integer getMemberCountByAccount(String account);

	public AccountStatusInfoDto getAccontStatus(String account);

	public ResponseModel<Void> updateVerifyTokenByMember(String memberID, String verifyToken);

	public String getAccountByToken(String token);

	public Integer resetPwdByToken(String token, String pwd);

}
