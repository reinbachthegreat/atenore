package tw.com.reinbach.Atenore.Model.category.place.dto;

import io.swagger.annotations.ApiModelProperty;

public class PlaceListSearchByTypeArgsDto {

	@ApiModelProperty(hidden = true)
	private Integer typeID;
	
	@ApiModelProperty(value = "fuzzy searching by name keywords", required = false, example = "醫")
	private String fuzzyFilter;

	@ApiModelProperty(example = "新北市")
	private String county;

	public Integer getTypeID() {
		return typeID;
	}

	public void setTypeID(Integer typeID) {
		this.typeID = typeID;
	}

	public String getFuzzyFilter() {
		return fuzzyFilter;
	}

	public void setFuzzyFilter(String fuzzyFilter) {
		this.fuzzyFilter = fuzzyFilter;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}
	
	
	
	
}
