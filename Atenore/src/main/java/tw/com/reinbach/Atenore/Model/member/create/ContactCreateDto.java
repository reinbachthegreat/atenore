package tw.com.reinbach.Atenore.Model.member.create;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;

public class ContactCreateDto {

	@ApiModelProperty(hidden = true)
	private String uuid;

	@NotEmpty(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@Size(max = 20, message = ApiError.INPUT_LENGTH_MISMATCHED)
	@ApiModelProperty(required = true)
	private String name;

	@Size(max = 20, message = ApiError.INPUT_LENGTH_MISMATCHED)
	private String phone;

	@Size(max = 100, message = ApiError.INPUT_LENGTH_MISMATCHED)
	private String mail;

	@NotEmpty(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@ApiModelProperty(required = true)
	private String content;

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
