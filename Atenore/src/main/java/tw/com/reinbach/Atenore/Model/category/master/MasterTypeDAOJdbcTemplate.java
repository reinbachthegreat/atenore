package tw.com.reinbach.Atenore.Model.category.master;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import tw.com.reinbach.Atenore.Model.category.master.dto.MasterListSearchByMainTypeArgsDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterListSearchByMiddleTypeArgsDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterListSearchByTypeArgsDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterSearchByMainTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterSearchByMiddleTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterSearchByTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.component.MasterMainTypeAdminDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.component.MasterMainTypeDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.component.MasterMiddleTypeAdminDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.component.MasterMiddleTypeDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.component.MasterTypeAdminDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.component.MasterTypeDto;

@Repository
public class MasterTypeDAOJdbcTemplate implements MasterTypeDAO {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private NamedParameterJdbcTemplate namedJdbcTemplate;

	@Override
	public List<MasterMainTypeDto> getMasterMainTypeList() {
		try {
			String sql = "select mmt.*, ifnull(mmtalive.aliveQty,0) as 'aliveQty', ifnull(mmtdead.deadQty,0) as 'deadQty', ifnull(mmtall.totalQty,0) as 'totalQty' from mastermaintype mmt left join " + 
					"(select mmt.*, ifnull(count(m.masterID), 0) as 'deadQty' from master m left join " + 
					"	mastertype mt on mt.typeID = m.typeID left join " + 
					"    mastermidtype mit on mt.midID = mit.midID left join " + 
					"    mastermaintype as mmt on mmt.mainID = mit.mainID where m.alive = 'false' group by mmt.mainID) as mmtdead " + 
					"on mmtdead.mainID = mmt.mainID left join " + 
					"(select mmt.*, count(m.masterID) as 'aliveQty' from master m left join " + 
					"	mastertype mt on mt.typeID = m.typeID left join " + 
					"    mastermidtype mit on mt.midID = mit.midID left join " + 
					"    mastermaintype as mmt on mmt.mainID = mit.mainID where m.alive = 'true' group by mmt.mainID) as mmtalive " + 
					"on mmtalive.mainID = mmt.mainID left join " + 
					"(select mmt.*, count(m.masterID) as 'totalQty' from master m left join " + 
					"	mastertype mt on mt.typeID = m.typeID left join " + 
					"    mastermidtype mit on mt.midID = mit.midID left join " + 
					"    mastermaintype as mmt on mmt.mainID = mit.mainID group by mmt.mainID) as mmtall " + 
					"on mmtall.mainID = mmt.mainID order by mmt.mainID asc ";
			
			return jdbcTemplate.query(sql, new BeanPropertyRowMapper<MasterMainTypeDto>(MasterMainTypeDto.class));
		}catch(DataAccessException e) {
			return null;
		}
	}
	
	@Override
	public List<MasterMiddleTypeDto> getMasterMiddleTypeList(){
		try {
			String sql = "select * from mastermidtype";
			return jdbcTemplate.query(sql, new BeanPropertyRowMapper<MasterMiddleTypeDto>(MasterMiddleTypeDto.class));
		}catch(DataAccessException e) {
			return null;
		}		
	}

	@Override
	public List<MasterMiddleTypeDto> getMasterMiddleTypeListByMainType(Integer mainID) {
		try {
			String sql = "select mit.*, ifnull(mitalive.aliveQty,0) as 'aliveQty', ifnull(mitdead.deadQty,0) as 'deadQty', ifnull(mitall.totalQty,0) as 'totalQty' from mastermidtype mit left join \r\n" + 
					"(select mit.*, count(m.masterID) as 'deadQty' from master m left join\r\n" + 
					"	mastertype mt on mt.typeID = m.typeID left join \r\n" + 
					"    mastermidtype mit on mt.midID = mit.midID \r\n" + 
					"					where m.alive = 'false' null and mit.mainID = ? \r\n" + 
					"					group by mit.midID) as mitdead\r\n" + 
					"on mitdead.midID = mit.midID left join\r\n" + 
					"(select mit.*, count(m.masterID) as 'aliveQty' from master m left join\r\n" + 
					"	mastertype mt on mt.typeID = m.typeID left join \r\n" + 
					"    mastermidtype mit on mt.midID = mit.midID \r\n" + 
					"					where m.alive = 'true' and mit.mainID = ? \r\n" + 
					"					group by mit.mainID) as mitalive\r\n" + 
					"on mitalive.midID = mit.midID left join\r\n" + 
					"(select mit.*, count(m.masterID) as 'totalQty' from master m left join\r\n" + 
					"	mastertype mt on mt.typeID = m.typeID left join \r\n" + 
					"    mastermidtype mit on mt.midID = mit.midID \r\n" + 
					"					where mit.mainID = ? \r\n" + 
					"					group by mit.mainID) as mitall\r\n" + 
					"on mitall.midID = mit.midID \r\n" + 
					"where mit.mainID = ? \r\n" + 
					"order by mit.midID asc;";
			return jdbcTemplate.query(sql,new Object[] {mainID, mainID, mainID, mainID} ,new BeanPropertyRowMapper<MasterMiddleTypeDto>(MasterMiddleTypeDto.class));
		}catch(DataAccessException e) {
			return null;
		}
	}
	
	@Override
	public List<MasterTypeDto> getMasterTypeList(){
		try {
			String sql = "select * from mastertype";
			return jdbcTemplate.query(sql, new BeanPropertyRowMapper<MasterTypeDto>(MasterTypeDto.class));
		}catch(DataAccessException e) {
			return null;
		}		
	}

	@Override
	public List<MasterTypeDto> getMasterTypeListByMiddleType(Integer midID) {
		try {
			String sql = "select mt.*, ifnull(mtalive.aliveQty,0) as 'aliveQty', ifnull(mtdead.deadQty,0) as 'deadQty', ifnull(mtall.totalQty,0) as 'totalQty' from mastertype mt left join \r\n" + 
					"(select mt.*, count(m.masterID) as 'deadQty' from master m left join \r\n" + 
					"	mastertype mt on mt.typeID = m.typeID \r\n" + 
					"					where m.alive = 'false' and mt.midID = ? \r\n" + 
					"					group by mt.midID) as mtdead\r\n" + 
					"on mtdead.typeID = mt.typeID left join\r\n" + 
					"(select mt.*, count(m.masterID) as 'aliveQty' from master m left join \r\n" + 
					"	mastertype mt on mt.typeID = m.typeID \r\n" + 
					"					where m.alive = 'true' and mt.midID = ? \r\n" + 
					"					group by mt.midID) as mtalive\r\n" + 
					"on mtalive.typeID = mt.typeID left join\r\n" + 
					"(select mt.*, count(m.masterID) as 'totalQty' from master m left join \r\n" + 
					"	mastertype mt on mt.typeID = m.typeID \r\n" + 
					"					where mt.midID = ? \r\n" + 
					"					group by mt.midID) as mtall\r\n" + 
					"on mtall.typeID = mt.typeID \r\n" + 
					"where mt.midID = ? \r\n" + 
					"order by mt.typeID asc;";
			return jdbcTemplate.query(sql, new Object[] {midID, midID, midID, midID} , new BeanPropertyRowMapper<MasterTypeDto>(MasterTypeDto.class));
		}catch(DataAccessException e) {
			return null;
		}
	}
	
	@Override
	public List<MasterMainTypeAdminDto> getMasterMainTypeListWithFilter(String fuzzyFilter){
		try {
			StringBuilder sql = new StringBuilder("select mmt.*, ifnull(mmtalive.aliveQty,0) as 'aliveQty', ifnull(mmtdead.deadQty,0) as 'deadQty', ifnull(mmtall.totalQty,0) as 'totalQty' from mastermaintype mmt left join " + 
					" (select mmt.*, ifnull(count(m.masterID), 0) as 'deadQty' from master m left join " + 
					"	mastertype mt on mt.typeID = m.typeID left join " + 
					"    mastermidtype mit on mt.midID = mit.midID left join " + 
					"    mastermaintype as mmt on mmt.mainID = mit.mainID where m.alive = false group by mmt.mainID) as mmtdead " + 
					" on mmtdead.mainID = mmt.mainID left join " + 
					" (select mmt.*, count(m.masterID) as 'aliveQty' from master m left join " + 
					"	mastertype mt on mt.typeID = m.typeID left join " + 
					"    mastermidtype mit on mt.midID = mit.midID left join " + 
					"    mastermaintype as mmt on mmt.mainID = mit.mainID where m.alive = true group by mmt.mainID) as mmtalive " + 
					" on mmtalive.mainID = mmt.mainID left join " + 
					" (select mmt.*, count(m.masterID) as 'totalQty' from master m left join " + 
					"	mastertype mt on mt.typeID = m.typeID left join " + 
					"    mastermidtype mit on mt.midID = mit.midID left join " + 
					"    mastermaintype as mmt on mmt.mainID = mit.mainID group by mmt.mainID) as mmtall " + 
					" on mmtall.mainID = mmt.mainID ");
			
			if(fuzzyFilter!=null) {
				sql.append(" where mmt.name like '%' ? '%' order by mmt.mainID asc  ");
				return jdbcTemplate.query(sql.toString(), new Object[] {fuzzyFilter}, new BeanPropertyRowMapper<MasterMainTypeAdminDto>(MasterMainTypeAdminDto.class));
			}else {
				sql.append(" order by mmt.mainID asc ");
				return jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper<MasterMainTypeAdminDto>(MasterMainTypeAdminDto.class));
			}
		}catch(DataAccessException e) {
			return null;
		}
	}
	
	@Override
	public List<MasterSearchByMainTypeInfoDto> getMasterListByMainTypeWithFilter(MasterListSearchByMainTypeArgsDto argsDto){
		try {
			StringBuilder sql = new StringBuilder(" select m.masterID, mmt.mainID, mmt.name as 'mainTypeName', m.lastname, m.firstname, m.phone, m.memberID from master m "
					+ " left join mastertype mt on m.typeID = mt.typeID "
					+ " left join mastermidtype mit on mit.midID = mt.midID "
					+ " left join mastermaintype mmt on mmt.mainID = mit.mainID "
					+ " where mmt.mainID = :mainID ");
			
			if(argsDto.getLastName()!=null && argsDto.getLastName().trim().length()!=0) {
				sql.append(" and m.lastname like '%' :lastName '%' ");
			}
			
			if(argsDto.getFirstName()!=null && argsDto.getFirstName().trim().length()!=0){
				sql.append(" and m.firstName like '%' :firstName '%' ");
			}
			
			sql.append(" order by m.insertTime desc");
			
			SqlParameterSource ps = new BeanPropertySqlParameterSource(argsDto);
			return namedJdbcTemplate.query(sql.toString(), ps, new BeanPropertyRowMapper<MasterSearchByMainTypeInfoDto>(MasterSearchByMainTypeInfoDto.class));
		}catch(DataAccessException e) {
			return null;
		}
	}
	
	@Override
	public List<MasterMiddleTypeAdminDto> getMasterMiddleTypeListByMainTypeWithFilter(Integer mainID, String fuzzyFilter){
		try {
			StringBuilder sql = new StringBuilder("select mit.midID, mit.name, mit.mainID, mmt.name as 'mainTypeName', ifnull(mitalive.aliveQty,0) as 'aliveQty', ifnull(mitdead.deadQty,0) as 'deadQty', ifnull(mitall.totalQty,0) as 'totalQty' from mastermidtype mit  "
					+ " left join mastermaintype mmt on mmt.mainID = mit.mainID "
					+ " left join (select mit.*, count(m.masterID) as 'deadQty' from master m left join\r\n" + 
					"	mastertype mt on mt.typeID = m.typeID left join \r\n" + 
					"    mastermidtype mit on mt.midID = mit.midID \r\n" + 
					"					where m.alive = false and mit.mainID = ? \r\n" + 
					"					group by mit.midID) as mitdead\r\n" + 
					"on mitdead.midID = mit.midID left join\r\n" + 
					"(select mit.*, count(m.masterID) as 'aliveQty' from master m left join\r\n" + 
					"	mastertype mt on mt.typeID = m.typeID left join \r\n" + 
					"    mastermidtype mit on mt.midID = mit.midID \r\n" + 
					"					where m.alive = true and mit.mainID = ? \r\n" + 
					"					group by mit.midID) as mitalive\r\n" + 
					"on mitalive.midID = mit.midID left join\r\n" + 
					"(select mit.*, count(m.masterID) as 'totalQty' from master m left join\r\n" + 
					"	mastertype mt on mt.typeID = m.typeID left join \r\n" + 
					"    mastermidtype mit on mt.midID = mit.midID \r\n" + 
					"					where mit.mainID = ? \r\n" + 
					"					group by mit.midID) as mitall\r\n" + 
					"on mitall.midID = mit.midID \r\n" + 
					"where mit.mainID = ? "); 
					
			
			if(fuzzyFilter!=null) {
				sql.append(" and mit.name like '%' ? '%' order by mit.midID asc ");
				return jdbcTemplate.query(sql.toString(), new Object[] {mainID, mainID, mainID, mainID, fuzzyFilter}, new BeanPropertyRowMapper<MasterMiddleTypeAdminDto>(MasterMiddleTypeAdminDto.class));
			}else {
				sql.append(" order by mit.midID asc ");
				return jdbcTemplate.query(sql.toString(), new Object[] {mainID, mainID, mainID, mainID}, new BeanPropertyRowMapper<MasterMiddleTypeAdminDto>(MasterMiddleTypeAdminDto.class));
			}
		}catch(DataAccessException e) {
			return null;
		}
	}
	
	@Override
	public List<MasterSearchByMiddleTypeInfoDto> getMasterListByMiddleTypeWithFilter(MasterListSearchByMiddleTypeArgsDto argsDto){
		try {
			StringBuilder sql = new StringBuilder(" select m.masterID, mmt.mainID, mmt.name as 'mainTypeName', mit.midID as 'middleID', mit.name as 'middleTypeName', m.lastname, m.firstname, m.phone, m.memberID from master m "
					+ " left join mastertype mt on m.typeID = mt.typeID "
					+ " left join mastermidtype mit on mit.midID = mt.midID "
					+ " left join mastermaintype mmt on mmt.mainID = mit.mainID "
					+ " where mit.midID = :midID ");
			
			if(argsDto.getLastName()!=null && argsDto.getLastName().trim().length()!=0) {
				sql.append(" and m.lastname like '%' :lastName '%' ");
			}
			
			if(argsDto.getFirstName()!=null && argsDto.getFirstName().trim().length()!=0){
				sql.append(" and m.firstName like '%' :firstName '%' ");
			}
			
			sql.append(" order by m.insertTime desc");
			
			SqlParameterSource ps = new BeanPropertySqlParameterSource(argsDto);
			return namedJdbcTemplate.query(sql.toString(), ps, new BeanPropertyRowMapper<MasterSearchByMiddleTypeInfoDto>(MasterSearchByMiddleTypeInfoDto.class));
		}catch(DataAccessException e) {
			return null;
		}		
	}
	
	@Override
	public List<MasterTypeAdminDto> getMasterTypeListByMiddleTypeWithFilter(Integer midID, String fuzzyFilter){
		try {
			StringBuilder sql = new StringBuilder("select mt.*, mit.name as 'middleTypeName', mmt.name as 'mainTypeName',  ifnull(mtalive.aliveQty,0) as 'aliveQty', ifnull(mtdead.deadQty,0) as 'deadQty', ifnull(mtall.totalQty,0) as 'totalQty' from mastertype mt "
					+ " left join mastermidtype mit on mit.midID = mt.midID\r\n" + 
					" left join mastermaintype mmt on mmt.mainID = mit.mainID"
					+ " left join (select mt.*, count(m.masterID) as 'deadQty' from master m left join \r\n" + 
					"	mastertype mt on mt.typeID = m.typeID \r\n" + 
					"					where m.alive = false and mt.midID = ? \r\n" + 
					"					group by mt.typeID) as mtdead\r\n" + 
					"on mtdead.typeID = mt.typeID left join\r\n" + 
					"(select mt.*, count(m.masterID) as 'aliveQty' from master m left join \r\n" + 
					"	mastertype mt on mt.typeID = m.typeID \r\n" + 
					"					where m.alive = true and mt.midID = ? \r\n" + 
					"					group by mt.typeID) as mtalive\r\n" + 
					"on mtalive.typeID = mt.typeID left join\r\n" + 
					"(select mt.*, count(m.masterID) as 'totalQty' from master m left join \r\n" + 
					"	mastertype mt on mt.typeID = m.typeID \r\n" + 
					"					where mt.midID = ? \r\n" + 
					"					group by mt.typeID) as mtall\r\n" + 
					"on mtall.typeID = mt.typeID \r\n" + 
					"where mt.midID = ?  ");
			
			if(fuzzyFilter!=null) {
				sql.append(" and mt.name like '%' ? '%' order by mt.typeID asc ");
				return jdbcTemplate.query(sql.toString(), new Object[] {midID, midID, midID, midID, fuzzyFilter} , new BeanPropertyRowMapper<MasterTypeAdminDto>(MasterTypeAdminDto.class));
			}else {
				sql.append(" order by mt.typeID asc ");
				return jdbcTemplate.query(sql.toString(), new Object[] {midID, midID, midID, midID} , new BeanPropertyRowMapper<MasterTypeAdminDto>(MasterTypeAdminDto.class));
			}
		}catch(DataAccessException e) {
			return null;
		}
	}
	
	@Override
	public List<MasterSearchByTypeInfoDto> getMasterListByTypeWithFilter(MasterListSearchByTypeArgsDto argsDto){
		try {
			StringBuilder sql = new StringBuilder("select m.masterID, mmt.mainID, mmt.name as 'mainTypeName', mit.midID as 'middleID', mit.name as 'middleTypeName', mt.typeID, mt.name as 'typeName', m.lastname, m.firstname, m.phone, m.memberID from master m "
					+ " left join mastertype mt on m.typeID = mt.typeID "
					+ " left join mastermidtype mit on mit.midID = mt.midID "
					+ " left join mastermaintype mmt on mmt.mainID = mit.mainID "
					+ " where mt.typeID = :typeID ");
			
			if(argsDto.getLastName()!=null && argsDto.getLastName().trim().length()!=0) {
				sql.append(" and m.lastname like '%' :lastName '%' ");
			}
			
			if(argsDto.getFirstName()!=null && argsDto.getFirstName().trim().length()!=0){
				sql.append(" and m.firstName like '%' :firstName '%' ");
			}
			
			sql.append(" order by m.insertTime desc");
			
			SqlParameterSource ps = new BeanPropertySqlParameterSource(argsDto);
			return namedJdbcTemplate.query(sql.toString(), ps, new BeanPropertyRowMapper<MasterSearchByTypeInfoDto>(MasterSearchByTypeInfoDto.class));
		}catch(DataAccessException e) {
			return null;
		}		
	}
	

}
