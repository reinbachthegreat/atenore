package tw.com.reinbach.Atenore.Model.category.event.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class EventListSearchByTypeInfoDto {

	private Integer mainID;

	private Integer typeID;

	private String mainTypeName;

	private String typeName;

	private List<EventSearchByTypeInfoDto> eventList;

	public List<EventSearchByTypeInfoDto> getEventList() {
		return eventList;
	}

	public void setEventList(List<EventSearchByTypeInfoDto> eventList) {
		this.eventList = eventList;
	}

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public Integer getTypeID() {
		return typeID;
	}

	public void setTypeID(Integer typeID) {
		this.typeID = typeID;
	}

	public String getMainTypeName() {
		return mainTypeName;
	}

	public void setMainTypeName(String mainTypeName) {
		this.mainTypeName = mainTypeName;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

}
