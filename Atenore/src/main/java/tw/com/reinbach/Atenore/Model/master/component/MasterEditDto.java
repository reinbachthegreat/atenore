package tw.com.reinbach.Atenore.Model.master.component;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MasterEditDto {

	@ApiModelProperty(example = "123e4567-e89b-42d3-a456-556642440000")
	private String editID;
	
	@ApiModelProperty(example = "123e4567-e89b-42d3-a456-556642440000")
	private String masterID;
	
	@ApiModelProperty(example = "1", dataType = "Integer")
	private Integer typeID;
	
	@ApiModelProperty(example = "醫院", dataType = "String")
	private String typeName;
	
	@ApiModelProperty(example = "1", dataType = "Integer")
	private Integer middleID;
	
	@ApiModelProperty(example = "醫療院所", dataType = "String")
	private String middleTypeName;
	
	@ApiModelProperty(example = "1", dataType = "Integer")
	private Integer mainID;
	
	@ApiModelProperty(example = "醫療", dataType = "String")
	private String mainTypeName;
	
	@ApiModelProperty(example = "喵英大統領")
	private String publicName;
	
	@ApiModelProperty(example = "蔡")
	private String lastName;
	
	@ApiModelProperty(example = "英文")
	private String firstName;
	
	@ApiModelProperty(example = "https://upload.wikimedia.org/wikipedia/commons/1/1b/%E8%94%A1%E8%8B%B1%E6%96%87%E5%AE%98%E6%96%B9%E5%85%83%E9%A6%96%E8%82%96%E5%83%8F%E7%85%A7.png")
	private String img;
	
	private String objectKey;
	
	@ApiModelProperty(example = "@TsaiCat")
	private String lineID;
	
	@ApiModelProperty(example = "https://www.facebook.com/tsaiingwen/")
	private String facebook;
	
	@ApiModelProperty(example = "https://www.president.gov.tw/Page/40")
	private String website;
	
	@ApiModelProperty(example = "xxxxx@pr.gov.tw")
	private String mail;
	
	@ApiModelProperty(example = "02-88888888")
	private String phone;
	
	@ApiModelProperty(example = "台北市")
	private String county;
	
	@ApiModelProperty(example = "凱達格蘭大道一段1號")
	private String address;
	
	@ApiModelProperty(example = "25.154558", dataType = "Double")
	private BigDecimal lat;
	
	@ApiModelProperty(example = "24.137895", dataType = "Double")
	private BigDecimal lng;
	
	@ApiModelProperty(example = "25314512120")
	private Integer click;
	
	@ApiModelProperty(example = "25314512120")
	private Integer follow;
	
	@ApiModelProperty(example = "Presidency Office")
	private String company;
	
	@ApiModelProperty(example = "true")
	private Boolean alive;
	
	@ApiModelProperty(example = "台灣喵英大統領是也")
	private String intro;
	
	@ApiModelProperty(notes = "master's time of death",example = "2100-05-20 00:00:00")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime death;
	
	@ApiModelProperty(notes = "exist while master is dead.", example = "1996台海危機")
	private String legend;
	
	@ApiModelProperty(notes="editor of this version",example = "123e4567-e89b-42d3-a456-556642440000")
	private String memberID;
	
	@ApiModelProperty(notes = "information if version in used.", example = "true")
	private Boolean inUsed;
	
	@ApiModelProperty(example = "2020-07-30 00:00:00", dataType = "LocalDateTime")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime insertTime;
	
	@ApiModelProperty(notes = "版本編輯者帳號")
	private String account;

	public String getEditID() {
		return editID;
	}

	public void setEditID(String editID) {
		this.editID = editID;
	}

	public String getMasterID() {
		return masterID;
	}

	public void setMasterID(String masterID) {
		this.masterID = masterID;
	}

	public Integer getTypeID() {
		return typeID;
	}

	public void setTypeID(Integer typeID) {
		this.typeID = typeID;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public Integer getMiddleID() {
		return middleID;
	}

	public void setMiddleID(Integer middleID) {
		this.middleID = middleID;
	}

	public String getMiddleTypeName() {
		return middleTypeName;
	}

	public void setMiddleTypeName(String middleTypeName) {
		this.middleTypeName = middleTypeName;
	}

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public String getMainTypeName() {
		return mainTypeName;
	}

	public void setMainTypeName(String mainTypeName) {
		this.mainTypeName = mainTypeName;
	}

	public String getPublicName() {
		return publicName;
	}

	public void setPublicName(String publicName) {
		this.publicName = publicName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getObjectKey() {
		return objectKey;
	}

	public void setObjectKey(String objectKey) {
		this.objectKey = objectKey;
	}

	public String getLineID() {
		return lineID;
	}

	public void setLineID(String lineID) {
		this.lineID = lineID;
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public BigDecimal getLat() {
		return lat;
	}

	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}

	public BigDecimal getLng() {
		return lng;
	}

	public void setLng(BigDecimal lng) {
		this.lng = lng;
	}

	public Integer getClick() {
		return click;
	}

	public void setClick(Integer click) {
		this.click = click;
	}

	public Integer getFollow() {
		return follow;
	}

	public void setFollow(Integer follow) {
		this.follow = follow;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public Boolean getAlive() {
		return alive;
	}

	public void setAlive(Boolean alive) {
		this.alive = alive;
	}

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public LocalDateTime getDeath() {
		return death;
	}

	public void setDeath(LocalDateTime death) {
		this.death = death;
	}

	public String getLegend() {
		return legend;
	}

	public void setLegend(String legend) {
		this.legend = legend;
	}

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public Boolean getInUsed() {
		return inUsed;
	}

	public void setInUsed(Boolean inUsed) {
		this.inUsed = inUsed;
	}

	public LocalDateTime getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(LocalDateTime insertTime) {
		this.insertTime = insertTime;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}
	
	
	
}
