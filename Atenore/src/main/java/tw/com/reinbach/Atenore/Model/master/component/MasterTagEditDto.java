package tw.com.reinbach.Atenore.Model.master.component;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MasterTagEditDto {

	@ApiModelProperty(example = "123e4567-e89b-42d3-a456-556642440000", dataType = "String", hidden = true)
	private String uuid;
	
	@ApiModelProperty(example = "123e4567-e89b-42d3-a456-556642440000", dataType = "String", hidden = true)
	private String editID;
	
	@ApiModelProperty(notes = "alias professional tag",example = "國際談判專長", dataType = "String")
	private String tag;

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getEditID() {
		return editID;
	}

	public void setEditID(String editID) {
		this.editID = editID;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}
	
}
