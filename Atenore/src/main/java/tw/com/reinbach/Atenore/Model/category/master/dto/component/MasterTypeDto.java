package tw.com.reinbach.Atenore.Model.category.master.dto.component;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MasterTypeDto {

	@ApiModelProperty(example = "1")
	private Integer typeID;

	@ApiModelProperty(hidden = true)
	private Integer mainID;
	
	@ApiModelProperty(hidden = true)
	private Integer midID;
	
	@ApiModelProperty(example = "醫院")
	private String name;

	public Integer getTypeID() {
		return typeID;
	}

	public void setTypeID(Integer typeID) {
		this.typeID = typeID;
	}

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public Integer getMidID() {
		return midID;
	}

	public void setMidID(Integer midID) {
		this.midID = midID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	
	
}
