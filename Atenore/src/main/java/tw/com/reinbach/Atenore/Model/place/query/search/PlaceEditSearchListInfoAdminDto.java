package tw.com.reinbach.Atenore.Model.place.query.search;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PlaceEditSearchListInfoAdminDto {

	private List<PlaceEditSearchInfoAdminDto> placeEditList;

	public List<PlaceEditSearchInfoAdminDto> getPlaceEditList() {
		return placeEditList;
	}

	public void setPlaceEditList(List<PlaceEditSearchInfoAdminDto> placeEditList) {
		this.placeEditList = placeEditList;
	}

}
