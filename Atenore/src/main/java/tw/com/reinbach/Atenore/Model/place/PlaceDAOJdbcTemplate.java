package tw.com.reinbach.Atenore.Model.place;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import tw.com.reinbach.Atenore.Model.place.component.PinPlaceTypeInfoDto;
import tw.com.reinbach.Atenore.Model.place.component.PlaceDto;
import tw.com.reinbach.Atenore.Model.place.component.PlaceImageDto;
import tw.com.reinbach.Atenore.Model.place.component.PlaceLiaisonDto;
import tw.com.reinbach.Atenore.Model.place.component.PlaceTag;
import tw.com.reinbach.Atenore.Model.place.create.PlaceCreateDto;
import tw.com.reinbach.Atenore.Model.place.create.PlaceLiaisonCreateDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceEditSearchArgsAdminDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceEditSearchInfoAdminDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceEditSearchInfoMemberDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceNearBySearchArgsDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceSearchArgsAdminDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceSearchArgsDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceSearchArgsMemberEditedDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceSearchInfoAdminDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceSearchInfoDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceSearchSimpleInfoDto;
import tw.com.reinbach.Atenore.Model.place.update.PinPlaceTypeUpdateDto;
import tw.com.reinbach.Atenore.Model.place.update.PlaceFollowedDto;
import tw.com.reinbach.Atenore.Model.place.update.PlaceUpdateDto;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;

@Repository
public class PlaceDAOJdbcTemplate implements PlaceDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private NamedParameterJdbcTemplate namedJdbcTemplate;

	@Override
	public List<PlaceDto> getPlaceList() {
		try {
			String sql = "select p.*, pt.name as typeName, pt.mainID, pmt.name as mainTypeName from place p "
					+ " left join placetype pt on p.typeID = pt.typeID "
					+ " left join placemaintype pmt on pt.mainID = pmt.mainID order by p.insertTime";
			return jdbcTemplate.query(sql, new BeanPropertyRowMapper<PlaceDto>(PlaceDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<PlaceSearchInfoDto> getPlaceListWithFilter(PlaceSearchArgsDto argsDto) {
		try {
			StringBuilder sql = new StringBuilder(
					"select p.*, (select pi.img from placeimg pi where pi.placeID = p.placeID and pi.isMain = 'true') as img, "
							+ " (select l.phone from liaison l where l.placeID = p.placeID and l.isMain = 'true') as phone, "
							+ " pt.name as typeName, pt.mainID, pmt.name as mainTypeName from place p "
							+ " left join placetype pt on p.typeID = pt.typeID left join placemaintype pmt on pt.mainID = pmt.mainID where 1 = 1 ");

			if (argsDto.getCounty() != null) {
				sql.append(" and p.county = :county ");
			}

			if (argsDto.getFuzzyFilter() != null) {
				sql.append(" and p.name like '%' :fuzzyFilter '%' or p.address like '%' :fuzzyFilter '%' ");
			}

			if (argsDto.getMainID() != null) {
				sql.append(" and pt.mainID = :mainID ");
				if (argsDto.getTypeID() != null) {
					sql.append(" and p.typeID = :typeID ");
				}
				if (argsDto.getCapacitySN() != null) {
					sql.append(" and p.max = :capacitySN ");
				}
			}

			switch (argsDto.getSearchPreferArg().getSearchPreferSN()) {
			case 1:
				sql.append(" order by (p.click + p.follow) desc limit 30 ");
				break;
			default:
				break;
			}

			SqlParameterSource ps = new BeanPropertySqlParameterSource(argsDto);
			return namedJdbcTemplate.query(sql.toString(), ps,
					new BeanPropertyRowMapper<PlaceSearchInfoDto>(PlaceSearchInfoDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}
	
	@Override
	public List<PlaceSearchInfoDto> getPlaceListNearBy(PlaceNearBySearchArgsDto argsDto){
		try {
			StringBuilder sql = new StringBuilder(
					"select p.*, (select pi.img from placeimg pi where pi.placeID = p.placeID and pi.isMain = 'true') as img, "
							+ " (select l.phone from liaison l where l.placeID = p.placeID and l.isMain = 'true') as phone, "
							+ " pt.name as typeName, pt.mainID, pmt.name as mainTypeName from place p "
							+ " left join placetype pt on p.typeID = pt.typeID left join placemaintype pmt on pt.mainID = pmt.mainID where 1 = 1 ");
			sql.append(" and p.lat between :minLat and :maxLat and p.lnt between :minLng and :maxLat ");
			
//			switch (argsDto.getSearchPreferArg().getSearchPreferSN()) {
//			case 1:
//				sql.append(" order by (p.click + p.follow) desc limit 30 ");
//				break;
//			default:
//				break;
//			}

			SqlParameterSource ps = new BeanPropertySqlParameterSource(argsDto);
			return namedJdbcTemplate.query(sql.toString(), ps,
					new BeanPropertyRowMapper<PlaceSearchInfoDto>(PlaceSearchInfoDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<PlaceDto> getPlaceListWithFilter(String placeFilter, String county) {
		try {
			StringBuilder sql = new StringBuilder("select p.*, pt.name as typeName, pt.mainID, pmt.name as mainTypeName from place p "
					+ " left join placetype pt on p.typeID = pt.typeID "
					+ " left join placemaintype pmt on pt.mainID = pmt.mainID order by p.insertTime");

			if (placeFilter == null && county == null) {
				return this.getPlaceList();
			}

			if (placeFilter != null && county != null) {
				String query = new StringBuilder("%").append(placeFilter).append("%").toString();
				sql.append("where p.name like ? and p.county = ? ").append("order by p.insertTime");
				return jdbcTemplate.query(sql.toString(), new Object[] { query, county },
						new BeanPropertyRowMapper<PlaceDto>(PlaceDto.class));
			}

			if (placeFilter == null && county != null) {
				sql.append("where p.county = ? ").append("order by p.insertTime");
				return jdbcTemplate.query(sql.toString(), new Object[] { county },
						new BeanPropertyRowMapper<PlaceDto>(PlaceDto.class));
			}

			if (placeFilter != null && county == null) {
				String query = new StringBuilder("%").append(placeFilter).append("%").toString();
				sql.append("where p.name like ? ").append("order by p.insertTime");
				return jdbcTemplate.query(sql.toString(), new Object[] { query }, new BeanPropertyRowMapper<PlaceDto>(PlaceDto.class));
			}

			return null;
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public PlaceDto getPlace(String placeID) {
		try {
			String sql = "select p.*, pt.name as typeName, pt.mainID, pmt.name as mainTypeName from place p "
					+ " left join placetype pt on p.typeID = pt.typeID left join placemaintype pmt on pt.mainID = pmt.mainID "
					+ " where p.placeID = ? ";
			return jdbcTemplate.queryForObject(sql, new Object[] { placeID }, new BeanPropertyRowMapper<PlaceDto>(PlaceDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public PlaceDto getPlaceWithMemberID(String placeID, String memberID) {
		try {
			String sql = "select p.*, pt.name as typeName, pt.mainID, pmt.name as mainTypeName, "
					+ " (select count(*) from memberplace where memberID = ? and placeID = ?) as isFollowed from place p "
					+ " left join placetype pt on p.typeID = pt.typeID left join placemaintype pmt on pt.mainID = pmt.mainID "
					+ " where p.placeID = ? ";
			return jdbcTemplate.queryForObject(sql, new Object[] { memberID, placeID, placeID },
					new BeanPropertyRowMapper<PlaceDto>(PlaceDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<PlaceTag> getPlaceTag(String placeID) {
		try {
			String sql = "select placeID, uuid as tagID, tag from placetag where placeID = ? ";
			return jdbcTemplate.query(sql, new Object[] { placeID }, new BeanPropertyRowMapper<PlaceTag>(PlaceTag.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<PlaceTag> getPlaceTagWithoutPlaceID(String placeID) {
		try {
			String sql = "select uuid as tagID, tag from placetag where placeID = ? ";
			return jdbcTemplate.query(sql, new Object[] { placeID }, new BeanPropertyRowMapper<PlaceTag>(PlaceTag.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<PlaceLiaisonDto> getPlaceLiaison(String placeID) {
		try {
			String sql = "select uuid as liaisonID, name, phone, isMain from liaison where placeID = ? order by isMain desc";
			return jdbcTemplate.query(sql, new Object[] { placeID },
					new BeanPropertyRowMapper<PlaceLiaisonDto>(PlaceLiaisonDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<PlaceImageDto> getPlaceImage(String placeID) {
		try {
			String sql = "select placeID, uuid as imageID, objectKey, img, isMain from placeimg where placeID = ? order by isMain desc";
			return jdbcTemplate.query(sql, new Object[] { placeID }, new BeanPropertyRowMapper<PlaceImageDto>(PlaceImageDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<PlaceImg> getPlaceImage(List<String> uuidList) {
		try {
			String sql = "select * from placeimg where uuid in (:uuidList)";
			Map<String, List<String>> photoParam = Collections.singletonMap("uuidList", uuidList);
			return namedJdbcTemplate.query(sql, photoParam, new BeanPropertyRowMapper<PlaceImg>(PlaceImg.class));
		} catch (DataAccessException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<String> getPlaceImageObjectkey(String placeID) {
		try {
			String sql = "select objectkey from placeimg where placeID = ? ";
			return jdbcTemplate.queryForList(sql, new Object[] { placeID }, String.class);
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<String> getPlaceEditID(String placeID) {
		try {
			String sql = "select editID from placeedit where placeID = ? ";
			return jdbcTemplate.queryForList(sql, new Object[] { placeID }, String.class);
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public PlaceUpdateDto getPlaceOriginalByPlaceID(String placeID) {
		try {
			String sql = "select * from place where placeID = ? ";
			return jdbcTemplate.queryForObject(sql, new Object[] { placeID },
					new BeanPropertyRowMapper<PlaceUpdateDto>(PlaceUpdateDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<PlaceEditSearchInfoAdminDto> getPlaceEditListByPlaceID(PlaceEditSearchArgsAdminDto argsDto) {
		try {
			StringBuilder sql = new StringBuilder(
					"select pe.editID, case when m.account is null then '後台新增' else m.account end as account, "
							+ " pe.inUsed, pe.insertTime from placeedit pe "
							+ " left join member m on m.memberID = pe.memberID where pe.placeID = :placeID ");

			if (argsDto.getAccount() != null) {
				sql.append(" and (m.account like '%' :account '%' or pe.memberID like '%' :account '%') ");
			}
			if (argsDto.getStartTime() != null && argsDto.getEndTime() != null) {
				sql.append(" and pe.insertTime between :startTime and :endTime ");
			} else if (argsDto.getStartTime() != null) {
				sql.append(" and pe.insertTime >= :startTime ");
			} else if (argsDto.getEndTime() != null) {
				sql.append(" and pe.insertTime <= :endTime ");
			}

			sql.append(" order by pe.insertTime desc ");

			SqlParameterSource ps = new BeanPropertySqlParameterSource(argsDto);
			return namedJdbcTemplate.query(sql.toString(), ps,
					new BeanPropertyRowMapper<PlaceEditSearchInfoAdminDto>(PlaceEditSearchInfoAdminDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<PlaceEditSearchInfoMemberDto> getPlaceMemberEditedListWithFilter(PlaceSearchArgsMemberEditedDto argsDto) {
		try {
			StringBuilder sql = new StringBuilder("select pe.placeID, pe.editID, pe.memberID, pe.name, pe.inUsed, pe.insertTime, "
					+ " (select pie.img from placeimgedit pie where pie.editID = pe.editID and pie.isMain = 'true') as img "
					+ " from placeedit pe left join placetype pt on p.typeID = pt.typeID "
					+ " left join placemaintype pmt on pt.mainID = pmt.mainID where pe.memberID = :memberID ");

			if (argsDto.getCounty() != null) {
				sql.append(" and pe.county = :county ");
			}
			if (argsDto.getMainID() != null) {
				sql.append(" and pmt.mainID = :mainID ");
				if (argsDto.getTypeID() != null) {
					sql.append(" and pe.typeID = :typeID ");
				}
			}
			sql.append(" order by pe.insertTime desc");

			SqlParameterSource ps = new BeanPropertySqlParameterSource(argsDto);
			return namedJdbcTemplate.query(sql.toString(), ps,
					new BeanPropertyRowMapper<PlaceEditSearchInfoMemberDto>(PlaceEditSearchInfoMemberDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<PlaceSearchSimpleInfoDto> getPlaceSimpleByName(String name) {
		try {
			name = "%" + name + "%";
			String sql = "select placeID, name from atenore.place where name like ? ";
			return jdbcTemplate.query(sql, new Object[] { name },
					new BeanPropertyRowMapper<PlaceSearchSimpleInfoDto>(PlaceSearchSimpleInfoDto.class));
		} catch (DataAccessException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public int deletePlaceTag(String placeID) {
		try {
			String sql = "delete from placetag where placeID = ?";

			int count = jdbcTemplate.update(sql, new Object[] { placeID });
			if (count != 0) {
				return count;
			} else {
				return 0;
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public int deletePlaceLiason(String placeID) {
		try {
			String sql = "delete from liaison where placeID = ?";
			int count = jdbcTemplate.update(sql, new Object[] { placeID });
			if (count != 0) {
				return count;
			} else {
				return 0;
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public int deletePlaceImage(List<String> uuidList) {
		try {
			String sql = "delete from placeimg where uuid in (:uuidList)";
			Map<String, List<String>> deleteUuidParam = Collections.singletonMap("uuidList", uuidList);
			int count = namedJdbcTemplate.update(sql, deleteUuidParam);

			if (count != 0) {
				return count;
			} else {
				return 0;
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public Boolean getPlaceIfMemberFollowed(PlaceFollowedDto follow) {
		try {
			String sql = "select count(*) from memberplace where placeID = :placeID and memberID = :memberID ";
			SqlParameterSource ps = new BeanPropertySqlParameterSource(follow);
			Integer count = namedJdbcTemplate.queryForObject(sql, ps, Integer.class);
			if (count == 0) {
				return false;
			} else {
				return true;
			}
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public ResponseModel<Void> setPlaceFollowed(PlaceFollowedDto follow) {
		try {
			String sql = "insert into memberplace (placeID, memberID) values(:placeID, :memberID)";
			SqlParameterSource ps = new BeanPropertySqlParameterSource(follow);
			int count = namedJdbcTemplate.update(sql, ps);
			if (count != 0) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<>(500, ApiError.PLACE_FOLLOWED_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deletePlaceFollowed(PlaceFollowedDto follow) {
		try {
			String sql = "delete from memberplace where placeID = :placeID and memberID = :memberID ";
			SqlParameterSource ps = new BeanPropertySqlParameterSource(follow);
			int count = namedJdbcTemplate.update(sql, ps);
			if (count != 0) {
				return new ResponseModel<Void>(204, "");
			} else {
				return new ResponseModel<>(500, ApiError.PLACE_DISFOLLOWED_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> updatePlaceFollowed(String placeID, Boolean wannaFollow) {
		try {
			String sql = "";
			if (wannaFollow == true) {
				sql = "update atenore.place set follow = IFNULL(follow, 0) + 1 where placeID = ? ";
			} else {
				sql = "update atenore.place set follow = IF(follow>1,IFNULL(follow, 0) - 1 ,0) where placeID = ? ";
			}
			int count = jdbcTemplate.update(sql, new Object[] { placeID });
			if (count != 0) {
				return new ResponseModel<Void>(204, "");
			} else {
				return new ResponseModel<>(409, ApiError.PLACE_FOLLOWED_UPDATE_FAILED);
			}
		} catch (DataAccessException e) {
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	/*
	 * Place Edit Version
	 * Series=======================================================================
	 * ================
	 */

	@Override
	public PlaceDto getPlaceEdit(String editID) {
		try {
			String sql = "select p.*, pt.name as typeName, pt.mainID, pmt.name as mainTypeName from placeedit p "
					+ " left join placetype pt on p.typeID = pt.typeID left join placemaintype pmt on pt.mainID = pmt.mainID "
					+ " where p.editID = ? ";
			return jdbcTemplate.queryForObject(sql, new Object[] { editID }, new BeanPropertyRowMapper<PlaceDto>(PlaceDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public PlaceDto getPlaceMemberEdit(String editID, String memberID) {
		try {
			String sql = "select pe.*, pt.name as typeName, pt.mainID, pmt.name as mainTypeName from placeedit pe "
					+ " left join placetype pt on pe.typeID = pt.typeID left join placemaintype pmt on pt.mainID = pmt.mainID "
					+ " where pe.editID = ? and pe.memberID = ?";
			return jdbcTemplate.queryForObject(sql, new Object[] { editID, memberID },
					new BeanPropertyRowMapper<PlaceDto>(PlaceDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<PlaceDto> getPlaceEditList(String placeID) {
		try {
			String sql = "select p.*, pt.name as typeName, pt.mainID, pmt.name as mainTypeName from placeedit p "
					+ " left join placetype pt on p.typeID = pt.typeID left join placemaintype pmt on pt.mainID = pmt.mainID "
					+ " where p.placeID = ?  order by p.insertTime desc";
			return jdbcTemplate.query(sql, new Object[] { placeID }, new BeanPropertyRowMapper<PlaceDto>(PlaceDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<PlaceTag> getPlaceTagListByEditID(String editID) {
		try {
			String sql = "select * from placetagedit where editID = ? ";
			return jdbcTemplate.query(sql, new Object[] { editID }, new BeanPropertyRowMapper<PlaceTag>(PlaceTag.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<PlaceImageDto> getPlaceImgListByEditID(String editID) {
		try {
			String sql = "select uuid as imageID, objectKey, img, isMain from placeimgedit where editID = ? ";
			return jdbcTemplate.query(sql, new Object[] { editID }, new BeanPropertyRowMapper<PlaceImageDto>(PlaceImageDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<PlaceLiaisonDto> getPlaceLiaisonListByEditID(String editID) {
		try {
			String sql = "select * from liaisonedit where editID = ? ";
			return jdbcTemplate.query(sql, new Object[] { editID }, new BeanPropertyRowMapper<PlaceLiaisonDto>(PlaceLiaisonDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<PlaceLiaison> getPlaceLiaisonEdit(String editID) {
		try {
			String sql = "select * from liaisonedit where editID = ? ";
			return jdbcTemplate.query(sql, new Object[] { editID }, new BeanPropertyRowMapper<PlaceLiaison>(PlaceLiaison.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public ResponseModel<Void> setPlaceEdit(List<PlaceCreateDto> list) {
		try {
			String sql = "insert into placeedit (editID, placeID, typeID, name, max, area, county, address, unit, "
					+ " mail, facebook, service, intro, website, lat, lng, insertTime, memberID, inUsed) "
					+ " values(:editID, :placeID, :typeID, :name, :max, :area, :county, :address, :unit, "
					+ " :mail, :facebook, :service, :intro, :website, :lat, :lng, now(), :memberID, :inUsed)";
			List<SqlParameterSource> psList = new ArrayList<SqlParameterSource>();
			for (PlaceCreateDto bean : list) {
				psList.add(new BeanPropertySqlParameterSource(bean));
			}
			SqlParameterSource[] psArry = new SqlParameterSource[psList.size()];
			psList.toArray(psArry);
			int[] countList = namedJdbcTemplate.batchUpdate(sql, psArry);
			if (this.resultIfBatchUpdateSuccess(countList)) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<Void>(201, "Some of " + ApiError.PLACE_INFO_EDIT_CREATION_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public ResponseModel<Void> setPlaceEdit(PlaceCreateDto place) {
		try {
			String sql = "insert into placeedit (editID, placeID, typeID, name, max, area, county, address, unit, "
					+ " mail, facebook, service, intro, website, lat, lng, insertTime, memberID, inUsed) "
					+ " values(:editID, :placeID, :typeID, :name, :max, :area, :county, :address, :unit, "
					+ " :mail, :facebook, :service, :intro, :website, :lat, :lng, now(), :memberID, :inUsed)";
			SqlParameterSource ps = new BeanPropertySqlParameterSource(place);
			int count = namedJdbcTemplate.update(sql, ps);
			if (count != 0) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<Void>(409, ApiError.PLACE_INFO_EDIT_CREATION_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> setPlaceEdit(PlaceUpdateDto place) {
		try {
			String sql = "insert into placeedit (editID, placeID, typeID, name, max, area, county, address, unit, "
					+ " mail, facebook, service, intro, website, lat, lng, insertTime, memberID, inUsed) "
					+ " values(:editID, :placeID, :typeID, :name, :max, :area, :county, :address, :unit, "
					+ " :mail, :facebook, :service, :intro, :website, :lat, :lng, now(), :memberID, :inUsed)";
			SqlParameterSource ps = new BeanPropertySqlParameterSource(place);
			int count = namedJdbcTemplate.update(sql, ps);
			if (count != 0) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<Void>(409, ApiError.PLACE_INFO_EDIT_CREATION_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> setPlaceEditInUsedFalse(String placeID) {
		try {
			String sql = "update placeedit set inUsed = 'false' where placeID = ?";
			int count = jdbcTemplate.update(sql, new Object[] { placeID });
			if (count != 0) {
				return new ResponseModel<Void>(204, "");
			} else {
				return new ResponseModel<Void>(409, ApiError.PLACE_INFO_UPDATE_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> setPlaceEditInUsedTrue(String editID) {
		try {
			String sql = "update placeedit set inUsed = 'true' where editID = ?";
			int count = jdbcTemplate.update(sql, new Object[] { editID });
			if (count != 0) {
				return new ResponseModel<Void>(204, "");
			} else {
				return new ResponseModel<Void>(409, ApiError.PLACE_INFO_UPDATE_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> setPlace(List<PlaceCreateDto> list) {
		try {
			String sql = "insert into place (placeID, typeID, name, max, area, county, address, unit, "
					+ " mail, facebook, service, intro, website, lat, lng, editable, insertTime, memberID) "
					+ " values(:placeID, :typeID, :name, :max, :area, :county, :address, :unit, "
					+ " :mail, :facebook, :service, :intro, :website, :lat, :lng, :editable, now(), :memberID)";
			List<SqlParameterSource> psList = new ArrayList<SqlParameterSource>();
			for (PlaceCreateDto bean : list) {
				psList.add(new BeanPropertySqlParameterSource(bean));
			}
			SqlParameterSource[] psArry = new SqlParameterSource[psList.size()];
			psList.toArray(psArry);
			int[] countList = namedJdbcTemplate.batchUpdate(sql, psArry);
			if (this.resultIfBatchUpdateSuccess(countList)) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<Void>(201, "Some of " + ApiError.PLACE_INFO_CREATION_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public ResponseModel<Void> setPlace(PlaceCreateDto place) {
		try {
			String sql = "insert into place (placeID, typeID, name, max, area, county, address, unit, "
					+ " mail, facebook, service, intro, website, lat, lng, editable, insertTime, memberID) "
					+ " values(:placeID, :typeID, :name, :max, :area, :county, :address, :unit, "
					+ " :mail, :facebook, :service, :intro, :website, :lat, :lng, :editable, now(), :memberID)";
			SqlParameterSource ps = new BeanPropertySqlParameterSource(place);
			int count = namedJdbcTemplate.update(sql, ps);
			if (count != 0) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<Void>(409, ApiError.PLACE_INFO_CREATION_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> updatePlace(PlaceUpdateDto place) {
		try {
			String sql = "update place set typeID = :typeID, name = :name, max = :max, area = :area, county = :county, address = :address, "
					+ " unit = :unit, mail = :mail, facebook = :facebook, service = :service, intro = :intro, website = :website, "
					+ " lat = :lat, lng = :lng, editable = :editable, insertTime = now(), memberID = :memberID where placeID = :placeID";
			SqlParameterSource ps = new BeanPropertySqlParameterSource(place);
			int count = namedJdbcTemplate.update(sql, ps);
			if (count != 0) {
				return new ResponseModel<Void>(204, "");
			} else {
				return new ResponseModel<Void>(409, ApiError.PLACE_INFO_UPDATE_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> updatePlace(String placeID, String editID) {
		try {
			String sql = "update place p inner join placeedit pe on p.placeID = pe.placeID "
					+ " set p.typeID = pe.typeID, p.name = pe.name, p.max = pe.max, p.area = pe.area,"
					+ " p.county = pe.county, p.address = pe.address, p.unit = pe.unit, p.mail = pe.mail, "
					+ " p.facebook = pe.facebook, p.service = pe.service, p.intro = pe.intro, p.website = pe.website, "
					+ " p.lat = pe.lat, p.lng = pe.lng, p.memberID = pe.memberID, "
					+ " p.insertTime = pe.insertTime where p.placeID = ? and pe.editID = ?";
			int count = jdbcTemplate.update(sql, new Object[] { placeID, editID });
			if (count != 0) {
				return new ResponseModel<Void>(204, "");
			} else {
				return new ResponseModel<Void>(409, ApiError.PLACE_INFO_UPDATE_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> editPlaceLiaison(String placeID, String editID) {
		try {
			String sql = "insert into liaison (uuid, placeID, name, phone, isMain) "
					+ " select le.uuid, ?, le.name, le.phone, le.isMain from liaisonedit le where le.editID = ? ";
			int count = jdbcTemplate.update(sql, new Object[] { placeID, editID });
			if (count != 0) {
				return new ResponseModel<Void>(204, "");
			} else {
				return new ResponseModel<Void>(409, ApiError.PLACE_LIAISON_UPDATE_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> editPlaceTag(String placeID, String editID) {
		try {
			String sql = "insert into placetag (uuid, placeID, tag) "
					+ " select pte.uuid, ?, pte.tag from placetagedit pte where pte.editID = ? ";
			int count = jdbcTemplate.update(sql, new Object[] { placeID, editID });
			if (count != 0) {
				return new ResponseModel<Void>(204, "");
			} else {
				return new ResponseModel<Void>(409, ApiError.PLACE_TAG_UPDATE_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> editPlaceImage(String placeID, String editID) {
		try {
			String sql = "insert into placeimg (uuid, placeID, objectKey, img, isMain) "
					+ " select pie.uuid, ?, pie.objectKey, pie.img, pie.isMain from placeimgedit pie where pie.editID = ? ";
			int count = jdbcTemplate.update(sql, new Object[] { placeID, editID });
			if (count != 0) {
				return new ResponseModel<Void>(204, "");
			} else {
				return new ResponseModel<Void>(409, ApiError.PLACE_IMAGE_UPDATE_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> setPlaceLiaisonEdit(List<PlaceLiaisonCreateDto> editList) {
		try {
			String sql = "insert into liaisonedit (uuid, editID, name, phone, isMain) "
					+ " values(:liaisonID, :editID, :name, :phone, :isMain)";
			List<SqlParameterSource> psList = new ArrayList<SqlParameterSource>();
			for (PlaceLiaisonCreateDto bean : editList) {
				psList.add(new BeanPropertySqlParameterSource(bean));
			}
			SqlParameterSource[] psArry = new SqlParameterSource[psList.size()];
			psList.toArray(psArry);
			int[] countList = namedJdbcTemplate.batchUpdate(sql, psArry);
			if (this.resultIfBatchUpdateSuccess(countList)) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<Void>(201, "Some of " + ApiError.PLACE_LIAISON_EDIT_CREATION_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public ResponseModel<Void> setPlaceLiaison(List<PlaceLiaisonCreateDto> list) {
		try {
			String sql = "insert into liaison (uuid, placeID, name, phone, isMain) "
					+ " values(:liaisonID, :placeID, :name, :phone, :isMain)";
			List<SqlParameterSource> psList = new ArrayList<SqlParameterSource>();
			for (PlaceLiaisonCreateDto bean : list) {
				psList.add(new BeanPropertySqlParameterSource(bean));
			}
			SqlParameterSource[] psArry = new SqlParameterSource[psList.size()];
			psList.toArray(psArry);
			int[] countList = namedJdbcTemplate.batchUpdate(sql, psArry);
			if (this.resultIfBatchUpdateSuccess(countList)) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<Void>(201, "Some of " + ApiError.PLACE_LIAISON_CREATION_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public ResponseModel<Void> setPlaceTagEdit(List<PlaceTag> editList) {
		try {
			String sql = "insert into placetagedit (uuid, editID, tag) values(:tagID, :editID, :tag)";
			List<SqlParameterSource> psList = new ArrayList<SqlParameterSource>();
			for (PlaceTag bean : editList) {
				psList.add(new BeanPropertySqlParameterSource(bean));
			}
			SqlParameterSource[] psArry = new SqlParameterSource[psList.size()];
			psList.toArray(psArry);
			int[] countList = namedJdbcTemplate.batchUpdate(sql, psArry);
			if (this.resultIfBatchUpdateSuccess(countList)) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<Void>(201, "Some of " + ApiError.PLACE_TAG_EDIT_CREATION_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> setPlaceTag(List<PlaceTag> list) {
		try {
			String sql = "insert into placetag (uuid, placeID, tag) values(:tagID, :placeID, :tag)";
			List<SqlParameterSource> psList = new ArrayList<SqlParameterSource>();
			for (PlaceTag bean : list) {
				psList.add(new BeanPropertySqlParameterSource(bean));
			}
			SqlParameterSource[] psArry = new SqlParameterSource[psList.size()];
			psList.toArray(psArry);
			int[] countList = namedJdbcTemplate.batchUpdate(sql, psArry);
			if (this.resultIfBatchUpdateSuccess(countList)) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<Void>(201, "Some of " + ApiError.PLACE_TAG_CREATION_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> setPlaceImageEdit(List<PlaceImg> list) {
		try {
			String sql = "insert into placeimgedit (uuid, editID, objectKey, img, isMain) "
					+ " values(:uuid, :editID, :objectKey, :img, :isMain)";
			List<SqlParameterSource> psList = new ArrayList<SqlParameterSource>();
			for (PlaceImg bean : list) {
				psList.add(new BeanPropertySqlParameterSource(bean));
			}
			SqlParameterSource[] psArry = new SqlParameterSource[psList.size()];
			psList.toArray(psArry);
			int[] countList = namedJdbcTemplate.batchUpdate(sql, psArry);
			if (this.resultIfBatchUpdateSuccess(countList)) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<Void>(201, "Some of " + ApiError.PLACE_IMAGE_CREATION_FAILED);
			}
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public ResponseModel<Void> setPlaceImage(List<PlaceImg> list) {
		try {
			String sql = "insert into placeimg (uuid, placeID, objectKey, img, isMain) "
					+ " values(:uuid, :placeID, :objectKey, :img, :isMain)";
			List<SqlParameterSource> psList = new ArrayList<SqlParameterSource>();
			for (PlaceImg bean : list) {
				psList.add(new BeanPropertySqlParameterSource(bean));
			}
			SqlParameterSource[] psArry = new SqlParameterSource[psList.size()];
			psList.toArray(psArry);
			int[] countList = namedJdbcTemplate.batchUpdate(sql, psArry);
			if (this.resultIfBatchUpdateSuccess(countList)) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<Void>(201, "Some of " + ApiError.PLACE_IMAGE_CREATION_FAILED);
			}
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<PinPlaceTypeInfoDto> getPinPlaceTypeListByMemberID(String memberID) {
		try {
			String sql = "select pp.typeID, pt.name as typeName, pp.mainID, pmt.name as mainTypeName from pinplacetype pp "
					+ " left join placetype pt on pp.typeID = pt.typeID left join placemaintype pmt on pp.mainID = pmt.mainID "
					+ " where pp.memberID = ? ";
			return jdbcTemplate.query(sql, new Object[] { memberID },
					new BeanPropertyRowMapper<PinPlaceTypeInfoDto>(PinPlaceTypeInfoDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<String> getPinPlaceTypeUUIDListByMemberID(String memberID) {
		try {
			String sql = "select uuid from pinplacetype where memberID = ? ";
			return jdbcTemplate.queryForList(sql, new Object[] { memberID }, String.class);
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public ResponseModel<Void> deletePinPlaceTypeList(List<String> uuidList) {
		try {
			String sql = "delete from pinplacetype where uuid in (:uuidList)";
			Map<String, List<String>> deleteUuidParam = Collections.singletonMap("uuidList", uuidList);
			int count = namedJdbcTemplate.update(sql, deleteUuidParam);
			if (count != 0) {
				return new ResponseModel<Void>(204, "");
			} else {
				return new ResponseModel<Void>(500, ApiError.PINNED_PLACE_TYPE_DELETE_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> setPinPlaceTypeList(List<PinPlaceTypeUpdateDto> list) {
		try {
			String sql = "insert into pinplacetype (uuid, memberID, mainID, typeID, insertTime) values(:uuid, :memberID, :mainID, :typeID, now())";
			List<SqlParameterSource> psList = new ArrayList<SqlParameterSource>();
			for (PinPlaceTypeUpdateDto dto : list) {
				psList.add(new BeanPropertySqlParameterSource(dto));
			}
			SqlParameterSource[] psArry = new SqlParameterSource[psList.size()];
			psList.toArray(psArry);
			namedJdbcTemplate.batchUpdate(sql, psArry);
			return new ResponseModel<Void>(201, "");
		} catch (DataAccessException e) {
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deletePlaceFollowedByPlaceID(String placeID) {
		try {
			String sql = "delete from memberplace where placeID = ?";
			jdbcTemplate.update(sql, new Object[] { placeID });
			return new ResponseModel<>(204, "");
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deletePlaceTagEditByEditID(List<String> editIDList) {
		try {
			String sql = "delete from placetagedit where editID in (:editIDList)";
			Map<String, List<String>> deleteUuidParam = Collections.singletonMap("editIDList", editIDList);
			namedJdbcTemplate.update(sql, deleteUuidParam);
			return new ResponseModel<Void>(204, "");
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deletePlaceTagByPlaceID(String placeID) {
		try {
			String sql = "delete from placetag where placeID = ?";
			jdbcTemplate.update(sql, new Object[] { placeID });
			return new ResponseModel<Void>(204, "");
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deletePlaceImgEditByEditID(List<String> editIDList) {
		try {
			String sql = "delete from placeimgedit where editID in (:editIDList)";
			Map<String, List<String>> deleteUuidParam = Collections.singletonMap("editIDList", editIDList);
			namedJdbcTemplate.update(sql, deleteUuidParam);
			return new ResponseModel<Void>(204, "");
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deletePlaceImgByPlaceID(String placeID) {
		try {
			String sql = "delete from placeimg where placeID = ?";
			jdbcTemplate.update(sql, new Object[] { placeID });
			return new ResponseModel<Void>(204, "");
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deletePlaceLiaisonEditByEditID(List<String> editIDList) {
		try {
			String sql = "delete from liaisonedit where editID in (:editIDList)";
			Map<String, List<String>> deleteUuidParam = Collections.singletonMap("editIDList", editIDList);
			namedJdbcTemplate.update(sql, deleteUuidParam);
			return new ResponseModel<Void>(204, "");
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deletePlaceLiaisonByPlaceID(String placeID) {
		try {
			String sql = "delete from liaison where placeID = ?";
			jdbcTemplate.update(sql, new Object[] { placeID });
			return new ResponseModel<Void>(204, "");
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deletePlaceEditByEditID(List<String> editIDList) {
		try {
			String sql = "delete from placeedit where editID in (:editIDList)";
			Map<String, List<String>> deleteUuidParam = Collections.singletonMap("editIDList", editIDList);
			namedJdbcTemplate.update(sql, deleteUuidParam);
			return new ResponseModel<Void>(204, "");
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deletePlaceByPlaceID(String placeID) {
		try {
			String sql = "delete from place where placeID = ?";
			int count = jdbcTemplate.update(sql, new Object[] { placeID });
			if (count != 0) {
				return new ResponseModel<>(204, "");
			} else {
				return new ResponseModel<>(409, ApiError.PLACE_DELETION_FAILED);
			}
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> updatePlaceClick(String placeID) {
		try {
			String sql = "update atenore.place set click = IFNULL(click, 0) + 1 where placeID = ? ";
			int count = jdbcTemplate.update(sql, new Object[] { placeID });
			if (count != 0) {
				return new ResponseModel<Void>(204, "");
			} else {
				return new ResponseModel<>(409, ApiError.PLACE_CLICK_UPDATE_FAILED);
			}
		} catch (DataAccessException e) {
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	/* ADMIN */

	@Override
	public List<PlaceSearchInfoAdminDto> getPlaceListWithFilter(PlaceSearchArgsAdminDto argsDto) {
		try {
			StringBuilder sql = new StringBuilder().append("select p.placeID, p.name, pt.mainID, p.county, p.editable, p.insertTime, "
					+ " case when p.memberID = '後台新增' then '後台新增' else m.account end as memberAccount, "
					+ " (select l.phone from liaison l where l.placeID = p.placeID and l.isMain = 'true') as phone,"
					+ " (select l.name from liaison l where l.placeID = p.placeID and l.isMain = 'true') as liaison "
					+ " from place p left join placetype pt on p.typeID = pt.typeID left join member m on p.memberID = m.memberID "
					+ " where 1 = 1 ");

			if (argsDto.getFuzzyFilter() != null && argsDto.getFuzzyFilter().trim().length() != 0) {
				sql.append(" and p.name like '%' :fuzzyFilter '%' ");
			}

			if (argsDto.getCounty() != null) {
				sql.append(" and p.county = :county ");
			}

			if (argsDto.getMainTypePreferArg() != null) {
				switch (argsDto.getMainTypePreferArg()) {
				case 0:
					sql.append(" and pt.mainID != 29 ");
					break;
				case 1:
					sql.append(" and pt.mainID = 29 ");
					break;
				default:
					break;
				}
			}

			sql.append(" order by p.insertTime desc ");

			SqlParameterSource ps = new BeanPropertySqlParameterSource(argsDto);
			return namedJdbcTemplate.query(sql.toString(), ps,
					new BeanPropertyRowMapper<PlaceSearchInfoAdminDto>(PlaceSearchInfoAdminDto.class));
		} catch (DataAccessException e) {
			e.printStackTrace();
			return null;
		}
	}

	private Boolean resultIfBatchUpdateSuccess(int[] countList) {
		Boolean flag = true;
		for (int count : countList) {
			if (count == 0) {
				flag = false;
				break;
			}
		}
		return flag;
	}

}
