package tw.com.reinbach.Atenore.Model.category.master.dto;

import java.time.LocalDateTime;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MasterSearchByTypeInfoDto {

	@ApiModelProperty(example = "123e4567-e89b-42d3-a456-556642440000", dataType = "String")
	private String masterID;
	
	@ApiModelProperty(example = "1", dataType = "Integer")
	private Integer typeID;
	
	@ApiModelProperty(example = "醫院", dataType = "String")
	private String typeName;
	
	@ApiModelProperty(example = "1", dataType = "Integer")
	private Integer middleID;
	
	@ApiModelProperty(example = "醫療院所", dataType = "String")
	private String middleTypeName;
	
	@ApiModelProperty(example = "1", dataType = "Integer")
	private Integer mainID;
	
	@ApiModelProperty(example = "醫療", dataType = "String")
	private String mainTypeName;
	
	@ApiModelProperty(example = "蔡", dataType = "String")
	private String lastName;
	
	@ApiModelProperty(example = "英文", dataType = "String")
	private String firstName;
	
	@ApiModelProperty(example = "02-88888888", dataType = "String")
	private String phone;
	
	@ApiModelProperty(hidden = true, example = "2020-07-30 00:00:00", dataType = "LocalDateTime")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime insertTime;
	
	@ApiModelProperty(notes="final version editor",example = "123e4567-e89b-42d3-a456-556642440000", dataType = "String")
	private String memberID;

	public String getMasterID() {
		return masterID;
	}

	public void setMasterID(String masterID) {
		this.masterID = masterID;
	}

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public String getMainTypeName() {
		return mainTypeName;
	}

	public void setMainTypeName(String mainTypeName) {
		this.mainTypeName = mainTypeName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public LocalDateTime getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(LocalDateTime insertTime) {
		this.insertTime = insertTime;
	}

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public Integer getTypeID() {
		return typeID;
	}

	public void setTypeID(Integer typeID) {
		this.typeID = typeID;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getMiddleTypeName() {
		return middleTypeName;
	}

	public void setMiddleTypeName(String middleTypeName) {
		this.middleTypeName = middleTypeName;
	}

	public Integer getMiddleID() {
		return middleID;
	}

	public void setMiddleID(Integer middleID) {
		this.middleID = middleID;
	}
	
}
