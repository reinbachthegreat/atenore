package tw.com.reinbach.Atenore.Model.place.query.search;

import java.util.List;

public class PlaceSearchListInfoAdminDto {

	private List<PlaceSearchInfoAdminDto> placeList;

	public List<PlaceSearchInfoAdminDto> getPlaceList() {
		return placeList;
	}

	public void setPlaceList(List<PlaceSearchInfoAdminDto> placeList) {
		this.placeList = placeList;
	}

}
