package tw.com.reinbach.Atenore.Model.category.master.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;
import tw.com.reinbach.Atenore.Model.category.event.dto.EventTypeDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.component.MasterTypeDto;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MasterTypePrimaryCascadeDto {
	
	@ApiModelProperty(example = "1")
	private Integer midID;
	
	@ApiModelProperty(example = "醫療院所")
	private String name;
	
	private List<MasterTypeDto> minorTypeList;

	public Integer getMidID() {
		return midID;
	}

	public void setMidID(Integer midID) {
		this.midID = midID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<MasterTypeDto> getMinorTypeList() {
		return minorTypeList;
	}

	public void setMinorTypeList(List<MasterTypeDto> minorTypeList) {
		this.minorTypeList = minorTypeList;
	}
	
	public void addMinorTypeList(MasterTypeDto masterTypeDto){
		this.minorTypeList.add(masterTypeDto);
	}

}
