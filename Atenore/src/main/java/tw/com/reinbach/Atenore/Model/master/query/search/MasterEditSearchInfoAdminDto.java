package tw.com.reinbach.Atenore.Model.master.query.search;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MasterEditSearchInfoAdminDto {

	private String editID;
	
	private String account;
	
	private Boolean inUsed;
	
	private LocalDateTime insertTime;

	public String getEditID() {
		return editID;
	}

	public void setEditID(String editID) {
		this.editID = editID;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Boolean getInUsed() {
		return inUsed;
	}

	public void setInUsed(Boolean inUsed) {
		this.inUsed = inUsed;
	}

	public LocalDateTime getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(LocalDateTime insertTime) {
		this.insertTime = insertTime;
	}
	
	
	
}
