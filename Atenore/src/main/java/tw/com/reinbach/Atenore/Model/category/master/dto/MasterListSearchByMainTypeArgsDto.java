package tw.com.reinbach.Atenore.Model.category.master.dto;

import io.swagger.annotations.ApiModelProperty;

public class MasterListSearchByMainTypeArgsDto {

	@ApiModelProperty(hidden = true)
	private Integer mainID;
	
	private String lastName;
	
	private String firstName;

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
}
