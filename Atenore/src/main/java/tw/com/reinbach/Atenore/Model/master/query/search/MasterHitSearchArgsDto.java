package tw.com.reinbach.Atenore.Model.master.query.search;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MasterHitSearchArgsDto {
	
	@ApiModelProperty(hidden = true)
	private String memberID;

	@ApiModelProperty(value = "user's lat", example = "25.0639368", required = true)
	private BigDecimal lat;
	
	@ApiModelProperty(value = "user's lng", example = "121.5471534", required = true)
	private BigDecimal lng;
	
	@ApiModelProperty(value = "alias hit search prefer type sn", example = "0", required = true, dataType = "Integer")
	private Integer searchPreferSN;

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public BigDecimal getLat() {
		return lat;
	}

	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}

	public BigDecimal getLng() {
		return lng;
	}

	public void setLng(BigDecimal lng) {
		this.lng = lng;
	}

	public Integer getSearchPreferSN() {
		return searchPreferSN;
	}

	public void setSearchPreferSN(Integer searchPreferSN) {
		this.searchPreferSN = searchPreferSN;
	}
	
	

}
