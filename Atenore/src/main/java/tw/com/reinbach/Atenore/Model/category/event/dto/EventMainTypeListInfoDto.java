package tw.com.reinbach.Atenore.Model.category.event.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import tw.com.reinbach.Atenore.Model.category.event.dto.component.EventMainTypeAdminDto;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class EventMainTypeListInfoDto {

	private List<EventMainTypeAdminDto> eventMainTypeList;

	public List<EventMainTypeAdminDto> getEventMainTypeList() {
		return eventMainTypeList;
	}

	public void setEventMainTypeList(List<EventMainTypeAdminDto> eventMainTypeList) {
		this.eventMainTypeList = eventMainTypeList;
	}
	
}
