package tw.com.reinbach.Atenore.Model.place.create;

public class PlaceCreateBatchResultInfoDto {

	private String batchUpdateUrl;

	private String batchUpdateObjectKey;

	public String getBatchUpdateUrl() {
		return batchUpdateUrl;
	}

	public void setBatchUpdateUrl(String batchUpdateUrl) {
		this.batchUpdateUrl = batchUpdateUrl;
	}

	public String getBatchUpdateObjectKey() {
		return batchUpdateObjectKey;
	}

	public void setBatchUpdateObjectKey(String batchUpdateObjectKey) {
		this.batchUpdateObjectKey = batchUpdateObjectKey;
	}

}
