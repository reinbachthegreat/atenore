package tw.com.reinbach.Atenore.Model.member.query.search;

import java.math.BigDecimal;

import javax.validation.constraints.NotEmpty;

import io.swagger.annotations.ApiModelProperty;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceSearchPreferArgDto;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;

public class PlaceFollowedSearchArgsDto {

	@ApiModelProperty(hidden = true)
	private String memberID;

	@ApiModelProperty(value = "user's lat", example = "25.0639368", required = true)
	private BigDecimal lat;

	@ApiModelProperty(value = "user's lng", example = "121.5471534", required = true)
	private BigDecimal lng;

	@ApiModelProperty(value = "場所縣市", example = "新北市")
	private String county;

	@NotEmpty(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@ApiModelProperty(required = true)
	private PlaceSearchPreferArgDto searchPreferArg;

//	分類屬性
	@ApiModelProperty(name = "typeID", value = "event type(minor)", example = "1")
	private Integer typeID;

	@ApiModelProperty(name = "mainID", value = "event main type(main)", example = "1")
	private Integer mainID;

	@ApiModelProperty(value = "capacity limit type")
	private Integer capacitySN;

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public BigDecimal getLat() {
		return lat;
	}

	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}

	public BigDecimal getLng() {
		return lng;
	}

	public void setLng(BigDecimal lng) {
		this.lng = lng;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public PlaceSearchPreferArgDto getSearchPreferArg() {
		return searchPreferArg;
	}

	public void setSearchPreferArg(PlaceSearchPreferArgDto searchPreferArg) {
		this.searchPreferArg = searchPreferArg;
	}

	public Integer getTypeID() {
		return typeID;
	}

	public void setTypeID(Integer typeID) {
		this.typeID = typeID;
	}

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public Integer getCapacitySN() {
		return capacitySN;
	}

	public void setCapacitySN(Integer capacitySN) {
		this.capacitySN = capacitySN;
	}

}
