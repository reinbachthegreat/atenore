package tw.com.reinbach.Atenore.RestController.ResponseModel;

import io.swagger.annotations.ApiModel;

@ApiModel
public class ApiError {

	public static final String UNAUTHORIZED = "Unauthorized.";

	public static final String UNAUTHORIZED_ACCESS_DATA_NOT_AUTHOR = "No data acceess, due to not original author.";

	public static final String DATA_ACCESS_ERROR = "Data access error.";

	public static final String PARARMETER_NOT_FOUND = "Parameter not found.";

	public static final String DATA_NOT_FOUND = "Data not found.";

	public static final String REQUIRED_INPUT_NOT_FONUD = "Requried input not found.";

	public static final String REQUIRED_INPUT_IS_NOT_LOGICAL = "Require input is not logical.";

	public static final String INPUT_TYPE_MISMATCHED = "Input type mismatched.";

	public static final String INPUT_LENGTH_MISMATCHED = "Input length mismatched.";

	public static final String ENCRYPT_EXCEPTION = "Encrypt exception happend.";

	public static final String VALIDATION_FAILED_PASSWORD = "Validation failed due to wrong password.";

	public static final String ACCONUT_CREATION_FAILED = "Account creation failed.";

	public static final String ACCOUNT_DELETION_FAILED = "Account deletion failed.";

	public static final String ACCOUNT_STATUS_UPDATE_FILED = "Account authority update failed.";

	public static final String ACCOUNTDETAIL_UPDATE_FAILED = "Account detail data update failed.";

	public static final String ACCONUT_ACTIVE_FAILED = "Account active failed.";

	public static final String ACCONUT_NOT_FOUND = "Account not found or Incorrect password.";

	public static final String ACCOUNT_EXISTED = "Account existed.";

	public static final String ACCOUNT_EXISTED_ACTVATED = "Account existed and activated.";

	public static final String ACCOUNT_EXISTED_NOT_ACTVATED = "Account existed and not activated.";

	public static final String ACCOUNT_CAN_NOT_MODIFY_PASSWORD = "Account belongs to third party and cannot reset or change password.";

	public static final String MAIL_SEND_FAILED = "Mail send failed.";

	public static final String AWS_FILE_UPLOAD_FAILED = "Amazon Web Service file uploading failed. ";

	public static final String REDIRECT_URL_FAILED = "Redirect url failed.";

	public static final String DATA_CREATION_FAILED = "Data creation failed.";

	public static final String EVENT_INFO_CREATION_FAILED = "Event info creation failed.";

	public static final String EVENT_TAG_CREATION_FAILED = "Event tags creation failed.";

	public static final String EVENT_RECOMMEND_CREATION_FAILED = "Event recommend creation failed.";

	public static final String EVENT_IMAGE_CREATION_FAILED = "Event image creation failed.";

	public static final String EVENT_APPLY_CANCLE_FAILED = "Event apply cancle failed.";

	public static final String EVENT_CLICK_UPDATE_FAILED = "Event click update failed.";

	public static final String EVENT_FOLLOWED_UPDATE_FAILED = "Event followed update failed.";

	public static final String EVENT_INFO_UPDATE_FAILED = "Event info update failed.";

	public static final String EVENT_TAG_UPDATE_FAILED = "Event tags update failed.";

	public static final String EVENT_RECOMMEND_UPDATE_FAILED = "Event recommend update failed.";

	public static final String EVENT_IMAGE_UPDATE_FAILED = "Event image update failed.";

	public static final String EVENT_TAG_DELETION_FAILED = "Event tags deletion failed.";

	public static final String EVENT_RECOMMEND_DELETION_FAILED = "Event recommend deletion failed.";

	public static final String EVENT_IMAGE_DELETION_FAILED = "Event image deletion failed.";

	public static final String EVENT_FOLLWED_DELETION_FAILED = "Event followed by member deletion failed.";

	public static final String EVENT_DELETION_FAILED = "Event deletion failed.";

	public static final String EVENT_FOLLOWED_FAILED = "Event followed by member failed.";

	public static final String EVENT_DISFOLLOWED_FAILED = "Event disfollowed by member failed.";

	public static final String EVENT_HAS_FOLLOWED = "Event has followed by member.";

	public static final String EVENT_HAS_NOT_FOLLOWED = "Event has not followed by member yet.";

	public static final String EVENT_APPLY_STATUS_FAILED = "Event apply status change failed";

	public static final String PINNED_EVENT_TYPE_DELETE_FAILED = "Pinned event type deletion failed.";

	public static final String PINNED_EVENT_TYPE_UPDATE_FAILED = "Pinned event type update failed.";

	public static final String PINNED_PLACE_TYPE_DELETE_FAILED = "Pinned place type deletion failed.";

	public static final String PINNED_PLACE_TYPE_UPDATE_FAILED = "Pinned place type update failed.";

	public static final String PLACE_INFO_CREATION_FAILED = "Place info creation failed.";

	public static final String PLACE_INFO_EDIT_CREATION_FAILED = "Place info edit version creation failed.";

	public static final String PLACE_LIAISON_CREATION_FAILED = "Place liaison creation failed.";

	public static final String PLACE_LIAISON_EDIT_CREATION_FAILED = "Place liaison edit version creation failed.";

	public static final String PLACE_TAG_CREATION_FAILED = "Place tags creation failed.";

	public static final String PLACE_TAG_EDIT_CREATION_FAILED = "Place tags edit version creation failed.";

	public static final String PLACE_IMAGE_CREATION_FAILED = "Place image creation failed.";

	public static final String PLACE_CLICK_UPDATE_FAILED = "Place click update failed.";

	public static final String PLACE_FOLLOWED_UPDATE_FAILED = "Place followed update failed.";

	public static final String PLACE_INFO_UPDATE_FAILED = "Place info update failed.";

	public static final String PLACE_LIAISON_UPDATE_FAILED = "Place liaison update failed.";

	public static final String PLACE_TAG_UPDATE_FAILED = "Place tags update failed.";

	public static final String PLACE_IMAGE_UPDATE_FAILED = "Place image update failed.";

	public static final String PLACE_TAG_DELETION_FAILED = "Place tags deletion failed.";

	public static final String PLACE_TAG_EDIT_DELETION_FAILED = "Place tags edit version deletion failed.";

	public static final String PLACE_IMAGE_DELETION_FAILED = "Place image deletion failed.";

	public static final String PLACE_IMAGE_EDIT_DELETION_FAILED = "Place image edit version deletion failed.";

	public static final String PLACE_LIAISON_DELETION_FAILED = "Place liaison deletion failed.";

	public static final String PLACE_LIAISON_EDIT_DELETION_FAILED = "Place liaison edit version deletion failed.";

	public static final String PLACE_DELETION_FAILED = "Place deletion failed.";

	public static final String PLACE_EDIT_DELETION_FAILED = "Place edit version deletion failed.";

	public static final String PLACE_FOLLWED_DELETION_FAILED = "Place followed by member deletion failed.";

	public static final String PLACE_FOLLOWED_FAILED = "Place followed by member failed.";

	public static final String PLACE_DISFOLLOWED_FAILED = "Place disfollowed by member failed.";

	public static final String PLACE_HAS_FOLLOWED = "Place has followed by member.";

	public static final String PLACE_HAS_NOT_FOLLOWED = "Place has not followed by member yet.";

	public static final String MASTER_FOLLOWED_FAILED = "Master followed by member failed.";

	public static final String MASTER_DISFOLLOWED_FAILED = "Master disfollowed by member failed.";

	public static final String MASTER_HAS_FOLLOWED = "Master has followed by member.";

	public static final String MASTER_HAS_NOT_FOLLOWED = "Master has not followed by member yet.";

	public static final String MASTER_INFO_CREATION_FAILED = "Master info creation failed.";

	public static final String MASTER_TAG_CREATION_FAILED = "Master tags creation failed.";

	public static final String MASTER_TAG_DELETION_FAILED = "Master tags deletion failed.";

	public static final String MASTER_CLICK_UPDATE_FAILED = "Master click update failed.";

	public static final String MASTER_FOLLOWED_UPDATE_FAILED = "Master followed update failed.";

	public static final String MASTER_INFO_UPDATE_FAILED = "Master info update failed.";

	public static final String MASTER_TAG_UPDATE_FAILED = "Master tags update failed.";

	public static final String MASTER_IMAGE_UPDATE_FAILED = "Master image update failed.";

	public static final String MASTER_RECOMMEND_DELETION_FAILED = "Master recommend deletion failed.";

	public static final String MASTER_IMAGE_DELETION_FAILED = "Master image deletion failed.";

	public static final String MASTER_FOLLWED_DELETION_FAILED = "Master followed by member deletion failed.";

	public static final String MASTER_DELETION_FAILED = "Master deletion failed.";

	public static final String MASTER_EDIT_APPLY_INUSED_FAILED = "Master edit version want to apply in used status failed.";

	public static final String MASTER_EDIT_DELETION_FAILED = "Master edit deletion failed.";

	public static final String ADS_INFO_CREATION_FAILED = "Ads info creation failed.";

	public static final String ADS_TAG_CREATION_FAILED = "Ads tag creation failed.";

	public static final String ADS_UPDATE_FAILED = "Ads update failed.";

	public static final String ADS_TAG_DELETION_FAILED = "Ads tag deletion failed.";

}
