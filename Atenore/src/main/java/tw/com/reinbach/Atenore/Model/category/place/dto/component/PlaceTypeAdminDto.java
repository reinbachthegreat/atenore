package tw.com.reinbach.Atenore.Model.category.place.dto.component;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PlaceTypeAdminDto {

	@ApiModelProperty(value = "小分類編號")
	private Integer typeID;

	@ApiModelProperty(value = "小分類名稱")
	private String name;
	
	@ApiModelProperty(value = "大分類編號")
	private Integer mainID;

	@ApiModelProperty(value = "大分類名稱")
	private String mainTypeName;

	@ApiModelProperty(value = "分類下所有場所數量")
	private Integer totalQty;

	public Integer getTypeID() {
		return typeID;
	}

	public void setTypeID(Integer typeID) {
		this.typeID = typeID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public String getMainTypeName() {
		return mainTypeName;
	}

	public void setMainTypeName(String mainTypeName) {
		this.mainTypeName = mainTypeName;
	}

	public Integer getTotalQty() {
		return totalQty;
	}

	public void setTotalQty(Integer totalQty) {
		this.totalQty = totalQty;
	}

	

}
