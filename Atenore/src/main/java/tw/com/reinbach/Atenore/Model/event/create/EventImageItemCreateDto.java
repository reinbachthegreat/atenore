package tw.com.reinbach.Atenore.Model.event.create;

import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiModelProperty;

public class EventImageItemCreateDto {

	@ApiModelProperty(hidden = true)
	private String imageID;
	
	@ApiModelProperty(hidden = true)
	private String eventID;
	
	@ApiModelProperty(hidden = true)
	private String img;
	
	@ApiModelProperty(hidden = true)
	private String objectKey;
	
	private MultipartFile file;

	public String getEventID() {
		return eventID;
	}

	public void setEventID(String eventID) {
		this.eventID = eventID;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getObjectKey() {
		return objectKey;
	}

	public void setObjectKey(String objectKey) {
		this.objectKey = objectKey;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public String getImageID() {
		return imageID;
	}

	public void setImageID(String imageID) {
		this.imageID = imageID;
	}
}
