package tw.com.reinbach.Atenore.Model.place.component;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PinPlaceTypeInfoDto {

	@ApiModelProperty(hidden = true)
	private String uuid;

	@ApiModelProperty(example = "1", notes = "活動小分類編號")
	private Integer typeID;

	@ApiModelProperty(example = "醫療院所", notes = "活動小分類名稱")
	private String typeName;

	@ApiModelProperty(example = "1", notes = "活動大分類編號")
	private Integer mainID;

	@ApiModelProperty(example = "醫療", notes = "活動大分類名稱")
	private String mainTypeName;

	@ApiModelProperty(hidden = true, example = "2020-08-05 19:50:00")
	private LocalDateTime insertTime;

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Integer getTypeID() {
		return typeID;
	}

	public void setTypeID(Integer typeID) {
		this.typeID = typeID;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public String getMainTypeName() {
		return mainTypeName;
	}

	public void setMainTypeName(String mainTypeName) {
		this.mainTypeName = mainTypeName;
	}

	public LocalDateTime getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(LocalDateTime insertTime) {
		this.insertTime = insertTime;
	}

}
