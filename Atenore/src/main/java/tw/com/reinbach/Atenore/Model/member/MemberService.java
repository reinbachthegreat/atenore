package tw.com.reinbach.Atenore.Model.member;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.multipart.MultipartFile;

import com.google.maps.model.LatLng;

import tw.com.reinbach.Atenore.Model.event.EventService;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchPreferArgDto;
import tw.com.reinbach.Atenore.Model.member.component.AccountStatusDto;
import tw.com.reinbach.Atenore.Model.member.component.AccountStatusListInfoDto;
import tw.com.reinbach.Atenore.Model.member.component.HotspotDto;
import tw.com.reinbach.Atenore.Model.member.create.ContactCreateDto;
import tw.com.reinbach.Atenore.Model.member.create.HotspotCreateDto;
import tw.com.reinbach.Atenore.Model.member.create.MemberCreateDto;
import tw.com.reinbach.Atenore.Model.member.query.HotspotInfoDto;
import tw.com.reinbach.Atenore.Model.member.query.HotspotListInfoDto;
import tw.com.reinbach.Atenore.Model.member.query.MemberInfoAdminDto;
import tw.com.reinbach.Atenore.Model.member.query.MemberInfoDto;
import tw.com.reinbach.Atenore.Model.member.query.search.EventFollowedSearchArgsDto;
import tw.com.reinbach.Atenore.Model.member.query.search.EventFollwedSearchInfoDto;
import tw.com.reinbach.Atenore.Model.member.query.search.MasterFollowedSearchArgsDto;
import tw.com.reinbach.Atenore.Model.member.query.search.MasterFollowedSearchInfoDto;
import tw.com.reinbach.Atenore.Model.member.query.search.MemberSearchInfoAdminDto;
import tw.com.reinbach.Atenore.Model.member.query.search.MemberSearchListInfoAdminDto;
import tw.com.reinbach.Atenore.Model.member.query.search.PlaceFollowedSearchArgsDto;
import tw.com.reinbach.Atenore.Model.member.query.search.PlaceFollowedSearchInfoDto;
import tw.com.reinbach.Atenore.Model.member.update.HotspotUpdateDto;
import tw.com.reinbach.Atenore.Model.member.update.MemberSatusUpdateDto;
import tw.com.reinbach.Atenore.Model.member.update.MemberUpdateDto;
import tw.com.reinbach.Atenore.Model.member.update.PasswordUpdateDto;
import tw.com.reinbach.Atenore.Model.validate.AccountStatusInfoDto;
import tw.com.reinbach.Atenore.Model.validate.ValidateDAO;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;
import tw.com.reinbach.Atenore.Utility.CipherUtility;
import tw.com.reinbach.Atenore.Utility.UUIDGenerator;
import tw.com.reinbach.Atenore.Utility.Utility;
import tw.com.reinbach.Atenore.Utility.aws.AmazonDto;
import tw.com.reinbach.Atenore.Utility.aws.AmazonService;
import tw.com.reinbach.Atenore.Utility.google.GpsService;

@Service
public class MemberService {

	@Autowired
	private MemberDAO memberDAO;

	@Autowired
	private ValidateDAO validateDAO;

	@Autowired
	private GpsService gpsService;

	@Autowired
	private AmazonService amazonService;

	@Autowired
	private Utility utility;

	public enum MemberStatus {
		INACTIVE, ACTIVE, BANNED;
	};

	private String[] county = { "基隆市", "台北市", "新北市", "宜蘭縣", "新竹市", "新竹縣", "桃園市", "苗栗縣", "台中市", "彰化縣", "南投縣", "嘉義市", "嘉義縣", "雲林縣",
			"台南市", "高雄市", "屏東縣", "台東縣", "花蓮縣", "金門縣", "連江縣", "澎湖縣" };

	@Autowired
	private PlatformTransactionManager transactionManager;

	/**
	 * 取得台灣國的縣市
	 * 
	 * @return 台灣的縣市
	 */
	public List<String> getTaiwanCounty() {
		List<String> list = new ArrayList<String>(Arrays.asList(this.county));
		return list;
	}

	/**
	 * Get member list for ADMIN by fuzzyFilter
	 * 
	 * @param fuzzyFilter
	 * @return
	 */
	public MemberSearchListInfoAdminDto getMemberListForAdminByFilter(String fuzzyFilter) {
		List<MemberSearchInfoAdminDto> list = memberDAO.getMemberListForAdminByFilter(fuzzyFilter);
		if (list != null && list.size() != 0) {
			MemberSearchListInfoAdminDto dto = new MemberSearchListInfoAdminDto();
			dto.setMemberList(list);
			return dto;
		} else {
			return null;
		}
	}

	/**
	 * Create account and set member detail by ADMIN
	 * 
	 * @param member
	 * @return
	 * @throws Throwable
	 */
	public ResponseModel<Void> createAccountByAdmin(MemberCreateDto member, MultipartFile file) throws Throwable {

		int memberExist = validateDAO.getMemberCountByAccount(member.getAccount());
		if (memberExist == -1) {
			return new ResponseModel<>(500, "Data Acceess Error");
		} else if (memberExist > 0) {
			return new ResponseModel<>(409, ApiError.ACCOUNT_EXISTED);
		}

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> resultModel = null;

		if (file != null) {
			amazonService.setFolderName("photo/MemberHead");
			try {
				AmazonDto awsDto = amazonService.uploadFile(file);
				member.setImg(awsDto.getUrl());
				member.setObjectKey(awsDto.getObjectKey());
			} catch (Exception e) {
				return new ResponseModel<>(500, e.getMessage());
			}
		}

		try {
			String memberID = UUIDGenerator.generateType4UUID().toString();
			String encryptedPwd = CipherUtility.encryptStringCommonKey(member.getAccount(), member.getPwd());
			member.setMemberID(memberID);
			member.setPwd(encryptedPwd);
			member.setStatus(MemberStatus.ACTIVE.ordinal());
			member.setAccountType("Email");

			resultModel = memberDAO.setAccountByAdmin(member);
			if (resultModel.getCode() != 201) {
				throw new Exception();
			}

			resultModel = memberDAO.setMemberDetailByAdmin(member);
			if (resultModel.getCode() != 201) {
				throw new Exception();
			}

			transactionManager.commit(status);
			return new ResponseModel<Void>(201, "");
		} catch (Exception e) {
			transactionManager.rollback(status);
			return resultModel;
		}
	}

	/**
	 * Get member's data for ADMIN
	 * 
	 * @param memberID
	 * @return
	 */
	public MemberInfoAdminDto getMemberWithDetailForAdminByMemberID(String memberID) {
		return memberDAO.getMemberWithDetailForAdminByMemberID(memberID);
	}

	/**
	 * Update member's data by memberID
	 * 
	 * @param dto
	 * @param memberID
	 * @return
	 */
	public ResponseModel<Void> updateMemberDataByMemberID(MemberUpdateDto dto, String memberID) {
		dto.setMemberID(memberID);
		MemberUpdateDto original_member = memberDAO.getMemberDetailForUpdateByMemberID(memberID);
		original_member.setMemberID(memberID);
		utility.updateByDto(dto, original_member);
		return memberDAO.updateMemberDetailByMemberID(original_member);
	}

	/**
	 * Update member's data with image file by memberID
	 * 
	 * @param dto
	 * @param memberID
	 * @param file
	 * @return
	 */
	public ResponseModel<Void> updateMemberDataByMemberID(MemberUpdateDto dto, String memberID, MultipartFile file) {
		dto.setMemberID(memberID);
		MemberUpdateDto original_member = memberDAO.getMemberDetailForUpdateByMemberID(memberID);
		original_member.setMemberID(memberID);
		String originObjectKey = original_member.getObjectKey();
		System.out.println(originObjectKey);

		utility.updateByDto(dto, original_member);
		AmazonDto awsBean = new AmazonDto();
		try {
			amazonService.setFolderName(new StringBuilder().append("photo").append("/").append("MemberHead").toString());
			awsBean = amazonService.uploadFile(file);
			original_member.setImg(awsBean.getUrl());
			original_member.setObjectKey(awsBean.getObjectKey());
		} catch (Exception e) {
			return new ResponseModel<Void>(409, ApiError.AWS_FILE_UPLOAD_FAILED);
		}

		ResponseModel<Void> updateModel = memberDAO.updateMemberDetailByMemberID(original_member);
		if (updateModel.getCode() != 204) {
			return updateModel;
		}
		ResponseModel<Void> delete = amazonService.deleteFileReturnResponse(originObjectKey);
		if (delete.getCode() == 204) {
			return updateModel;
		} else {
			return new ResponseModel<Void>(204,
					"Member data update success, but origin image file deletion failed. Due to : " + delete.getErrMsg());
		}
	}

	/**
	 * Update account status by memberID
	 * 
	 * @param memberID
	 * @param code
	 * @return
	 */
	public ResponseModel<Void> updateAccountStatusByMemberID(String memberID, MemberSatusUpdateDto status) {
		status.setMemberID(memberID);
		return memberDAO.updateAccountStatusByMemberID(status);
	}

	/**
	 * Delete account by memberID. Delete related data by account's memberID despite
	 * of account and account detail. For account and account detail, set all data
	 * null despite of 'account', 'pulbicName' and 'status', these 3 info set by
	 * system as default string or code.
	 * 
	 * @param memberID
	 * @return
	 */
	public ResponseModel<Void> deleteAccountByMemberID(String memberID) {

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> resultModel = null;

		try {

			resultModel = memberDAO.deletePinPlaceTypeByMemberID(memberID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			resultModel = memberDAO.deletePinPlaceTypeByMemberID(memberID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			resultModel = memberDAO.deletePinEventTypeByMemberID(memberID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			resultModel = memberDAO.deleteHotspotByMemberID(memberID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			resultModel = memberDAO.deleteMasterFollwedByMemberID(memberID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			resultModel = memberDAO.deletePlaceFollowedByMemberID(memberID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			resultModel = memberDAO.deleteEventFollowedByMemberID(memberID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			resultModel = memberDAO.deleteMemberDetailByMemberID(memberID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			resultModel = memberDAO.deleteAccountByMemberID(memberID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			transactionManager.commit(status);
			return new ResponseModel<Void>(204, "");
		} catch (Exception e) {
			transactionManager.rollback(status);
			return resultModel;
		}
	}

	/**
	 * Get account status list
	 * 
	 * @return
	 */
	public AccountStatusListInfoDto getAccountStatusList() {
		List<AccountStatusDto> statusList = new ArrayList<AccountStatusDto>();
		for (MemberStatus status : MemberStatus.values()) {
			AccountStatusDto dto = new AccountStatusDto();
			dto.setStatusCode(status.ordinal());
			dto.setStatus(status.name());
			statusList.add(dto);
		}
		AccountStatusListInfoDto dto = new AccountStatusListInfoDto();
		dto.setStatusList(statusList);
		return dto;
	}

	/**
	 * Get member's simple data by memberID
	 * 
	 * @param memberID
	 * @return
	 */
	public MemberInfoDto getMemberSimpleByMemberID(String memberID) {
		return memberDAO.getMemberSimpleByMemberID(memberID);
	}

	/**
	 * Get member's data by memberID
	 * 
	 * @param memberID
	 * @return
	 */
	public MemberInfoDto getMemberDetailByMemberID(String memberID) {
		return memberDAO.getMemberDetailByMemberID(memberID);
	}

	/**
	 * Update password by memberID
	 * 
	 * @param memberID
	 * @param dto
	 * @return
	 * @throws Throwable 
	 */
	public ResponseModel<Void> updatePassword(String memberID, PasswordUpdateDto dto) throws Throwable {

		AccountStatusInfoDto statusDto = memberDAO.getAccontStatus(memberID);
		if (statusDto.getErrMsg() != null) {
			return new ResponseModel<Void>(500, statusDto.getErrMsg());
		} else if (statusDto.getHasAccount() == false) {
			return new ResponseModel<Void>(404, ApiError.ACCONUT_NOT_FOUND);
		} else if (statusDto.getHasAccount() == true && !statusDto.getAccountType().equalsIgnoreCase("Email")) {
			return new ResponseModel<Void>(406, ApiError.ACCOUNT_CAN_NOT_MODIFY_PASSWORD);
		} else {

			String originalPwdEncrypted = CipherUtility.encryptStringCommonKey(statusDto.getAccount(), dto.getOriginPassword());
			String updatePwdEncrypted = CipherUtility.encryptStringCommonKey(statusDto.getAccount(), dto.getUpdatePaswword());
			dto.setUpdatePaswword(updatePwdEncrypted);
			
			Integer statusCode = statusDto.getStatus();

			MemberStatus memberStatus = MemberStatus.values()[statusCode];
			switch (memberStatus) {
			case INACTIVE:
				return new ResponseModel<Void>(409, ApiError.ACCOUNT_EXISTED_NOT_ACTVATED);
			default:
				break;
			}

			if (!statusDto.getPwd().equalsIgnoreCase(originalPwdEncrypted)) {
				return new ResponseModel<Void>(406, "Original password incorrcet.");
			}

//			if (statusDto.getPwd().equalsIgnoreCase(dto.getUpdatePaswword())) {
//				return new ResponseModel<Void>(406, "The password input is not matched with confirm password input.");
//			}
			return memberDAO.updatePassword(dto);
		}

	}

	public ResponseModel<Void> setHotspot(HotspotCreateDto dto) {

		String hotspotID = UUIDGenerator.generateType4UUID().toString();
		dto.setHotspotID(hotspotID);

		LatLng location = gpsService.getSimpleLocationFromAddress(dto.getCounty() + dto.getAddress1() + dto.getAddress2());
		if (location != null) {
			dto.setLat(BigDecimal.valueOf(location.lat));
			dto.setLng(BigDecimal.valueOf(location.lng));
		}

		return memberDAO.setHotspot(dto);
	}

	public HotspotListInfoDto getHotspotListByMemberID(String memberID) {

		HotspotListInfoDto dto = new HotspotListInfoDto();
		dto.setMemberID(memberID);
		List<HotspotDto> list = memberDAO.getHotspotListByMemberID(memberID);
		dto.setHotspotList(list);

		return dto;
	}

	public HotspotInfoDto getHotspotByHotspotID(String hotspotID) {
		return memberDAO.getHotspotByHotspotID(hotspotID);
	}

	public ResponseModel<Void> updateHotspotByHotspotID(HotspotUpdateDto dto, String hotspotID) {
		dto.setHotspotID(hotspotID);
		LatLng location = gpsService.getSimpleLocationFromAddress(dto.getCounty() + dto.getAddress1() + dto.getAddress2());
		if (location != null) {
			dto.setLat(BigDecimal.valueOf(location.lat));
			dto.setLng(BigDecimal.valueOf(location.lng));
		}

		return memberDAO.updateHotspotByHotspotID(dto);
	}

	public ResponseModel<Void> deleteHotspotByHotspotID(String hotspotID) {
		return memberDAO.deleteHotspotByHotspotID(hotspotID);
	}

	public List<EventFollwedSearchInfoDto> getEventListFollwedByMemberIDWithFilter(EventFollowedSearchArgsDto argsDto,
			String memberID) {
		argsDto.setMemberID(memberID);
		EventSearchPreferArgDto prefer = argsDto.getSearchPreferArg();
		Integer preferSN = prefer.getSearchPreferSN();
		Integer orderSN = prefer.getSearchOrderSN();
		String expired = new EventService().getApplyStatusList().get(4);

		List<EventFollwedSearchInfoDto> list = memberDAO.getEventListFollwedByMemberIDWithFilter(argsDto);
		if (list != null) {
			int index = 0;
			for (EventFollwedSearchInfoDto event : list) {
				if (argsDto.getExpired() == true) {
					event.setExpired(expired);
				}
				if (event.getLat() != null && event.getLng() != null) {
					event = this.calculator(argsDto.getLat(), argsDto.getLng(), event);
					list.set(index, event);
				}
				index++;
			}

			switch (preferSN) {
			// All, default not selected.
			case 0:
				switch (orderSN) {
				// distance order by DESC.
				case 0:

					Collections.sort(list, Comparator.comparing(EventFollwedSearchInfoDto::getDistance,
							Comparator.nullsLast(Comparator.reverseOrder())));

					break;
				// start time order by DESC.
				case 1:

					Collections.sort(list, Comparator.comparing(EventFollwedSearchInfoDto::getStartTime,
							Comparator.nullsLast(Comparator.reverseOrder())));

					break;
				// end time order by DESC.
				case 2:

					Collections.sort(list, Comparator.comparing(EventFollwedSearchInfoDto::getEndTime,
							Comparator.nullsLast(Comparator.reverseOrder())));

					break;
				}
				break;
			// Newest selected.
			case 1:
				switch (orderSN) {
				// distance order by DESC.
				case 0:

					Collections.sort(list,
							Comparator
									.comparing(EventFollwedSearchInfoDto::getInsertTime,
											Comparator.nullsLast(Comparator.reverseOrder()))
									.thenComparing(Comparator.comparing(EventFollwedSearchInfoDto::getDistance,
											Comparator.nullsLast(Comparator.naturalOrder()))));

					break;
				// start time order by DESC.
				case 1:

					Collections.sort(list,
							Comparator
									.comparing(EventFollwedSearchInfoDto::getInsertTime,
											Comparator.nullsLast(Comparator.reverseOrder()))
									.thenComparing(Comparator.comparing(EventFollwedSearchInfoDto::getStartTime,
											Comparator.nullsLast(Comparator.reverseOrder()))));

					break;
				// end time order by DESC.
				case 2:

					Collections.sort(list,
							Comparator
									.comparing(EventFollwedSearchInfoDto::getInsertTime,
											Comparator.nullsLast(Comparator.reverseOrder()))
									.thenComparing(Comparator.comparing(EventFollwedSearchInfoDto::getEndTime,
											Comparator.nullsLast(Comparator.reverseOrder()))));

					break;
				}
				break;
			// Hottest selected.
			case 2:
				switch (orderSN) {
				// distance order by DESC.
				case 0:

					Collections.sort(list,
							Comparator.comparing(EventFollwedSearchInfoDto::getClick, Comparator.nullsLast(Comparator.reverseOrder()))
									.thenComparing(Comparator.comparing(EventFollwedSearchInfoDto::getDistance,
											Comparator.nullsLast(Comparator.naturalOrder()))));

					break;
				// start time order by DESC.
				case 1:

					Collections.sort(list,
							Comparator.comparing(EventFollwedSearchInfoDto::getClick, Comparator.nullsLast(Comparator.reverseOrder()))
									.thenComparing(Comparator.comparing(EventFollwedSearchInfoDto::getStartTime,
											Comparator.nullsLast(Comparator.reverseOrder()))));

					break;
				// end time order by DESC.
				case 2:

					Collections.sort(list,
							Comparator.comparing(EventFollwedSearchInfoDto::getClick, Comparator.nullsLast(Comparator.reverseOrder()))
									.thenComparing(Comparator.comparing(EventFollwedSearchInfoDto::getEndTime,
											Comparator.nullsLast(Comparator.reverseOrder()))));

					break;
				// click order by DESC.
				case 3:

					Collections.sort(list, Comparator.comparing(EventFollwedSearchInfoDto::getClick,
							Comparator.nullsLast(Comparator.reverseOrder())));

					break;
				// follow order by DESC.
				case 4:

					Collections.sort(list, Comparator.comparing(EventFollwedSearchInfoDto::getFollow,
							Comparator.nullsLast(Comparator.reverseOrder())));

					break;
				}

				break;
			// Both newest and hottest selected.
			case 3:
				switch (orderSN) {
				// distance order by DESC.
				case 0:

					Collections.sort(list,
							Comparator.comparing(EventFollwedSearchInfoDto::getClick, Comparator.nullsLast(Comparator.reverseOrder()))
									.thenComparing(Comparator.comparing(EventFollwedSearchInfoDto::getDistance,
											Comparator.nullsLast(Comparator.naturalOrder()))));

					break;
				// start time order by DESC.
				case 1:

					Collections.sort(list,
							Comparator.comparing(EventFollwedSearchInfoDto::getClick, Comparator.nullsLast(Comparator.reverseOrder()))
									.thenComparing(Comparator.comparing(EventFollwedSearchInfoDto::getStartTime,
											Comparator.nullsLast(Comparator.reverseOrder()))));

					break;
				// end time order by DESC.
				case 2:

					Collections.sort(list,
							Comparator.comparing(EventFollwedSearchInfoDto::getClick, Comparator.nullsLast(Comparator.reverseOrder()))
									.thenComparing(Comparator.comparing(EventFollwedSearchInfoDto::getEndTime,
											Comparator.nullsLast(Comparator.reverseOrder()))));

					break;
				// click order by DESC.
				case 3:

					Collections.sort(list, Comparator.comparing(EventFollwedSearchInfoDto::getClick,
							Comparator.nullsLast(Comparator.reverseOrder())));

					break;
				// follow order by DESC.
				case 4:

					Collections.sort(list, Comparator.comparing(EventFollwedSearchInfoDto::getFollow,
							Comparator.nullsLast(Comparator.reverseOrder())));

					break;
				}
				break;
			}

		}

		return list;
	}

	public List<MasterFollowedSearchInfoDto> getMasterListFollowedByMemberIDWithFilter(MasterFollowedSearchArgsDto argsDto,
			String memberID) {
		argsDto.setMemberID(memberID);
		List<MasterFollowedSearchInfoDto> list = memberDAO.getMasterListFollowedByMemberIDWithFilter(argsDto);
		if (list != null) {
			int index = 0;
			for (MasterFollowedSearchInfoDto master : list) {

				if (master.getLat() != null && master.getLng() != null) {
					master = this.calculator(argsDto.getLat(), argsDto.getLng(), master);
					list.set(index, master);
				}
				index++;
			}

			Collections.sort(list,
					Comparator.comparing(MasterFollowedSearchInfoDto::getDistance, Comparator.nullsLast(Comparator.reverseOrder())));

		}

		return list;
	}

	public List<PlaceFollowedSearchInfoDto> getPlaceListFollowedByMemberIDWithFilter(PlaceFollowedSearchArgsDto argsDto,
			String memberID) {
		argsDto.setMemberID(memberID);
		List<PlaceFollowedSearchInfoDto> list = memberDAO.getPlaceListFollowedByMemberIDWithFilter(argsDto);
		if (list != null) {
			int index = 0;
			for (PlaceFollowedSearchInfoDto master : list) {
				if (master.getLat() != null && master.getLng() != null) {
					master = this.calculator(argsDto.getLat(), argsDto.getLng(), master);
					list.set(index, master);
				}
				index++;
			}

			switch (argsDto.getSearchPreferArg().getSearchOrderSN()) {
			// distance order by DESC.
			case 0:
				Collections.sort(list, Comparator.comparing(PlaceFollowedSearchInfoDto::getDistance,
						Comparator.nullsLast(Comparator.reverseOrder())));
				break;
			// click order by DESC.
			case 1:
				Collections.sort(list,
						Comparator.comparing(PlaceFollowedSearchInfoDto::getClick, Comparator.nullsLast(Comparator.reverseOrder())));
				break;
			// follow order by DESC.
			case 2:
				Collections.sort(list,
						Comparator.comparing(PlaceFollowedSearchInfoDto::getFollow, Comparator.nullsLast(Comparator.reverseOrder())));
				break;
			}

		}
		return list;
	}

	public ResponseModel<Void> setContact(ContactCreateDto dto) {
		dto.setUuid(UUIDGenerator.generateType4UUID().toString());
		return memberDAO.setContact(dto);
	}

	private EventFollwedSearchInfoDto calculator(BigDecimal user_lat, BigDecimal user_lng, EventFollwedSearchInfoDto event) {
		Double distance = utility.calculateDistance(user_lat.doubleValue(), user_lng.doubleValue(), event.getLat().doubleValue(),
				event.getLng().doubleValue());
		event.setDistance(distance);
		return event;
	}

	private MasterFollowedSearchInfoDto calculator(BigDecimal user_lat, BigDecimal user_lng, MasterFollowedSearchInfoDto master) {
		Double distance = utility.calculateDistance(user_lat.doubleValue(), user_lng.doubleValue(), master.getLat().doubleValue(),
				master.getLng().doubleValue());
		master.setDistance(distance);
		return master;
	}

	private PlaceFollowedSearchInfoDto calculator(BigDecimal user_lat, BigDecimal user_lng, PlaceFollowedSearchInfoDto master) {
		Double distance = utility.calculateDistance(user_lat.doubleValue(), user_lng.doubleValue(), master.getLat().doubleValue(),
				master.getLng().doubleValue());
		master.setDistance(distance);
		return master;
	}

}
