package tw.com.reinbach.Atenore.Model.place.query;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import tw.com.reinbach.Atenore.Model.place.component.PinPlaceTypeInfoDto;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PinPlaceTypeListInfoDto {

	private String memberID;

	private List<PinPlaceTypeInfoDto> pinPlaceTypeList;

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public List<PinPlaceTypeInfoDto> getPinPlaceTypeList() {
		return pinPlaceTypeList;
	}

	public void setPinPlaceTypeList(List<PinPlaceTypeInfoDto> pinPlaceTypeList) {
		this.pinPlaceTypeList = pinPlaceTypeList;
	}

}
