package tw.com.reinbach.Atenore.Model.admin.query;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;
import tw.com.reinbach.Atenore.Model.admin.component.LimitDto;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class FeatureInfoDto {

	private String featureID;
	
	private String name;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date updateTime;
	
	@ApiModelProperty(hidden = true)
	private List<LimitDto> limitList;
	
	private Boolean topFeature;

	public String getFeatureID() {
		return featureID;
	}

	public void setFeatureID(String featureID) {
		this.featureID = featureID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public List<LimitDto> getLimitList() {
		return limitList;
	}

	public void setLimitList(List<LimitDto> limitList) {
		this.limitList = limitList;
	}

	public Boolean getTopFeature() {
		return topFeature;
	}

	public void setTopFeature(Boolean topFeature) {
		this.topFeature = topFeature;
	}
}
