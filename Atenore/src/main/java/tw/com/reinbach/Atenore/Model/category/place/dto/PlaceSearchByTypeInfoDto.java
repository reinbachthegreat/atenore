package tw.com.reinbach.Atenore.Model.category.place.dto;

import java.time.LocalDateTime;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PlaceSearchByTypeInfoDto {

	@ApiModelProperty(example = "123e4567-e89b-42d3-a456-556642440000")
	private String placeID;
	
	/* 分類屬性
	 * 
	 */
	@ApiModelProperty(value = "場所分類編號", example = "1")
	private Integer typeID;
	@ApiModelProperty(value = "場所分類名稱", example = "醫療院所")
	private String typeName;
	@ApiModelProperty(value = "場所大分類編號", example = "1")
	private Integer mainID;
	@ApiModelProperty(value = "場所大分類名稱", example = "醫療")
	private String mainTypeName;

	@ApiModelProperty(value = "場所名稱", example = "台北榮總")
	private String name;

	@ApiModelProperty(example = "陳大文")
	private String mainContactor;

	@ApiModelProperty(example = "02-29298875")
	private String mainContactPhone;

	@ApiModelProperty(hidden = true)
	private String isMain;

	@ApiModelProperty(value = "場所創立者編號", example = "我高興咬我啊")
	private String memberID;

	@ApiModelProperty(hidden = true, value = "資訊創立時間", example = "2020-01-10 10:00:00")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime insertTime;

	public String getPlaceID() {
		return placeID;
	}

	public void setPlaceID(String placeID) {
		this.placeID = placeID;
	}

	public Integer getTypeID() {
		return typeID;
	}

	public void setTypeID(Integer typeID) {
		this.typeID = typeID;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public String getMainTypeName() {
		return mainTypeName;
	}

	public void setMainTypeName(String mainTypeName) {
		this.mainTypeName = mainTypeName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMainContactor() {
		return mainContactor;
	}

	public void setMainContactor(String mainContactor) {
		this.mainContactor = mainContactor;
	}

	public String getMainContactPhone() {
		return mainContactPhone;
	}

	public void setMainContactPhone(String mainContactPhone) {
		this.mainContactPhone = mainContactPhone;
	}

	public String getIsMain() {
		return isMain;
	}

	public void setIsMain(String isMain) {
		this.isMain = isMain;
	}

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public LocalDateTime getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(LocalDateTime insertTime) {
		this.insertTime = insertTime;
	}

}
