package tw.com.reinbach.Atenore.Model.admin;

import java.util.List;

import tw.com.reinbach.Atenore.Model.admin.component.FeatureLimitDto;
import tw.com.reinbach.Atenore.Model.admin.component.LimitDto;
import tw.com.reinbach.Atenore.Model.admin.create.AdminCreateDto;
import tw.com.reinbach.Atenore.Model.admin.create.FeatureCreateDto;
import tw.com.reinbach.Atenore.Model.admin.query.AdminInfoDto;
import tw.com.reinbach.Atenore.Model.admin.query.FeatureInfoDto;
import tw.com.reinbach.Atenore.Model.admin.query.search.AdminSearchInfoDto;
import tw.com.reinbach.Atenore.Model.admin.update.AdminUpdateDto;
import tw.com.reinbach.Atenore.Model.admin.update.FeatureUpdateDto;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;

public interface AdminDAO {
	
	public List<AdminSearchInfoDto> getAdminByFeatureID(String featureID);
	
	public AdminInfoDto getAdminByAdminID(String adminID);
	
	public ResponseModel<Void> setAdmin(AdminCreateDto admin);
	
	public ResponseModel<Void> updateAdmin(AdminUpdateDto admin);
	
	public AdminUpdateDto getAdminForUpdateByAdminID(String adminID);
	
	public ResponseModel<Void> deleteAdminByAdminID(String adminID);
	
	public List<FeatureInfoDto> getFeatureList();
	
	public FeatureInfoDto getFeatureByFeaturID(String featureID);
	
	public FeatureUpdateDto getFeatureForUpdateByFeaturID(String featureID);
	
	public List<LimitDto> getLimitListByFeatureID(String featureID);
	
	public ResponseModel<Void> setFeature(FeatureCreateDto feature);
	
	public ResponseModel<Void> setFeatureLimitList(List<FeatureLimitDto> limitList);
	
	public ResponseModel<Void> deleteLimitListByFeatureID(String featureID);
	
	public List<LimitDto> getLimitList();
	
	public ResponseModel<Void> updateFeature(FeatureUpdateDto feature);
	
	public ResponseModel<Void> deleteFeatureByFeatureID(String featureID);
	
	public AdminInfoDto getAdminByAccount(String account);
	
//	// 核對帳號密碼並取得帳號資料
//	public List<Map<String, Object>> checkAccountAndPwd(AdminDto bean);
//
//	// 查詢單筆帳號by adminID
//	public List<Map<String, Object>> findByID(String adminID);
//
//	// 查詢單筆帳號by account
//	public Map<String, Object> findByAccountAll(String account);
//
//	// 查詢單筆帳號by account
//	public List<Map<String, Object>> findByAccount(String account);
//
//	// 顯示管理者帳號清單
//	public List<Map<String, Object>> getALLAdminAcount(int begin, int end, String featureID, String order);
//
//	// 新增管理者
//	public int insertAdmin(AdminDto bean, String adminID);
//
//	// 變更密碼
//	public int updatePwd(String account, String pwd);
//
//	// 變更管理者資料
//	public int updateAdminInfo(AdminDto bean);
//
//	// 變更管理者權限
//	public int updateAdminLimit(String id, String featureID);
//
//	// 刪除管理者
//	public int deleteAdmin(String adminID);
//
////	-------------------------------------------------------------------------------------------------------------------
//	// feature
//	// 顯示所有身分別
//	public List<Map<String, Object>> getALLFeature();
//
//	// 顯示所有身分別 (有排序)
//	public List<Map<String, Object>> getALLFeature(int begin, int end, String order);
//
//	// 新增管理者身分別
//	public int insertFeature(String featureID, String name);
//
//	// 編輯身分別
//	public int updateFeature(String featureID, String featureName);
//
//	// 刪除身分別
//	public int deleteFeature(String featureID);
//
//	// 查詢身分別權限
//	public List<Map<String, Object>> findLimitByFeatureID(String featureID);
//
//	// 新增身分別權限
//	public int[] insertFeatureLimit(String featureID, List<Map<String, Object>> limitIDList);
//
//	// 刪除身分別權限
//	public int deleteFeatureLimitByFeatureID(String featureID);
//
////	-------------------------------------------------------------------------------------------------------------------
////	featurelimit
////	查詢管理者權限
//	public List<Map<String, Object>> getFeaturelimitByID(String featureID);
//
////	-------------------------------------------------------------------------------------------------------------------
////	limited
////	查詢所有權限
//	public List<Map<String, Object>> getAllLimitID();
//
//// 	取得Admin單一使用者的資料
//	public Map<String, Object> getUserData(String account);
//
////	取得權限資料
//	public List<Map<String, Object>> getLimitValue(String limitName);
}
