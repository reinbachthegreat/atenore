package tw.com.reinbach.Atenore.Model.category.place.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PlaceListSearchByMainTypeInfoDto {
	
	private Integer mainID;
	
	private String mainTypeName;

	private List<PlaceSearchByMainTypeInfoDto> placeList;

	public List<PlaceSearchByMainTypeInfoDto> getPlaceList() {
		return placeList;
	}

	public void setPlaceList(List<PlaceSearchByMainTypeInfoDto> placeList) {
		this.placeList = placeList;
	}

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public String getMainTypeName() {
		return mainTypeName;
	}

	public void setMainTypeName(String mainTypeName) {
		this.mainTypeName = mainTypeName;
	}
}
