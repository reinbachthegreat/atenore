package tw.com.reinbach.Atenore.Model.event.query.search;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class EventSearchListInfoAdminDto {
	
	private List<EventSearchInfoAdminDto> eventList;

	public List<EventSearchInfoAdminDto> getEventList() {
		return eventList;
	}

	public void setEventList(List<EventSearchInfoAdminDto> eventList) {
		this.eventList = eventList;
	}
	
}
