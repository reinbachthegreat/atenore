package tw.com.reinbach.Atenore.Model.place.query.search;

import io.swagger.annotations.ApiModelProperty;

public class PlaceSearchArgsMemberEditedDto {

	@ApiModelProperty(value = "會員編號", hidden = true)
	private String memberID;

	@ApiModelProperty(value = "區域縣市")
	private String county;

	@ApiModelProperty(value = "minor type")
	private Integer typeID;

	@ApiModelProperty(value = "main type")
	private Integer mainID;

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public Integer getTypeID() {
		return typeID;
	}

	public void setTypeID(Integer typeID) {
		this.typeID = typeID;
	}

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}
}
