package tw.com.reinbach.Atenore.Model.member.component;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountStatusListInfoDto {

	private List<AccountStatusDto> statusList;

	public List<AccountStatusDto> getStatusList() {
		return statusList;
	}

	public void setStatusList(List<AccountStatusDto> statusList) {
		this.statusList = statusList;
	}
}
