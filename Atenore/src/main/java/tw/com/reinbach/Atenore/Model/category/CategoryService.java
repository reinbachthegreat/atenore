package tw.com.reinbach.Atenore.Model.category;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tw.com.reinbach.Atenore.Model.category.event.EventTypeDAO;
import tw.com.reinbach.Atenore.Model.category.event.dto.EventListSearchByMainTypeArgsDto;
import tw.com.reinbach.Atenore.Model.category.event.dto.EventListSearchByMainTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.event.dto.EventListSearchByTypeArgsDto;
import tw.com.reinbach.Atenore.Model.category.event.dto.EventListSearchByTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.event.dto.EventMainTypeListInfoDto;
import tw.com.reinbach.Atenore.Model.category.event.dto.EventSearchByMainTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.event.dto.EventSearchByTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.event.dto.EventTypeCascadeDto;
import tw.com.reinbach.Atenore.Model.category.event.dto.EventTypeDto;
import tw.com.reinbach.Atenore.Model.category.event.dto.EventTypeListSearchByMainTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.event.dto.component.EventMainTypeAdminDto;
import tw.com.reinbach.Atenore.Model.category.event.dto.component.EventTypeAdminDto;
import tw.com.reinbach.Atenore.Model.category.master.MasterTypeDAO;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterListSearchByMainTypeArgsDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterListSearchByMainTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterListSearchByMiddleTypeArgsDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterListSearchByMiddleTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterListSearchByTypeArgsDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterListSearchByTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterMainTypeListInfoDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterMiddleTypeListSearchByMainTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterSearchByMainTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterSearchByMiddleTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterSearchByTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterTypeListSearchByMiddleTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterTypePrimaryCascadeDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterTypeSencondaryCascadeDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.component.MasterMainTypeDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.component.MasterMiddleTypeAdminDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.component.MasterMiddleTypeDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.component.MasterTypeAdminDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.component.MasterTypeDto;
import tw.com.reinbach.Atenore.Model.category.place.PlaceTypeDAO;
import tw.com.reinbach.Atenore.Model.category.place.dto.CapacityTypeDto;
import tw.com.reinbach.Atenore.Model.category.place.dto.PlaceListSearchByMainTypeArgsDto;
import tw.com.reinbach.Atenore.Model.category.place.dto.PlaceListSearchByMainTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.place.dto.PlaceListSearchByTypeArgsDto;
import tw.com.reinbach.Atenore.Model.category.place.dto.PlaceListSearchByTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.place.dto.PlaceMainTypeDto;
import tw.com.reinbach.Atenore.Model.category.place.dto.PlaceMainTypeListInfoDto;
import tw.com.reinbach.Atenore.Model.category.place.dto.PlaceSearchByMainTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.place.dto.PlaceSearchByTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.place.dto.PlaceTypeCascadeDto;
import tw.com.reinbach.Atenore.Model.category.place.dto.PlaceTypeDto;
import tw.com.reinbach.Atenore.Model.category.place.dto.PlaceTypeListSearchByMainTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.place.dto.component.PlaceTypeAdminDto;
import tw.com.reinbach.Atenore.Model.event.component.EventDto;
import tw.com.reinbach.Atenore.Model.place.PlaceService;

@Service
public class CategoryService {

	@Autowired
	private EventTypeDAO eventTypeDAO;

	@Autowired
	private MasterTypeDAO masterTypeDAO;

	@Autowired
	private PlaceTypeDAO placeTypeDAO;

	public final Integer placeMainID = 29;

//	private String[] capacityLimit = { "1-10人", "11-100人", "101-500人", "501-1000人" };
//
//	/**
//	 * 取得場所容納人數
//	 * 
//	 * @return {"1-10人", "11-100人", "101-500人", "501-1000人"}
//	 */
//	public ArrayList<String> getCapacityLimitList() {
//		return new ArrayList<String>(Arrays.asList(this.capacityLimit));
//	}

	/*
	 * Event Type
	 * Series=======================================================================
	 * =============
	 */

	/**
	 * ADMIN:Get event main type list with static data by searching filter
	 * 
	 * 
	 * @param filter
	 * @return
	 */
	public EventMainTypeListInfoDto getEventMainTypeListWithFilter(String filter) {
		EventMainTypeListInfoDto dto = new EventMainTypeListInfoDto();
		dto.setEventMainTypeList(eventTypeDAO.getEventMainTypeListWithFilter(filter));
		return dto;
	}

	/**
	 * ADMIN:Get event list by event main type ID and filters
	 * 
	 * @param mainID
	 * @param argsDto
	 * @return
	 */
	public EventListSearchByMainTypeInfoDto getEventListByMainTypeWithFilter(Integer mainID,
			EventListSearchByMainTypeArgsDto argsDto) {
		argsDto.setMainID(mainID);
		List<EventSearchByMainTypeInfoDto> list = eventTypeDAO.getEventListByMainTypeWithFilter(argsDto);
		if (list != null && list.size() != 0) {
			EventListSearchByMainTypeInfoDto dto = new EventListSearchByMainTypeInfoDto();
			dto.setEventList(list);
			dto.setMainTypeName(list.get(0).getMainTypeName());
			return dto;
		} else {
			return null;
		}
	}

	/**
	 * ADMIN:Get event type list with static data by main type and type name fuzzy filter
	 * 
	 * @param mainID
	 * @param fuzzyFilter
	 * @return
	 */
	public EventTypeListSearchByMainTypeInfoDto getEventTypeListByMainTypeWithFilter(Integer mainID, String fuzzyFilter) {
		if (fuzzyFilter == null || fuzzyFilter != null && fuzzyFilter.trim().length() == 0) {
			fuzzyFilter = null;
		}

		List<EventTypeAdminDto> list = eventTypeDAO.getEventTypeListByMainTypeWithFilter(mainID, fuzzyFilter);
		if (list != null && list.size() != 0) {
			EventTypeListSearchByMainTypeInfoDto dto = new EventTypeListSearchByMainTypeInfoDto();
			dto.setEventTypeList(list);
			dto.setMainTypeName(list.get(0).getMainType());
			return dto;
		} else {
			return null;
		}
	}

	/**
	 * ADMIN:Get event list by event type ID and filters
	 * 
	 * @param typeID
	 * @param argsDto
	 * @return
	 */
	public EventListSearchByTypeInfoDto getEventListByTypeWithFilter(Integer typeID, EventListSearchByTypeArgsDto argsDto) {
		argsDto.setTypeID(typeID);

		List<EventSearchByTypeInfoDto> list = eventTypeDAO.getEventListByTypeWithFilter(argsDto);

		if (list != null && list.size() != 0) {
			EventListSearchByTypeInfoDto dto = new EventListSearchByTypeInfoDto();
			dto.setEventList(list);
			dto.setMainTypeName(list.get(0).getMainTypeName());
			dto.setTypeName(list.get(0).getTypeName());
			dto.setMainID(list.get(0).getMainID());
			return dto;
		} else {
			return null;
		}
	}

	/**
	 * ADMIN:Get event type list with filter
	 *
	 * 
	 * @param filter
	 * @return
	 */
	public List<EventTypeAdminDto> getEventTypeListWithFilter(String filter) {
		if (filter == null || filter != null && filter.trim().length() == 0) {
			return eventTypeDAO.getEventTypeListWithStatics();
		} else {
			return eventTypeDAO.getEventTypeListWithFilter(filter);
		}
	}

	/**
	 * 取得活動分類下的活動列表：篩選器
	 * 
	 * @param typeID
	 * @param eventFilter
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public List<EventDto> getEventListByTypeWithFilter(Integer typeID, String eventFilter, LocalDateTime startTime,
			LocalDateTime endTime) {
		if (eventFilter != null || startTime != null || endTime != null) {
			return eventTypeDAO.getEventListByTypeWithFilter(typeID, eventFilter, startTime, endTime);
		} else {
			return eventTypeDAO.getEventListByType(typeID);
		}
	}

	/**
	 * 取得活動大分類與活動分類的階層式列表
	 * 
	 * @return
	 */
	public List<EventTypeCascadeDto> getEventTypeCascadeWithMainType() {

		List<EventMainTypeAdminDto> mainList = eventTypeDAO.getEventMainTypeListWithFilter(null);
		List<EventTypeDto> typeList = eventTypeDAO.getEventTypeList();
		List<EventTypeCascadeDto> list = new ArrayList<EventTypeCascadeDto>();

		for (EventMainTypeAdminDto main : mainList) {
			EventTypeCascadeDto cascade = new EventTypeCascadeDto();
			cascade.setMinorTypeList(new ArrayList<EventTypeDto>());
			for (EventTypeDto type : typeList) {
				if (type.getMainID() == main.getMainID()) {
					type.setMainID(null);
					cascade.addMinorTypeList(type);
				} else {
					continue;
				}
			}
			cascade.setMainID(main.getMainID());
			cascade.setName(main.getName());
			list.add(cascade);
		}

		return list;
	}

	/*
	 * Place Type
	 * Series=======================================================================
	 * =============
	 */

	/**
	 * ADMIN:Get place main type list with static data by searching filter
	 * 
	 * 
	 * @param filter
	 * @return
	 */
	public PlaceMainTypeListInfoDto getPlaceMainTypeListWithFilter(String filter) {
		PlaceMainTypeListInfoDto dto = new PlaceMainTypeListInfoDto();
		dto.setPlaceMainTypeList(placeTypeDAO.getPlaceMainTypeListWithFilter(filter));
		return dto;
	}

	/**
	 * ADMIN:Get place list by place main type ID and filters
	 * 
	 * @param mainID
	 * @param argsDto
	 * @return
	 */
	public PlaceListSearchByMainTypeInfoDto getPlaceListByMainTypeWithFilter(Integer mainID,
			PlaceListSearchByMainTypeArgsDto argsDto) {
		argsDto.setMainID(mainID);
		List<PlaceSearchByMainTypeInfoDto> list = placeTypeDAO.getPlaceListByMainTypeWithFilter(argsDto);
		if (list != null && list.size() != 0) {
			PlaceListSearchByMainTypeInfoDto dto = new PlaceListSearchByMainTypeInfoDto();
			dto.setMainID(mainID);
			dto.setMainTypeName(list.get(0).getMainTypeName());
			List<PlaceSearchByMainTypeInfoDto> briefList = new ArrayList<PlaceSearchByMainTypeInfoDto>();
			for (PlaceSearchByMainTypeInfoDto info : list) {
				if (info.getIsMain() == null || info.getIsMain().equals("true")) {
					briefList.add(info);
				}
			}
			dto.setPlaceList(briefList);
			return dto;
		} else {
			return null;
		}
	}

	/**
	 * ADMIN:Get place type list with static data by main type and type name fuzzy
	 * filter
	 * 
	 * @param mainID
	 * @param filter
	 * @return
	 */
	public PlaceTypeListSearchByMainTypeInfoDto getPlaceTypeListWithFilter(Integer mainID, String filter) {
		List<PlaceTypeAdminDto> list = placeTypeDAO.getPlaceTypeListByMainTypeWithFilter(mainID, filter);
		if (list != null && list.size() != 0) {
			PlaceTypeListSearchByMainTypeInfoDto dto = new PlaceTypeListSearchByMainTypeInfoDto();
			dto.setMainTypeName(list.get(0).getMainTypeName());
			dto.setPlaceTypeList(list);
			return dto;
		} else {
			return null;
		}
	}

	/**
	 * ADMIN:Get place list by place type ID and filters
	 * 
	 * @param typeID
	 * @param argsDto
	 * @return
	 */
	public PlaceListSearchByTypeInfoDto getPlaceListByTypeWithFilter(Integer typeID, PlaceListSearchByTypeArgsDto argsDto) {
		argsDto.setTypeID(typeID);
		List<PlaceSearchByTypeInfoDto> list = placeTypeDAO.getPlaceListByTypeWithFilter(argsDto);
		if (list != null && list.size() != 0) {
			PlaceListSearchByTypeInfoDto dto = new PlaceListSearchByTypeInfoDto();
			dto.setMainID(list.get(0).getMainID());
			dto.setTypeID(typeID);
			dto.setMainTypeName(list.get(0).getMainTypeName());
			dto.setTypeName(list.get(0).getTypeName());
			List<PlaceSearchByTypeInfoDto> briefList = new ArrayList<PlaceSearchByTypeInfoDto>();
			for (PlaceSearchByTypeInfoDto info : list) {
				if (info.getIsMain() == null || info.getIsMain().equals("true")) {
					briefList.add(info);
				}
			}
			dto.setPlaceList(briefList);
			return dto;
		} else {
			return null;
		}
	}

	/**
	 * 取得場所大分類與場所分類的階層式列表
	 * 
	 * @return
	 */
	public List<PlaceTypeCascadeDto> getPlaceTypeCascadeWithMainType() {

		List<PlaceMainTypeDto> mainList = placeTypeDAO.getPlaceMainTypeList();
		List<PlaceTypeDto> typeList = placeTypeDAO.getPlaceTypeList();
		List<PlaceTypeCascadeDto> list = new ArrayList<PlaceTypeCascadeDto>();
		List<CapacityTypeDto> capacityList = new ArrayList<CapacityTypeDto>();
		List<String> capacityLimit = new PlaceService().getCapacity();
		int capacityIndex = 0;
		for (String c : capacityLimit) {
			CapacityTypeDto capacity = new CapacityTypeDto();
			capacity.setCapacitySN(capacityIndex);
			capacity.setCapacityName(c);
			capacityList.add(capacity);
			capacityIndex++;
		}

		for (PlaceMainTypeDto main : mainList) {
			PlaceTypeCascadeDto cascade = new PlaceTypeCascadeDto();
			cascade.setMinorTypeList(new ArrayList<PlaceTypeDto>());
			for (PlaceTypeDto type : typeList) {
				if (type.getMainID() == main.getMainID()) {
					type.setMainID(null);
					cascade.addMinorTypeList(type);
					if (main.getMainID() == this.placeMainID) {
						type.setCapacityList(capacityList);
					}
				} else {
					continue;
				}
			}
			cascade.setMainID(main.getMainID());
			cascade.setName(main.getName());
			list.add(cascade);
		}

		return list;
	}

	/*
	 * Master Type Series
	 * 
	 */

	/**
	 * ADMIN:Get master main type list with static data by searching filter
	 * 
	 * @param fuzzyFilter
	 * @return
	 */
	public MasterMainTypeListInfoDto getMasterMainTypeListWithFilter(String fuzzyFilter) {
		MasterMainTypeListInfoDto dto = new MasterMainTypeListInfoDto();
		dto.setMasterMainTypeList(masterTypeDAO.getMasterMainTypeListWithFilter(fuzzyFilter));
		return dto;
	}

	/**
	 * ADMIN:Get master list by master main type ID and filters
	 * 
	 * @param mainID
	 * @param argsDto
	 * @return
	 */
	public MasterListSearchByMainTypeInfoDto getMasterListByMainTypeWithFilter(Integer mainID,
			MasterListSearchByMainTypeArgsDto argsDto) {
		argsDto.setMainID(mainID);

		List<MasterSearchByMainTypeInfoDto> list = masterTypeDAO.getMasterListByMainTypeWithFilter(argsDto);
		if (list != null && list.size() != 0) {
			MasterListSearchByMainTypeInfoDto dto = new MasterListSearchByMainTypeInfoDto();
			dto.setMainID(mainID);
			dto.setMainTypeName(list.get(0).getMainTypeName());
			dto.setMasterList(list);
			return dto;
		} else {
			return null;
		}
	}

	/**
	 * ADMIN:Get master middle type list with static data by main type and type name
	 * fuzzy filter
	 * 
	 * @param mainID
	 * @param fuzzyFilter
	 * @return
	 */
	public MasterMiddleTypeListSearchByMainTypeInfoDto getMasterMiddleTypeListByMainTypeWithFilter(Integer mainID,
			String fuzzyFilter) {
		List<MasterMiddleTypeAdminDto> list = masterTypeDAO.getMasterMiddleTypeListByMainTypeWithFilter(mainID, fuzzyFilter);
		if (list != null && list.size() != 0) {
			MasterMiddleTypeListSearchByMainTypeInfoDto dto = new MasterMiddleTypeListSearchByMainTypeInfoDto();
			dto.setMasterMiddleTypeList(list);
			dto.setMainTypeName(list.get(0).getMainTypeName());
			return dto;
		} else {
			return null;
		}
	}

	/**
	 * ADMIN:Get master list by master middle type ID and filters
	 * 
	 * @param midID
	 * @param argsDto
	 * @return
	 */
	public MasterListSearchByMiddleTypeInfoDto getMasterListByMiddleTypeWithFilter(Integer midID,
			MasterListSearchByMiddleTypeArgsDto argsDto) {
		argsDto.setMidID(midID);
		List<MasterSearchByMiddleTypeInfoDto> list = masterTypeDAO.getMasterListByMiddleTypeWithFilter(argsDto);
		if (list != null && list.size() != 0) {
			MasterListSearchByMiddleTypeInfoDto dto = new MasterListSearchByMiddleTypeInfoDto();
			dto.setMainID(list.get(0).getMainID());
			dto.setMainTypeName(list.get(0).getMainTypeName());
			dto.setMiddleID(midID);
			dto.setMiddleTypeName(list.get(0).getMiddleTypeName());
			dto.setMasterList(list);
			return dto;
		} else {
			return null;
		}
	}

	/**
	 * ADMIN:Get master type list with static data by middle type and type name
	 * fuzzy filter
	 * 
	 * @param midID
	 * @param fuzzyFilter
	 * @return
	 */
	public MasterTypeListSearchByMiddleTypeInfoDto getMasterTypeListByMiddleTypeWithFilter(Integer midID, String fuzzyFilter) {
		List<MasterTypeAdminDto> list = masterTypeDAO.getMasterTypeListByMiddleTypeWithFilter(midID, fuzzyFilter);
		if (list != null && list.size() != 0) {
			MasterTypeListSearchByMiddleTypeInfoDto dto = new MasterTypeListSearchByMiddleTypeInfoDto();
			dto.setMainTypeName(list.get(0).getMainTypeName());
			dto.setMiddleTypeName(list.get(0).getMiddleTypeName());
			dto.setMasterTypeList(list);
			return dto;
		} else {
			return null;
		}
	}

	/**
	 * ADMIN:Get master list by master type ID and filters
	 * 
	 * @param typeID
	 * @param argsDto
	 * @return
	 */
	public MasterListSearchByTypeInfoDto getMasterListByTypeWithFilter(Integer typeID, MasterListSearchByTypeArgsDto argsDto) {
		argsDto.setTypeID(typeID);
		List<MasterSearchByTypeInfoDto> list = masterTypeDAO.getMasterListByTypeWithFilter(argsDto);
		if (list != null && list.size() != 0) {
			MasterListSearchByTypeInfoDto dto = new MasterListSearchByTypeInfoDto();
			dto.setTypeID(typeID);
			dto.setTypeName(list.get(0).getTypeName());
			dto.setMiddleID(list.get(0).getMiddleID());
			dto.setMiddleTypeName(list.get(0).getMiddleTypeName());
			dto.setMainID(list.get(0).getMainID());
			dto.setMainTypeName(list.get(0).getMainTypeName());
			dto.setMasterList(list);
			return dto;
		} else {
			return null;
		}

	}

	/**
	 * 取得達人大分類雨中分類與小分類的階層式列表
	 * 
	 * @return
	 */
	public List<MasterTypeSencondaryCascadeDto> getMasterTypeCascadeWithMainType() {

		List<MasterMainTypeDto> mainList = masterTypeDAO.getMasterMainTypeList();
		List<MasterMiddleTypeDto> middleList = masterTypeDAO.getMasterMiddleTypeList();
		List<MasterTypeDto> typeList = masterTypeDAO.getMasterTypeList();
		List<MasterTypeSencondaryCascadeDto> list = new LinkedList<MasterTypeSencondaryCascadeDto>();
		for (MasterMainTypeDto main : mainList) {
			List<MasterTypePrimaryCascadeDto> listPrime = new LinkedList<MasterTypePrimaryCascadeDto>();
			MasterTypeSencondaryCascadeDto cascadeSecondary = new MasterTypeSencondaryCascadeDto();
			List<MasterTypePrimaryCascadeDto> masterTypePrimaryCascadeDtoList = new LinkedList<MasterTypePrimaryCascadeDto>();
			cascadeSecondary.setMiddleTypeList(masterTypePrimaryCascadeDtoList);
			for (MasterMiddleTypeDto mid : middleList) {

				MasterTypePrimaryCascadeDto cascadePrimary = new MasterTypePrimaryCascadeDto();
				List<MasterTypeDto> masterTypeDtoList = new LinkedList<MasterTypeDto>();
				cascadePrimary.setMinorTypeList(masterTypeDtoList);

				if (mid.getMainID() == main.getMainID()) {
					mid.setMainID(null);
					cascadeSecondary.addMinorTypeList(cascadePrimary);

					for (MasterTypeDto type : typeList) {
						if ((int) type.getMidID() == (int) mid.getMidID()) {
							cascadePrimary.addMinorTypeList(type);
						} else {
							continue;
						}
					}
					cascadePrimary.setMidID(mid.getMidID());
					cascadePrimary.setName(mid.getName());
					listPrime.add(cascadePrimary);
				} else {
					continue;
				}

			}
			cascadeSecondary.setMainID(main.getMainID());
			cascadeSecondary.setName(main.getName());
			list.add(cascadeSecondary);
		}

//		for(EventMainTypeDto main : mainList){
//			EventTypeCascadeDto cascade = new EventTypeCascadeDto();
//			cascade.setMinorTypeList(new ArrayList<EventTypeDto>());
//			for(EventTypeDto type : typeList) {
//				if(type.getMainID()==main.getMainID()) {
//					type.setMainID(null);
//					cascade.addMinorTypeList(type);
//				}else {
//					continue;
//				}
//			}
//			cascade.setMainID(main.getMainID());
//			cascade.setName(main.getName());
//			list.add(cascade);
//		}

		return list;
	}

}
