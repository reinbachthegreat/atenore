package tw.com.reinbach.Atenore.Model.member.query.search;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MemberSearchListInfoAdminDto {

	private List<MemberSearchInfoAdminDto> memberList;

	public List<MemberSearchInfoAdminDto> getMemberList() {
		return memberList;
	}

	public void setMemberList(List<MemberSearchInfoAdminDto> memberList) {
		this.memberList = memberList;
	}
}
