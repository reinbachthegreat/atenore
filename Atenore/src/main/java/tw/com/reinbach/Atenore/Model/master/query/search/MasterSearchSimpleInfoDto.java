package tw.com.reinbach.Atenore.Model.master.query.search;

import io.swagger.annotations.ApiModelProperty;

public class MasterSearchSimpleInfoDto {

	@ApiModelProperty(example = "123e4567-e89b-42d3-a456-556642440000")
	private String masterID;

	@ApiModelProperty(example = "鮮果達人")
	private String publicName;

	@ApiModelProperty(example = "https://upload.wikimedia.org/wikipedia/commons/1/1b/%E8%94%A1%E8%8B%B1%E6%96%87%E5%AE%98%E6%96%B9%E5%85%83%E9%A6%96%E8%82%96%E5%83%8F%E7%85%A7.png")
	private String img;

	public String getMasterID() {
		return masterID;
	}

	public void setMasterID(String masterID) {
		this.masterID = masterID;
	}

	public String getPublicName() {
		return publicName;
	}

	public void setPublicName(String publicName) {
		this.publicName = publicName;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

}
