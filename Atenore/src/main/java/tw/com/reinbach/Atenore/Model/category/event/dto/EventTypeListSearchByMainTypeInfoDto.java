package tw.com.reinbach.Atenore.Model.category.event.dto;

import java.util.List;

import tw.com.reinbach.Atenore.Model.category.event.dto.component.EventTypeAdminDto;

public class EventTypeListSearchByMainTypeInfoDto {

	private String mainTypeName;
	
	private String typeName;
	
	private List<EventTypeAdminDto> eventTypeList;

	public List<EventTypeAdminDto> getEventTypeList() {
		return eventTypeList;
	}

	public void setEventTypeList(List<EventTypeAdminDto> eventTypeList) {
		this.eventTypeList = eventTypeList;
	}

	public String getMainTypeName() {
		return mainTypeName;
	}

	public void setMainTypeName(String mainTypeName) {
		this.mainTypeName = mainTypeName;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	
}
