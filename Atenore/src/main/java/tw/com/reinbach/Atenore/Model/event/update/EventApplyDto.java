package tw.com.reinbach.Atenore.Model.event.update;

import io.swagger.annotations.ApiModelProperty;

public class EventApplyDto {

	@ApiModelProperty(hidden = true)
	private String eventID;
	
	@ApiModelProperty(required = true)
	private Boolean isPass;
	
	@ApiModelProperty(hidden = true)
	private String applyStatus;

	@ApiModelProperty(notes = "拒絕通過的原因")
	private String refuseReason;

	public String getEventID() {
		return eventID;
	}

	public void setEventID(String eventID) {
		this.eventID = eventID;
	}

	public String getApplyStatus() {
		return applyStatus;
	}

	public void setApplyStatus(String applyStatus) {
		this.applyStatus = applyStatus;
	}

	public String getRefuseReason() {
		return refuseReason;
	}

	public void setRefuseReason(String refuseReason) {
		this.refuseReason = refuseReason;
	}

	public Boolean getIsPass() {
		return isPass;
	}

	public void setIsPass(Boolean isPass) {
		this.isPass = isPass;
	}
	
	
	
}
