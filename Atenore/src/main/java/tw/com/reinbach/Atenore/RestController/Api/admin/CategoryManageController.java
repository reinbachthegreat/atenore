package tw.com.reinbach.Atenore.RestController.Api.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import tw.com.reinbach.Atenore.Model.category.CategoryService;
import tw.com.reinbach.Atenore.Model.category.event.dto.EventListSearchByMainTypeArgsDto;
import tw.com.reinbach.Atenore.Model.category.event.dto.EventListSearchByMainTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.event.dto.EventListSearchByTypeArgsDto;
import tw.com.reinbach.Atenore.Model.category.event.dto.EventListSearchByTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.event.dto.EventMainTypeListInfoDto;
import tw.com.reinbach.Atenore.Model.category.event.dto.EventTypeListSearchByMainTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterListSearchByMainTypeArgsDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterListSearchByMainTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterListSearchByMiddleTypeArgsDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterListSearchByMiddleTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterListSearchByTypeArgsDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterListSearchByTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterMainTypeListInfoDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterMiddleTypeListSearchByMainTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterTypeListSearchByMiddleTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.place.dto.PlaceListSearchByMainTypeArgsDto;
import tw.com.reinbach.Atenore.Model.category.place.dto.PlaceListSearchByMainTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.place.dto.PlaceListSearchByTypeArgsDto;
import tw.com.reinbach.Atenore.Model.category.place.dto.PlaceListSearchByTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.place.dto.PlaceMainTypeListInfoDto;
import tw.com.reinbach.Atenore.Model.category.place.dto.PlaceTypeListSearchByMainTypeInfoDto;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;

@RestController
@RequestMapping(value = { "/admin/category" })
public class CategoryManageController {

	@Autowired
	private CategoryService catergoryService;

	@InitBinder
	public void initBinder(WebDataBinder webDataBinder) {
		webDataBinder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}

	/**
	 * Get event main type with static data list by name fuzzy filter 
	 * 2020.08.12 upgrade
	 * 
	 * @param fuzzyFilter
	 * @return event main type with static data list
	 */
	@ApiOperation(notes = "ApiNo:A1", value = "Get event main type with static data list by name fuzzy filter")
	@RequestMapping(value = { "/event/main" }, method = { RequestMethod.GET })
	public ResponseModel<EventMainTypeListInfoDto> getEventMainTypeListWithFilter(
			@ApiParam(value = "fuzzy searching by name keywords", required = false, example = "醫") @RequestParam(required = false) String fuzzyFilter) {

		// 檢核篩選器是否存在
		if (fuzzyFilter == null || fuzzyFilter != null && fuzzyFilter.trim().length() == 0) {
			fuzzyFilter = null;
		}

		EventMainTypeListInfoDto dto = catergoryService.getEventMainTypeListWithFilter(fuzzyFilter);
		if (dto == null || dto != null && dto.getEventMainTypeList().size() == 0) {
			return new ResponseModel<EventMainTypeListInfoDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<EventMainTypeListInfoDto>(200, dto);
		}
	}

	/**
	 * Get event list by main type ID and filters
	 * 
	 * @param mainID
	 * @param argsDto
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A2", value = "Get event list by main type ID and filters")
	@RequestMapping(value = { "/event/main/eventlist/{mainID}" }, method = { RequestMethod.GET })
	public ResponseModel<EventListSearchByMainTypeInfoDto> getEventListByMainTypeWithFilter(
			@ApiParam(required = true, example = "1") @PathVariable(required = true) Integer mainID,
			EventListSearchByMainTypeArgsDto argsDto) {
		EventListSearchByMainTypeInfoDto dto = catergoryService.getEventListByMainTypeWithFilter(mainID, argsDto);
		if (dto == null || dto.getEventList() != null && dto.getEventList().size() == 0) {
			return new ResponseModel<>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<EventListSearchByMainTypeInfoDto>(200, dto);
		}
	}

	/**
	 * Get event type list by main type ID and type name fuzzy filter
	 * 
	 * @param mainID
	 * @param fuzzyFilter
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A3", value = "Get event type list by main type ID and type name fuzzy filter")
	@RequestMapping(value = { "/event/main/typelist/{mainID}" }, method = RequestMethod.GET)
	public ResponseModel<EventTypeListSearchByMainTypeInfoDto> getEventTypeListByMainTypeWithFilter(
			@PathVariable(required = true) Integer mainID, @RequestParam(required = false) String fuzzyFilter) {
		EventTypeListSearchByMainTypeInfoDto dto = catergoryService.getEventTypeListByMainTypeWithFilter(mainID, fuzzyFilter);
		if (dto == null || dto.getEventTypeList() != null && dto.getEventTypeList().size() == 0) {
			return new ResponseModel<EventTypeListSearchByMainTypeInfoDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<EventTypeListSearchByMainTypeInfoDto>(200, dto);
		}
	}

	/**
	 * Get event list by type ID and filters
	 * 
	 * @param typeID
	 * @param argsDto
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A4", value = "Get event list by type ID and filters")
	@RequestMapping(value = { "/event/type/eventlist/{typeID}" }, method = { RequestMethod.GET })
	public ResponseModel<EventListSearchByTypeInfoDto> getEventListByTypeWithFilter(
			@ApiParam(required = true, example = "1") @PathVariable(required = true) Integer typeID,
			EventListSearchByTypeArgsDto argsDto) {
		EventListSearchByTypeInfoDto dto = catergoryService.getEventListByTypeWithFilter(typeID, argsDto);
		if (dto == null || dto.getEventList() != null && dto.getEventList().size() == 0) {
			return new ResponseModel<>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<EventListSearchByTypeInfoDto>(200, dto);
		}
	}

	/**
	 * Get place main type with static data list by name fuzzy filter
	 * 
	 * @param fuzzyFilter
	 * @return place main type with static data list
	 */
	@ApiOperation(notes = "ApiNo:A5", value = "Get place main type with static data list by name fuzzy filter")
	@RequestMapping(value = { "/place/main" }, method = { RequestMethod.GET })
	public ResponseModel<PlaceMainTypeListInfoDto> getPlaceMainTypeListWithFilter(
			@ApiParam(value = "fuzzy searching by name keywords", required = false, example = "醫") @RequestParam(required = false) String fuzzyFilter) {
		// 檢核篩選器是否存在
		if (fuzzyFilter == null || fuzzyFilter != null && fuzzyFilter.trim().length() == 0) {
			fuzzyFilter = null;
		}

		PlaceMainTypeListInfoDto dto = catergoryService.getPlaceMainTypeListWithFilter(fuzzyFilter);
		if (dto == null || dto.getPlaceMainTypeList() != null && dto.getPlaceMainTypeList().size() == 0) {
			return new ResponseModel<PlaceMainTypeListInfoDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<PlaceMainTypeListInfoDto>(200, dto);
		}
	}

	/**
	 * Get place list by main type ID and filters
	 * 
	 * @param mainID
	 * @param argsDto
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A6", value = "Get plcae list by main type ID and filters")
	@RequestMapping(value = { "/place/main/placelist/{mainID}" }, method = { RequestMethod.GET })
	public ResponseModel<PlaceListSearchByMainTypeInfoDto> getPlaceListByMainTypeWithFilter(
			@ApiParam(required = true, example = "1") @PathVariable(required = true) Integer mainID,
			PlaceListSearchByMainTypeArgsDto argsDto) {
		PlaceListSearchByMainTypeInfoDto dto = catergoryService.getPlaceListByMainTypeWithFilter(mainID, argsDto);
		if (dto == null || dto != null && dto.getPlaceList().size() == 0) {
			return new ResponseModel<PlaceListSearchByMainTypeInfoDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<PlaceListSearchByMainTypeInfoDto>(200, dto);
		}
	}

	/**
	 * Get place type list by main type ID and type name fuzzy filter
	 * 
	 * @param mainID
	 * @param fuzzyFilter
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A7", value = "Get place type list by main type ID and type name fuzzy filter")
	@RequestMapping(value = { "/place/main/typelist/{mainID}" }, method = { RequestMethod.GET })
	public ResponseModel<PlaceTypeListSearchByMainTypeInfoDto> getPlaceTypeListByMainTypeWithFilter(
			@PathVariable(required = true) Integer mainID, @RequestParam(required = false) String fuzzyFilter) {
		// 檢核篩選器是否存在
		if (fuzzyFilter == null || fuzzyFilter != null && fuzzyFilter.trim().length() == 0) {
			fuzzyFilter = null;
		}

		PlaceTypeListSearchByMainTypeInfoDto dto = catergoryService.getPlaceTypeListWithFilter(mainID, fuzzyFilter);
		if (dto == null || dto.getPlaceTypeList() != null && dto.getPlaceTypeList().size() == 0) {
			return new ResponseModel<PlaceTypeListSearchByMainTypeInfoDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<PlaceTypeListSearchByMainTypeInfoDto>(200, dto);
		}
	}

	/**
	 * Get place list by type ID and filters
	 * 
	 * @param typeID
	 * @param argsDto
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A8", value = "Get place list by type ID and filters")
	@RequestMapping(value = { "/place/type/placelist/{typeID}" }, method = { RequestMethod.GET })
	public ResponseModel<PlaceListSearchByTypeInfoDto> getPlaceListByTypeWithFilter(
			@ApiParam(required = true, example = "1") @PathVariable(required = true) Integer typeID,
			PlaceListSearchByTypeArgsDto argsDto) {
		PlaceListSearchByTypeInfoDto dto = catergoryService.getPlaceListByTypeWithFilter(typeID, argsDto);
		if (dto == null || dto.getPlaceList() != null && dto.getPlaceList().size() == 0) {
			return new ResponseModel<PlaceListSearchByTypeInfoDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<PlaceListSearchByTypeInfoDto>(200, dto);
		}
	}

	/**
	 * Get master main type with static data list by name fuzzy filter
	 * 
	 * @param fuzzyFilter
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A9", value = "Get master main type with static data list by name fuzzy filter")
	@RequestMapping(value = { "/master/main" }, method = { RequestMethod.GET })
	public ResponseModel<MasterMainTypeListInfoDto> getMasterMainTypeListWithFilter(
			@ApiParam(value = "fuzzy searching by name keywords", required = false, example = "醫") @RequestParam(required = false) String fuzzyFilter) {

		// 檢核篩選器是否存在
		if (fuzzyFilter == null || fuzzyFilter != null && fuzzyFilter.trim().length() == 0) {
			fuzzyFilter = null;
		}

		MasterMainTypeListInfoDto dto = catergoryService.getMasterMainTypeListWithFilter(fuzzyFilter);
		if (dto == null || dto != null && dto.getMasterMainTypeList().size() == 0) {
			return new ResponseModel<MasterMainTypeListInfoDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<MasterMainTypeListInfoDto>(200, dto);
		}
	}

	/**
	 * Get master list by main type ID and filters
	 * 
	 * @param mainID
	 * @param argsDto
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A10", value = "Get master list by main type ID and filters")
	@RequestMapping(value = { "/master/main/masterlist/{mainID}" }, method = { RequestMethod.GET })
	public ResponseModel<MasterListSearchByMainTypeInfoDto> getMasterListByMainTypeWithFilter(
			@ApiParam(required = true, example = "1") @PathVariable(required = true) Integer mainID,
			MasterListSearchByMainTypeArgsDto argsDto) {
		MasterListSearchByMainTypeInfoDto dto = catergoryService.getMasterListByMainTypeWithFilter(mainID, argsDto);
		if (dto.getMasterList() == null || dto.getMasterList() != null && dto.getMasterList().size() == 0) {
			return new ResponseModel<MasterListSearchByMainTypeInfoDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<MasterListSearchByMainTypeInfoDto>(200, dto);
		}
	}

	/**
	 * Get master middle type list by main type ID and type name fuzzy filter
	 * 
	 * @param mainID
	 * @param fuzzyFilter
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A11", value = "Get master middle type list by main type ID and type name fuzzy filter")
	@RequestMapping(value = { "/master/main/middletypelist/{mainID}" }, method = RequestMethod.GET)
	public ResponseModel<MasterMiddleTypeListSearchByMainTypeInfoDto> getMasterMiddleTypeListByMainTypeWithFilter(
			@PathVariable(required = true) Integer mainID, @RequestParam(required = false) String fuzzyFilter) {
		MasterMiddleTypeListSearchByMainTypeInfoDto dto = catergoryService.getMasterMiddleTypeListByMainTypeWithFilter(mainID,
				fuzzyFilter);
		if (dto == null || dto.getMasterMiddleTypeList() != null && dto.getMasterMiddleTypeList().size() == 0) {
			return new ResponseModel<MasterMiddleTypeListSearchByMainTypeInfoDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<MasterMiddleTypeListSearchByMainTypeInfoDto>(200, dto);
		}
	}

	/**
	 * Get master list by middle type ID and filters
	 * 
	 * @param middleID
	 * @param argsDto
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A12", value = "Get master list by middle type ID and filters")
	@RequestMapping(value = { "/master/middle/masterlist/{middleID}" }, method = { RequestMethod.GET })
	public ResponseModel<MasterListSearchByMiddleTypeInfoDto> getMasterListByMiddleTypeWithFilter(
			@ApiParam(required = true, example = "1") @PathVariable(required = true) Integer middleID,
			MasterListSearchByMiddleTypeArgsDto argsDto) {
		MasterListSearchByMiddleTypeInfoDto dto = catergoryService.getMasterListByMiddleTypeWithFilter(middleID, argsDto);
		if (dto == null || dto.getMasterList() != null && dto.getMasterList().size() == 0) {
			return new ResponseModel<MasterListSearchByMiddleTypeInfoDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<MasterListSearchByMiddleTypeInfoDto>(200, dto);
		}
	}

	/**
	 * Get master type list by middle type ID and type name fuzzy filter
	 * 
	 * @param middleID
	 * @param fuzzyFilter
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A13", value = "Get master type list by middle type ID and type name fuzzy filter")
	@RequestMapping(value = { "/master/middle/typelist/{middleID}" }, method = RequestMethod.GET)
	public ResponseModel<MasterTypeListSearchByMiddleTypeInfoDto> getMasterTypeListByMiddleTypeWithFilter(
			@PathVariable(required = true) Integer middleID, @RequestParam(required = false) String fuzzyFilter) {
		MasterTypeListSearchByMiddleTypeInfoDto dto = catergoryService.getMasterTypeListByMiddleTypeWithFilter(middleID, fuzzyFilter);
		if (dto == null || dto.getMasterTypeList() != null && dto.getMasterTypeList().size() == 0) {
			return new ResponseModel<MasterTypeListSearchByMiddleTypeInfoDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<MasterTypeListSearchByMiddleTypeInfoDto>(200, dto);
		}
	}

	/**
	 * Get master list by type ID and filters
	 * 
	 * @param typeID
	 * @param argsDto
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A14", value = "Get master list by type ID and filters")
	@RequestMapping(value = { "/master/type/masterlist/{typeID}" }, method = { RequestMethod.GET })
	public ResponseModel<MasterListSearchByTypeInfoDto> getMasterListByTypeWithFilter(
			@ApiParam(required = true, example = "1") @PathVariable(required = true) Integer typeID,
			MasterListSearchByTypeArgsDto argsDto) {
		MasterListSearchByTypeInfoDto dto = catergoryService.getMasterListByTypeWithFilter(typeID, argsDto);
		if (dto == null || dto.getMasterList() != null && dto.getMasterList().size() == 0) {
			return new ResponseModel<MasterListSearchByTypeInfoDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<MasterListSearchByTypeInfoDto>(200, dto);
		}
	}

}
