package tw.com.reinbach.Atenore.Model.member.component;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class HotspotDto {

	private String hotspotID;
	
	private String name;
	
	private String county;
	
	private String address1;
	
	private String address2;
	
	private BigDecimal lat;
	
	private BigDecimal lng;

	public String getHotspotID() {
		return hotspotID;
	}

	public void setHotspotID(String hotspotID) {
		this.hotspotID = hotspotID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public BigDecimal getLat() {
		return lat;
	}

	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}

	public BigDecimal getLng() {
		return lng;
	}

	public void setLng(BigDecimal lng) {
		this.lng = lng;
	}

	
	
}
