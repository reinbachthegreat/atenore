package tw.com.reinbach.Atenore.Model.master.update;

import javax.validation.constraints.NotEmpty;

import io.swagger.annotations.ApiModelProperty;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;

public class MasterFollowedDto {
	
	@NotEmpty(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@ApiModelProperty(required = true)
	private String masterID;
	
	@NotEmpty(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@ApiModelProperty(required = true)
	private String memberID;
	
	@ApiModelProperty(hidden = true)
	private Boolean wannaFollow;

	public String getMasterID() {
		return masterID;
	}

	public void setMasterID(String masterID) {
		this.masterID = masterID;
	}

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public Boolean getWannaFollow() {
		return wannaFollow;
	}

	public void setWannaFollow(Boolean wannaFollow) {
		this.wannaFollow = wannaFollow;
	}
	
	

}
