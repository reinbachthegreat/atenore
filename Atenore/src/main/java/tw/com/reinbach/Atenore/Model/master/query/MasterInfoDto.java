package tw.com.reinbach.Atenore.Model.master.query;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import tw.com.reinbach.Atenore.Model.master.component.MasterDto;
import tw.com.reinbach.Atenore.Model.master.component.MasterRecommend;
import tw.com.reinbach.Atenore.Model.master.component.MasterTag;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MasterInfoDto {

	private MasterDto master;
	
	private List<MasterTag> tagList;
	
	private List<MasterRecommend> recommendList;

	public MasterDto getMaster() {
		return master;
	}

	public void setMaster(MasterDto master) {
		this.master = master;
	}

	public List<MasterTag> getTagList() {
		return tagList;
	}

	public void setTagList(List<MasterTag> tagList) {
		this.tagList = tagList;
	}

	public List<MasterRecommend> getRecommendList() {
		return recommendList;
	}

	public void setRecommendList(List<MasterRecommend> recommendList) {
		this.recommendList = recommendList;
	}
	
	
	
}
