package tw.com.reinbach.Atenore.Model.category.place.dto;

import io.swagger.annotations.ApiModelProperty;

public class PlaceListSearchByMainTypeArgsDto {

	@ApiModelProperty(hidden = true)
	private Integer mainID;
	
	@ApiModelProperty(value = "fuzzy searching by name keywords", required = false, example = "醫")
	private String fuzzyFilter;

	@ApiModelProperty(example = "新北市")
	private String county;

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public String getFuzzyFilter() {
		return fuzzyFilter;
	}

	public void setFuzzyFilter(String fuzzyFilter) {
		this.fuzzyFilter = fuzzyFilter;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}
	
}
