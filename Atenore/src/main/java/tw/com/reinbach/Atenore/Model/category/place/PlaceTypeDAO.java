package tw.com.reinbach.Atenore.Model.category.place;

import java.util.List;

import tw.com.reinbach.Atenore.Model.category.place.dto.PlaceListSearchByMainTypeArgsDto;
import tw.com.reinbach.Atenore.Model.category.place.dto.PlaceListSearchByTypeArgsDto;
import tw.com.reinbach.Atenore.Model.category.place.dto.PlaceMainTypeDto;
import tw.com.reinbach.Atenore.Model.category.place.dto.PlaceSearchByMainTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.place.dto.PlaceSearchByTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.place.dto.PlaceTypeDto;
import tw.com.reinbach.Atenore.Model.category.place.dto.component.PlaceMainTypeAdminDto;
import tw.com.reinbach.Atenore.Model.category.place.dto.component.PlaceTypeAdminDto;
import tw.com.reinbach.Atenore.Model.place.component.PlaceDto;

public interface PlaceTypeDAO {

	public List<PlaceMainTypeDto> getPlaceMainTypeList();

	public List<PlaceMainTypeAdminDto> getPlaceMainTypeListWithFilter(String filter);

//	public List<PlaceDto> getPlaceListByMainType(Integer mainID);

	public List<PlaceSearchByMainTypeInfoDto> getPlaceListByMainTypeWithFilter(PlaceListSearchByMainTypeArgsDto argsDto);

	public List<PlaceTypeDto> getPlaceTypeList();

	public List<PlaceTypeAdminDto> getPlaceTypeListByMainTypeWithFilter(Integer mainID, String filter);

	public List<PlaceDto> getPlaceListByType(Integer typeID);

	public List<PlaceSearchByTypeInfoDto> getPlaceListByTypeWithFilter(PlaceListSearchByTypeArgsDto argsDto);

}
