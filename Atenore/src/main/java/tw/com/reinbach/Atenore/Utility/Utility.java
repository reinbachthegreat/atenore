package tw.com.reinbach.Atenore.Utility;

import java.lang.reflect.Field;
import java.text.DecimalFormat;

import org.springframework.stereotype.Service;

@Service
public class Utility {
	
	private final Double AVERAGE_RADIUS_OF_EARTH = 6371.0;


	/**以DTO作為Update時，整合修改與未修改的屬性值 By Java Reflection
	 * 
	 * @param formDto   由form表單接收的DTO
	 * @param originalDto 原始Data DTO
	 */
	public void updateByDto(Object formDto, Object originalDto) {
		Field[] o_Field = originalDto.getClass().getDeclaredFields();
		for (Field o : o_Field) {
			try {
				o.setAccessible(true);
				if (o.get(formDto) == null || o.get(formDto).equals("null")) {
					o.setAccessible(false);
					continue;
				}

				if (o.get(originalDto) == null && o.get(formDto) != null) {
					o.set(originalDto, o.get(formDto));
					o.setAccessible(false);
					continue;
				}

				if (!o.get(originalDto).equals(o.get(formDto))) {
//					System.out.println("origin:" + o.get(originBean));
//					System.out.println("modify:"+ o.get(formBean));
					o.set(originalDto, o.get(formDto));
				}
				o.setAccessible(false);
			} catch (Exception e) {
				e.printStackTrace();
				continue;
			}
		}
	}
	
	/** Calculate two end point by passing LAT and LNG
	 * 
	 * @param user1_Lat
	 * @param user1_Lng
	 * @param user2_Lat
	 * @param user2_Lng
	 * @return
	 */
	public Double calculateDistance(Double user1_Lat, Double user1_Lng, Double user2_Lat, Double user2_Lng) {
		Double latDistance = Math.toRadians(user2_Lat - user1_Lat);
		Double lngDistance = Math.toRadians(user2_Lng - user1_Lng);
		Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
				+ Math.cos(Math.toRadians(user1_Lat)) * Math.cos(Math.toRadians(user2_Lat)) 
				* Math.sin(lngDistance / 2) * Math.sin(lngDistance / 2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		Double distance = AVERAGE_RADIUS_OF_EARTH * c * 1000; // convert to meters
	    distance = Math.pow(distance, 2) + Math.pow(0, 2);
		DecimalFormat df = new DecimalFormat("##.000");
		return Double.parseDouble(df.format(Math.sqrt(distance)));
	}

}
