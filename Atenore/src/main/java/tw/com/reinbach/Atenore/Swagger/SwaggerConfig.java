package tw.com.reinbach.Atenore.Swagger;

import java.util.Collections;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicates;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
	public static final String AUTHORIZATION_HEADER = "Authorization";

	@SuppressWarnings("unchecked")
	@Bean
	public Docket appApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("AppApi")
				.globalOperationParameters(globalParameterList())
				.apiInfo(apiInfo()).select()
				.apis(RequestHandlerSelectors.basePackage("tw.com.reinbach.Atenore.RestController.Api"))
				.paths(Predicates.or(PathSelectors.ant("/app/**"),
						PathSelectors.ant("/sign**"),
						PathSelectors.ant("/signIn/**"),
						PathSelectors.ant("/account/active/**"),
						PathSelectors.ant("/key/reset/**")))// paths(PathSelectors.regex("/.*"))
				.build()
				.securitySchemes(Collections.singletonList(apiKey()));
	}
	
	@SuppressWarnings("unchecked")
	@Bean
	public Docket webApi() {

	return new Docket(DocumentationType.SWAGGER_2)
	    .groupName("WebApi")
	    .apiInfo(apiInfo()).select()
		.apis(RequestHandlerSelectors.basePackage("tw.com.reinbach.Atenore.RestController.Api"))
		.paths(Predicates.or(PathSelectors.ant("/web/**"),
				PathSelectors.ant("/admin/**"),
				PathSelectors.ant("/sign**"),
				PathSelectors.ant("/signIn/**"),
				PathSelectors.ant("/account/active/**"),
				PathSelectors.ant("/key/reset/**")))// paths(PathSelectors.regex("/.*"))
		.build();
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title("Atenore RESTful APIs")
				.version("1.0")
				.description("**Atenore API**")
				.license("Apache 2.0")
				.licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html").build();
	}
	
	
    private ApiKey apiKey() {
        return new ApiKey("Basic Auth", AUTHORIZATION_HEADER, "header");
    }
    
    private List<Parameter> globalParameterList() {

    	Parameter authTokenHeader =
            new ParameterBuilder()
                .name("Authorization") // name of the header
                .modelRef(new ModelRef("string")) // data-type of the header
                .required(true) // required/optional
                .parameterType("header") // for query-param, this value can be 'query'
                .description("API TOKEN")
                .build();

        return Collections.singletonList(authTokenHeader);
      }
	
}