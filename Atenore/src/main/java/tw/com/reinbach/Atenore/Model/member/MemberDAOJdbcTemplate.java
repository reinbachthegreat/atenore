package tw.com.reinbach.Atenore.Model.member;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import tw.com.reinbach.Atenore.Model.member.component.HotspotDto;
import tw.com.reinbach.Atenore.Model.member.create.ContactCreateDto;
import tw.com.reinbach.Atenore.Model.member.create.HotspotCreateDto;
import tw.com.reinbach.Atenore.Model.member.create.MemberCreateDto;
import tw.com.reinbach.Atenore.Model.member.query.HotspotInfoDto;
import tw.com.reinbach.Atenore.Model.member.query.MemberInfoAdminDto;
import tw.com.reinbach.Atenore.Model.member.query.MemberInfoDto;
import tw.com.reinbach.Atenore.Model.member.query.search.EventFollowedSearchArgsDto;
import tw.com.reinbach.Atenore.Model.member.query.search.EventFollwedSearchInfoDto;
import tw.com.reinbach.Atenore.Model.member.query.search.MasterFollowedSearchArgsDto;
import tw.com.reinbach.Atenore.Model.member.query.search.MasterFollowedSearchInfoDto;
import tw.com.reinbach.Atenore.Model.member.query.search.MemberSearchInfoAdminDto;
import tw.com.reinbach.Atenore.Model.member.query.search.PlaceFollowedSearchArgsDto;
import tw.com.reinbach.Atenore.Model.member.query.search.PlaceFollowedSearchInfoDto;
import tw.com.reinbach.Atenore.Model.member.update.HotspotUpdateDto;
import tw.com.reinbach.Atenore.Model.member.update.MemberSatusUpdateDto;
import tw.com.reinbach.Atenore.Model.member.update.MemberUpdateDto;
import tw.com.reinbach.Atenore.Model.member.update.PasswordUpdateDto;
import tw.com.reinbach.Atenore.Model.validate.AccountStatusInfoDto;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;

@Repository
public class MemberDAOJdbcTemplate implements MemberDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private NamedParameterJdbcTemplate namedJdbcTemplate;

	private Integer deleteStatusCode = -1;

	@Override
	public List<MemberSearchInfoAdminDto> getMemberListForAdminByFilter(String fuzzyFilter) {
		try {
			StringBuilder sql = new StringBuilder(
					"select m.memberID, m.account, md.publicName, md.firstName, md.lastName, m.status, md.lastLogin from atenore.member m "
							+ " left join atenore.memberdetail md on md.memberID = m.memberID " + " where 1=1 and m.status != ")
									.append(this.deleteStatusCode);
			if (fuzzyFilter != null && fuzzyFilter.length() != 0) {
				String sqlParam = "%" + fuzzyFilter + "%";
				sql.append(" and md.publicName like ? or m.account like ? or m.memberID like ? order by md.insertTime desc ");
				return jdbcTemplate.query(sql.toString(), new Object[] { sqlParam, sqlParam, sqlParam },
						new BeanPropertyRowMapper<MemberSearchInfoAdminDto>(MemberSearchInfoAdminDto.class));
			} else {
				sql.append(" order by md.insertTime desc ");
				return jdbcTemplate.query(sql.toString(),
						new BeanPropertyRowMapper<MemberSearchInfoAdminDto>(MemberSearchInfoAdminDto.class));
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public ResponseModel<Void> setAccountByAdmin(MemberCreateDto member) {
		try {
			String sql = "insert into atenore.member (memberID, account, pwd, accountType, status) "
					+ " values(:memberID, :account, :pwd, :accountType, :status) ";
			SqlParameterSource ps = new BeanPropertySqlParameterSource(member);
			int count = namedJdbcTemplate.update(sql, ps);
			if (count != 0) {
				return new ResponseModel<>(201, "");
			} else {
				return new ResponseModel<>(409, ApiError.ACCONUT_CREATION_FAILED);
			}
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> setMemberDetailByAdmin(MemberCreateDto member) {
		try {
			String sql = "insert into atenore.memberdetail (memberID, publicName, lastName, firstName, img, objectKey, phone, cell, county, address1, address2, insertTime) "
					+ " values(:memberID, :publicName, :lastName, :firstName, :img, :objectKey, :phone, :cell, :county, :address1, :address2, now()) ";
			SqlParameterSource ps = new BeanPropertySqlParameterSource(member);
			int count = namedJdbcTemplate.update(sql, ps);
			if (count != 0) {
				return new ResponseModel<>(201, "");
			} else {
				return new ResponseModel<>(409, ApiError.ACCONUT_CREATION_FAILED);
			}
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> updateMemberDetailByMemberID(MemberUpdateDto dto) {
		try {
			String sqltext = "update atenore.memberdetail set publicName=:publicName, lastName=:lastName, firstName=:firstName, img=:img, objectKey=:objectKey, "
					+ " phone=:phone, cell=:cell, county=:county, address1=:address1, address2=:address2 "
					+ " where memberID = :memberID";
			SqlParameterSource ps = new BeanPropertySqlParameterSource(dto);
			int count = namedJdbcTemplate.update(sqltext, ps);

			if (count != 0) {
				return new ResponseModel<Void>(204, "");
			} else {
				return new ResponseModel<>(409, ApiError.ACCOUNTDETAIL_UPDATE_FAILED);
			}
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> updateAccountStatusByMemberID(MemberSatusUpdateDto status) {
		try {
			String sql = "update atenore.member set status = :status where memberID = :memberID ";

			SqlParameterSource ps = new BeanPropertySqlParameterSource(status);
			int count = namedJdbcTemplate.update(sql, ps);
			if (count != 0) {
				return new ResponseModel<Void>(204, "");
			} else {
				return new ResponseModel<>(409, ApiError.ACCOUNT_STATUS_UPDATE_FILED);
			}
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deleteAccountByMemberID(String memberID) {
		try {
			String deleteAccount = "帳號已刪除";
			String sql = "update atenore.member set account = ?, pwd = null, token = null, accountType= null, status = ? where memberID = ? ";

			int count = jdbcTemplate.update(sql, new Object[] { deleteAccount, this.deleteStatusCode, memberID });
			if (count != 0) {
				return new ResponseModel<Void>(204, "");
			} else {
				return new ResponseModel<>(409, ApiError.ACCOUNT_DELETION_FAILED);
			}
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deleteMemberDetailByMemberID(String memberID) {
		try {
			String deleteName = "帳號已刪除";
			String sql = "update memberdetail set email = null, publicName = ?, lastName = null, firstName = null "
					+ " , img = null, objectKey = null, phone = null, cell = null, county = null, address1 = null, address2 = null "
					+ " , deviceToken = null, lat = null, lng = null, lastLogin = null, insertTime = null where memberID = ? ";

			int count = jdbcTemplate.update(sql, new Object[] { deleteName, memberID });
			if (count != 0) {
				return new ResponseModel<Void>(204, "");
			} else {
				return new ResponseModel<>(409, ApiError.ACCOUNT_DELETION_FAILED);
			}
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deleteEventFollowedByMemberID(String memberID) {
		try {
			String sql = "delete from memberevent where memberID = ? ";
			jdbcTemplate.update(sql, new Object[] { memberID });
			return new ResponseModel<Void>(204, "");
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deletePlaceFollowedByMemberID(String memberID) {
		try {
			String sql = "delete from memberplace where memberID = ? ";
			jdbcTemplate.update(sql, new Object[] { memberID });
			return new ResponseModel<Void>(204, "");
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deleteMasterFollwedByMemberID(String memberID) {
		try {
			String sql = "delete from membermaster where memberID = ? ";
			jdbcTemplate.update(sql, new Object[] { memberID });
			return new ResponseModel<Void>(204, "");
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deleteHotspotByMemberID(String memberID) {
		try {
			String sql = "delete from hotspot where memberID = ? ";
			jdbcTemplate.update(sql, new Object[] { memberID });
			return new ResponseModel<Void>(204, "");
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deletePinEventTypeByMemberID(String memberID) {
		try {
			String sql = "delete from pineventtype where memberID = ? ";
			jdbcTemplate.update(sql, new Object[] { memberID });
			return new ResponseModel<Void>(204, "");
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deletePinPlaceTypeByMemberID(String memberID) {
		try {
			String sql = "delete from pinplacetype where memberID = ? ";
			jdbcTemplate.update(sql, new Object[] { memberID });
			return new ResponseModel<Void>(204, "");
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public MemberInfoDto getMemberSimpleByMemberID(String memberID) {
		try {
			String sql = "select case when m.accounttype = 'Email' then m.account else m.accounttype end as account, img, publicname , m.accounttype"
					+ " from atenore.member m join atenore.memberdetail md on m.memberID = md.memberID where m.memberID = ? ";
			return jdbcTemplate.queryForObject(sql, new Object[] { memberID },
					new BeanPropertyRowMapper<MemberInfoDto>(MemberInfoDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public MemberInfoDto getMemberDetailByMemberID(String memberID) {
		try {
			String sql = "select * from atenore.memberdetail where memberID = ? ";
			return jdbcTemplate.queryForObject(sql, new Object[] { memberID },
					new BeanPropertyRowMapper<MemberInfoDto>(MemberInfoDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public MemberInfoAdminDto getMemberWithDetailForAdminByMemberID(String memberID) {
		try {
			String sql = "select m.memberID, m.account, md.publicName, md.lastName, md.firstName, md.phone, md.cell, md.img, md.county, md.address1, md.address2 from atenore.member m "
					+ " left join memberdetail md on m.memberID = md.memberID where m.memberID = ? ";
			return jdbcTemplate.queryForObject(sql, new Object[] { memberID },
					new BeanPropertyRowMapper<MemberInfoAdminDto>(MemberInfoAdminDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public MemberUpdateDto getMemberDetailForUpdateByMemberID(String memberID) {
		try {
			String sql = "select * from atenore.memberdetail where memberID = ? ";
			return jdbcTemplate.queryForObject(sql, new Object[] { memberID },
					new BeanPropertyRowMapper<MemberUpdateDto>(MemberUpdateDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public AccountStatusInfoDto getAccontStatus(String memberID) {
		AccountStatusInfoDto dto;
		try {
			String sql = "select m.memberID, m.account, m.pwd, m.accountType, m.status from atenore.member m "
					+ " where m.memberID = ? ";
			dto = jdbcTemplate.queryForObject(sql, new Object[] { memberID },
					new BeanPropertyRowMapper<AccountStatusInfoDto>(AccountStatusInfoDto.class));
			if (dto == null) {
				dto = new AccountStatusInfoDto();
				dto.setHasAccount(false);
			} else {
				dto.setHasAccount(true);
			}
			return dto;
		} catch (EmptyResultDataAccessException e) {
			dto = new AccountStatusInfoDto();
			dto.setErrMsg(ApiError.ACCONUT_NOT_FOUND);
			return dto;
		} catch (DataAccessException e) {
			dto = new AccountStatusInfoDto();
			dto.setErrMsg(e.getMessage());
			return dto;
		}
	}

	@Override
	public ResponseModel<Void> updatePassword(PasswordUpdateDto dto) {
		try {
			String sql = "update atenore.member set pwd = :updatePaswword where memberID = :memberID ";

			SqlParameterSource ps = new BeanPropertySqlParameterSource(dto);
			int count = namedJdbcTemplate.update(sql, ps);

			if (count != 0) {
				return new ResponseModel<Void>(204, "");
			} else {
				return new ResponseModel<>(409, "password update failed.");
			}

		} catch (DataAccessException e) {
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> setHotspot(HotspotCreateDto dto) {
		try {
			String sql = "insert into hotspot (uuid, memberID, name, county, address1, address2, lat, lng) "
					+ " values(:hotspotID, :memberID, :name, :county, :address1, :address2, :lat, :lng) ";
			SqlParameterSource ps = new BeanPropertySqlParameterSource(dto);
			int count = namedJdbcTemplate.update(sql, ps);
			if (count != 0) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<>(409, "Hotspot creation failed.");
			}
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public List<HotspotDto> getHotspotListByMemberID(String memberID) {
		try {
			String sql = "select h.uuid as 'hotspotID', name, county, address1, address2, lat, lng "
					+ " from hotspot h where memberID = ? ";

			return jdbcTemplate.query(sql, new Object[] { memberID }, new BeanPropertyRowMapper<HotspotDto>(HotspotDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public HotspotInfoDto getHotspotByHotspotID(String hotspotID) {
		System.out.println(hotspotID);
		try {
			String sql = "select * from hotspot where uuid = ? ";
			return jdbcTemplate.queryForObject(sql, new Object[] { hotspotID },
					new BeanPropertyRowMapper<HotspotInfoDto>(HotspotInfoDto.class));
		} catch (DataAccessException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public ResponseModel<Void> updateHotspotByHotspotID(HotspotUpdateDto dto) {
		try {
			String sql = "update hotspot set name=:name, county=:county, address1=:address1, "
					+ " address2=:address2, lat=:lat, lng=:lng where uuid =:hotspotID ";

			SqlParameterSource ps = new BeanPropertySqlParameterSource(dto);
			int count = namedJdbcTemplate.update(sql, ps);

			if (count != 0) {
				return new ResponseModel<Void>(204, "");
			} else {
				return new ResponseModel<>(409, "Hotspot update failed.");
			}
		} catch (DataAccessException e) {
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deleteHotspotByHotspotID(String hotspotID) {
		try {
			String sql = "delete from hotspot where uuid = ? ";

			int count = jdbcTemplate.update(sql, new Object[] { hotspotID });

			if (count != 0) {
				return new ResponseModel<Void>(204, "");
			} else {
				return new ResponseModel<>(409, "Hotspot delete failed.");
			}
		} catch (DataAccessException e) {
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public List<EventFollwedSearchInfoDto> getEventListFollwedByMemberIDWithFilter(EventFollowedSearchArgsDto argsDto) {
		try {
			StringBuilder sql = new StringBuilder(
					"select e.*, IF(e.endTime < now(), true, false) as isExpired, (select ei.img from eventimg ei where ei.eventID = fe.eventID and ei.isMain=true) as img, (select ei.uuid from eventimg ei where ei.eventID = fe.eventID and ei.isMain=true) as imageID, "
							+ " (select ei.objectKey from eventimg ei where ei.eventID = fe.eventID and ei.isMain=true) as objectKey, (select ei.isMain from eventimg ei where ei.eventID = fe.eventID and ei.isMain=true) as isMain,  "
							+ " et.name as typeName, et.mainID, emt.name as mainTypeName from memberevent fe "
							+ " left join atenore.event e on fe.eventID = e.eventID "
							+ " left join eventtype et on e.typeID = et.typeID "
							+ " left join eventmaintype emt on et.mainID = emt.mainID where fe.memberID =:memberID ");

			switch (argsDto.getSearchPreferArg().getSearchPreferSN()) {
			case 1:
				sql.append(" and e.insertTime between date_sub(now(), interval 10 day) and now() ");
				break;

			case 2:
				sql.append(" and e.click is not null and e.follow is not null ");
				break;

			case 3:
				sql.append(" and e.insertTime between date_sub(now(), interval 10 day) and now() "
						+ " and e.click is not null and e.follow is not null ");
				break;

			default:
				break;
			}

			if (argsDto.getExpired() == true) {
				sql.append(" and e.endTime < now()");
			}

			if (argsDto.getTodayOnly() == true) {
				sql.append(" and date(e.startTime) = CURDATE()");
			} else {
				sql.append(" and date(e.startTime) > CURDATE()");
			}

			if (argsDto.getFuzzyFilter() != null) {
				sql.append(
						" and ( e.name like '%' :fuzzyFilter '%'  or e.county like %:fuzzyFilter% or e.address like '%' :fuzzyFilter '%' )");
			}

			if (argsDto.getMainID() != null) {
				sql.append(" and et.mainID = :mainID ");
				if (argsDto.getTypeID() != null) {
					sql.append(" and e.typeID = :typeID ");
				}
			}

			if (argsDto.getStartTime() != null) {
				sql.append(" and e.startTime = :startTime ");
			}

			if (argsDto.getEndTime() != null) {
				sql.append(" and e.endTime = :endTime ");
			}

			if (argsDto.getApply() != null) {
				sql.append(" and e.apply = :apply ");
			}

			SqlParameterSource ps = new BeanPropertySqlParameterSource(argsDto);
			return namedJdbcTemplate.query(sql.toString(), ps,
					new BeanPropertyRowMapper<EventFollwedSearchInfoDto>(EventFollwedSearchInfoDto.class));
		} catch (DataAccessException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<MasterFollowedSearchInfoDto> getMasterListFollowedByMemberIDWithFilter(MasterFollowedSearchArgsDto argsDto) {
		try {
			StringBuilder sql = new StringBuilder("select m.* , \r\n"
					+ "If(length(GROUP_CONCAT(t.tag)) - length(replace(GROUP_CONCAT(t.tag), ',', ''))>1,  \r\n"
					+ "       SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 1), ',', -1) ,NULL)  as tag1,\r\n"
					+ "if(SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 2), ',', -1)!= SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 1), ',', -1),\r\n"
					+ "		SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 2), ',', -1), NULL )  as tag2,\r\n"
					+ "if(SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 3), ',', -1)!= SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 2), ',', -1),\r\n"
					+ "		SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 3), ',', -1), NULL ) as tag3\r\n"
					+ "       from membermaster fm  " + "					left join master m on fm.masterID = m.masterID "
					+ "					left join mastertype mt on mt.typeId = m.typeID\r\n"
					+ "					left join mastermidtype mit on mit.midID = mt.midID \r\n"
					+ "					left join mastermaintype mmt on mmt.mainID = mit.mainID \r\n"
					+ "                   left join mastertag t on m.masterID = t.masterID\r\n" + "					where 1=1  ");

			if (argsDto.getFuzzyFilter() != null) {
				sql.append(" and m.publicName  like %:fuzzyFilter% ");
			}

			if (argsDto.getCounty() != null) {
				sql.append(" and m.count = :county ");
			}

			if (argsDto.getMainID() != null) {
				sql.append(" and mmt.mainID = :mainID ");
			}

			if (argsDto.getMidID() != null) {
				sql.append(" and mit.midID = :midID ");
			}

			if (argsDto.getTypeID() != null) {
				sql.append(" and mt.typeID = :typeID ");
			}

			sql.append(" group by m.masterID ");

			SqlParameterSource ps = new BeanPropertySqlParameterSource(argsDto);
			return namedJdbcTemplate.query(sql.toString(), ps,
					new BeanPropertyRowMapper<MasterFollowedSearchInfoDto>(MasterFollowedSearchInfoDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<PlaceFollowedSearchInfoDto> getPlaceListFollowedByMemberIDWithFilter(PlaceFollowedSearchArgsDto argsDto) {
		try {
			StringBuilder sql = new StringBuilder(

					"select p.*, (select pi.img from placeimg pi where pi.placeID = mp.placeID and pi.isMain = 'true') as img, "
							+ " (select l.phone from liaison l where l.placeID = mp.placeID and l.isMain = 'true') as phone, "
							+ " pt.name as typeName, pt.mainID, pmt.name as mainTypeName from memberplace mp "
							+ " left join place p on mp.placeID = p.placeID left join placetype pt on p.typeID = pt.typeID "
							+ " left join placemaintype pmt on pt.mainID = pmt.mainID where mp.memberID = :memberID ");

			if (argsDto.getCounty() != null) {
				sql.append(" and p.county = :county ");
			}

			if (argsDto.getMainID() != null) {
				sql.append(" and pt.mainID = :mainID ");
				if (argsDto.getTypeID() != null) {
					sql.append(" and p.typeID = :typeID ");
				}
				if (argsDto.getCapacitySN() != null) {
					switch (argsDto.getCapacitySN()) {
					case 0:
						sql.append(" and p.max between 1 and 10 ");
						break;
					case 1:
						sql.append(" and p.max between 11 and 100 ");
						break;
					case 2:
						sql.append(" and p.max between 101 and 500 ");
						break;
					case 3:
						sql.append(" and p.max between 501 and 1000 ");
						break;
					default:
						break;
					}
				}
			}

			SqlParameterSource ps = new BeanPropertySqlParameterSource(argsDto);
			return namedJdbcTemplate.query(sql.toString(), ps,
					new BeanPropertyRowMapper<PlaceFollowedSearchInfoDto>(PlaceFollowedSearchInfoDto.class));
		} catch (DataAccessException e) {
			e.printStackTrace();

			return null;
		}
	}

	@Override
	public ResponseModel<Void> setContact(ContactCreateDto dto) {
		try {
			String sql = "insert into contact (uuid, name, phone, mail, content) values(:uuid, :name, :phone, :mail, :content)";
			SqlParameterSource ps = new BeanPropertySqlParameterSource(dto);
			int count = namedJdbcTemplate.update(sql, ps);
			if (count != 0) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<>(500, "Contact creation failed.");
			}
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

}
