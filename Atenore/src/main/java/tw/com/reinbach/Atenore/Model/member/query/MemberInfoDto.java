package tw.com.reinbach.Atenore.Model.member.query;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MemberInfoDto {

//	@ApiModelProperty(hidden = true, example = "75c6538b-facd-461b-b143-919c0913028c")
//	private String memberID;
	
	@ApiModelProperty(example = "reinbachrddev@gmail.com")
	private String account;
	
	@ApiModelProperty(example = "Sora醬")
	private String publicName;
	
	@ApiModelProperty(example = "Lee")
	private String lastName;
	
	@ApiModelProperty(example = "Anita")
	private String firstName;
	
	@ApiModelProperty(value = "URL of member's headshot", example = "https://mosaicstorage.s3-ap-northeast-1.amazonaws.com/photo/MemberHead/bf194ce20d2f94a6226b3cd7677623ea.jpg")
	private String img;
	
	@ApiModelProperty(value = "AWS ObjectKey of member's headshot", example = "photo/MemberHead/bf194ce20d2f94a6226b3cd7677623ea.jpg")
	private String objectKey;
	
	@ApiModelProperty(example = "0229887744")
	private String phone;
	
	@ApiModelProperty(example = "0922558774")
	private String cell;
	
	@ApiModelProperty(example = "新北市")
	private String county;
	
	@ApiModelProperty(value = "According to client's requirement, it ends with road description", example = "板橋區縣民大道55號")
	private String address1;

	@ApiModelProperty(example = "2巷158號6樓")
	private String address2;
	
	private String accounttype;

//	public String getMemberID() {
//		return memberID;
//	}
//
//	public void setMemberID(String memberID) {
//		this.memberID = memberID;
//	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPublicName() {
		return publicName;
	}

	public void setPublicName(String publicName) {
		this.publicName = publicName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getObjectKey() {
		return objectKey;
	}

	public void setObjectKey(String objectKey) {
		this.objectKey = objectKey;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCell() {
		return cell;
	}

	public void setCell(String cell) {
		this.cell = cell;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAccounttype() {
		return accounttype;
	}

	public void setAccounttype(String accounttype) {
		this.accounttype = accounttype;
	}

	
	
	
	
}
