package tw.com.reinbach.Atenore.Utility.oauth;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Description;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpMethod;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.filter.OAuth2ClientAuthenticationProcessingFilter;
import org.springframework.security.oauth2.client.filter.OAuth2ClientContextFilter;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.resource.UserRedirectRequiredException;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.security.oauth2.common.AuthenticationScheme;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.context.SecurityContextPersistenceFilter;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.filter.CompositeFilter;
import org.springframework.web.filter.OncePerRequestFilter;

import tw.com.reinbach.Atenore.Model.validate.SignInDto;
import tw.com.reinbach.Atenore.Model.validate.SignInInfoDto;
import tw.com.reinbach.Atenore.Model.validate.ValidateService;
import tw.com.reinbach.Atenore.Model.validate.ValidateService.MemberStatus;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;
import tw.com.reinbach.Atenore.Utility.oauth.facebook.FacebookUserInfoTokenServices;
import tw.com.reinbach.Atenore.Utility.oauth.google.GoogleUserInfoTokenServices;

/**
 * @author Sora Date 2020-06-12
 */
@Configuration
public class OAuth2ServerConfig {

	private static final String RESOURCE_ID = "app";

	// This is made possible because of @EnableOAuth2Client
	// and RequestContextListener.
	@Autowired
	private OAuth2ClientContext oauth2ClientContext;

	/**
	 * <p>
	 * Handles a {@link UserRedirectRequiredException} that is thrown from
	 * {@link OAuth2ClientAuthenticationProcessingFilter}.
	 * </p>
	 * <p>
	 * This bean is configured because of <code>@EnableOAuth2Client</code>.
	 * </p>
	 */
	@Autowired
	private OAuth2ClientContextFilter oauth2ClientContextFilter;

	@Value("${spring.security.oauth2.client.registration.google.client-id}")
	private String googleClientId;

	@Value("${spring.security.oauth2.client.registration.google.client-secret}")
	private String googleClientSecret;

	@Value("${spring.security.oauth2.client.provider.google.authorization-uri}")
	private String googleUserAuthorizationUri;

	@Value("${spring.security.oauth2.client.provider.google.token-uri}")
	private String googleAccessTokenUri;

	@Value("${oauth2.google.tokenName}")
	private String googleTokenName;

	@Value("${spring.security.oauth2.client.registration.google.scope}")
	private String googleScope;

	@Value("${spring.security.oauth2.client.provider.goole.user-info-uri}")
	private String googleUserInfoUri;

	@Value("${spring.security.oauth2.client.registration.google.redirect-uri}")
	private String googleOauth2FilterCallbackPath;

	@Value("${spring.security.oauth2.client.registration.facebook.client-id}")
	private String facebookClientId;

	@Value("${spring.security.oauth2.client.registration.facebook.client-secret}")
	private String facebookClientSecret;

	@Value("${spring.security.oauth2.client.provider.faceook.authorization-uri}")
	private String facebookUserAuthorizationUri;

	@Value("${spring.security.oauth2.client.provider.faceook.token-uri}")
	private String facebookAccessTokenUri;

	@Value("${oauth2.facebook.tokenName}")
	private String facebookTokenName;

	@Value("${spring.security.oauth2.client.registration.facebook.scope}")
	private String facebookScope;

	@Value("${spring.security.oauth2.client.provider.faceook.user-info-uri}")
	private String facebookUserInfoUri;

	@Value("${spring.security.oauth2.client.registration.facebook.redirect-uri}")
	private String facebookOauth2FilterCallbackPath;

	@Resource(name = "tokenStore")
	private TokenStore tokenStore;

	@Autowired
	private AuthorizationServerTokenServices authSerivce;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private PasswordEncoder encoder;

	@Value("${spring.security.oauth2.client.registration.client-mode.client-id}")
	private String clientId;

	@Value("${spring.security.oauth2.client.registration.client-mode.client-secret}")
	private String clientSecret;

	@Value("${spring.security.oauth2.client.registration.password-mode.client-id}")
	private String clientId_password_mode;

	@Configuration
	public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

		@Value("${spring.security.user.name}")
		private String username;

		@Value("${spring.security.user.password}")
		private String userpwd;

		@Bean
		@Override
		protected UserDetailsService userDetailsService() {
			InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
			manager.createUser(User.withUsername(this.username).password(passwordEncoder().encode(this.userpwd)).authorities("USER").build());
//	        manager.createUser(User.withUsername("user_2").password(finalPassword).authorities("USER").build());
			return manager;
		}

		@Bean
		PasswordEncoder passwordEncoder() {
			return PasswordEncoderFactories.createDelegatingPasswordEncoder();
		}

		/**
		 * 這一步的配置是必不可少的，否則SpringBoot會自動配置一個AuthenticationManager,覆蓋掉内存中的用户
		 */
		@Bean
		@Override
		public AuthenticationManager authenticationManagerBean() throws Exception {
			AuthenticationManager manager = super.authenticationManagerBean();
			return manager;
		}

		@Override
		public void configure(WebSecurity security) {
			security.ignoring().antMatchers("/css/**", "/images/**", "/js/**"
					, "/app/event/search/prefer", "/app/event/type", "/app/master/hit/search/prefer", "/app/matser/type"
					, "/app/place/search/prefer", "/app/place/type", "/app/county");
		}
	}

	@Configuration
	@EnableResourceServer
	@EnableOAuth2Client
	protected class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

		@Autowired
		private ValidateService validateService;

		@Override
		public void configure(ResourceServerSecurityConfigurer resources) {
			resources.resourceId(RESOURCE_ID).stateless(true);
		}

		@Override
		public void configure(HttpSecurity http) throws Exception {
			http.authorizeRequests().antMatchers("/app/member/**", "/app/eventtype/pin/**", 
					"/app/event/follow", "/app/placetype/pin/**", "/app/place/follow", "/app/hotspot",  "/app/hotspot/**", "/login/google", "/login/facebook").authenticated() // 配置app訪問控制，必須認證過後才可以訪問
					.and().logout().logoutUrl("/logout").logoutSuccessUrl("/").and()
					.addFilterAfter(new TokenInterceptorFilter(), BasicAuthenticationFilter.class)
					.addFilterBefore(ssoFilter(), TokenInterceptorFilter.class)
					.addFilterAfter(oauth2ClientContextFilter, SecurityContextPersistenceFilter.class).exceptionHandling()
					.authenticationEntryPoint(new LoginUrlAuthenticationEntryPoint("/login"))
					.accessDeniedHandler(new OAuth2AccessDeniedHandler()).and().sessionManagement()
					.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
					.and().csrf().disable().anonymous().disable();

		}

		private List<String> parseScopes(String commaSeparatedScopes) {
			List<String> scopes = new LinkedList<>();
			Collections.addAll(scopes, commaSeparatedScopes.split(","));
			return scopes;
		}

		/**
		 * 
		 * @return OAuth2 client authentication processing filters
		 */
		private Filter ssoFilter() {
			CompositeFilter filter = new CompositeFilter();
			List<Filter> filters = new ArrayList<Filter>();

			/**
			 * Google Filter
			 */
			filters.add(ssoFilter(
					getAuthorizationCodeResource("google-oauth-client", googleClientId, googleClientSecret, googleUserAuthorizationUri,
							googleAccessTokenUri, googleTokenName, googleScope),
					googleOauth2FilterCallbackPath, this.googleUserInfoTokenServices(), new SimpleUrlAuthenticationSuccessHandler() {
						public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
								Authentication authentication) throws IOException, ServletException {

							// Set redirect after success.
							this.setDefaultTargetUrl("/signIn/google");

//		    				this.setUseReferer(true);

							// Generate token for SSO and return it.
							OAuth2AccessToken ssoTokenObj = ssoTokenGenerator(authentication);
							ResponseModel<SignInInfoDto> signInInfo = null;
							try {
								signInInfo = ssoSignInGoogle(authentication, ssoTokenObj);
							} catch (Throwable e) {
								signInInfo = new ResponseModel<SignInInfoDto>(500, e.getMessage());
								e.printStackTrace();
							}

							ServletRequestAttributes servletAttr = (ServletRequestAttributes) RequestContextHolder
									.currentRequestAttributes();
							HttpSession session = servletAttr.getRequest().getSession(true);
							session.setAttribute("response_model", signInInfo);

							super.onAuthenticationSuccess(request, response, authentication);
						}
					}));
			/**
			 * Facebook Filter
			 */
			filters.add(ssoFilter(
					getAuthorizationCodeResource("facebook-oauth-client", facebookClientId, facebookClientSecret,
							facebookUserAuthorizationUri, facebookAccessTokenUri, facebookTokenName, facebookScope),
					facebookOauth2FilterCallbackPath, facebookUserInfoTokenServices(), new SimpleUrlAuthenticationSuccessHandler() {
						public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
								Authentication authentication) throws IOException, ServletException {

							// Set redirect after success.
							this.setDefaultTargetUrl("/signIn/facebook");

//		    				this.setUseReferer(true);

							// Generate token for SSO and return it.
							OAuth2AccessToken ssoTokenObj = ssoTokenGenerator(authentication);
							System.out.println("ssoTokenObj : " + ssoTokenObj);
							ResponseModel<SignInInfoDto> signInInfo = null;
							try {
								signInInfo = ssoSignInFacebook(authentication, ssoTokenObj);
							} catch (Throwable e) {
								signInInfo = new ResponseModel<SignInInfoDto>(500, e.getMessage());
								e.printStackTrace();
							}

							ServletRequestAttributes servletAttr = (ServletRequestAttributes) RequestContextHolder
									.currentRequestAttributes();
							HttpSession session = servletAttr.getRequest().getSession(true);
							session.setAttribute("response_model", signInInfo);

							super.onAuthenticationSuccess(request, response, authentication);
						}
					}));

			filter.setFilters(filters);
			return filter;
		}

		/**
		 * @param resource
		 * @param oauth2FilterCallbackPath
		 * @param tokenServices
		 * @param successHandler
		 * @return an OAuth2 client authentication processing filter
		 */
		private Filter ssoFilter(OAuth2ProtectedResourceDetails resource, String oauth2FilterCallbackPath,
				ResourceServerTokenServices tokenServices, AuthenticationSuccessHandler successHandler) {

			OAuth2ClientAuthenticationProcessingFilter oAuth2ClientAuthenticationFilter = new OAuth2ClientAuthenticationProcessingFilter(
					oauth2FilterCallbackPath);
			OAuth2RestOperations oAuth2RestTemplate = new OAuth2RestTemplate(resource, oauth2ClientContext);
			oAuth2ClientAuthenticationFilter.setRestTemplate(oAuth2RestTemplate);
			oAuth2ClientAuthenticationFilter.setTokenServices(tokenServices);
			oAuth2ClientAuthenticationFilter.setAuthenticationSuccessHandler(successHandler);
			return oAuth2ClientAuthenticationFilter;
		}

		/**
		 * 
		 * @param authentication
		 * @return access token from token store
		 */
		private OAuth2AccessToken ssoTokenGenerator(Authentication authentication) {

			OAuth2Authentication auth = (OAuth2Authentication) authentication;
			System.out.println("Auth details : " + auth.getUserAuthentication().getDetails());
			OAuth2AccessToken grant = authSerivce.createAccessToken(auth);
			tokenStore.storeAccessToken(grant, auth);
			System.out.println("From TokenStore(SSO_User): " + tokenStore.getAccessToken(auth));

			return grant;
		}

		/** Sign in SSO, Google
		 * 
		 * @param authentication
		 * @param token
		 * @return
		 * @throws Throwable
		 */
		private ResponseModel<SignInInfoDto> ssoSignInGoogle(Authentication authentication, OAuth2AccessToken token) throws Throwable {
			OAuth2Authentication auth = (OAuth2Authentication) authentication;

			LinkedHashMap<String, Object> properties = (LinkedHashMap<String, Object>) auth.getUserAuthentication().getDetails();
			String user_id = properties.get("sub").toString();
			String public_name = properties.get("name").toString();
			String first_name = properties.get("given_name").toString();
			String last_name = properties.get("family_name").toString();
			String picture = properties.get("picture").toString();
			String email = properties.get("email").toString();

			SignInDto signInDto = new SignInDto();
			signInDto.setAccount(user_id);
			signInDto.setEmail(email);
			signInDto.setPublicName(public_name);
			signInDto.setFirst_name(first_name);
			signInDto.setLast_name(last_name);
			signInDto.setImg(picture);
			signInDto.setAccountType("Google");
			signInDto.setStatus(MemberStatus.ACTIVE.ordinal());	
			
			ResponseModel<SignInInfoDto> signInResult = validateService.signIn(signInDto);
			if(signInResult.getCode()!=200) {
				return signInResult;
			}
			SignInInfoDto signInfo = signInResult.getData();
			// Set SSO token
			signInfo.setOauth2AccessToken(token);
			ResponseModel<Void> renew =  validateService.setAccountTokenByMemberID(signInfo.getMemberID(), token);
			if(renew.getCode()!=204) {
				new ResponseModel<SignInInfoDto>(renew.getCode(), renew.getErrMsg());
			}
			return new ResponseModel<SignInInfoDto>(200, signInfo);
		}

		/** Sign in SSO, Facebook
		 * 
		 * @param authentication
		 * @param token
		 * @return
		 * @throws Throwable
		 */
		private ResponseModel<SignInInfoDto> ssoSignInFacebook(Authentication authentication, OAuth2AccessToken token)
				throws Throwable {
			OAuth2Authentication auth = (OAuth2Authentication) authentication;

			LinkedHashMap<String, Object> properties = (LinkedHashMap<String, Object>) auth.getUserAuthentication().getDetails();
			String user_id = properties.get("id").toString();
			String public_name = properties.get("name").toString();
			String first_name = properties.get("first_name").toString();
			String last_name = properties.get("last_name").toString();
			Map<String, Object> pictureRawObject = (Map<String, Object>) properties.get("picture");
			Map<String, Object> pictureRawData = (Map<String, Object>) pictureRawObject.get("data");
			URI picture = new URI((String) pictureRawData.get("url"));
			String email = properties.get("email").toString();

			SignInDto signInDto = new SignInDto();
			signInDto.setAccount(user_id);
			signInDto.setEmail(email);
			signInDto.setPublicName(public_name);
			signInDto.setFirst_name(first_name);
			signInDto.setLast_name(last_name);
			signInDto.setImg(picture.toString());
			signInDto.setAccountType("Facebook");
			signInDto.setStatus(MemberStatus.ACTIVE.ordinal());	
			
			ResponseModel<SignInInfoDto> signInResult = validateService.signIn(signInDto);
			if(signInResult.getCode()!=200) {
				return signInResult;
			}
			SignInInfoDto signInfo = signInResult.getData();
			// Set SSO token
			signInfo.setOauth2AccessToken(token);
			ResponseModel<Void> renew =  validateService.setAccountTokenByMemberID(signInfo.getMemberID(), token);
			if(renew.getCode()!=204) {
				new ResponseModel<SignInInfoDto>(renew.getCode(), renew.getErrMsg());
			}
			return new ResponseModel<SignInInfoDto>(200, signInfo);
		}

		/**
		 * 
		 * @param resoucreID
		 * @param clientId
		 * @param clientSecret
		 * @param userAuthorizationUri
		 * @param accessTokenUri
		 * @param tokenName
		 * @param scope
		 * @return OAuth2ProtectedResourceDetails
		 */
		private OAuth2ProtectedResourceDetails getAuthorizationCodeResource(String resoucreID, String clientId, String clientSecret,
				String userAuthorizationUri, String accessTokenUri, String tokenName, String scope) {

			AuthorizationCodeResourceDetails details = new AuthorizationCodeResourceDetails();
			details.setId(resoucreID);
			details.setClientId(clientId);
			details.setClientSecret(clientSecret);
			details.setUserAuthorizationUri(userAuthorizationUri);
			if (accessTokenUri != null) {
				details.setAccessTokenUri(accessTokenUri);
			}
			if (tokenName != null) {
				details.setTokenName(tokenName);
			}

			String commaSeparatedScopes = scope;
			details.setScope(parseScopes(commaSeparatedScopes));
			details.setAuthenticationScheme(AuthenticationScheme.query);
			details.setClientAuthenticationScheme(AuthenticationScheme.form);
			return details;
		}

		@Description("Google API UserInfo resource server")
		private GoogleUserInfoTokenServices googleUserInfoTokenServices() {
			GoogleUserInfoTokenServices userInfoTokenServices = new GoogleUserInfoTokenServices(googleUserInfoUri, googleClientId);
			return userInfoTokenServices;
		}

		@Description("Facebook API UserInfo resource server")
		private FacebookUserInfoTokenServices facebookUserInfoTokenServices() {
			FacebookUserInfoTokenServices userInfoTokenServices = new FacebookUserInfoTokenServices(facebookUserInfoUri,
					facebookClientId);
			return userInfoTokenServices;
		}

	}

	/**
	 * 
	 * @author Sora
	 *
	 */
	protected static class TokenInterceptorFilter extends OncePerRequestFilter {

		@Override
		protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
				throws ServletException, IOException {

			if (request.getParameter("access_token") != null) {
				String token = request.getParameter("access_token").toString();
				System.out.println(token);
				OAuth2AuthenticationDetails details = new OAuth2AuthenticationDetails(request);
				System.out.println(details.getTokenValue());
				response.setStatus(200);
			} else {
				System.out.println("no access token");
			}

			filterChain.doFilter(request, response);
		}
	}

	@Configuration
	@EnableAuthorizationServer
	protected class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

		@Override
		public void configure(ClientDetailsServiceConfigurer clients) throws Exception {

			// 配置兩個客戶端,一個用於password認證另一個用於client認證
			clients.inMemory().withClient(clientId).resourceIds(RESOURCE_ID)
					.authorizedGrantTypes("client_credentials", "refresh_token", "authorization_code").scopes("select")
					.accessTokenValiditySeconds(86400)
					.authorities("oauth2").autoApprove(true).secret(encoder.encode(clientSecret)).and()
					.withClient(clientId_password_mode).resourceIds(RESOURCE_ID).authorizedGrantTypes("password", "refresh_token")
					.scopes("select").authorities("oauth2").secret(encoder.encode(clientSecret)).and().withClient(googleClientId)
					.resourceIds(RESOURCE_ID).authorizedGrantTypes("client_credentials", "refresh_token", "authorization_code")
					.scopes(googleScope).authorities("oauth2").autoApprove(true).secret(encoder.encode(googleClientSecret)).and()
					.withClient(facebookClientId).resourceIds(RESOURCE_ID)
					.authorizedGrantTypes("client_credentials", "refresh_token", "authorization_code").scopes(facebookScope)
					.authorities("oauth2").autoApprove(true).secret(encoder.encode(facebookClientSecret));

		}

		@Override
		public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
			endpoints.tokenStore(tokenStore).authenticationManager(authenticationManager)
					.allowedTokenEndpointRequestMethods(HttpMethod.GET, HttpMethod.POST);
		}

		@Override
		public void configure(AuthorizationServerSecurityConfigurer oauthServer) {
			// 允許表單驗證
			oauthServer.allowFormAuthenticationForClients();
			oauthServer.tokenKeyAccess("permitAll()").checkTokenAccess("isAuthenticated()");
		}

	}

	@Configuration
	protected static class JDBCTokenConfig {
		@Value("${spring.datasource.url}")
		private String datasourceUrl;

		@Value("${spring.datasource.driver-class-name}")
		private String dbDriverClassName;

		@Value("${spring.datasource.username}")
		private String dbUsername;

		@Value("${spring.datasource.password}")
		private String dbPassword;

		@Bean
		public DataSource dataSource() {
			final DriverManagerDataSource dataSource = new DriverManagerDataSource();
			dataSource.setDriverClassName(dbDriverClassName);
			dataSource.setUrl(datasourceUrl);
			dataSource.setUsername(dbUsername);
			dataSource.setPassword(dbPassword);
			return dataSource;
		}

		@Bean
		public TokenStore tokenStore() {
			return new JdbcTokenStores(dataSource());
		}
	}

	public static class JdbcTokenStores extends JdbcTokenStore {
		private static final Log LOG = LogFactory.getLog(JdbcTokenStores.class);

		public JdbcTokenStores(DataSource dataSource) {
			super(dataSource);
		}

		@Override
		public OAuth2AccessToken readAccessToken(String tokenValue) {
			OAuth2AccessToken accessToken = null;

			try {
				accessToken = new DefaultOAuth2AccessToken(tokenValue);
				System.out.println(accessToken.getValue());
			} catch (EmptyResultDataAccessException e) {
				if (LOG.isInfoEnabled()) {
					LOG.info("Failed to find access token for token " + tokenValue);
				}
			} catch (IllegalArgumentException e) {
				LOG.warn("Failed to deserialize access token for " + tokenValue, e);
				removeAccessToken(tokenValue);
			}

			return accessToken;
		}
	}

}
