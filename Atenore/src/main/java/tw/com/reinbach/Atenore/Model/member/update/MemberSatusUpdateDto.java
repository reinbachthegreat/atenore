package tw.com.reinbach.Atenore.Model.member.update;

public class MemberSatusUpdateDto {

	private String memberID;
	
	private Integer status;

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}
