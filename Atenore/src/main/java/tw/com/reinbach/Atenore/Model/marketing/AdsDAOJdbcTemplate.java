package tw.com.reinbach.Atenore.Model.marketing;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import tw.com.reinbach.Atenore.Model.marketing.component.AdsDto;
import tw.com.reinbach.Atenore.Model.marketing.component.AdsTagDto;
import tw.com.reinbach.Atenore.Model.marketing.create.AdsCreateDto;
import tw.com.reinbach.Atenore.Model.marketing.query.search.AdsSearchArgsDto;
import tw.com.reinbach.Atenore.Model.marketing.query.search.AdsSearchInfoDto;
import tw.com.reinbach.Atenore.Model.marketing.update.AdsUpdateDto;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;

@Repository
public class AdsDAOJdbcTemplate implements AdsDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private NamedParameterJdbcTemplate namedJdbcTemplate;
	
	@Override
	public List<AdsSearchInfoDto> getAdsListByFilters(AdsSearchArgsDto argsDto) {
		try {
			StringBuilder sql = new StringBuilder("select a.adsID, a.type, a.title, a.insertTime, a.startTime, a.endTime, "
					+ " case when a.endTime >= now()  and  now() >= a.startTime then '已開始' when  a.endTime < now() then '已結束' else '排程中' end as 'status'  "
					+ " from ads a where 1=1 ");

			if(argsDto.getType()!=null && argsDto.getType().length()!=0) {
				sql.append(" and a.type = :type ");
			}
			
			if(argsDto.getFuzzyFilter()!=null && argsDto.getFuzzyFilter().length()!=0) {
				sql.append(" and a.title like '%' :fuzzyFilter '%' ");
			}
			
			if(argsDto.getStartTime()!=null) {
				sql.append(" and DATE(a.startTime) = Date(:startTime) ");
			}
			
			SqlParameterSource ps = new BeanPropertySqlParameterSource(argsDto);
			return namedJdbcTemplate.query(sql.toString(), ps, new BeanPropertyRowMapper<AdsSearchInfoDto>(AdsSearchInfoDto.class));
		}catch(DataAccessException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public AdsDto getAdsByAdsID(String adsID) {
		try {
			String sql = "select a.adsID, a.type, a.title, a.img, a.objectKey, a.insertTime, a.startTime, a.endTime, a.link from ads a "
					+ " where a.adsID = ? ";
			
			return jdbcTemplate.queryForObject(sql, new Object[]{adsID}, new BeanPropertyRowMapper<AdsDto>(AdsDto.class));
		}catch(DataAccessException e) {
			return null;
		}
	}
	
	@Override
	public AdsUpdateDto getAdsForUpdateByAdsID(String adsID) {
		try {
			String sql = "select a.adsID, a.type, a.title, a.img, a.objectKey, a.insertTime, a.startTime, a.endTime, a.link from ads a "
					+ " where a.adsID = ? ";
			
			return jdbcTemplate.queryForObject(sql, new Object[]{adsID}, new BeanPropertyRowMapper<AdsUpdateDto>(AdsUpdateDto.class));
		}catch(DataAccessException e) {
			return null;
		}		
	}

	@Override
	public List<AdsTagDto> getAdsTagByAdsID(String adsID) {
		try {
			String sql = "select ag.uuid as 'tagID', ag.tag from adstag ag "
					+ " where ag.adsID = ? ";
			return jdbcTemplate.query(sql, new Object[]{adsID}, new BeanPropertyRowMapper<AdsTagDto>(AdsTagDto.class));
		}catch(DataAccessException e) {
			return null;
		}
	}

	@Override
	public ResponseModel<Void> setAds(AdsCreateDto ads) {
		try {
			String sql = "insert into ads (adsID, type, title, img, objectKey, insertTime, startTime, endTime, link) "
					+ " values(:adsID, :type, :title, :img, :objectKey, now(), :startTime, :endTime, :link) ";
			
			SqlParameterSource ps = new BeanPropertySqlParameterSource(ads);
			int count = namedJdbcTemplate.update(sql, ps);
			
			if(count!=0) {
				return new ResponseModel<>(201, "");
			}else {
				return new ResponseModel<>(409, ApiError.ADS_INFO_CREATION_FAILED);
			}
		}catch(DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> setAdsTag(List<AdsTagDto> tagList) {
		try {
			String sql = "insert into adstag (uuid, adsID, tag) "
					+ " values(:tagID, :adsID, :tag) ";
			
			List<SqlParameterSource> psList = new ArrayList<SqlParameterSource>();
			for (AdsTagDto dto : tagList) {
				psList.add(new BeanPropertySqlParameterSource(dto));
			}
			SqlParameterSource[] psArry = new SqlParameterSource[psList.size()];
			psList.toArray(psArry);
			int[] countList = namedJdbcTemplate.batchUpdate(sql, psArry);
			if(this.resultIfBatchUpdateSuccess(countList)) {
				return new ResponseModel<Void>(201, "");	
			}else {
				return new ResponseModel<Void>(201, "Some of " + ApiError.ADS_TAG_CREATION_FAILED);
			}
		}catch(DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> updateAds(AdsUpdateDto ads) {
		try {
			String sql = "update ads set type = :type, title = :title, img = :img, objectKey = :objectKey, "
					+ " startTime = :startTime, endTime = :endTime, link = :link "
					+ " where adsID = :adsID ";
			
			SqlParameterSource ps = new BeanPropertySqlParameterSource(ads);
			int count = namedJdbcTemplate.update(sql, ps);
			
			if(count!=0) {
				return new ResponseModel<>(204, "");
			}else {
				return new ResponseModel<>(409, ApiError.ADS_UPDATE_FAILED);
			}
		}catch(DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deleteAdsTagByAdsID(String adsID) {
		try {
			String sql = "delete from adstag where adsID = ?";
			jdbcTemplate.update(sql, new Object[] {adsID});
			return new ResponseModel<>(204, "");
		}catch(DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deleteAdsByAdsID(String adsID) {
		try {
			String sql = "delete from ads where adsID = ?";
			jdbcTemplate.update(sql, new Object[] {adsID});
			return new ResponseModel<>(204, "");
		}catch(DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}
	
	
	private Boolean resultIfBatchUpdateSuccess(int[] countList) {
		Boolean flag = true;
		for(int count : countList) {
			if(count==0) {
				flag = false;
				break;
			}
		}
		return flag;
	}
	
}
