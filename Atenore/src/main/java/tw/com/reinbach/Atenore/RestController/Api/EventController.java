package tw.com.reinbach.Atenore.RestController.Api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import tw.com.reinbach.Atenore.Model.category.CategoryService;
import tw.com.reinbach.Atenore.Model.category.event.dto.EventTypeCascadeDto;
import tw.com.reinbach.Atenore.Model.event.EventService;
import tw.com.reinbach.Atenore.Model.event.component.EventDto;
import tw.com.reinbach.Atenore.Model.event.component.EventImageDto;
import tw.com.reinbach.Atenore.Model.event.component.EventRecommend;
import tw.com.reinbach.Atenore.Model.event.component.EventTag;
import tw.com.reinbach.Atenore.Model.event.component.PinEventTypeInfoDto;
import tw.com.reinbach.Atenore.Model.event.query.EventInfoDto;
import tw.com.reinbach.Atenore.Model.event.query.PinEventTypeListInfoDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventNearBySearchArgsDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchArgsDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchInfoDto;
import tw.com.reinbach.Atenore.Model.event.update.EventFollowedDto;
import tw.com.reinbach.Atenore.Model.event.update.PinEventTypeUpdateDto;
import tw.com.reinbach.Atenore.Model.search.SearchPreferDto;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;

@RestController
public class EventController {

	@Autowired
	private EventService eventService;

	@Autowired
	private CategoryService categoryService;
	

	/**
	 * Get event searching prefer list
	 * 
	 * @return
	 */
	@ApiImplicitParam(name = "Authorization", required = false, paramType = "header", dataType = "String", access = "hidden")
	@ApiOperation(notes = "ApiNo:1-1", value = "Get event searching prefer list")
	@RequestMapping(value = { "/web/event/search/prefer", "/app/event/search/prefer" }, method = { RequestMethod.GET })
	public ResponseModel<List<SearchPreferDto>> getEventSearcingPreferList() {
		return new ResponseModel<List<SearchPreferDto>>(200, eventService.getEventSearcingPreferList());
	}

	/**
	 * Get event list by searching filter
	 * 
	 * @param searchArgs (EventSearchArgsDto)
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:1", value = "Get event list by searching filter")
	@RequestMapping(value = { "/web/event/list", "/app/event/list" }, method = { RequestMethod.GET })
	@ApiImplicitParam(name = "Authorization", required = false, paramType = "header", dataType = "String", access = "hidden")
	public ResponseModel<List<EventSearchInfoDto>> getEventList(EventSearchArgsDto searchArgs) {

		searchArgs.setApply(eventService.getApplyStatusList().get(1));
		List<EventSearchInfoDto> list = eventService.getEventListWithFilter(searchArgs);
		if (list == null || list.size() == 0) {
			return new ResponseModel<List<EventSearchInfoDto>>(404, ApiError.DATA_NOT_FOUND);
		}
		return new ResponseModel<List<EventSearchInfoDto>>(200, list);
	}

	/**
	 * Get event type cascade list
	 * 
	 * @return
	 */
	@ApiImplicitParam(name = "Authorization", required = false, paramType = "header", dataType = "String", access = "hidden")
	@ApiOperation(notes = "ApiNo:2", value = "Get event type cascade list")
	@RequestMapping(value = { "/web/event/type", "/app/event/type" }, method = RequestMethod.GET)
	public ResponseModel<List<EventTypeCascadeDto>> getEventTypeCascadeWithMainType() {
		List<EventTypeCascadeDto> list = categoryService.getEventTypeCascadeWithMainType();
		if (list == null || list.size() == 0) {
			return new ResponseModel<List<EventTypeCascadeDto>>(404, ApiError.DATA_NOT_FOUND);
		}
		return new ResponseModel<List<EventTypeCascadeDto>>(200, list);
	}

	/** Get member pinned event type list
	 * 
	 * @param memberID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:3", value = "Get member pinned event type list")
	@RequestMapping(value = {"/web/eventtype/pin/{memberID}", "/app/eventtype/pin/{memberID}"}, method = RequestMethod.GET)
	public ResponseModel<PinEventTypeListInfoDto> getPinEventTypeList(
			@PathVariable(required = true) String memberID){
		
		List<PinEventTypeInfoDto> list = eventService.getPinEventTypeListByMemberID(memberID);
		if(list==null || list.size()==0) {
			return new ResponseModel<PinEventTypeListInfoDto>(404, ApiError.DATA_NOT_FOUND);
		}
		
		PinEventTypeListInfoDto dto= new PinEventTypeListInfoDto();
		dto.setMemberID(memberID);
		dto.setPinEventTypeList(list);
		
		return new ResponseModel<PinEventTypeListInfoDto>(200, dto);
	}
	
	/** Update member pinned event type list
	 * 
	 * @param memberID
	 * @param list
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:4", value = "Update member pinned event type list")
	@RequestMapping(value = {"/web/eventtype/pin/{memberID}", "/app/eventtype/pin/{memberID}"}, method = RequestMethod.PUT)
	public ResponseModel<PinEventTypeListInfoDto> updatePinEventTypeList(
			@PathVariable(required = true) String memberID,
			@RequestBody List<PinEventTypeUpdateDto> list){
		
		ResponseModel<Void> result = eventService.updatePinEventTypeList(memberID, list);
		if(result.getCode()!=204) {
			return new ResponseModel<PinEventTypeListInfoDto>(result.getCode(), result.getErrMsg());
		}
		
		ResponseModel<PinEventTypeListInfoDto> resultModel = getPinEventTypeList(memberID); 
		if(resultModel.getCode()==200) {
			resultModel.setCode(204);
		}
		
		return resultModel;
	}
	
	@ApiOperation(notes = "ApiNo:W2", value = "Get event fully information.")
	@RequestMapping(value = { "/web/event/info/{eventID}"}, method = { RequestMethod.GET })
	public ResponseModel<EventInfoDto> getEventFullyInfo(
			@ApiParam(example = "e5a55b02-b65e-446f-928f-1121ca14b060", required = true) 
			@PathVariable(value = "eventID", required = true) String eventID,
			@ApiParam(example = "75c6538b-facd-461b-b143-919c0913028c", required = false) 
			@RequestParam(required = false) String memberID) {

		EventDto eventInfo = eventService.getEvent(eventID, memberID);

		if (eventInfo == null) {
			return new ResponseModel<EventInfoDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			List<EventTag> eventTagList = eventService.getEventTagWithoutEventID(eventID);
			List<EventImageDto> eventImgList = eventService.getEventImage(eventID);
			List<EventRecommend> eventRecommendList = eventService.getEventRecommend(eventID);
			EventInfoDto event = new EventInfoDto();
			event.setEventInfo(eventInfo);
			event.setEventTagList(eventTagList);
			event.setEventImgList(eventImgList);
			event.setEventRecommendList(eventRecommendList);
			return new ResponseModel<EventInfoDto>(200, event);
		}
	}

	/** Get event information without master recommend list
	 * 
	 * @param eventID
	 * @param memberID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:5", value = "Get event information without master recommend list.")
	@RequestMapping(value = { "/app/event/info/{eventID}" }, method = { RequestMethod.GET })
	@ApiImplicitParam(name = "Authorization", required = false, paramType = "header", dataType = "String", access = "hidden")
	public ResponseModel<EventInfoDto> getEventWithoutMasterRecommend(
			@ApiParam(example = "e5a55b02-b65e-446f-928f-1121ca14b060", required = true) @PathVariable(value = "eventID", required = true) String eventID,
			@ApiParam(example = "75c6538b-facd-461b-b143-919c0913028c", required = false) @RequestParam(required = false) String memberID) {

		EventDto eventInfo = eventService.getEvent(eventID, memberID);

		if (eventInfo == null) {
			return new ResponseModel<EventInfoDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			List<EventTag> eventTagList = eventService.getEventTagWithoutEventID(eventID);
			List<EventImageDto> eventImgList = eventService.getEventImage(eventID);
			EventInfoDto event = new EventInfoDto();
			event.setEventInfo(eventInfo);
			event.setEventTagList(eventTagList);
			event.setEventImgList(eventImgList);
			return new ResponseModel<EventInfoDto>(200, event);
		}
	}

	/** Get event's master recommend list
	 * 
	 * @param eventID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:6", value = "Get event's master recommend list.")
	@RequestMapping(value = { "/web/event/recommend/{eventID}", "/app/event/recommend/{eventID}" }, method = { RequestMethod.GET })
	@ApiImplicitParam(name = "Authorization", required = false, paramType = "header", dataType = "String", access = "hidden")
	public ResponseModel<List<EventRecommend>> getEventMasterRecommendOnly(
			@ApiParam(example = "e5a55b02-b65e-446f-928f-1121ca14b060", required = true) @PathVariable(value = "eventID", required = true) String eventID) {

		List<EventRecommend> eventRecommendList = eventService.getEventRecommendWithoutEventID(eventID);

		if (eventRecommendList == null) {
			return new ResponseModel<List<EventRecommend>>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<List<EventRecommend>>(200, eventRecommendList);
		}
	}

	/** Event followed by Member
	 * 
	 * @param follow
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:7", value = "Set event followed by member.")
	@RequestMapping(value = { "/web/event/follow", "/app/event/follow" }, method = { RequestMethod.POST })
	public ResponseModel<Void> setEventFollowed(EventFollowedDto follow) {
		follow.setWannaFollow(true);
		ResponseModel<Void> resultModel = eventService.updateEventFollowByMemberID(follow);
		if (resultModel.getCode() == 201) {
			return new ResponseModel<Void>(204, "Event followed success.");
		} else {
			return resultModel;
		}
	}

	/** Event cancel followed by Member
	 * 
	 * @param follow
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:8", value = "Delete event followed by member.")
	@RequestMapping(value = { "/web/event/follow", "/app/event/follow" }, method = { RequestMethod.DELETE })
	public ResponseModel<Void> deleteEventFollowed(EventFollowedDto follow) {
		follow.setWannaFollow(false);
		ResponseModel<Void> resultModel = eventService.updateEventFollowByMemberID(follow);
		if (resultModel.getCode() == 201) {
			return new ResponseModel<Void>(204, "Event cancel followed success.");
		} else {
			return resultModel;
		}
	}
	
	/** Get event list at GIS location near by
	 * 
	 * @param searchArgs
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:W19", value = "Get event list at GIS location near by")
	@RequestMapping(value = { "/web/event/nearby"}, method = { RequestMethod.GET })
	public ResponseModel<List<EventSearchInfoDto>> getEventListNearBy(EventNearBySearchArgsDto searchArgs) {
		searchArgs.setApply(eventService.getApplyStatusList().get(1));
		List<EventSearchInfoDto> list = eventService.getEventListNearBy(searchArgs);
		if (list == null || list.size() == 0) {
			return new ResponseModel<List<EventSearchInfoDto>>(404, ApiError.DATA_NOT_FOUND);
		}
		return new ResponseModel<List<EventSearchInfoDto>>(200, list);
	}
}
