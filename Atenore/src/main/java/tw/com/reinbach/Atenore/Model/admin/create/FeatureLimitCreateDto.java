package tw.com.reinbach.Atenore.Model.admin.create;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class FeatureLimitCreateDto {

	private String featureID;
	
	private String limitID;
	
	private Boolean limitValue = false;

	public String getFeatureID() {
		return featureID;
	}

	public void setFeatureID(String featureID) {
		this.featureID = featureID;
	}

	public String getLimitID() {
		return limitID;
	}

	public void setLimitID(String limitID) {
		this.limitID = limitID;
	}

	public Boolean getLimitValue() {
		return limitValue;
	}

	public void setLimitValue(Boolean limitValue) {
		this.limitValue = limitValue;
	}
	
}
