package tw.com.reinbach.Atenore.Model.event.query;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class EventNotReviewCountDto {

	private Integer notReviewCount;

	public Integer getNotReviewCount() {
		return notReviewCount;
	}

	public void setNotReviewCount(Integer notReviewCount) {
		this.notReviewCount = notReviewCount;
	}
	
}
