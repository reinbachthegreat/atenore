package tw.com.reinbach.Atenore.Model.place.component;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PlaceLiaisonDto {

	private String liaisonID;

	@ApiModelProperty(hidden = true)
	private String placeID;

	@ApiModelProperty(value = "聯絡人")
	private String name;

	@ApiModelProperty(value = "聯絡電話")
	private String phone;

	private String isMain;

	public String getLiaisonID() {
		return liaisonID;
	}

	public void setLiaisonID(String liaisonID) {
		this.liaisonID = liaisonID;
	}

	public String getPlaceID() {
		return placeID;
	}

	public void setPlaceID(String placeID) {
		this.placeID = placeID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getIsMain() {
		return isMain;
	}

	public void setIsMain(String isMain) {
		this.isMain = isMain;
	}

}
