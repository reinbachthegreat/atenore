package tw.com.reinbach.Atenore.Model.event.component;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class EventTag {

	@ApiModelProperty(example = "123e4567-e89b-42d3-a456-556642440000", dataType = "String")
	private String tagID;
	
	@ApiModelProperty(example = "123e4567-e89b-42d3-a456-556642440000", dataType = "String")
	private String eventID;
	
	@ApiModelProperty(example = "活動tag", dataType = "String")
	private String tag;

	public String getTagID() {
		return tagID;
	}

	public void setTagID(String tagID) {
		this.tagID = tagID;
	}

	public String getEventID() {
		return eventID;
	}

	public void setEventID(String eventID) {
		this.eventID = eventID;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}
	
	
	
}
