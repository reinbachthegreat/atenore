package tw.com.reinbach.Atenore.Utility;

import java.io.UnsupportedEncodingException;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class SendMail {

	@Autowired
	private JavaMailSender mailSender;
	
	@Value("${spring.mail.username}")
	private String systemMail;
	
	public void SendMailToUser(String mail,String subject, String text){
		
		SimpleMailMessage email = new SimpleMailMessage();
		email.setTo(mail);
		email.setSubject(subject);
		email.setText(text);
		
		mailSender.send(email);
	}
	
	public void SendMailToUserByHTML(String mail,String subject, String text) throws MessagingException, UnsupportedEncodingException {
		
		MimeMessage msg = mailSender.createMimeMessage(); 
		MimeMessageHelper helper = new MimeMessageHelper(msg, true);;
		
		String exceptionMsg = null;
		String exceptionHandlingMsg = null;
		
		try {
			helper.setFrom(new InternetAddress(systemMail, "Atenore"));
			helper.setTo(mail);
			helper.setSubject(subject);
			helper.setText(text,true);
		} catch (UnsupportedEncodingException e) {
			exceptionMsg = exceptionMsg + "\n" +  e.getMessage();
			exceptionHandlingMsg = exceptionHandlingMsg + "信件郵寄錯誤：寄件人地址有誤。";
		} catch (MessagingException e) {
			exceptionMsg = exceptionMsg + "\n" +  e.getMessage();
			exceptionHandlingMsg = exceptionHandlingMsg + "信件郵寄錯誤：信件內容有誤。請檢查寄件人，收件人，主題設定或郵件內容文字。";
		}

		try {
			mailSender.send(msg);
		} catch (MailException e) {
			exceptionMsg = exceptionMsg + "\n" +  e.getMessage();			
			exceptionHandlingMsg = exceptionHandlingMsg + "信件郵寄錯誤：無法發送信件！可能的問題是系統郵件驗證錯誤，收件人地址錯誤或找不到，或其他導致郵件寄送因素。";
		}
		
		if(exceptionMsg!=null) {
			System.out.println(exceptionMsg);
			helper.setFrom(new InternetAddress(systemMail, "Atenore：系統通知"));
			helper.setTo(new InternetAddress(systemMail, "Atenore"));
			helper.setSubject("郵件發送錯誤！");
			helper.setText(exceptionHandlingMsg,true);
		}
	}
}
