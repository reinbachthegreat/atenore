package tw.com.reinbach.Atenore.Model.master;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import tw.com.reinbach.Atenore.Model.master.component.MasterDto;
import tw.com.reinbach.Atenore.Model.master.component.MasterEditDto;
import tw.com.reinbach.Atenore.Model.master.component.MasterRecommend;
import tw.com.reinbach.Atenore.Model.master.component.MasterTag;
import tw.com.reinbach.Atenore.Model.master.component.MasterTagEditDto;
import tw.com.reinbach.Atenore.Model.master.create.MasterCreateDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterAliveSearchArgsDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterAliveSearchInfoDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterDeadSearchArgsDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterDeadSearchInfoDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterEditSearchArgsAdminDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterEditSearchInfoAdminDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterEditSearchInfoMemberDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterHitSearchInfoDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterSearchArgsAdminDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterSearchArgsDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterSearchArgsMemberEditedDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterSearchInfoAdminDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterSearchInfoDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterSearchSimpleInfoDto;
import tw.com.reinbach.Atenore.Model.master.update.MasterFollowedDto;
import tw.com.reinbach.Atenore.Model.master.update.MasterUpdateDto;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;

@Repository
public class MasterDAOJdbcTemplate implements MasterDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private NamedParameterJdbcTemplate namedJdbcTemplate;

	private String[] searchPreferColumnSQL = { " , m.follow ", " , m.click " };

	private String[] searchPreferSQL = { " and m.follow is not null ", " and m.click is not null " };

	private String[] searchOrderSQL = { " order by m.follow desc ", "  order by m.click desc " };

	@Override
	public List<MasterSearchInfoDto> getMasterListWithFilter(MasterSearchArgsDto argsDto) {
		try {
//			StringBuilder sql = new StringBuilder("select m.* ,(\r\n" + 
//					"select mt.tag from mastertag as mt left join master m on m.masterID = mt.masterID limit 0,1\r\n" + 
//					") as 'tag1',(\r\n" + 
//					"select mt.tag from mastertag as mt left join master m on m.masterID = mt.masterID limit 1,1\r\n" + 
//					") as 'tag2', (\r\n" + 
//					"select mt.tag from mastertag as mt left join master m on m.masterID = mt.masterID limit 2,1\r\n" + 
//					") as 'tag3' from master m\r\n" + 
//					"left join mastertype mt on mt.typeId = m.typeID\r\n" + 
//					"left join mastermidtype mit on mit.midID = mt.midID\r\n" + 
//					"left join mastermaintype mmt on mmt.mainID = mit.mainID\r\n" + 
//					"where 1=1");

			StringBuilder sql = new StringBuilder("select m.* , \r\n"
					+ "If(length(GROUP_CONCAT(t.tag)) - length(replace(GROUP_CONCAT(t.tag), ',', ''))>1,  \r\n"
					+ "       SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 1), ',', -1) ,NULL)  as tag1,\r\n"
					+ "if(SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 2), ',', -1)!= SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 1), ',', -1),\r\n"
					+ "		SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 2), ',', -1), NULL )  as tag2,\r\n"
					+ "if(SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 3), ',', -1)!= SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 2), ',', -1),\r\n"
					+ "		SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 3), ',', -1), NULL ) as tag3\r\n"
					+ "       from master m\r\n" + "					left join mastertype mt on mt.typeId = m.typeID\r\n"
					+ "					left join mastermidtype mit on mit.midID = mt.midID \r\n"
					+ "					left join mastermaintype mmt on mmt.mainID = mit.mainID \r\n"
					+ "                    left join mastertag t on m.masterID = t.masterID\r\n" + "					where 1=1  ");

			if (argsDto.getFuzzyFilter() != null) {
				sql.append(" and m.publicName  like %:fuzzyFilter% ");
			}

			if (argsDto.getCounty() != null) {
				sql.append(" and m.county = :county ");
			}

			if (argsDto.getMainID() != null) {
				sql.append(" and mmt.mainID = :mainID ");
			}

			if (argsDto.getMidID() != null) {
				sql.append(" and mit.midID = :midID ");
			}

			if (argsDto.getTypeID() != null) {
				sql.append(" and mt.typeID = :typeID ");
			}

			sql.append(" group by m.masterID ");

			SqlParameterSource ps = new BeanPropertySqlParameterSource(argsDto);
			return namedJdbcTemplate.query(sql.toString(), ps,
					new BeanPropertyRowMapper<MasterSearchInfoDto>(MasterSearchInfoDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<MasterHitSearchInfoDto> getMasterListByHitSearchPreferSN(Integer searchPreferSN) {
		try {
//			StringBuilder sql = new StringBuilder("select m.masterID, m.img, m.publicName " + getSearchPreferColumnSQLBySN(searchPreferSN) + " ,(\r\n" + 
//					"select mt.tag from mastertag as mt left join master m on m.masterID = mt.masterID limit 0,1\r\n" + 
//					") as 'tag1',(\r\n" + 
//					"select mt.tag from mastertag as mt left join master m on m.masterID = mt.masterID limit 1,1\r\n" + 
//					") as 'tag2', (\r\n" + 
//					"select mt.tag from mastertag as mt left join master m on m.masterID = mt.masterID limit 2,1\r\n" + 
//					") as 'tag3', (\r\n" + 
//					"select mt.tag from mastertag as mt left join master m on m.masterID = mt.masterID limit 3,1\r\n" + 
//					") as 'tag4', (\r\n" + 
//					"select mt.tag from mastertag as mt left join master m on m.masterID = mt.masterID limit 4,1\r\n" + 
//					") as 'tag5',(\r\n" + 
//					"select mt.tag from mastertag as mt left join master m on m.masterID = mt.masterID limit 5,1\r\n" + 
//					") as 'tag6', (\r\n" + 
//					"select mt.tag from mastertag as mt left join master m on m.masterID = mt.masterID limit 6,1\r\n" + 
//					") as 'tag7', (\r\n" + 
//					"select mt.tag from mastertag as mt left join master m on m.masterID = mt.masterID limit 7,1\r\n" + 
//					") as 'tag8' from master m\r\n" + 
//					"left join mastertype mt on mt.typeId = m.typeID\r\n" + 
//					"left join mastermidtype mit on mit.midID = mt.midID\r\n" + 
//					"left join mastermaintype mmt on mmt.mainID = mit.mainID\r\n" + 
//					"where 1=1\r\n ");

			StringBuilder sql = new StringBuilder("select m.masterID, m.img, m.publicName "
					+ getSearchPreferColumnSQLBySN(searchPreferSN) + ", \r\n"
					+ "If(length(GROUP_CONCAT(t.tag)) - length(replace(GROUP_CONCAT(t.tag), ',', ''))>1,  \r\n"
					+ "       SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 1), ',', -1) ,NULL)  as tag1,\r\n"
					+ "if(SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 2), ',', -1)!= SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 1), ',', -1),\r\n"
					+ "		SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 2), ',', -1), NULL )  as tag2,\r\n"
					+ "if(SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 3), ',', -1)!= SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 2), ',', -1),\r\n"
					+ "		SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 3), ',', -1), NULL )  as tag3,\r\n"
					+ "if(SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 4), ',', -1)!= SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 3), ',', -1),\r\n"
					+ "		SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 4), ',', -1), NULL )  as tag4,\r\n"
					+ "if(SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 5), ',', -1)!= SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 4), ',', -1),\r\n"
					+ "		SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 5), ',', -1), NULL )  as tag5,\r\n"
					+ "if(SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 6), ',', -1)!= SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 5), ',', -1),\r\n"
					+ "		SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 6), ',', -1), NULL ) as tag6,\r\n"
					+ "if(SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 7), ',', -1)!= SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 6), ',', -1),\r\n"
					+ "		SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 7), ',', -1), NULL )  as tag7,       \r\n"
					+ "if(SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 8), ',', -1)!= SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 7), ',', -1),\r\n"
					+ "		SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 8), ',', -1), NULL )  as tag8       \r\n"
					+ "       from master m\r\n" + "					left join mastertype mt on mt.typeId = m.typeID\r\n"
					+ "					left join mastermidtype mit on mit.midID = mt.midID \r\n"
					+ "					left join mastermaintype mmt on mmt.mainID = mit.mainID \r\n"
					+ "                    left join mastertag t on m.masterID = t.masterID\r\n" + "					where 1=1 ");

			sql.append(getSearchPreferSQLBySN(searchPreferSN));
			sql.append(" group by m.masterID ");
			sql.append(getSearchOrderSQLBySN(searchPreferSN));

			return namedJdbcTemplate.query(sql.toString(),
					new BeanPropertyRowMapper<MasterHitSearchInfoDto>(MasterHitSearchInfoDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<MasterAliveSearchInfoDto> getMasterAliveListWithFilter(MasterAliveSearchArgsDto argsDto) {
		try {
//			StringBuilder sql = new StringBuilder("select m.* ,(\r\n" + 
//					"select mt.tag from mastertag as mt left join master m on m.masterID = mt.masterID limit 0,1\r\n" + 
//					") as 'tag1',(\r\n" + 
//					"select mt.tag from mastertag as mt left join master m on m.masterID = mt.masterID limit 1,1\r\n" + 
//					") as 'tag2', (\r\n" + 
//					"select mt.tag from mastertag as mt left join master m on m.masterID = mt.masterID limit 2,1\r\n" + 
//					") as 'tag3' from master m\r\n" + 
//					"left join mastertype mt on mt.typeId = m.typeID\r\n" + 
//					"left join mastermidtype mit on mit.midID = mt.midID\r\n" + 
//					"left join mastermaintype mmt on mmt.mainID = mit.mainID\r\n" + 
//					"where 1=1 and m.alive = 'true' ");

			StringBuilder sql = new StringBuilder("select m.* , \r\n"
					+ "If(length(GROUP_CONCAT(t.tag)) - length(replace(GROUP_CONCAT(t.tag), ',', ''))>1,  \r\n"
					+ "       SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 1), ',', -1) ,NULL)  as tag1,\r\n"
					+ "if(SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 2), ',', -1)!= SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 1), ',', -1),\r\n"
					+ "		SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 2), ',', -1), NULL )  as tag2,\r\n"
					+ "if(SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 3), ',', -1)!= SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 2), ',', -1),\r\n"
					+ "		SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 3), ',', -1), NULL ) as tag3\r\n"
					+ "       from master m\r\n" + "					left join mastertype mt on mt.typeId = m.typeID\r\n"
					+ "					left join mastermidtype mit on mit.midID = mt.midID \r\n"
					+ "					left join mastermaintype mmt on mmt.mainID = mit.mainID \r\n"
					+ "                    left join mastertag t on m.masterID = t.masterID\r\n"
					+ "					where 1=1  and m.alive = 1 ");

			if (argsDto.getCounty() != null) {
				sql.append(" and m.county = :county ");
			}

			if (argsDto.getMainID() != null) {
				sql.append(" and mmt.mainID = :mainID ");
			}

			if (argsDto.getMidID() != null) {
				sql.append(" and mit.midID = :midID ");
			}

			if (argsDto.getTypeID() != null) {
				sql.append(" and mt.typeID = :typeID ");
			}

			sql.append(" group by m.masterID ");

			SqlParameterSource ps = new BeanPropertySqlParameterSource(argsDto);
			return namedJdbcTemplate.query(sql.toString(), ps,
					new BeanPropertyRowMapper<MasterAliveSearchInfoDto>(MasterAliveSearchInfoDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<MasterDeadSearchInfoDto> getMasterDeadListWithFilter(MasterDeadSearchArgsDto argsDto) {
		try {
//			StringBuilder sql = new StringBuilder("select m.* ,(\r\n" + 
//					"select mt.tag from mastertag as mt left join master m on m.masterID = mt.masterID limit 0,1\r\n" + 
//					") as 'tag1',(\r\n" + 
//					"select mt.tag from mastertag as mt left join master m on m.masterID = mt.masterID limit 1,1\r\n" + 
//					") as 'tag2', (\r\n" + 
//					"select mt.tag from mastertag as mt left join master m on m.masterID = mt.masterID limit 2,1\r\n" + 
//					") as 'tag3' from master m\r\n" + 
//					"left join mastertype mt on mt.typeId = m.typeID\r\n" + 
//					"left join mastermidtype mit on mit.midID = mt.midID\r\n" + 
//					"left join mastermaintype mmt on mmt.mainID = mit.mainID\r\n" + 
//					"where 1=1 and m.alive = 'false' ");

			StringBuilder sql = new StringBuilder("select m.* , \r\n"
					+ "If(length(GROUP_CONCAT(t.tag)) - length(replace(GROUP_CONCAT(t.tag), ',', ''))>1,  \r\n"
					+ "       SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 1), ',', -1) ,NULL)  as tag1,\r\n"
					+ "if(SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 2), ',', -1)!= SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 1), ',', -1),\r\n"
					+ "		SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 2), ',', -1), NULL )  as tag2,\r\n"
					+ "if(SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 3), ',', -1)!= SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 2), ',', -1),\r\n"
					+ "		SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(t.tag), ',', 3), ',', -1), NULL ) as tag3\r\n"
					+ "       from master m\r\n" + "					left join mastertype mt on mt.typeId = m.typeID\r\n"
					+ "					left join mastermidtype mit on mit.midID = mt.midID \r\n"
					+ "					left join mastermaintype mmt on mmt.mainID = mit.mainID \r\n"
					+ "                    left join mastertag t on m.masterID = t.masterID\r\n"
					+ "					where 1=1  and m.alive = 0 ");

			if (argsDto.getCounty() != null) {
				sql.append(" and m.county = :county ");
			}

			if (argsDto.getMainID() != null) {
				sql.append(" and mmt.mainID = :mainID ");
			}

			if (argsDto.getMidID() != null) {
				sql.append(" and mit.midID = :midID ");
			}

			if (argsDto.getTypeID() != null) {
				sql.append(" and mt.typeID = :typeID ");
			}

			sql.append(" group by m.masterID ");

			SqlParameterSource ps = new BeanPropertySqlParameterSource(argsDto);
			return namedJdbcTemplate.query(sql.toString(), ps,
					new BeanPropertyRowMapper<MasterDeadSearchInfoDto>(MasterDeadSearchInfoDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<MasterSearchSimpleInfoDto> getMasterSimpleByPublicName(String name, Integer alive) {
		try {
			String publicName = "%" + name + "%";
			StringBuilder sql = new StringBuilder("select masterID, publicName, img from atenore.master where publicName like ? ");
			if (alive != null && alive == 0) {
				sql.append(" and alive = 'false' ");
			} else if (alive != null && alive == 1) {
				sql.append(" and alive = 'true' ");
			}
			return jdbcTemplate.query(sql.toString(), new Object[] { publicName },
					new BeanPropertyRowMapper<MasterSearchSimpleInfoDto>(MasterSearchSimpleInfoDto.class));
		} catch (DataAccessException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public MasterDto getMasterByMasterIDWithMemberID(String masterID, String memberID) {
		try {
			String sql = "select m.*, mt.name as 'typeName', mmt.mainID, mmt.name as 'mainTypeName', mit.midID as 'middleID', mit.name as 'middleTypeName', "
					+ " (select count(*) from membermaster where memberID = ? and masterID = ?) as isFollowed  from master m "
					+ " left join mastertype mt on mt.typeID = m.typeID " + " left join mastermidtype mit on mit.midID = mt.midID "
					+ " left join mastermaintype mmt on mmt.mainID = mit.mainID " + " where m.masterID = ? ";
			return jdbcTemplate.queryForObject(sql, new Object[] {memberID, masterID, masterID}, new BeanPropertyRowMapper<MasterDto>(MasterDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}
	
	@Override
	public MasterDto getMasterByMasterID(String masterID) {
		try {
			String sql = "select m.*, mt.name as 'typeName', mmt.mainID, mmt.name as 'mainTypeName', mit.midID as 'middleID', mit.name as 'middleTypeName'  from master m "
					+ " left join mastertype mt on mt.typeID = m.typeID left join mastermidtype mit on mit.midID = mt.midID "
					+ " left join mastermaintype mmt on mmt.mainID = mit.mainID where masterID = ?";
			return jdbcTemplate.queryForObject(sql, new Object[] { masterID }, new BeanPropertyRowMapper<MasterDto>(MasterDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public MasterDto getMasterAliveWithMemberID(String masterID, String memberID) {
		try {
			String sql = "select m.publicName, m.img, m.click, m.follow, m.lineID, m.facebook, m.mail, m.phone, m.company, "
					+ " m.county, m.address, m.website, m.intro, "
					+ " (select count(*) from membermaster where memberID = ? and masterID = ?) as isFollowed  from master m "
					+ " left join mastertype mt on mt.typeID = m.typeID " + " left join mastermidtype mit on mit.midID = mt.midID "
					+ " left join mastermaintype mmt on mmt.mainID = mit.mainID " + " where m.masterID = ? ";

			return jdbcTemplate.queryForObject(sql, new Object[] { memberID, masterID, masterID },
					new BeanPropertyRowMapper<MasterDto>(MasterDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public MasterDto getMasterAlive(String masterID) {
		try {
			String sql = "select m.publicName, m.img, m.click, m.follow, m.lineID, m.facebook, m.mail, m.phone, m.company, "
					+ " m.county, m.address, m.website, m.intro from master m "
					+ " left join mastertype mt on mt.typeID = m.typeID left join mastermidtype mit on mit.midID = mt.midID "
					+ " left join mastermaintype mmt on mmt.mainID = mit.mainID where m.masterID = ? ";

			return jdbcTemplate.queryForObject(sql, new Object[] { masterID }, new BeanPropertyRowMapper<MasterDto>(MasterDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public MasterDto getMasterDeadWithMemberID(String masterID, String memberID) {
		try {
			String sql = "select m.publicName, m.img, m.click, m.follow, " + " m.intro, m.legend, "
					+ " (select count(*) from membermaster where memberID = ? and masterID = ?) as isFollowed  from master m "
					+ " left join mastertype mt on mt.typeID = m.typeID " + " left join mastermidtype mit on mit.midID = mt.midID "
					+ " left join mastermaintype mmt on mmt.mainID = mit.mainID " + " where m.masterID = ? ";

			return jdbcTemplate.queryForObject(sql, new Object[] { memberID, masterID, masterID },
					new BeanPropertyRowMapper<MasterDto>(MasterDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public MasterDto getMasterDead(String masterID) {
		try {
			String sql = "select m.publicName, m.img, m.click, m.follow, " + " m.intro, m.legend " + " from master m "
					+ " left join mastertype mt on mt.typeID = m.typeID " + " left join mastermidtype mit on mit.midID = mt.midID "
					+ " left join mastermaintype mmt on mmt.mainID = mit.mainID " + " where m.masterID = ? ";

			return jdbcTemplate.queryForObject(sql, new Object[] { masterID }, new BeanPropertyRowMapper<MasterDto>(MasterDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<MasterTag> getMasterTagListByMasterID(String masterID) {
		try {
			String sql = "select mt.tag from mastertag mt where mt.masterID = ? ";

			return jdbcTemplate.query(sql, new Object[] { masterID }, new BeanPropertyRowMapper<MasterTag>(MasterTag.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<MasterRecommend> getMasterRecommendListByMasterID(String masterID) {
		try {
			String sql = "select m.memberID, m.publicName as 'memberName' , from masteredit me "
					+ " left join memberdetail m on me.memberID = m.memberID " + " where me.masterID = ? order by me.insertTime ";
			return jdbcTemplate.query(sql, new Object[] { masterID },
					new BeanPropertyRowMapper<MasterRecommend>(MasterRecommend.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public Boolean getMasterIfMemberFollowed(MasterFollowedDto follow) {
		try {
			String sql = "select count(*) from membermaster where masterID = :masterID and memberID= :memberID ";
			SqlParameterSource ps = new BeanPropertySqlParameterSource(follow);
			Integer count = namedJdbcTemplate.queryForObject(sql, ps, Integer.class);
			if (count == 0) {
				return false;
			} else {
				return true;
			}
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public ResponseModel<Void> setMasterFollowed(MasterFollowedDto follow) {
		try {
			String sql = "insert into membermaster (masterID, memberID) values(:masterID, :memberID)";
			SqlParameterSource ps = new BeanPropertySqlParameterSource(follow);
			int count = namedJdbcTemplate.update(sql, ps);
			if (count != 0) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<>(500, ApiError.MASTER_FOLLOWED_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deleteMasterFollowed(MasterFollowedDto follow) {
		try {
			String sql = "delete from membermaster where masterID = :masterID and memberID = :memberID ";
			SqlParameterSource ps = new BeanPropertySqlParameterSource(follow);
			int count = namedJdbcTemplate.update(sql, ps);
			if (count != 0) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<>(500, ApiError.MASTER_DISFOLLOWED_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> updateMasterFollowed(String masterID, Boolean wannaFollow) {
		try {
			String sql = "";
			if (wannaFollow == true) {
				sql = "update atenore.master set follow = IFNULL(follow, 0) + 1 where masterID = ? ";
			} else {
				sql = "update atenore.master set follow = IF(follow>1,IFNULL(follow, 0) - 1 ,0) where masterID = ? ";
			}
			int count = jdbcTemplate.update(sql, new Object[] { masterID });
			if (count != 0) {
				return new ResponseModel<Void>(204, "");
			} else {
				return new ResponseModel<>(409, ApiError.MASTER_FOLLOWED_UPDATE_FAILED);
			}
		} catch (DataAccessException e) {
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> setMaster(MasterCreateDto master) {
		try {
			String sql = "insert into master(masterID, typeID, publicName, lastName, firstName, img, objectKey, "
					+ " lineID, facebook, website, mail, phone, county, address, lat, lng, company, alive, intro, death, legend, insertTime, memberID) "
					+ " values(:masterID, :typeID, :publicName, :lastName, :firstName, :img, :objectKey, "
					+ " :lineID, :facebook, :website, :mail, :phone, :county, :address, :lat, :lng, :company, :alive, :intro, :death, :legend, now(), :memberID) ";
			SqlParameterSource ps = new BeanPropertySqlParameterSource(master);
			int count = namedJdbcTemplate.update(sql, ps);
			if (count != 0) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<Void>(409, ApiError.MASTER_INFO_CREATION_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> setMaster(List<MasterCreateDto> list) {
		try {
			String sql = "insert into master(masterID, typeID, publicName, lastName, firstName, img, objectKey, "
					+ " lineID, facebook, website, mail, phone, county, address, lat, lng, company, alive, intro, death, legend, memberID, insertTime) "
					+ " values(:masterID, :typeID, :publicName, :lastName, :firstName, :img, :objectKey, "
					+ " :lineID, :facebook, :website, :mail, :phone, :county, :address, :lat, :lng, :company, :alive, :intro, :death, :legend, :memberID, now())";

			List<SqlParameterSource> psList = new ArrayList<SqlParameterSource>();
			for (MasterCreateDto dto : list) {
				psList.add(new BeanPropertySqlParameterSource(dto));
			}
			SqlParameterSource[] psArry = new SqlParameterSource[psList.size()];
			psList.toArray(psArry);
			int[] countList = namedJdbcTemplate.batchUpdate(sql, psArry);
			if (this.resultIfBatchUpdateSuccess(countList)) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<Void>(201, "Some of " + ApiError.MASTER_INFO_CREATION_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> setMasterTag(List<MasterTag> list) {
		try {
			String sql = "insert into mastertag (uuid, masterID, tag) values(:tagID, :masterID, :tag)";

			List<SqlParameterSource> psList = new ArrayList<SqlParameterSource>();
			for (MasterTag bean : list) {
				System.out.println(bean.getMasterID());
				psList.add(new BeanPropertySqlParameterSource(bean));
			}
			SqlParameterSource[] psArry = new SqlParameterSource[psList.size()];
			psList.toArray(psArry);
			int[] countList = namedJdbcTemplate.batchUpdate(sql, psArry);
			if (this.resultIfBatchUpdateSuccess(countList)) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<Void>(201, "Some of " + ApiError.MASTER_TAG_CREATION_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public ResponseModel<Void> setMasterTagEdit(List<MasterTagEditDto> list) {
		try {
			String sql = "insert into mastertagedit (uuid, editID, tag) values(:uuid, :editID, :tag)";

			List<SqlParameterSource> psList = new ArrayList<SqlParameterSource>();
			for (MasterTagEditDto dto : list) {
				psList.add(new BeanPropertySqlParameterSource(dto));
			}
			SqlParameterSource[] psArry = new SqlParameterSource[psList.size()];
			psList.toArray(psArry);
			int[] countList = namedJdbcTemplate.batchUpdate(sql, psArry);
			if (this.resultIfBatchUpdateSuccess(countList)) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<Void>(201, "Some of " + ApiError.MASTER_TAG_CREATION_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public ResponseModel<Void> setMasterEdit(MasterCreateDto master) {
		System.out.println(master.getMasterID());
		System.out.println(master.getEditID());
		try {
			String sql = "insert into masteredit(editID, masterID, typeID, publicName, lastName, firstName, img, objectKey, "
					+ " lineID, facebook, website, mail, phone, county, address, lat, lng, company, alive, intro, death, legend, insertTime, memberID, inUsed) "
					+ " values(:editID, :masterID, :typeID, :publicName, :lastName, :firstName, :img, :objectKey, "
					+ " :lineID, :facebook, :website, :mail, :phone, :county, :address, :lat, :lng, :company, :alive, :intro, :death, :legend, now(), :memberID, 'true') ";
			SqlParameterSource ps = new BeanPropertySqlParameterSource(master);
			int count = namedJdbcTemplate.update(sql, ps);
			if (count != 0) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<Void>(409, ApiError.MASTER_INFO_CREATION_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> setMasterEdit(List<MasterCreateDto> list) {
		try {
			String sql = "insert into masteredit(editID, masterID, typeID, publicName, lastName, firstName, img, objectKey, "
					+ " lineID, facebook, website, mail, phone, county, address, lat, lng, company, alive, intro, death, legend, memberID, insertTime, inUsed) "
					+ " values(:editID, :masterID, :typeID, :publicName, :lastName, :firstName, :img, :objectKey, "
					+ " :lineID, :facebook, :website, :mail, :phone, :county, :address, :lat, :lng, :company, :alive, :intro, :death, :legend, :memberID, now(), 'true')";

			List<SqlParameterSource> psList = new ArrayList<SqlParameterSource>();
			for (MasterCreateDto dto : list) {
				psList.add(new BeanPropertySqlParameterSource(dto));
			}
			SqlParameterSource[] psArry = new SqlParameterSource[psList.size()];
			psList.toArray(psArry);
			int[] countList = namedJdbcTemplate.batchUpdate(sql, psArry);
			if (this.resultIfBatchUpdateSuccess(countList)) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<Void>(201, "Some of " + ApiError.MASTER_INFO_CREATION_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> setMasterEdit(MasterUpdateDto master) {
		try {
			String sql = "insert into masteredit(editID, masterID, typeID, publicName, lastName, firstName, img, objectKey, "
					+ " lineID, facebook, website, mail, phone, county, address, lat, lng, company, alive, intro, death, legend, insertTime, memberID, inUsed) "
					+ " values(:editID, :masterID, :typeID, :publicName, :lastName, :firstName, :img, :objectKey, "
					+ " :lineID, :facebook, :website, :mail, :phone, :county, :address, :lat, :lng, :company, :alive, :intro, :death, :legend, now(), :memberID, 'true') ";
			SqlParameterSource ps = new BeanPropertySqlParameterSource(master);
			int count = namedJdbcTemplate.update(sql, ps);
			if (count != 0) {
				return new ResponseModel<Void>(201, "");
			} else {
				return new ResponseModel<Void>(409, ApiError.MASTER_INFO_CREATION_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> updateMaster(MasterUpdateDto master) {
		try {
			String sql = "update master set typeID = :typeID, publicName = :publicName, lastName = :lastName, firstName = :firstName, img = :img, objectKey = :objectKey, "
					+ " lineID = :lineID, facebook = :facebook, website = :website, mail = :mail, phone = :phone, county = :county, address = :address, lat = :lat, lng = :lng, company = :company, alive = :alive, intro = :intro, death = :death, legend = :legend, insertTime = now(), memberID = :memberID "
					+ " where masterID = :masterID";
			SqlParameterSource ps = new BeanPropertySqlParameterSource(master);
			int count = namedJdbcTemplate.update(sql, ps);
			if (count != 0) {
				return new ResponseModel<Void>(204, "");
			} else {
				return new ResponseModel<Void>(409, ApiError.MASTER_INFO_UPDATE_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> updateMaster(MasterEditDto master) {
		try {
			String sql = "update master set typeID = :typeID, publicName = :publicName, lastName = :lastName, firstName = :firstName, img = :img, objectKey = :objectKey, "
					+ " lineID = :lineID, facebook = :facebook, website = :website, mail = :mail, phone = :phone, county = :county, address = :address, lat = :lat, lng = :lng, company = :company, alive = :alive, intro = :intro, death = :death, legend = :legend, insertTime = now(), memberID = :memberID "
					+ " where masterID = :masterID";
			SqlParameterSource ps = new BeanPropertySqlParameterSource(master);
			int count = namedJdbcTemplate.update(sql, ps);
			if (count != 0) {
				return new ResponseModel<Void>(204, "");
			} else {
				return new ResponseModel<Void>(409, ApiError.MASTER_INFO_UPDATE_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public MasterUpdateDto getMasterOriginalByMasterID(String masterID) {
		try {
			String sql = "select m.* from master m where m.masterID = ? ";
			return jdbcTemplate.queryForObject(sql, new Object[] { masterID },
					new BeanPropertyRowMapper<MasterUpdateDto>(MasterUpdateDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public ResponseModel<Void> deleteMasterTagByMasterID(String masterID) {
		try {
			String sql = "delete from mastertag where masterID = ? ";

			jdbcTemplate.update(sql, new Object[] { masterID });
			return new ResponseModel<Void>(204, "");
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deleteMasterTagByMasterID(List<String> masterIDList) {
		try {
			String sql = "delete from mastertag where masterID in (:masterIDList)";
			Map<String, List<String>> deleteUuidParam = Collections.singletonMap("masterIDList", masterIDList);
			int count = namedJdbcTemplate.update(sql, deleteUuidParam);
			if (count != 0) {
				return new ResponseModel<Void>(204, "");
			} else {
				return new ResponseModel<Void>(409, ApiError.MASTER_TAG_DELETION_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public List<MasterEditSearchInfoAdminDto> getMaserEditListByMasterID(MasterEditSearchArgsAdminDto argsDto) {
		try {
			StringBuilder sql = new StringBuilder("select me.editID, m.account, me.inUsed, me.insertTime from masteredit me "
					+ " left join atenore.member m on m.memberID = me.memberID " + " where me.masterID = :masterID ");

			if (argsDto.getAccount() != null && !argsDto.getAccount().contains("後台")) {
				sql.append(" and m.account like '%' :account '%' ");
			} else if (argsDto.getAccount() != null && argsDto.getAccount().contains("後台")) {
				sql.append(" and me.memberID like '%' :account '%' ");
			}

			if (argsDto.getStartTime() != null) {
				sql.append(" and  :startTime <= me.insertTime ");
			}

			if (argsDto.getEndTime() != null) {
				sql.append(" and  :endTime >= me.insertTime ");
			}

			sql.append(" order by me.insertTime desc");

//			if(argsDto.getInsertTime()!=null) {
//				sql.append(" and me.insertTime = :insertTime ");
//			}

			SqlParameterSource ps = new BeanPropertySqlParameterSource(argsDto);
			return namedJdbcTemplate.query(sql.toString(), ps,
					new BeanPropertyRowMapper<MasterEditSearchInfoAdminDto>(MasterEditSearchInfoAdminDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<MasterEditSearchInfoMemberDto> getMasterMemberEditedListWithFilter(MasterSearchArgsMemberEditedDto argsDto) {
		try {

			StringBuilder sql = new StringBuilder(
					"select me.masterID, me.editID, me.memberID, me.publicName, me.img, me.inUsed, me.insertTime "
							+ " from masteredit me left join mastertype mt on mt.typeID = me.typeID "
							+ " left join mastermidtype mit on mit.midID = mt.midID "
							+ " left join mastermaintype mmt on mmt.mainID = mit.mainID where me.memberID = :memberID ");

			if (argsDto.getCounty() != null) {
				sql.append(" and me.county = :county ");
			}
			if (argsDto.getMainID() != null) {
				sql.append(" and mmt.mainID = :mainID ");
				if (argsDto.getMidID() != null) {
					sql.append(" and mit.midID = :midID ");
				}
				if (argsDto.getTypeID() != null) {
					sql.append(" and me.typeID = :typeID ");
				}
			}
			sql.append(" order by me.insertTime desc");

			SqlParameterSource ps = new BeanPropertySqlParameterSource(argsDto);
			return namedJdbcTemplate.query(sql.toString(), ps,
					new BeanPropertyRowMapper<MasterEditSearchInfoMemberDto>(MasterEditSearchInfoMemberDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public MasterEditDto getMasterEditByEditID(String editID) {
		try {
			String sql = "select me.*, m.account, mmt.mainID, mmt.name as mainTypeName, mt.name as typeName, mit.midID as middleID, mit.name as middleTypeName "
					+ " from masteredit me left join mastertype mt on mt.typeID = me.typeID "
					+ " left join mastermidtype mit on mit.midID = mt.midID "
					+ " left join mastermaintype mmt on mmt.mainID = mit.mainID "
					+ " left join atenore.member m on m.memberID = me.memberID where me.editID = ? ";
			return jdbcTemplate.queryForObject(sql, new Object[] { editID },
					new BeanPropertyRowMapper<MasterEditDto>(MasterEditDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public MasterEditDto getMasterEditByEditID(String editID, String memberID) {
		try {
			String sql = "select me.*, mmt.mainID, mmt.name as mainTypeName, mt.name as typeName, mit.midID as middleID, mit.name as middleTypeName "
					+ " from masteredit me left join mastertype mt on mt.typeID = me.typeID "
					+ " left join mastermidtype mit on mit.midID = mt.midID "
					+ " left join mastermaintype mmt on mmt.mainID = mit.mainID where me.editID = ? and me.memberID = ?";
			return jdbcTemplate.queryForObject(sql, new Object[] { editID, memberID },
					new BeanPropertyRowMapper<MasterEditDto>(MasterEditDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public List<MasterTagEditDto> getMasterTagEditListByEditID(String editID) {
		try {
			String sql = "select * from mastertagedit me " + " where me.editID = ? ";
			return jdbcTemplate.query(sql, new Object[] { editID },
					new BeanPropertyRowMapper<MasterTagEditDto>(MasterTagEditDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public ResponseModel<Void> updateMasterEditAllNotInUsed(String masterID) {
		try {
			StringBuilder sql = new StringBuilder("update masteredit set inUsed = 'false' where masterID = ? ; ");
			int count = jdbcTemplate.update(sql.toString(), new Object[] { masterID });
			if (count != 0) {
				return new ResponseModel<>(204, "");
			} else {
				return new ResponseModel<>(409, ApiError.MASTER_EDIT_APPLY_INUSED_FAILED);
			}
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> updateMasterEditAllNotInUsed(List<String> masterIDList) {
		try {
			String sql = "update masteredit set inUsed = 'false' where masterID in (:masterIDList)";
			Map<String, List<String>> deleteUuidParam = Collections.singletonMap("masterIDList", masterIDList);
			int count = namedJdbcTemplate.update(sql, deleteUuidParam);
			if (count != 0) {
				return new ResponseModel<Void>(204, "");
			} else {
				return new ResponseModel<Void>(409, ApiError.MASTER_EDIT_APPLY_INUSED_FAILED);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> updateMasterEditInUsed(String editID) {
		try {
			StringBuilder sql = new StringBuilder("update masteredit set inUsed = 'true' where editID = ? ; ");
			int count = jdbcTemplate.update(sql.toString(), new Object[] { editID });
			if (count != 0) {
				return new ResponseModel<>(204, "");
			} else {
				return new ResponseModel<>(409, ApiError.MASTER_EDIT_APPLY_INUSED_FAILED);
			}
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public List<String> getMasterEditIDListByMasterID(String masterID) {
		try {
			String sql = "select editID from masteredit where masterID = ?";
			return jdbcTemplate.queryForList(sql, new Object[] { masterID }, String.class);
		} catch (DataAccessException e) {
			return null;
		}
	}

	@Override
	public ResponseModel<Void> deleteMasterTagEditByEditIDList(List<String> editIDList) {
		try {
			String sql = "delete from mastertagedit where editID in (:editIDList)";
			Map<String, List<String>> deleteUuidParam = Collections.singletonMap("editIDList", editIDList);
			namedJdbcTemplate.update(sql, deleteUuidParam);
			return new ResponseModel<Void>(204, "");
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deleteMasterEditByMasterID(String masterID) {
		try {
			String sql = "delete from masteredit where masterID = ? ";
			jdbcTemplate.update(sql, new Object[] { masterID });
			return new ResponseModel<Void>(204, "");
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deleteMasterMemberFollowedByMasterID(String masterID) {
		try {
			String sql = "delete from membermaster where masterID = ? ";
			jdbcTemplate.update(sql, new Object[] { masterID });
			return new ResponseModel<Void>(204, "");
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deleteEventRecommendByMasterID(String masterID) {
		try {
			String sql = "delete from eventrecommend where masterID = ? ";
			jdbcTemplate.update(sql, new Object[] { masterID });
			return new ResponseModel<Void>(204, "");
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deletePlaceRecommendByMasterID(String masterID) {
		try {
			String sql = "delete from placerecommend where masterID = ? ";
			jdbcTemplate.update(sql, new Object[] { masterID });
			return new ResponseModel<Void>(204, "");
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deletePlaceRecommendEditByMasterID(String masterID) {
		try {
			String sql = "delete from placerecommendedit where masterID = ? ";
			jdbcTemplate.update(sql, new Object[] { masterID });
			return new ResponseModel<Void>(204, "");
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deleteMasterByMasterID(String masterID) {
		try {
			String sql = "delete from master where masterID = ? ";
			int count = jdbcTemplate.update(sql, new Object[] { masterID });
			if (count != 0) {
				return new ResponseModel<Void>(204, "");
			} else {
				return new ResponseModel<>(409, ApiError.MASTER_DELETION_FAILED);
			}
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deleteMasterEditByEditID(String editID) {
		try {
			String sql = "delete from masteredit where editID = ? ";
			int count = jdbcTemplate.update(sql, new Object[] { editID });
			if (count != 0) {
				return new ResponseModel<Void>(204, "");
			} else {
				return new ResponseModel<>(409, ApiError.MASTER_EDIT_DELETION_FAILED);
			}
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> deleteMasterTagEditByEditID(String editID) {
		try {
			String sql = "delete from mastertagedit where editID = ? ";
			jdbcTemplate.update(sql, new Object[] { editID });
			return new ResponseModel<Void>(204, "");
		} catch (DataAccessException e) {
			return new ResponseModel<>(500, e.getMessage());
		}
	}

	@Override
	public ResponseModel<Void> updateMasterClick(String masterID) {
		try {
			String sql = "update atenore.master set click = IFNULL(click, 0) + 1 where masterID = ? ";
			int count = jdbcTemplate.update(sql, new Object[] { masterID });
			if (count != 0) {
				return new ResponseModel<Void>(204, "");
			} else {
				return new ResponseModel<>(409, ApiError.MASTER_CLICK_UPDATE_FAILED);
			}
		} catch (DataAccessException e) {
			return new ResponseModel<Void>(500, e.getMessage());
		}
	}

	/* ADMIN */

	@Override
	public List<MasterSearchInfoAdminDto> getMasterListWithFilter(MasterSearchArgsAdminDto argsDto) {
		try {

			StringBuilder sql = new StringBuilder(
					"select m.masterID , mmt.mainID, mmt.name as 'mainTypeName', m.lastname, m.firstname, m.phone, m.alive, m.memberID "
							+ "       			from master m "
							+ "					left join mastertype mt on mt.typeId = m.typeID\r\n"
							+ "					left join mastermidtype mit on mit.midID = mt.midID \r\n"
							+ "					left join mastermaintype mmt on mmt.mainID = mit.mainID \r\n"
							+ "					where 1=1  ");

			if (argsDto.getLastName() != null) {
				sql.append(" and m.lastname = :lastName ");
			}

			if (argsDto.getFirstName() != null) {
				sql.append(" and m.firstname = :firstName ");
			}

			if (argsDto.getAlive() != null) {
				sql.append(" and m.alive = :alive ");
			}

			if (argsDto.getMainID() != null) {
				sql.append(" and mmt.mainID = :mainID ");
			}

			sql.append(" order by m.insertTime ");

			SqlParameterSource ps = new BeanPropertySqlParameterSource(argsDto);
			return namedJdbcTemplate.query(sql.toString(), ps,
					new BeanPropertyRowMapper<MasterSearchInfoAdminDto>(MasterSearchInfoAdminDto.class));
		} catch (DataAccessException e) {
			return null;
		}
	}

	private String getSearchPreferColumnSQLBySN(Integer searchPreferSN) {
		return this.searchPreferColumnSQL[searchPreferSN];
	}

	private String getSearchPreferSQLBySN(Integer searchPreferSN) {
		return this.searchPreferSQL[searchPreferSN];
	}

	private String getSearchOrderSQLBySN(Integer searchPreferSN) {
		return this.searchOrderSQL[searchPreferSN];
	}

	private Boolean resultIfBatchUpdateSuccess(int[] countList) {
		Boolean flag = true;
		for (int count : countList) {
			if (count == 0) {
				flag = false;
				break;
			}
		}
		return flag;
	}

}
