package tw.com.reinbach.Atenore.Model.admin.query;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import tw.com.reinbach.Atenore.Model.admin.component.LimitDto;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class LimitListInfoDto {

	private List<LimitDto> limitList;

	public List<LimitDto> getLimitList() {
		return limitList;
	}

	public void setLimitList(List<LimitDto> limitList) {
		this.limitList = limitList;
	}
	
}
