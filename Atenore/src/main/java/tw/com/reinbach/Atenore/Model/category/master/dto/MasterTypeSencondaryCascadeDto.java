package tw.com.reinbach.Atenore.Model.category.master.dto;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class MasterTypeSencondaryCascadeDto {

	@ApiModelProperty(example = "1")
	private Integer mainID;
	
	@ApiModelProperty(example = "醫療")
	private String name;
	
	private List<MasterTypePrimaryCascadeDto> middleTypeList;

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<MasterTypePrimaryCascadeDto> getMiddleTypeList() {
		return middleTypeList;
	}

	public void setMiddleTypeList(List<MasterTypePrimaryCascadeDto> middleTypeList) {
		this.middleTypeList = middleTypeList;
	}
	
	public void addMinorTypeList(MasterTypePrimaryCascadeDto masterTypePrimaryCascadeDto){
		this.middleTypeList.add(masterTypePrimaryCascadeDto);
	}
	
}
