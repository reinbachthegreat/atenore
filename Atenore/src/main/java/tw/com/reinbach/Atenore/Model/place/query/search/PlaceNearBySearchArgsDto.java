package tw.com.reinbach.Atenore.Model.place.query.search;

import java.math.BigDecimal;

import javax.validation.constraints.NotEmpty;

import io.swagger.annotations.ApiModelProperty;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;

public class PlaceNearBySearchArgsDto {

	@ApiModelProperty(hidden = true)
	private String memberID;

	@ApiModelProperty(value = "user's lat", example = "25.0639368", required = true)
	private BigDecimal lat;

	@ApiModelProperty(value = "user's lng", example = "121.5471534", required = true)
	private BigDecimal lng;

	@NotEmpty(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@ApiModelProperty(required = true)
	private PlaceSearchPreferArgDto searchPreferArg;
	
	@ApiModelProperty(hidden = true)
	private BigDecimal minLat;
	
	@ApiModelProperty(hidden = true)
	private BigDecimal maxLat;
	
	@ApiModelProperty(hidden = true)
	private BigDecimal minLng;
	
	@ApiModelProperty(hidden = true)
	private BigDecimal maxLng;

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public BigDecimal getLat() {
		return lat;
	}

	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}

	public BigDecimal getLng() {
		return lng;
	}

	public void setLng(BigDecimal lng) {
		this.lng = lng;
	}

	public PlaceSearchPreferArgDto getSearchPreferArg() {
		return searchPreferArg;
	}

	public void setSearchPreferArg(PlaceSearchPreferArgDto searchPreferArg) {
		this.searchPreferArg = searchPreferArg;
	}

	public BigDecimal getMinLat() {
		return minLat;
	}

	public void setMinLat(BigDecimal minLat) {
		this.minLat = minLat;
	}

	public BigDecimal getMaxLat() {
		return maxLat;
	}

	public void setMaxLat(BigDecimal maxLat) {
		this.maxLat = maxLat;
	}

	public BigDecimal getMinLng() {
		return minLng;
	}

	public void setMinLng(BigDecimal minLng) {
		this.minLng = minLng;
	}

	public BigDecimal getMaxLng() {
		return maxLng;
	}

	public void setMaxLng(BigDecimal maxLng) {
		this.maxLng = maxLng;
	}

}
