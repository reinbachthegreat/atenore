package tw.com.reinbach.Atenore.Model.place;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.medialive.model.ArchiveGroupSettings;
import com.google.maps.model.LatLng;

import tw.com.reinbach.Atenore.Model.place.component.PinPlaceTypeInfoDto;
import tw.com.reinbach.Atenore.Model.place.component.PlaceDto;
import tw.com.reinbach.Atenore.Model.place.component.PlaceImageDto;
import tw.com.reinbach.Atenore.Model.place.component.PlaceLiaisonDto;
import tw.com.reinbach.Atenore.Model.place.component.PlaceTag;
import tw.com.reinbach.Atenore.Model.place.create.PlaceCreateBatchResultInfoDto;
import tw.com.reinbach.Atenore.Model.place.create.PlaceCreateDto;
import tw.com.reinbach.Atenore.Model.place.create.PlaceLiaisonCreateDto;
import tw.com.reinbach.Atenore.Model.place.query.PlaceInfoDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceEditSearchArgsAdminDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceEditSearchInfoAdminDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceEditSearchInfoMemberDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceEditSearchListInfoAdminDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceNearBySearchArgsDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceSearchArgsAdminDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceSearchArgsDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceSearchArgsMemberEditedDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceSearchInfoDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceSearchListInfoAdminDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceSearchPreferArgDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceSearchSimpleInfoDto;
import tw.com.reinbach.Atenore.Model.place.update.PinPlaceTypeUpdateDto;
import tw.com.reinbach.Atenore.Model.place.update.PlaceFollowedDto;
import tw.com.reinbach.Atenore.Model.place.update.PlaceUpdateDto;
import tw.com.reinbach.Atenore.Model.search.SearchOrderDto;
import tw.com.reinbach.Atenore.Model.search.SearchPreferDto;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;
import tw.com.reinbach.Atenore.Utility.UUIDGenerator;
import tw.com.reinbach.Atenore.Utility.Utility;
import tw.com.reinbach.Atenore.Utility.aws.AmazonDto;
import tw.com.reinbach.Atenore.Utility.aws.AmazonService;
import tw.com.reinbach.Atenore.Utility.google.GpsService;

@Service
public class PlaceService {

	@Autowired
	private PlaceDAO placeDAO;

	@Autowired
	private AmazonService amazonService;

	@Autowired
	private GpsService gpsService;

	@Autowired
	private Utility utility;

	@Autowired
	private PlatformTransactionManager transactionManager;

	@Value("${place.update.excel.template.from}")
	private String updateExcelTemplateUrl;

	@Value("${place.update.excel.export.aws.to}")
	private String updateExcelFolder;

	private String[] capacity = { "1-10人", "11-100人", "101-500人", "501-1000人" };

	private String[] searchPrefer = { "全部", "熱門" };

	private String[][] searchOrder = { { "距離近到遠", "以點閱率高到低", "以收藏人數高到低" }, { "距離近到遠", "以點閱率高到低", "以收藏人數高到低" } };

	/**
	 * 取得場地的容納人數範圍
	 * 
	 * @return 場地的容納人數範圍
	 */
	public List<String> getCapacity() {
		List<String> list = new ArrayList<String>(Arrays.asList(this.capacity));
		return list;
	}

	/**
	 * 取得活動清單：PlaceSearchArgsDto
	 * 
	 * @param argsDto
	 * @return
	 */
	public List<PlaceSearchInfoDto> getPlaceListWithFilter(PlaceSearchArgsDto argsDto) {
		PlaceSearchPreferArgDto prefer = argsDto.getSearchPreferArg();
		Integer preferSN = prefer.getSearchPreferSN();
		Integer orderSN = prefer.getSearchOrderSN();

		List<PlaceSearchInfoDto> list = placeDAO.getPlaceListWithFilter(argsDto);
		if (list != null) {
			int index = 0;
			for (PlaceSearchInfoDto place : list) {
				if (place.getLat() != null && place.getLng() != null) {
					place = this.calculator(argsDto.getLat(), argsDto.getLng(), place);
					list.set(index, place);
				}
				index++;
			}

			switch (preferSN) {
//			All, default not selected.
			case 0:
				switch (orderSN) {
				// distance order by ASC.
				case 0:
					Collections.sort(list,
							Comparator.comparing(PlaceSearchInfoDto::getDistance, Comparator.nullsLast(Comparator.naturalOrder())));
					break;
				// click order by DESC.
				case 1:
					Collections.sort(list,
							Comparator.comparing(PlaceSearchInfoDto::getClick, Comparator.nullsLast(Comparator.reverseOrder())));
					break;
				// follow order by DESC.
				case 2:
					Collections.sort(list,
							Comparator.comparing(PlaceSearchInfoDto::getFollow, Comparator.nullsLast(Comparator.reverseOrder())));
					break;
				}
				break;

//			Hottest selected.
			case 1:
				switch (orderSN) {
				// distance order by ASC.
				case 0:
					Collections.sort(list,
							Comparator.comparing(PlaceSearchInfoDto::getDistance, Comparator.nullsLast(Comparator.naturalOrder())));
					break;
				// click order by DESC.
				case 1:
					Collections.sort(list,
							Comparator.comparing(PlaceSearchInfoDto::getClick, Comparator.nullsLast(Comparator.reverseOrder())));
					break;
				// follow order by DESC.
				case 2:
					Collections.sort(list,
							Comparator.comparing(PlaceSearchInfoDto::getFollow, Comparator.nullsLast(Comparator.reverseOrder())));
					break;
				}
				break;
			}

		}
		return list;
	}

	/**
	 * Get place list at GIS location near by
	 * 
	 * @param argsDto
	 * @return
	 */
	public List<PlaceSearchInfoDto> getPlaceListNearBy(PlaceNearBySearchArgsDto argsDto) {
		argsDto.setMinLat(argsDto.getLat().subtract(new BigDecimal(0.001)));
		argsDto.setMinLng(argsDto.getLng().subtract(new BigDecimal(0.001)));
		argsDto.setMaxLat(argsDto.getLat().add(new BigDecimal(0.001)));
		argsDto.setMaxLng(argsDto.getLng().add(new BigDecimal(0.001)));

		PlaceSearchPreferArgDto prefer = argsDto.getSearchPreferArg();
		Integer preferSN = prefer.getSearchPreferSN();
		Integer orderSN = prefer.getSearchOrderSN();

		List<PlaceSearchInfoDto> list = placeDAO.getPlaceListNearBy(argsDto);
		if (list != null) {
			int index = 0;
			for (PlaceSearchInfoDto place : list) {
				if (place.getLat() != null && place.getLng() != null) {
					place = this.calculator(argsDto.getLat(), argsDto.getLng(), place);
					list.set(index, place);
				}
				index++;
			}

			switch (preferSN) {
//			All, default not selected.
			case 0:
				switch (orderSN) {
				// distance order by ASC.
				case 0:
					Collections.sort(list,
							Comparator.comparing(PlaceSearchInfoDto::getDistance, Comparator.nullsLast(Comparator.naturalOrder())));
					break;
				// click order by DESC.
				case 1:
					Collections.sort(list,
							Comparator.comparing(PlaceSearchInfoDto::getClick, Comparator.nullsLast(Comparator.reverseOrder())));
					break;
				// follow order by DESC.
				case 2:
					Collections.sort(list,
							Comparator.comparing(PlaceSearchInfoDto::getFollow, Comparator.nullsLast(Comparator.reverseOrder())));
					break;
				}
				break;

//			Hottest selected.
			case 1:
				switch (orderSN) {
				// distance order by ASC.
				case 0:
					Collections.sort(list,
							Comparator.comparing(PlaceSearchInfoDto::getDistance, Comparator.nullsLast(Comparator.naturalOrder())));
					break;
				// click order by DESC.
				case 1:
					Collections.sort(list,
							Comparator.comparing(PlaceSearchInfoDto::getClick, Comparator.nullsLast(Comparator.reverseOrder())));
					break;
				// follow order by DESC.
				case 2:
					Collections.sort(list,
							Comparator.comparing(PlaceSearchInfoDto::getFollow, Comparator.nullsLast(Comparator.reverseOrder())));
					break;
				}
				break;
			}

		}
		return list;
	}

//	/**
//	 * 取得場所清單：篩選器
//	 * 
//	 * @param placeFilter
//	 * @param county
//	 * @return
//	 */
//	public List<PlaceDto> getPlaceListWithFilter(String placeFilter, String county) {
//		if (placeFilter != null || county != null) {
//			return placeDAO.getPlaceListWithFilter(placeFilter, county);
//		} else {
//			return placeDAO.getPlaceList();
//		}
//	}

	/**
	 * Get place list with PlaceSearchArgsAdminDto filter
	 * 
	 * @param argsDto
	 * @return
	 */
	public PlaceSearchListInfoAdminDto getPlaceListWithFilter(PlaceSearchArgsAdminDto argsDto) {
		PlaceSearchListInfoAdminDto dto = new PlaceSearchListInfoAdminDto();
		dto.setPlaceList(placeDAO.getPlaceListWithFilter(argsDto));
		return dto;
	}

	/**
	 * Get place searching prefer list
	 * 
	 * @return
	 */
	public List<SearchPreferDto> getPlaceSearcingPreferList() {
		int preferIndex = 0;
		List<SearchPreferDto> preferList = new ArrayList<SearchPreferDto>();
		for (String p : searchPrefer) {
			SearchPreferDto prefer = new SearchPreferDto();
			prefer.setSearchPreferSN(preferIndex);
			prefer.setSearchPreferName(p);
			List<SearchOrderDto> orderList = new ArrayList<SearchOrderDto>();
			int orderIndex = 0;
			for (String o : searchOrder[preferIndex]) {
				SearchOrderDto order = new SearchOrderDto();
				order.setSearchOrderSN(orderIndex);
				order.setSearchOrderName(o);
				orderList.add(order);
				orderIndex++;
			}
			prefer.setSearchOrderList(orderList);
			preferList.add(prefer);
			preferIndex++;
		}
		return preferList;
	}

	/**
	 * 取得場所現行版本資訊
	 * 
	 * @param placeID
	 * @return
	 */
	public PlaceDto getPlace(String placeID, String memberID) {
		PlaceDto place = new PlaceDto();
		placeDAO.updatePlaceClick(placeID);
		if (memberID != null && memberID.trim().length() != 0) {
			place = placeDAO.getPlaceWithMemberID(placeID, memberID);
		} else {
			place = placeDAO.getPlace(placeID);
		}
		List<PlaceLiaisonDto> placeLiaison = this.getPlaceLiaison(placeID);
		if (placeLiaison != null && placeLiaison.size() > 0) {
			place.setLiaison(placeLiaison);
		}
		return place;
	}

	/**
	 * Get place fully information by placeID
	 * 
	 * @param placeID
	 * @return
	 */
	public PlaceInfoDto getPlaceFullyByPlaceID(String placeID) {
		PlaceDto place = placeDAO.getPlace(placeID);
		if (place != null) {
			List<PlaceTag> tagList = placeDAO.getPlaceTag(placeID);
			List<PlaceImageDto> imageList = placeDAO.getPlaceImage(placeID);
			PlaceInfoDto dto = new PlaceInfoDto();
			List<PlaceLiaisonDto> liaison = placeDAO.getPlaceLiaison(placeID);
			if (place.getMax() != null) {
				place.setMaxName(capacity[place.getMax()]);
			}
			place.setLiaison(liaison);
			dto.setPlaceInfo(place);
			dto.setPlaceTagList(tagList);
			dto.setPlaceImgList(imageList);
			return dto;
		} else {
			return null;
		}
	}

	/**
	 * 取得場所現行版本Tag
	 * 
	 * @param placeID
	 * @return
	 */
	public List<PlaceTag> getPlaceTag(String placeID) {
		return placeDAO.getPlaceTag(placeID);
	}

	/**
	 * 取得場所現行版本Tag
	 * 
	 * @param placeID
	 * @return
	 */
	public List<PlaceTag> getPlaceTagWithoutPlaceID(String placeID) {
		return placeDAO.getPlaceTagWithoutPlaceID(placeID);
	}

	/**
	 * 取得場所現行版本聯絡人資訊
	 * 
	 * @param placeID
	 * @return
	 */
	public List<PlaceLiaisonDto> getPlaceLiaison(String placeID) {
		return placeDAO.getPlaceLiaison(placeID);
	}

	/**
	 * 取得場所現行版本圖片
	 * 
	 * @param placeID
	 * @return
	 */
	public List<PlaceImageDto> getPlaceImage(String placeID) {
		return placeDAO.getPlaceImage(placeID);
	}

	/**
	 * 取得場所資訊歷史編輯紀錄清單
	 * 
	 * @param placeID
	 * @return
	 */
	public List<PlaceDto> getPlaceEditList(String placeID) {
		return placeDAO.getPlaceEditList(placeID);
	}

	/**
	 * 取得場所資訊編輯紀錄
	 * 
	 * @param editID
	 * @return
	 */
	public PlaceDto getPlaceEdit(String editID) {
		return placeDAO.getPlaceEdit(editID);
	}

	/**
	 * 取得場所Tag編輯紀錄
	 * 
	 * @param editID
	 * @return
	 */
	public List<PlaceTag> getPlaceTagEdit(String editID) {
		return placeDAO.getPlaceTagListByEditID(editID);
	}

	/**
	 * 取得場所聯絡人編輯紀錄
	 * 
	 * @param editID
	 * @return
	 */
	public List<PlaceLiaison> getPlaceLiaisonEdit(String editID) {
		return placeDAO.getPlaceLiaisonEdit(editID);
	}

	/**
	 * 新增場所(含照片)
	 * 
	 * @param place
	 * @param liaisonList
	 * @param tagList
	 * @param masterIDList
	 * @param files
	 * @return
	 */
	public ResponseModel<Void> setPlace(PlaceCreateDto place) {

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> resultModel = null;
		List<MultipartFile> files = place.getFiles();
		List<PlaceLiaisonCreateDto> placeLiaisonList = place.getPlaceLiaisonList();
		List<String> tagList = place.getTagList();

		try {
			String placeID = UUIDGenerator.generateType4UUID().toString();
			place.setPlaceID(placeID);
			String editID = UUIDGenerator.generateType4UUID().toString();
			place.setEditID(editID);
			place.setEditable("true");
			place.setInUsed("true");
			// Google Map回傳所在lat, lng
			if (place.getCounty() != null && place.getAddress() != null) {

//				LatLng location = gpsService.getSimpleLocationFromAddress(place.getCounty() + place.getAddress());
//				if (location != null) {
//					place.setLat(BigDecimal.valueOf(location.lat));
//					place.setLng(BigDecimal.valueOf(location.lng));
//				}

				ResponseModel<LatLng> latlng = gpsService.getSimpleLocationFromAddress(place.getCounty(), place.getAddress());
				LatLng location = latlng.getData();
				if (location != null) {
					place.setLat(BigDecimal.valueOf(location.lat));
					place.setLng(BigDecimal.valueOf(location.lng));
				} else {
					resultModel = new ResponseModel<>(latlng.getCode(), latlng.getErrMsg());
					throw new Exception();
				}

			}
			resultModel = placeDAO.setPlaceEdit(place);
			if (resultModel.getCode() != 201) {
				throw new Exception();
			}
			resultModel = placeDAO.setPlace(place);
			if (resultModel.getCode() != 201) {
				throw new Exception();
			}

			if (placeLiaisonList != null && placeLiaisonList.size() != 0) {
				// Set Place Liaison list and insert.
				int liaisonIndex = 0;
				for (PlaceLiaisonCreateDto dto : placeLiaisonList) {
					String uuid = UUIDGenerator.generateType4UUID().toString();
					dto.setLiaisonID(uuid);
					dto.setPlaceID(placeID);
					dto.setEditID(editID);
					if (liaisonIndex == 0) {
						dto.setIsMain("true");
					} else {
						dto.setIsMain("false");
					}
					liaisonIndex++;
				}
				resultModel = placeDAO.setPlaceLiaisonEdit(placeLiaisonList);
				if (resultModel.getCode() != 201) {
					throw new Exception();
				}
				resultModel = placeDAO.setPlaceLiaison(placeLiaisonList);
				if (resultModel.getCode() != 201) {
					throw new Exception();
				}
			}

			if (tagList != null && tagList.size() != 0) {
				// Set Place tag list and insert.
				List<PlaceTag> tagInsertList = new ArrayList<PlaceTag>();
				for (String t : tagList) {
					PlaceTag tag = new PlaceTag();
					tag.setPlaceID(placeID);
					tag.setTagID(UUIDGenerator.generateType4UUID().toString());
					tag.setTag(t);
					tag.setEditID(editID);
					tagInsertList.add(tag);
				}
				resultModel = placeDAO.setPlaceTagEdit(tagInsertList);
				if (resultModel.getCode() != 201) {
					throw new Exception();
				}
				resultModel = placeDAO.setPlaceTag(tagInsertList);
				if (resultModel.getCode() != 201) {
					throw new Exception();
				}
			}

			// Upload place image list to aws, and set place Image Bean then insert it.
			if (files != null && files.size() != 0) {
				amazonService.setFolderName(new StringBuffer().append("Place").append("/").append(placeID).toString());
				try {
					List<AmazonDto> amazonList = amazonService.uploadFile(files);
					List<PlaceImg> imageList = new ArrayList<PlaceImg>();
					int index = 0;
					for (AmazonDto aws : amazonList) {
						PlaceImg img = new PlaceImg();
						img.setPlaceID(placeID);
						img.setImg(aws.getUrl());
						img.setObjectKey(aws.getObjectKey());
						img.setEditID(editID);
						if (index == 0) {
							img.setIsMain("true");
						} else {
							img.setIsMain("false");
						}
						String uuid = UUIDGenerator.generateType4UUID().toString();
						img.setUuid(uuid);
						imageList.add(img);
						index++;
					}
					resultModel = placeDAO.setPlaceImageEdit(imageList);
					if (resultModel.getCode() != 201) {
						throw new Exception();
					}
					resultModel = placeDAO.setPlaceImage(imageList);
					if (resultModel.getCode() != 201) {
						throw new Exception();
					}
				} catch (IOException e) {
					e.printStackTrace();
					throw new Exception();
				}
			}

			transactionManager.commit(status);
			return new ResponseModel<Void>(201, "");
		} catch (Exception e) {
			e.printStackTrace();
			transactionManager.rollback(status);
			return resultModel;
		}
	}

	/**
	 * Place batch creation
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public ResponseModel<PlaceCreateBatchResultInfoDto> setPlaceByImportExcel(MultipartFile file) throws IOException {

		List<PlaceCreateDto> placeImportList = new ArrayList<PlaceCreateDto>();
		List<PlaceLiaisonCreateDto> liaisonList = new ArrayList<PlaceLiaisonCreateDto>();
		XSSFWorkbook workbook = null;
		// Output file read from Template
		XSSFWorkbook outputBook = null;
		File f = null;
		try {
			workbook = new XSSFWorkbook(file.getInputStream());
			URL url = new URL(updateExcelTemplateUrl);
			InputStream in = url.openStream();
			Files.copy(in, Paths.get("place_template.xlsx"), StandardCopyOption.REPLACE_EXISTING);
			f = new File("place_template.xlsx");
			InputStream input = new FileInputStream(f);
			outputBook = new XSSFWorkbook(input);
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseModel<PlaceCreateBatchResultInfoDto>(500, e.getMessage());
		}
		XSSFSheet worksheet = workbook.getSheetAt(0);

		for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {

			PlaceCreateDto place = new PlaceCreateDto();
			XSSFRow row = worksheet.getRow(i);

			String placeID = UUIDGenerator.generateType4UUID().toString();
			String editID = UUIDGenerator.generateType4UUID().toString();
			place.setPlaceID(placeID);
			place.setEditID(editID);
			place.setEditable("true");
			place.setInUsed("true");
			place.setTypeID((int) row.getCell(0).getNumericCellValue());
			if (row.getCell(1) != null && row.getCell(1).getStringCellValue().length() != 0) {
				place.setName(row.getCell(1).getStringCellValue());
			}

			CellStyle cellStyle = outputBook.createCellStyle();
			XSSFFont font = outputBook.createFont();
			font.setFontName("Microsoft JhengHei");
			font.setFontHeight(12);
			cellStyle.setWrapText(true);
			cellStyle.setFont(font);
			cellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

			for (int s = 0; s <= 1; s++) {
				XSSFRow row1 = outputBook.getSheetAt(s).createRow(i);
				XSSFCell cell1 = row1.createCell(0);
				XSSFCell cell2 = row1.createCell(1);
				XSSFCell cell3 = row1.createCell(2);
				cell1.setCellValue(placeID);
				cell2.setCellValue(editID);
				cell3.setCellValue(place.getName());
				cell1.setCellStyle(cellStyle);
				cell2.setCellStyle(cellStyle);
				cell3.setCellStyle(cellStyle);
			}

			for (int k = 2; k <= 17; k++) {
				if (row.getCell(k) != null) {
					if (k != 15) {
						row.getCell(k).setCellType(Cell.CELL_TYPE_STRING);
					} else {
						row.getCell(k).setCellType(Cell.CELL_TYPE_NUMERIC);
					}
				}
			}

			if (row.getCell(2) != null && row.getCell(2).getStringCellValue().length() != 0) {
				place.setCounty(row.getCell(2).getStringCellValue());
			}

			if (row.getCell(3) != null && row.getCell(3).getStringCellValue().length() != 0) {
				place.setAddress(row.getCell(3).getStringCellValue());
			}

			if (row.getCell(10) != null && row.getCell(10).getStringCellValue().length() != 0) {
				place.setMail(row.getCell(10).getStringCellValue());
			}

			if (row.getCell(11) != null && row.getCell(11).getStringCellValue().length() != 0) {
				place.setWebsite(row.getCell(11).getStringCellValue());
			}

			if (row.getCell(12) != null && row.getCell(12).getStringCellValue().length() != 0) {
				place.setFacebook(row.getCell(12).getStringCellValue());
			}

			if (row.getCell(13) != null && row.getCell(13).getStringCellValue().length() != 0) {
				place.setService(row.getCell(13).getStringCellValue());
			}

			if (row.getCell(14) != null && row.getCell(14).getStringCellValue().length() != 0) {
				place.setIntro(row.getCell(14).getStringCellValue());
			}

			if (row.getCell(15) != null) {
				place.setMax((int) row.getCell(15).getNumericCellValue());
			}

			if (row.getCell(16) != null && row.getCell(16).getStringCellValue().length() != 0) {
				place.setArea(row.getCell(16).getStringCellValue());
			}

			if (row.getCell(17) != null && row.getCell(17).getStringCellValue().length() != 0) {
				place.setUnit(row.getCell(17).getStringCellValue());
			}

			place.setMemberID("後台新增");

			// Google Map回傳所在Lat, Lng
			if (place.getCounty() != null && place.getAddress() != null) {
//				LatLng location = gpsService.getSimpleLocationFromAddress(place.getCounty() + place.getAddress());
//				if (location != null) {
//					place.setLat(BigDecimal.valueOf(location.lat));
//					place.setLng(BigDecimal.valueOf(location.lng));
//				}

				ResponseModel<LatLng> latlng = gpsService.getSimpleLocationFromAddress(place.getCounty(), place.getAddress());
				LatLng location = latlng.getData();
				if (location != null) {
					place.setLat(BigDecimal.valueOf(location.lat));
					place.setLng(BigDecimal.valueOf(location.lng));
				} else {
					return new ResponseModel<PlaceCreateBatchResultInfoDto>(latlng.getCode(), latlng.getErrMsg());
				}

			}
			placeImportList.add(place);

			int liaisonIndex = 0;
			for (int c = 4; c <= 9; c += 2) {
				if (row.getCell(c) != null && row.getCell(c).getStringCellValue().length() != 0
						|| row.getCell(c + 1) != null && row.getCell(c + 1).getStringCellValue().length() != 0) {
					PlaceLiaisonCreateDto dto = new PlaceLiaisonCreateDto();
					String uuid = UUIDGenerator.generateType4UUID().toString();
					dto.setLiaisonID(uuid);
					dto.setPlaceID(placeID);
					dto.setEditID(editID);
					dto.setName(row.getCell(c).getStringCellValue());
					dto.setPhone(row.getCell(c + 1).getStringCellValue());
					if (liaisonIndex == 0) {
						dto.setIsMain("true");
					} else {
						dto.setIsMain("false");
					}
					liaisonIndex++;
					liaisonList.add(dto);
				} else {
					continue;
				}
			}

		}

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> placeModel = placeDAO.setPlace(placeImportList);
		ResponseModel<Void> placeModelEdit = placeDAO.setPlaceEdit(placeImportList);
		ResponseModel<Void> liaisonModel = placeDAO.setPlaceLiaison(liaisonList);
		ResponseModel<Void> liaisonModelEdit = placeDAO.setPlaceLiaisonEdit(liaisonList);
		if (placeModel.getCode() == 201 && placeModelEdit.getCode() == 201 && liaisonModel.getCode() == 201
				&& liaisonModelEdit.getCode() == 201) {
			PlaceCreateBatchResultInfoDto dto = new PlaceCreateBatchResultInfoDto();
			try {
				String outputTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss"));
				amazonService.setFolderName(this.updateExcelFolder);
				File outputFile = new File("PlaceBatchUpdate_" + outputTime + ".xlsx");
				outputFile.createNewFile();
				OutputStream o = new FileOutputStream(outputFile);
				outputBook.write(o);
				o.flush();
				o.close();
				outputBook.close();
				workbook.close();
				AmazonDto bean = amazonService.uploadFileNotEncrypted(outputFile);
				dto.setBatchUpdateUrl(bean.getUrl());
				dto.setBatchUpdateObjectKey(bean.getObjectKey());
				f.delete();
				outputFile.delete();
			} catch (IOException e) {
				e.printStackTrace();
				transactionManager.rollback(status);
				return new ResponseModel<PlaceCreateBatchResultInfoDto>(500, e.getMessage());
			}
			transactionManager.commit(status);
			return new ResponseModel<PlaceCreateBatchResultInfoDto>(201, dto);
		} else {
			outputBook.close();
			workbook.close();
			transactionManager.rollback(status);
			return new ResponseModel<PlaceCreateBatchResultInfoDto>(placeModel.getCode(), placeModel.getErrMsg());
		}
	}

	/**
	 * Place batch update from previous creation result template
	 * 
	 * @param file
	 * @return
	 */
	public ResponseModel<Void> updatePlaceTag_ImageByImportExcel(MultipartFile file) {

		ResponseModel<Void> resultModel = null;
		XSSFWorkbook workbook = null;
		try {
			workbook = new XSSFWorkbook(file.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseModel<>(500, e.getMessage());
		}

		XSSFSheet tagSheet = workbook.getSheetAt(0);
		XSSFSheet imageSheet = workbook.getSheetAt(1);

		List<PlaceTag> tagInsertList = new ArrayList<PlaceTag>();
		List<PlaceImg> imageList = new ArrayList<PlaceImg>();

		for (int i = 1; i < tagSheet.getPhysicalNumberOfRows(); i++) {
			XSSFRow row = tagSheet.getRow(i);
			System.out.println("row.getPhysicalNumberOfCells() = " + row.getPhysicalNumberOfCells());
			for (int c = 3; c < row.getPhysicalNumberOfCells(); c++) {
				if (row.getCell(c) != null && row.getCell(c).getStringCellValue().length() != 0) {
					PlaceTag tag = new PlaceTag();
					tag.setTagID(UUIDGenerator.generateType4UUID().toString());
					tag.setPlaceID(row.getCell(0).getStringCellValue());
					tag.setEditID(row.getCell(1).getStringCellValue());
					tag.setTag(row.getCell(c).getStringCellValue());
					tagInsertList.add(tag);
				} else {
					continue;
				}
			}

		}

		for (int i = 1; i < imageSheet.getPhysicalNumberOfRows(); i++) {
			XSSFRow row = imageSheet.getRow(i);
			int index = 0;
			for (int c = 3; c < row.getPhysicalNumberOfCells(); c += 2) {
				if (row.getCell(c) != null && row.getCell(c).getStringCellValue().length() != 0
						|| row.getCell(c + 1) != null && row.getCell(c + 1).getStringCellValue().length() != 0) {
					PlaceImg img = new PlaceImg();
					img.setUuid(UUIDGenerator.generateType4UUID().toString());
					img.setPlaceID(row.getCell(0).getStringCellValue());
					img.setEditID(row.getCell(1).getStringCellValue());
					img.setImg(row.getCell(c).getStringCellValue());
					img.setObjectKey(row.getCell(c + 1).getStringCellValue());
					if (index == 0) {
						img.setIsMain("true");
					} else {
						img.setIsMain("false");
					}
					imageList.add(img);
					index++;
				} else {
					continue;
				}
			}
		}

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);

		try {
			resultModel = placeDAO.setPlaceTagEdit(tagInsertList);
			if (resultModel.getCode() != 201) {
				workbook.close();
				throw new Exception();
			}
			resultModel = placeDAO.setPlaceTag(tagInsertList);
			if (resultModel.getCode() != 201) {
				workbook.close();
				throw new Exception();
			}
			resultModel = placeDAO.setPlaceImage(imageList);
			if (resultModel.getCode() != 201) {
				workbook.close();
				throw new Exception();
			}
			resultModel = placeDAO.setPlaceImageEdit(imageList);
			if (resultModel.getCode() != 201) {
				workbook.close();
				throw new Exception();
			}

			transactionManager.commit(status);
			workbook.close();
			return new ResponseModel<Void>(201, "");
		} catch (Exception e) {
			transactionManager.rollback(status);
			return resultModel;
		}

	}

	/**
	 * Place update. On the sequence, to create a new version of edit record then
	 * update current information by using the latest edit version. In short, this
	 * mechanism let everyone to edit, information correct or not depends on 'speech
	 * market' theory.
	 * 
	 * @param placeID
	 * @param place
	 * @param imageIDList
	 * @return
	 */
	public ResponseModel<Void> updatePlace(String placeID, PlaceUpdateDto place, List<String> imageIDList) {

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		List<MultipartFile> files = place.getFiles();
		List<PlaceImg> imageList = new ArrayList<PlaceImg>();
		List<String> tagList = place.getTagList();
		List<PlaceLiaisonCreateDto> placeLiaisonList = place.getPlaceLiaisonList();

		ResponseModel<Void> resultModel = null;
		String editID = UUIDGenerator.generateType4UUID().toString();

		try {
			PlaceUpdateDto originalPlace = placeDAO.getPlaceOriginalByPlaceID(placeID);
			utility.updateByDto(place, originalPlace);

			// Change old version in used as false
			resultModel = placeDAO.setPlaceEditInUsedFalse(placeID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			// Create a version of place edit
			if (place.getCounty() != null && place.getAddress() != null) {
				LatLng location = gpsService.getSimpleLocationFromAddress(place.getCounty() + place.getAddress());
				if (location != null) {
					originalPlace.setLat(BigDecimal.valueOf(location.lat));
					originalPlace.setLng(BigDecimal.valueOf(location.lng));
				}
			}
			originalPlace.setEditID(editID);
			originalPlace.setInUsed("true");
			originalPlace.setEditable("true");
			resultModel = placeDAO.setPlaceEdit(originalPlace);
			if (resultModel.getCode() != 201) {
				throw new Exception();
			}
			// Update current place information
			resultModel = placeDAO.updatePlace(originalPlace);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			// Delete current tag list
			resultModel = placeDAO.deletePlaceTagByPlaceID(placeID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}
			if (tagList != null && tagList.size() != 0) {
				List<PlaceTag> tagDtoList = new ArrayList<PlaceTag>();
				for (String t : tagList) {
					PlaceTag tag = new PlaceTag();
					tag.setTagID(UUIDGenerator.generateType4UUID().toString());
					tag.setPlaceID(placeID);
					tag.setEditID(editID);
					tag.setTag(t);
					tagDtoList.add(tag);
				}
				resultModel = placeDAO.setPlaceTag(tagDtoList);
				if (resultModel.getCode() != 201) {
					throw new Exception();
				}
				resultModel = placeDAO.setPlaceTagEdit(tagDtoList);
				if (resultModel.getCode() != 201) {
					throw new Exception();
				}
			}

			// Delete current liaison list
			resultModel = placeDAO.deletePlaceLiaisonByPlaceID(placeID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}
			if (placeLiaisonList != null && placeLiaisonList.size() != 0) {
				int liaisonIndex = 0;
				for (PlaceLiaisonCreateDto dto : placeLiaisonList) {
					String uuid = UUIDGenerator.generateType4UUID().toString();
					dto.setLiaisonID(uuid);
					dto.setPlaceID(placeID);
					dto.setEditID(editID);
					if (liaisonIndex == 0) {
						dto.setIsMain("true");
					} else {
						dto.setIsMain("false");
					}
					liaisonIndex++;
				}
				resultModel = placeDAO.setPlaceLiaisonEdit(placeLiaisonList);
				if (resultModel.getCode() != 201) {
					throw new Exception();
				}
				resultModel = placeDAO.setPlaceLiaison(placeLiaisonList);
				if (resultModel.getCode() != 201) {
					throw new Exception();
				}
			}

			int index = 0;
			if (imageIDList != null && imageIDList.size() != 0) {
				List<PlaceImg> originalImg = placeDAO.getPlaceImage(imageIDList);
				for (PlaceImg o : originalImg) {
					if (o.getIsMain().equals("true")) {
						index = 1;
					} else if (o.getIsMain().equals("false") && originalImg.size() == 1) {
						o.setIsMain("true");
					}
					o.setUuid(UUIDGenerator.generateType4UUID().toString());
					o.setEditID(editID);
					imageList.add(o);
				}
			}
			if (files != null && files.size() != 0) {
				amazonService.setFolderName(new StringBuilder("Place").append("/").append(placeID).toString());
				List<AmazonDto> amazonList = amazonService.uploadFile(files);
				for (AmazonDto aws : amazonList) {
					PlaceImg img = new PlaceImg();
					img.setPlaceID(placeID);
					img.setImg(aws.getUrl());
					img.setObjectKey(aws.getObjectKey());
					img.setEditID(editID);
					if (index == 0) {
						img.setIsMain("true");
					} else {
						img.setIsMain("false");
					}
					img.setUuid(UUIDGenerator.generateType4UUID().toString());
					imageList.add(img);
					index++;
				}
			}

			resultModel = placeDAO.deletePlaceImgByPlaceID(placeID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}
			if (imageList != null && imageList.size() != 0) {
				resultModel = placeDAO.setPlaceImageEdit(imageList);
				if (resultModel.getCode() != 201) {
					throw new Exception();
				}
				resultModel = placeDAO.setPlaceImage(imageList);
				if (resultModel.getCode() != 201) {
					throw new Exception();
				}
			}

			transactionManager.commit(status);
			return new ResponseModel<Void>(204, "");
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseModel<>(500, ApiError.AWS_FILE_UPLOAD_FAILED + "Due to: " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			transactionManager.rollback(status);
			return resultModel;
		}

	}

	/**
	 * Update place edit version by editID in used.
	 * 
	 * @param editID
	 * @return
	 */
	public ResponseModel<Void> updatePlaceEditApplyInUsed(String editID) {

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> resultModel = null;
		PlaceDto edit = placeDAO.getPlaceEdit(editID);
		if (edit != null) {
			String placeID = edit.getPlaceID();

			try {
				// Change old version in used as false
				resultModel = placeDAO.setPlaceEditInUsedFalse(placeID);
				if (resultModel.getCode() != 204) {
					throw new Exception();
				}
				resultModel = placeDAO.setPlaceEditInUsedTrue(editID);
				if (resultModel.getCode() != 204) {
					throw new Exception();
				}

				// Update current place information
				resultModel = placeDAO.updatePlace(placeID, editID);
				if (resultModel.getCode() != 204) {
					throw new Exception();
				}

				resultModel = placeDAO.deletePlaceLiaisonByPlaceID(placeID);
				if (resultModel.getCode() != 204) {
					throw new Exception();
				}
				resultModel = placeDAO.editPlaceLiaison(placeID, editID);
				if (resultModel.getCode() != 204) {
					throw new Exception();
				}

				resultModel = placeDAO.deletePlaceTagByPlaceID(placeID);
				if (resultModel.getCode() != 204) {
					throw new Exception();
				}
				resultModel = placeDAO.editPlaceTag(placeID, editID);
				if (resultModel.getCode() == 500) {
					throw new Exception();
				}

				resultModel = placeDAO.deletePlaceImgByPlaceID(placeID);
				if (resultModel.getCode() != 204) {
					throw new Exception();
				}
				resultModel = placeDAO.editPlaceImage(placeID, editID);
				if (resultModel.getCode() != 204) {
					throw new Exception();
				}

				transactionManager.commit(status);
				return new ResponseModel<Void>(204, "");
			} catch (Exception e) {
				transactionManager.rollback(status);
				return resultModel;
			}
		} else {
			return new ResponseModel<>(404, ApiError.DATA_NOT_FOUND);
		}
	}

	/**
	 * Get place simple information list by place name
	 * 
	 * @param name
	 * @return
	 */
	public List<PlaceSearchSimpleInfoDto> getPlaceSimpleByName(String name) {
		List<PlaceSearchSimpleInfoDto> place = placeDAO.getPlaceSimpleByName(name.trim());
		if (place != null) {
			return place;
		} else {
			return null;
		}
	}

	/**
	 * Get place edit list
	 * 
	 * @param placeID
	 * @param argsDto
	 * @return
	 */
	public PlaceEditSearchListInfoAdminDto getPlaceEditListByPlaceID(String placeID, PlaceEditSearchArgsAdminDto argsDto) {
		argsDto.setPlaceID(placeID);
		List<PlaceEditSearchInfoAdminDto> list = placeDAO.getPlaceEditListByPlaceID(argsDto);
		if (list != null && list.size() != 0) {
			PlaceEditSearchListInfoAdminDto dto = new PlaceEditSearchListInfoAdminDto();
			dto.setPlaceEditList(list);
			return dto;
		} else {
			return null;
		}
	}

	/**
	 * Get member edited place list by filters
	 * 
	 * @param argsDto
	 * @return
	 */
	public List<PlaceEditSearchInfoMemberDto> getPlaceMemberEditedListWithFilter(PlaceSearchArgsMemberEditedDto argsDto) {
		List<PlaceEditSearchInfoMemberDto> list = placeDAO.getPlaceMemberEditedListWithFilter(argsDto);
		if (list != null) {
			return list;
		} else {
			return null;
		}
	}

	/**
	 * Update place followed by member
	 * 
	 * @param follow
	 * @return Message of Doing result;
	 */
	public ResponseModel<Void> updatePlaceFollowByMemberID(PlaceFollowedDto follow) {

		Boolean ifFollowed = placeDAO.getPlaceIfMemberFollowed(follow);

		if (ifFollowed == true && follow.getWannaFollow() == true) {
			return new ResponseModel<Void>(500, ApiError.PLACE_HAS_FOLLOWED);
		} else if (ifFollowed == false && follow.getWannaFollow() == false) {
			return new ResponseModel<Void>(500, ApiError.PLACE_HAS_NOT_FOLLOWED);
		} else if (ifFollowed == null) {
			return new ResponseModel<Void>(500, ApiError.DATA_ACCESS_ERROR);
		}

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> resultModel = null;

		try {

			resultModel = placeDAO.updatePlaceFollowed(follow.getPlaceID(), follow.getWannaFollow());
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			if (follow.getWannaFollow() == true) {
				resultModel = placeDAO.setPlaceFollowed(follow);
				if (resultModel.getCode() != 201) {
					throw new Exception();
				}
			} else {
				resultModel = placeDAO.deletePlaceFollowed(follow);
				if (resultModel.getCode() != 204) {
					throw new Exception();
				}
			}

			transactionManager.commit(status);
			return new ResponseModel<Void>(201, "");
		} catch (Exception e) {
			transactionManager.rollback(status);
			return resultModel;
		}

	}

	/**
	 * 
	 * @param memberID
	 * @return
	 */
	public List<PinPlaceTypeInfoDto> getPinPlaceTypeListByMemberID(String memberID) {
		return placeDAO.getPinPlaceTypeListByMemberID(memberID);
	}

	/**
	 * Update pinned place type by memberID.
	 * 
	 * @param memberID
	 * @param list
	 * @return
	 */
	public ResponseModel<Void> updatePinPlaceTypeList(String memberID, List<PinPlaceTypeUpdateDto> list) {

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> resultModel = null;

		try {
			List<String> uuidList = placeDAO.getPinPlaceTypeUUIDListByMemberID(memberID);
			if (uuidList != null && uuidList.size() != 0) {
				resultModel = placeDAO.deletePinPlaceTypeList(uuidList);
				if (resultModel.getCode() != 204) {
					throw new Exception();
				}
			}

			// if front end sent {[]}, should create flag for MainType.

			for (PinPlaceTypeUpdateDto dto : list) {
				String uuid = UUIDGenerator.generateType4UUID().toString();
				dto.setUuid(uuid);
				dto.setMemberID(memberID);
			}

			resultModel = placeDAO.setPinPlaceTypeList(list);
			if (resultModel.getCode() != 201) {
				throw new Exception();
			}

			transactionManager.commit(status);
			return new ResponseModel<Void>(201, "");
		} catch (Exception e) {
			transactionManager.rollback(status);
			return resultModel;
		}
	}

	/**
	 * Get place edit single recored
	 * 
	 * @param editID
	 * @return
	 */
	public PlaceInfoDto getPlaceEditByEditID(String editID) {
		PlaceDto placeEdit = placeDAO.getPlaceEdit(editID);
		if (placeEdit != null) {
			PlaceInfoDto dto = new PlaceInfoDto();
			List<PlaceLiaisonDto> liaisonList = placeDAO.getPlaceLiaisonListByEditID(editID);
			List<PlaceTag> tagList = placeDAO.getPlaceTagListByEditID(editID);
			List<PlaceImageDto> imgList = placeDAO.getPlaceImgListByEditID(editID);
			if (placeEdit.getMax() != null) {
				placeEdit.setMaxName(capacity[placeEdit.getMax()]);
			}
			placeEdit.setLiaison(liaisonList);
			dto.setPlaceInfo(placeEdit);
			dto.setPlaceTagList(tagList);
			dto.setPlaceImgList(imgList);
			return dto;
		} else {
			return null;
		}
	}

	/**
	 * Get member place edit single recored
	 * 
	 * @param editID
	 * @param memberID
	 * @return
	 */
	public PlaceInfoDto getPlaceMemberEditByEditID(String editID, String memberID) {
		PlaceDto placeEdit = placeDAO.getPlaceMemberEdit(editID, memberID);
		if (placeEdit != null) {
			PlaceInfoDto dto = new PlaceInfoDto();
			List<PlaceLiaisonDto> liaisonList = placeDAO.getPlaceLiaisonListByEditID(editID);
			List<PlaceTag> tagList = placeDAO.getPlaceTagListByEditID(editID);
			List<PlaceImageDto> imgList = placeDAO.getPlaceImgListByEditID(editID);
			placeEdit.setLiaison(liaisonList);
			dto.setPlaceInfo(placeEdit);
			dto.setPlaceTagList(tagList);
			dto.setPlaceImgList(imgList);
			return dto;
		} else {
			return null;
		}
	}

	/**
	 * Delete place all information cascade with other related table by placeID
	 * 
	 * @param eventID
	 * @return
	 */
	public ResponseModel<Void> deletePlaceByPlaceID(String placeID) {

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> resultModel = null;
		// Retrieve all object keys belongs to this place.
		List<String> objectKeyList = placeDAO.getPlaceImageObjectkey(placeID);
		List<String> editIDList = placeDAO.getPlaceEditID(placeID);
		try {
			resultModel = placeDAO.deletePlaceFollowedByPlaceID(placeID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			resultModel = placeDAO.deletePlaceTagByPlaceID(placeID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}
			resultModel = placeDAO.deletePlaceTagEditByEditID(editIDList);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			resultModel = placeDAO.deletePlaceImgByPlaceID(placeID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}
			resultModel = placeDAO.deletePlaceImgEditByEditID(editIDList);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			resultModel = placeDAO.deletePlaceLiaisonByPlaceID(placeID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}
			resultModel = placeDAO.deletePlaceLiaisonEditByEditID(editIDList);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			resultModel = placeDAO.deletePlaceEditByEditID(editIDList);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			resultModel = placeDAO.deletePlaceByPlaceID(placeID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			transactionManager.commit(status);
			// Delete place images from AWS S3
			if (objectKeyList != null && objectKeyList.size() != 0) {
				amazonService.deleteFile(objectKeyList);
			}
			return new ResponseModel<Void>(204, "");
		} catch (Exception e) {
			transactionManager.rollback(status);
			return resultModel;
		}
	}

	/**
	 * Delete place edit version cascade with other related data by editID
	 * 
	 * @param editID
	 * @return
	 */
	public ResponseModel<Void> deletePlaceEditByEditID(String editID) {
		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> resultModel = null;
		List<String> editIDList = new ArrayList<String>();
		editIDList.add(editID);

		PlaceDto dto = placeDAO.getPlaceEdit(editID);
		if (dto.getInUsed().equals("false")) {
			try {
				resultModel = placeDAO.deletePlaceLiaisonEditByEditID(editIDList);
				if (resultModel.getCode() != 204) {
					throw new Exception();
				}
				resultModel = placeDAO.deletePlaceTagEditByEditID(editIDList);
				if (resultModel.getCode() != 204) {
					throw new Exception();
				}
				resultModel = placeDAO.deletePlaceImgEditByEditID(editIDList);
				if (resultModel.getCode() != 204) {
					throw new Exception();
				}
				resultModel = placeDAO.deletePlaceEditByEditID(editIDList);
				if (resultModel.getCode() != 204) {
					throw new Exception();
				}
				transactionManager.commit(status);
				return new ResponseModel<>(204, "");
			} catch (Exception e) {
				transactionManager.rollback(status);
				return resultModel;
			}
		} else {
			return new ResponseModel<>(500, "The version is in used.");
		}
	}

	private PlaceSearchInfoDto calculator(BigDecimal user_lat, BigDecimal user_lng, PlaceSearchInfoDto place) {
		Double distance = utility.calculateDistance(user_lat.doubleValue(), user_lng.doubleValue(), place.getLat().doubleValue(),
				place.getLng().doubleValue());
		place.setDistance(distance);
		return place;
	}

}
