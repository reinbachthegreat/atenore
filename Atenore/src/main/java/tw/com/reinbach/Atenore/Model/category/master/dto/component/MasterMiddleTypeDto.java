package tw.com.reinbach.Atenore.Model.category.master.dto.component;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MasterMiddleTypeDto {

	@ApiModelProperty(example = "1")
	private Integer midID;

	@ApiModelProperty(hidden = true)
	private Integer mainID;
	
	@ApiModelProperty(example = "醫療院所")
	private String name;

	public Integer getMidID() {
		return midID;
	}

	public void setMidID(Integer midID) {
		this.midID = midID;
	}

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
