package tw.com.reinbach.Atenore.Model.member.query.search;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModelProperty;

public class PlaceFollowedSearchInfoDto {

	@ApiModelProperty(value = "場所編號")
	private String placeID;

//	private PlaceImageDto coverImage = new PlaceImageDto();

	private String img;

//	分類屬性
	@ApiModelProperty(value = "場所名稱")
	private String name;

	@ApiModelProperty(value = "場所最大容留人數")
	private Integer max;

	@ApiModelProperty(value = "場所最大容留人數名稱")
	private String maxName;

	@ApiModelProperty(value = "場所面積", example = "50.5坪")
	private String area;

	@ApiModelProperty(value = "場所縣市", example = "新北市")
	private String county;

	@ApiModelProperty(value = "場所地址(排除縣市)", example = "板橋區縣民大道55號2巷158號6樓")
	private String address;

	@ApiModelProperty(value = "聯絡電話")
	private String phone;

	@ApiModelProperty(value = "場所簡介")
	private String intro;

	@ApiModelProperty(value = "場所累計點閱數")
	private Integer click;

	@ApiModelProperty(value = "場所累計追蹤數")
	private Integer follow;

	@ApiModelProperty(value = "GPS, Lat", example = "25.05788")
	private BigDecimal lat;

	@ApiModelProperty(value = "GPS, Lng", example = "24.55855")
	private BigDecimal lng;

	@ApiModelProperty(value = "calculate between current user and this place, use 'meter' as unit.", example = "913.13")
	private Double distance;

//	@ApiModelProperty(value = "資訊創立時間", example = "2020-01-10 10:00:00")
//	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//	@JsonInclude(JsonInclude.Include.NON_NULL)
//	private LocalDateTime insertTime;

	public String getPlaceID() {
		return placeID;
	}

	public void setPlaceID(String placeID) {
		this.placeID = placeID;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getMax() {
		return max;
	}

	public void setMax(Integer max) {
		this.max = max;
	}

	public String getMaxName() {
		return maxName;
	}

	public void setMaxName(String maxName) {
		this.maxName = maxName;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public Integer getClick() {
		return click;
	}

	public void setClick(Integer click) {
		this.click = click;
	}

	public Integer getFollow() {
		return follow;
	}

	public void setFollow(Integer follow) {
		this.follow = follow;
	}

	public BigDecimal getLat() {
		return lat;
	}

	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}

	public BigDecimal getLng() {
		return lng;
	}

	public void setLng(BigDecimal lng) {
		this.lng = lng;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

}
