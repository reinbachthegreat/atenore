package tw.com.reinbach.Atenore.Utility;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.lang3.RandomStringUtils;

public class CipherUtility {

	// 加密字串
	static String key = "!AtenorePassword";// key必須為16位數
	
	/**
	 * 自訂key必須為16位數
	 */
	public static String customkey; 

	/**
	 * 本方法可對字串message(Plaintext)加密，key為加密金鑰 (共用上方static金鑰) 傳回值為加密後的字串(Ciphertext)
	 * 
	 */
	public static String encryptStringCommonKey(String account, String pwd) throws Throwable {

		String message = account + pwd;

		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		SecretKeySpec secretKey = new SecretKeySpec(key.getBytes(), "AES");
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);
		String encryptedString = DatatypeConverter.printBase64Binary(cipher.doFinal(message.getBytes()));
		// String urlEncodedString = URLEncoder.encode(encryptedString,"UTF-8");
		return encryptedString;
	}

	/**
	 * 本方法可對加密之字串(Ciphertext)解密，key為當初加密時的金鑰 (共用上方static金鑰) 傳回值為解密後的字串(Plaintext)
	 * 
	 */
	public static String decryptStringCommonKey(String account, String pwd) throws Throwable {

		String stringToDecrypt = account + pwd;

		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
		SecretKeySpec secretKey = new SecretKeySpec(key.getBytes(), "AES");
		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		// String stringToDecrypt = URLDecoder.decode(stringToDecoded, "UTF-8");
		byte[] b = DatatypeConverter.parseBase64Binary(stringToDecrypt);
		String decryptedString = new String(cipher.doFinal(b));
		return decryptedString;

	}
	
	/**
	 * 本方法可對字串message(Plaintext)加密，key為加密金鑰 (共用上方static金鑰) 傳回值為加密後的字串(Ciphertext)
	 * 
	 */
	public static String encryptStringPasswordOnly(String pwd) throws Throwable {

		String message = pwd;

		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		SecretKeySpec secretKey = new SecretKeySpec(key.getBytes(), "AES");
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);
		String encryptedString = DatatypeConverter.printBase64Binary(cipher.doFinal(message.getBytes()));
		// String urlEncodedString = URLEncoder.encode(encryptedString,"UTF-8");
		return encryptedString;
	}

	/**
	 * 本方法可對加密之字串(Ciphertext)解密，key為當初加密時的金鑰 (共用上方static金鑰) 傳回值為解密後的字串(Plaintext)
	 * 
	 */
	public static String decryptStringPasswordOnly(String pwd) throws Throwable {

		String stringToDecrypt = pwd;

		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
		SecretKeySpec secretKey = new SecretKeySpec(key.getBytes(), "AES");
		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		// String stringToDecrypt = URLDecoder.decode(stringToDecoded, "UTF-8");
		byte[] b = DatatypeConverter.parseBase64Binary(stringToDecrypt);
		String decryptedString = new String(cipher.doFinal(b));
		return decryptedString;

	}
	
	/**
	 * 本方法可對字串message(Plaintext)加密，customKey為加密金鑰 (須設定) 傳回值為加密後的字串(Ciphertext)
	 * 
	 */
	public static String encryptStringWithCustomKey(String pureString) throws Throwable {

		String message = pureString;

		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		SecretKeySpec secretKey = new SecretKeySpec(customkey.getBytes(), "AES");
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);
		String encryptedString = DatatypeConverter.printBase64Binary(cipher.doFinal(message.getBytes()));
		// String urlEncodedString = URLEncoder.encode(encryptedString,"UTF-8");
		return encryptedString;
	}

	/**
	 * 本方法可對加密之字串(Ciphertext)解密，customKey為加密金鑰 (須設定) 傳回值為解密後的字串(Plaintext)
	 * 
	 */
	public static String decryptStringWithCustomKey(String encryptString) throws Throwable {

		String stringToDecrypt = encryptString;

		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
		SecretKeySpec secretKey = new SecretKeySpec(customkey.getBytes(), "AES");
		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		// String stringToDecrypt = URLDecoder.decode(stringToDecoded, "UTF-8");
		byte[] b = DatatypeConverter.parseBase64Binary(stringToDecrypt);
		String decryptedString = new String(cipher.doFinal(b));
		return decryptedString;

	}
	
	/**
	 * MD5加密
	 */
	public static String getMD5Encoding(String str) {
		final StringBuffer buffer = new StringBuffer();
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(str.getBytes());
			byte[] digest = md.digest();

			for (int i = 0; i < digest.length; ++i) {
				final byte b = digest[i];
				final int value = (b & 0x7F) + (b < 0 ? 128 : 0);
				buffer.append(value < 16 ? "0" : "");
				buffer.append(Integer.toHexString(value));
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
		return buffer.toString();
	}

	// 亂數密碼 (數字 + 小寫英文)
	public static String createPassWord() {

		int[] word = new int[8];
		int mod;
		for (int i = 2; i < 8; i++) {

			word[0] = (char) ((Math.random() * 26) + 97);
			word[1] = (int) ((Math.random() * 10) + 48);

			mod = (int) ((Math.random() * 7) % 2);
			if (mod == 1) {
				word[i] = (int) ((Math.random() * 10) + 48);
			} else {
				word[i] = (char) ((Math.random() * 26) + 97);
			}
		}
		StringBuffer newPassword = new StringBuffer();
		for (int j = 0; j < 8; j++) {
			newPassword.append((char) word[j]);
		}
		return newPassword.toString();
	}
	
	// 產生亂碼：Apache
	public static String getRandomStringByApache(int stringLength, boolean useLetters, boolean useNumbers) {
		return RandomStringUtils.random(stringLength, useLetters, useNumbers);
	}

}
