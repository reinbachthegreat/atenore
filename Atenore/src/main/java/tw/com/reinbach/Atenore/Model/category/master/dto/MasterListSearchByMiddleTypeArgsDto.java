package tw.com.reinbach.Atenore.Model.category.master.dto;

import io.swagger.annotations.ApiModelProperty;

public class MasterListSearchByMiddleTypeArgsDto {

	@ApiModelProperty(hidden = true)
	private Integer midID;
	
	private String lastName;
	
	private String firstName;

	public Integer getMidID() {
		return midID;
	}

	public void setMidID(Integer midID) {
		this.midID = midID;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
}
