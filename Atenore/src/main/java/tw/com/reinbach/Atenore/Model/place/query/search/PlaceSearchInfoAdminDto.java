package tw.com.reinbach.Atenore.Model.place.query.search;

import io.swagger.annotations.ApiModelProperty;

public class PlaceSearchInfoAdminDto {

	@ApiModelProperty(value = "場所編號")
	private String placeID;

	@ApiModelProperty(value = "場所大分類編號")
	private Integer mainID;

	@ApiModelProperty(value = "場所名稱")
	private String name;

	@ApiModelProperty(value = "場所縣市", example = "台北市")
	private String county;

	@ApiModelProperty(value = "聯絡人")
	private String liaison;

	@ApiModelProperty(value = "聯絡電話")
	private String phone;

	@ApiModelProperty(value = "場所資訊是否開放編輯", example = "true/false")
	private String editable;

	@ApiModelProperty(value = "場所發布者編號(現行版本或各歷史編輯版本)")
	private String memberAccount;

	public String getPlaceID() {
		return placeID;
	}

	public void setPlaceID(String placeID) {
		this.placeID = placeID;
	}

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getLiaison() {
		return liaison;
	}

	public void setLiaison(String liaison) {
		this.liaison = liaison;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEditable() {
		return editable;
	}

	public void setEditable(String editable) {
		this.editable = editable;
	}

	public String getMemberAccount() {
		return memberAccount;
	}

	public void setMemberAccount(String memberAccount) {
		this.memberAccount = memberAccount;
	}

}
