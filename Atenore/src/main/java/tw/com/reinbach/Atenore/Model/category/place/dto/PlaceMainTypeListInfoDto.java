package tw.com.reinbach.Atenore.Model.category.place.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import tw.com.reinbach.Atenore.Model.category.place.dto.component.PlaceMainTypeAdminDto;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PlaceMainTypeListInfoDto {
	
	private List<PlaceMainTypeAdminDto> placeMainTypeList;

	public List<PlaceMainTypeAdminDto> getPlaceMainTypeList() {
		return placeMainTypeList;
	}

	public void setPlaceMainTypeList(List<PlaceMainTypeAdminDto> placeMainTypeList) {
		this.placeMainTypeList = placeMainTypeList;
	}
	
	

}
