package tw.com.reinbach.Atenore.Model.master.query.search;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MasterEditSearchListInfoAdminDto {

	private List<MasterEditSearchInfoAdminDto> masterEditList;

	public List<MasterEditSearchInfoAdminDto> getMasterEditList() {
		return masterEditList;
	}

	public void setMasterEditList(List<MasterEditSearchInfoAdminDto> masterEditList) {
		this.masterEditList = masterEditList;
	}
}
