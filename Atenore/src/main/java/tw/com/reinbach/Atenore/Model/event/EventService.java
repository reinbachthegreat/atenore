package tw.com.reinbach.Atenore.Model.event;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.multipart.MultipartFile;

import com.google.maps.model.LatLng;

import tw.com.reinbach.Atenore.Model.event.component.EventAdminDto;
import tw.com.reinbach.Atenore.Model.event.component.EventDto;
import tw.com.reinbach.Atenore.Model.event.component.EventImageDto;
import tw.com.reinbach.Atenore.Model.event.component.EventRecommend;
import tw.com.reinbach.Atenore.Model.event.component.EventTag;
import tw.com.reinbach.Atenore.Model.event.component.EventVisibilityDto;
import tw.com.reinbach.Atenore.Model.event.component.PinEventTypeInfoDto;
import tw.com.reinbach.Atenore.Model.event.create.EventCreateBatchResultInfoDto;
import tw.com.reinbach.Atenore.Model.event.create.EventCreateDto;
import tw.com.reinbach.Atenore.Model.event.create.EventImageItemCreateDto;
import tw.com.reinbach.Atenore.Model.event.query.EventInfoAllDto;
import tw.com.reinbach.Atenore.Model.event.query.EventNotReviewCountDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventNearBySearchArgsDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchAppliedInfoDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchArgsAdminDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchArgsDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchInfoAdminDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchInfoDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchListInfoAdminDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchPreferArgDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchReviewArgsAdminDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchReviewInfoAdminDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchReviewListInfoAdminDto;
import tw.com.reinbach.Atenore.Model.event.update.EventApplyDto;
import tw.com.reinbach.Atenore.Model.event.update.EventFollowedDto;
import tw.com.reinbach.Atenore.Model.event.update.EventUpdateDto;
import tw.com.reinbach.Atenore.Model.event.update.PinEventTypeUpdateDto;
import tw.com.reinbach.Atenore.Model.search.SearchOrderDto;
import tw.com.reinbach.Atenore.Model.search.SearchPreferDto;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;
import tw.com.reinbach.Atenore.Utility.UUIDGenerator;
import tw.com.reinbach.Atenore.Utility.Utility;
import tw.com.reinbach.Atenore.Utility.aws.AmazonDto;
import tw.com.reinbach.Atenore.Utility.aws.AmazonService;
import tw.com.reinbach.Atenore.Utility.google.GpsService;

@Service
public class EventService {

	@Autowired
	private EventDAO eventDAO;

	@Autowired
	private AmazonService amazonService;

	@Autowired
	private GpsService gpsService;

	@Autowired
	private Utility utility;

	@Autowired
	private PlatformTransactionManager transactionManager;

	@Value("${event.update.excel.template.from}")
	private String updateExcelTemplateUrl;

	@Value("${event.update.excel.export.aws.to}")
	private String updateExcelFolder;

	private String[] applyStatus = { "待審核", "已發布", "已拒絕", "已取消", "已過期" };

	private String[] visibility = { "開放 a.「歡迎參加」:不限資格、不限費用", "開放 b.「非請勿入」:限制資格、不限費用", "開放 c.「需先繳費」:不限資格、限制費用", "開放 d.「需先報名」:限制資格、限制費用",
			"不開放" };

	private String[] searchPrefer = { "全部", "最新", "熱門", "最新與熱門" };

	private String[][] searchOrder = { { "距離近到遠", "開始日期近到遠", "結束日期近到遠" }, { "距離近到遠", "開始日期近到遠", "結束日期近到遠" },
			{ "距離近到遠", "開始日期近到遠", "結束日期近到遠", "以點閱率高到低", "以收藏人數高到低" }, { "距離近到遠", "開始日期近到遠", "結束日期近到遠", "以點閱率高到低", "以收藏人數高到低" } };

	/**
	 * 取得活動審核狀態清單
	 * 
	 * @return {"待審核", "已發布", "已拒絕", "已取消", "已過期" }
	 */
	public ArrayList<String> getApplyStatusList() {
		return new ArrayList<String>(Arrays.asList(this.applyStatus));
	}

	/**
	 * 取得活動開放狀態清單
	 * 
	 * @return {"開放 a.「歡迎參加」:不限資格、不限費用", "開放 b.「非請勿入」:限制資格、不限費用", "開放
	 *         c.「需先繳費」:不限資格、限制費用", "開放 d.「需先報名」:限制資格、限制費用", "不開放"}
	 */
	public List<EventVisibilityDto> getVisibilityList() {

		List<String> list = new ArrayList<String>(Arrays.asList(this.visibility));
		List<EventVisibilityDto> visibilityList = new ArrayList<EventVisibilityDto>();
		int index = 0;

		for (String v : list) {
			EventVisibilityDto dto = new EventVisibilityDto();
			dto.setVisibilitySN(index);
			dto.setVisibility(v);
			visibilityList.add(dto);
			index++;
		}

		return visibilityList;
	}

	/**
	 * Get event list with EventSearchArgsDto filter
	 * 
	 * @param argsDto
	 * @return
	 */
	public List<EventSearchInfoDto> getEventListWithFilter(EventSearchArgsDto argsDto) {
		EventSearchPreferArgDto prefer = argsDto.getSearchPreferArg();
		Integer preferSN = prefer.getSearchPreferSN();
		Integer orderSN = prefer.getSearchOrderSN();

		List<EventSearchInfoDto> list = eventDAO.getEventListWithFilter(argsDto);
		if (list != null) {
			int index = 0;
			for (EventSearchInfoDto event : list) {
				if (event.getLat() != null && event.getLng() != null) {
					event = this.calculator(argsDto.getLat(), argsDto.getLng(), event);
					list.set(index, event);
				}
				index++;
			}

			switch (preferSN) {
			// All, default not selected.
			case 0:
				switch (orderSN) {
				// distance order by ASC.
				case 0:

					Collections.sort(list,
							Comparator.comparing(EventSearchInfoDto::getDistance, Comparator.nullsLast(Comparator.naturalOrder())));

					break;
				// start time order by DESC.
				case 1:

					Collections.sort(list,
							Comparator.comparing(EventSearchInfoDto::getStartTime, Comparator.nullsLast(Comparator.reverseOrder())));

					break;
				// end time order by DESC.
				case 2:

					Collections.sort(list,
							Comparator.comparing(EventSearchInfoDto::getEndTime, Comparator.nullsLast(Comparator.reverseOrder())));

					break;
				}
				break;
			// Newest selected.
			case 1:
				switch (orderSN) {
				// distance order by ASC.
				case 0:
					Collections.sort(list,
							Comparator.comparing(EventSearchInfoDto::getDistance, Comparator.nullsLast(Comparator.naturalOrder()))
									.thenComparing(Comparator.comparing(EventSearchInfoDto::getInsertTime,
											Comparator.nullsLast(Comparator.reverseOrder()))));

					break;
				// start time order by DESC.
				case 1:

					Collections.sort(list,
							Comparator.comparing(EventSearchInfoDto::getInsertTime, Comparator.nullsLast(Comparator.reverseOrder()))
									.thenComparing(Comparator.comparing(EventSearchInfoDto::getStartTime,
											Comparator.nullsLast(Comparator.reverseOrder()))));

					break;
				// end time order by DESC.
				case 2:

					Collections.sort(list,
							Comparator.comparing(EventSearchInfoDto::getInsertTime, Comparator.nullsLast(Comparator.reverseOrder()))
									.thenComparing(Comparator.comparing(EventSearchInfoDto::getEndTime,
											Comparator.nullsLast(Comparator.reverseOrder()))));

					break;
				}
				break;
			// Hottest selected.
			case 2:
				switch (orderSN) {
				// distance order by ASC.
				case 0:

					Collections.sort(list,
							Comparator.comparing(EventSearchInfoDto::getDistance, Comparator.nullsLast(Comparator.naturalOrder()))
									.thenComparing(Comparator.comparing(EventSearchInfoDto::getClick,
											Comparator.nullsLast(Comparator.reverseOrder()))));

					break;
				// start time order by DESC.
				case 1:

					Collections.sort(list,
							Comparator.comparing(EventSearchInfoDto::getClick, Comparator.nullsLast(Comparator.reverseOrder()))
									.thenComparing(Comparator.comparing(EventSearchInfoDto::getStartTime,
											Comparator.nullsLast(Comparator.reverseOrder()))));

					break;
				// end time order by DESC.
				case 2:

					Collections.sort(list,
							Comparator.comparing(EventSearchInfoDto::getClick, Comparator.nullsLast(Comparator.reverseOrder()))
									.thenComparing(Comparator.comparing(EventSearchInfoDto::getEndTime,
											Comparator.nullsLast(Comparator.reverseOrder()))));

					break;
				// click order by DESC.
				case 3:

					Collections.sort(list,
							Comparator.comparing(EventSearchInfoDto::getClick, Comparator.nullsLast(Comparator.reverseOrder())));

					break;
				// follow order by DESC.
				case 4:

					Collections.sort(list,
							Comparator.comparing(EventSearchInfoDto::getFollow, Comparator.nullsLast(Comparator.reverseOrder())));

					break;
				}

				break;
			// Both newest and hottest selected.
			case 3:
				switch (orderSN) {
				// distance order by ASC.
				case 0:

					Collections.sort(list,
							Comparator.comparing(EventSearchInfoDto::getDistance, Comparator.nullsLast(Comparator.naturalOrder()))
									.thenComparing(Comparator.comparing(EventSearchInfoDto::getClick,
											Comparator.nullsLast(Comparator.reverseOrder()))));
					break;
				// start time order by DESC.
				case 1:

					Collections.sort(list,
							Comparator.comparing(EventSearchInfoDto::getClick, Comparator.nullsLast(Comparator.reverseOrder()))
									.thenComparing(Comparator.comparing(EventSearchInfoDto::getStartTime,
											Comparator.nullsLast(Comparator.reverseOrder()))));

					break;
				// end time order by DESC.
				case 2:

					Collections.sort(list,
							Comparator.comparing(EventSearchInfoDto::getClick, Comparator.nullsLast(Comparator.reverseOrder()))
									.thenComparing(Comparator.comparing(EventSearchInfoDto::getEndTime,
											Comparator.nullsLast(Comparator.reverseOrder()))));

					break;
				// click order by DESC.
				case 3:

					Collections.sort(list,
							Comparator.comparing(EventSearchInfoDto::getClick, Comparator.nullsLast(Comparator.reverseOrder())));

					break;
				// follow order by DESC.
				case 4:

					Collections.sort(list,
							Comparator.comparing(EventSearchInfoDto::getFollow, Comparator.nullsLast(Comparator.reverseOrder())));

					break;
				}
				break;
			}

		}

		return list;
	}

	/**
	 * Get event list at GIS location near by
	 * 
	 * @param argsDto
	 * @return
	 */
	public List<EventSearchInfoDto> getEventListNearBy(EventNearBySearchArgsDto argsDto) {

		argsDto.setMinLat(argsDto.getLat().subtract(new BigDecimal(0.001)));
		argsDto.setMinLng(argsDto.getLng().subtract(new BigDecimal(0.001)));
		argsDto.setMaxLat(argsDto.getLat().add(new BigDecimal(0.001)));
		argsDto.setMaxLng(argsDto.getLng().add(new BigDecimal(0.001)));

		EventSearchPreferArgDto prefer = argsDto.getSearchPreferArg();
		Integer preferSN = prefer.getSearchPreferSN();
		Integer orderSN = prefer.getSearchOrderSN();

		List<EventSearchInfoDto> list = eventDAO.getEventListNearBy(argsDto);
		if (list != null) {
			int index = 0;
			for (EventSearchInfoDto event : list) {
				if (event.getLat() != null && event.getLng() != null) {
					event = this.calculator(argsDto.getLat(), argsDto.getLng(), event);
					list.set(index, event);
				}
				index++;
			}

			switch (preferSN) {
			// All, default not selected.
			case 0:
				switch (orderSN) {
				// distance order by DESC.
				case 0:

					Collections.sort(list,
							Comparator.comparing(EventSearchInfoDto::getDistance, Comparator.nullsLast(Comparator.reverseOrder())));

					break;
				// start time order by DESC.
				case 1:

					Collections.sort(list,
							Comparator.comparing(EventSearchInfoDto::getStartTime, Comparator.nullsLast(Comparator.reverseOrder())));

					break;
				// end time order by DESC.
				case 2:

					Collections.sort(list,
							Comparator.comparing(EventSearchInfoDto::getEndTime, Comparator.nullsLast(Comparator.reverseOrder())));

					break;
				}
				break;
			// Newest selected.
			case 1:
				switch (orderSN) {
				// distance order by DESC.
				case 0:

					Collections.sort(list,
							Comparator.comparing(EventSearchInfoDto::getInsertTime, Comparator.nullsLast(Comparator.reverseOrder()))
									.thenComparing(Comparator.comparing(EventSearchInfoDto::getDistance,
											Comparator.nullsLast(Comparator.naturalOrder()))));

					break;
				// start time order by DESC.
				case 1:

					Collections.sort(list,
							Comparator.comparing(EventSearchInfoDto::getInsertTime, Comparator.nullsLast(Comparator.reverseOrder()))
									.thenComparing(Comparator.comparing(EventSearchInfoDto::getStartTime,
											Comparator.nullsLast(Comparator.reverseOrder()))));

					break;
				// end time order by DESC.
				case 2:

					Collections.sort(list,
							Comparator.comparing(EventSearchInfoDto::getInsertTime, Comparator.nullsLast(Comparator.reverseOrder()))
									.thenComparing(Comparator.comparing(EventSearchInfoDto::getEndTime,
											Comparator.nullsLast(Comparator.reverseOrder()))));

					break;
				}
				break;
			// Hottest selected.
			case 2:
				switch (orderSN) {
				// distance order by DESC.
				case 0:

					Collections.sort(list,
							Comparator.comparing(EventSearchInfoDto::getClick, Comparator.nullsLast(Comparator.reverseOrder()))
									.thenComparing(Comparator.comparing(EventSearchInfoDto::getDistance,
											Comparator.nullsLast(Comparator.naturalOrder()))));

					break;
				// start time order by DESC.
				case 1:

					Collections.sort(list,
							Comparator.comparing(EventSearchInfoDto::getClick, Comparator.nullsLast(Comparator.reverseOrder()))
									.thenComparing(Comparator.comparing(EventSearchInfoDto::getStartTime,
											Comparator.nullsLast(Comparator.reverseOrder()))));

					break;
				// end time order by DESC.
				case 2:

					Collections.sort(list,
							Comparator.comparing(EventSearchInfoDto::getClick, Comparator.nullsLast(Comparator.reverseOrder()))
									.thenComparing(Comparator.comparing(EventSearchInfoDto::getEndTime,
											Comparator.nullsLast(Comparator.reverseOrder()))));

					break;
				// click order by DESC.
				case 3:

					Collections.sort(list,
							Comparator.comparing(EventSearchInfoDto::getClick, Comparator.nullsLast(Comparator.reverseOrder())));

					break;
				// follow order by DESC.
				case 4:

					Collections.sort(list,
							Comparator.comparing(EventSearchInfoDto::getFollow, Comparator.nullsLast(Comparator.reverseOrder())));

					break;
				}

				break;
			// Both newest and hottest selected.
			case 3:
				switch (orderSN) {
				// distance order by DESC.
				case 0:

					Collections.sort(list,
							Comparator.comparing(EventSearchInfoDto::getClick, Comparator.nullsLast(Comparator.reverseOrder()))
									.thenComparing(Comparator.comparing(EventSearchInfoDto::getDistance,
											Comparator.nullsLast(Comparator.naturalOrder()))));

					break;
				// start time order by DESC.
				case 1:

					Collections.sort(list,
							Comparator.comparing(EventSearchInfoDto::getClick, Comparator.nullsLast(Comparator.reverseOrder()))
									.thenComparing(Comparator.comparing(EventSearchInfoDto::getStartTime,
											Comparator.nullsLast(Comparator.reverseOrder()))));

					break;
				// end time order by DESC.
				case 2:

					Collections.sort(list,
							Comparator.comparing(EventSearchInfoDto::getClick, Comparator.nullsLast(Comparator.reverseOrder()))
									.thenComparing(Comparator.comparing(EventSearchInfoDto::getEndTime,
											Comparator.nullsLast(Comparator.reverseOrder()))));

					break;
				// click order by DESC.
				case 3:

					Collections.sort(list,
							Comparator.comparing(EventSearchInfoDto::getClick, Comparator.nullsLast(Comparator.reverseOrder())));

					break;
				// follow order by DESC.
				case 4:

					Collections.sort(list,
							Comparator.comparing(EventSearchInfoDto::getFollow, Comparator.nullsLast(Comparator.reverseOrder())));

					break;
				}
				break;
			}

		}

		return list;
	}

	/**
	 * Get event list with EventSearchArgsAdminDto filter
	 * 
	 * @param argsDto
	 * @return
	 */
	public EventSearchListInfoAdminDto getEventListWithFilter(EventSearchArgsAdminDto argsDto) {
		List<EventSearchInfoAdminDto> list = eventDAO.getEventListWithFilter(argsDto);
		if (list != null && list.size() != 0) {
			EventSearchListInfoAdminDto dto = new EventSearchListInfoAdminDto();
			dto.setEventList(list);
			return dto;
		} else {
			return null;
		}
	}

	/**
	 * Get event searching prefer list
	 * 
	 * @return
	 */
	public List<SearchPreferDto> getEventSearcingPreferList() {
		int preferIndex = 0;
		List<SearchPreferDto> preferList = new ArrayList<SearchPreferDto>();
		for (String p : searchPrefer) {
			SearchPreferDto prefer = new SearchPreferDto();
			prefer.setSearchPreferSN(preferIndex);
			prefer.setSearchPreferName(p);
			List<SearchOrderDto> orderList = new ArrayList<SearchOrderDto>();
			int orderIndex = 0;
			for (String o : searchOrder[preferIndex]) {
				SearchOrderDto order = new SearchOrderDto();
				order.setSearchOrderSN(orderIndex);
				order.setSearchOrderName(o);
				orderList.add(order);
				orderIndex++;
			}
			prefer.setSearchOrderList(orderList);
			preferList.add(prefer);
			preferIndex++;
		}
		return preferList;
	}

	/**
	 * Get event information by eventID
	 * 
	 * @param eventID
	 * @param memberID
	 * @return
	 */
	public EventDto getEvent(String eventID, String memberID) {

		eventDAO.updateEventClick(eventID);

		if (memberID != null && memberID.trim().length() != 0) {
			return eventDAO.getEventWithMemberID(eventID, memberID);
		} else {
			return eventDAO.getEvent(eventID);
		}

	}

	/**
	 * Get event fully information by eventID
	 * 
	 * @param eventID
	 * @return
	 */
	public EventInfoAllDto getEventFullyByEventID(String eventID) {
		EventAdminDto event = eventDAO.getEventAll(eventID);
		if (event != null) {
			List<EventTag> tagList = eventDAO.getEventTagWithoutEventID(eventID);
			List<EventRecommend> recommendList = eventDAO.getEventRecommendWithoutEventID(eventID);
			List<EventImageDto> imageList = eventDAO.getEventImage(eventID);
			setEventStatus(event);
			EventInfoAllDto dto = new EventInfoAllDto();
			dto.setEventInfo(event);
			dto.setEventTagList(tagList);
			dto.setEventRecommendList(recommendList);
			dto.setEventImgList(imageList);
			return dto;
		} else {
			return null;
		}
	}

	/**
	 * Get event not reviewed count
	 * 
	 * @param argsDto
	 * @return
	 */
	public EventNotReviewCountDto getEventListNotReviewCount() {
		EventSearchReviewArgsAdminDto argsDto = new EventSearchReviewArgsAdminDto();
		argsDto.setHasReviewed(this.getApplyStatusList().get(1));
		return eventDAO.getEventListNotReviewCount(argsDto);
	}

	/**
	 * Get event not reviewed list by filters
	 * 
	 * @param argsDto
	 * @return
	 */
	public EventSearchReviewListInfoAdminDto getEventListNotReview(EventSearchReviewArgsAdminDto argsDto) {
		argsDto.setHasReviewed(this.getApplyStatusList().get(1));
		List<EventSearchReviewInfoAdminDto> list = eventDAO.getEventListNotReview(argsDto);
		if (list != null && list.size() != 0) {
			EventSearchReviewListInfoAdminDto dto = new EventSearchReviewListInfoAdminDto();
			dto.setEventList(list);
			return dto;
		} else {
			return null;
		}
	}

	public List<EventTag> getEventTag(String eventID) {
		return eventDAO.getEventTag(eventID);
	}

	public List<EventTag> getEventTagWithoutEventID(String eventID) {
		return eventDAO.getEventTagWithoutEventID(eventID);
	}

	public List<EventRecommend> getEventRecommend(String eventID) {
		return eventDAO.getEventRecommend(eventID);
	}

	public List<EventRecommend> getEventRecommendWithoutEventID(String eventID) {
		return eventDAO.getEventRecommendWithoutEventID(eventID);
	}

	public List<EventImageDto> getEventImage(String eventID) {
		return eventDAO.getEventImage(eventID);
	}

	/**
	 * Event batch creation
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public ResponseModel<EventCreateBatchResultInfoDto> setEventByImportExcel(MultipartFile file) throws IOException {

		List<EventCreateDto> eventImportList = new ArrayList<EventCreateDto>();
		XSSFWorkbook workbook = null;
		// Output file read from Template
		XSSFWorkbook outputBook = null;
		File f = null;
		try {
			workbook = new XSSFWorkbook(file.getInputStream());
			URL url = new URL(updateExcelTemplateUrl);
			InputStream in = url.openStream();
			Files.copy(in, Paths.get("event_template.xlsx"), StandardCopyOption.REPLACE_EXISTING);
			f = new File("event_template.xlsx");
			InputStream input = new FileInputStream(f);
			outputBook = new XSSFWorkbook(input);
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseModel<EventCreateBatchResultInfoDto>(500, e.getMessage());
		}
		XSSFSheet worksheet = workbook.getSheetAt(0);
		System.out.println(worksheet.getPhysicalNumberOfRows() + "rows");
		for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {

			EventCreateDto event = new EventCreateDto();
			XSSFRow row = worksheet.getRow(i);

			// If typeID read in is 0, means this cell values is empty or 0.
			int typeID;
			if(row==null || row.getCell(0)==null) {
				continue;
			}else {
				typeID = (int) row.getCell(0).getNumericCellValue();
				if (typeID == 0) {
					continue;
				}
			}

			String eventID = UUIDGenerator.generateType4UUID().toString();
			event.setEventID(eventID);
			event.setTypeID(typeID);
			if (row.getCell(1) != null && row.getCell(1).getStringCellValue().length() != 0) {
				event.setName(row.getCell(1).getStringCellValue());
			} else {
				event.setName("No event name!");
			}

			CellStyle cellStyle = outputBook.createCellStyle();
			XSSFFont font = outputBook.createFont();
			font.setFontName("Microsoft JhengHei");
			font.setFontHeight(12);
			cellStyle.setWrapText(true);
			cellStyle.setFont(font);
			cellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

//			for (int s = 0; s <= 2; s++) {
//				if (s == 0) {
//					outputBook.getSheetAt(s).getRow(i).getCell(0).setCellValue(event.getEventID());
//					outputBook.getSheetAt(s).getRow(i).getCell(1).setCellValue(event.getName());
//				} else {
//					XSSFRow row1 = outputBook.getSheetAt(s).createRow(i);
//					XSSFCell cell1 = row1.createCell(0);
//					XSSFCell cell2 = row1.createCell(1);
//					cell1.setCellValue(eventID);
//					cell2.setCellValue(event.getName());
//					cell1.setCellStyle(cellStyle);
//					cell2.setCellStyle(cellStyle);
//				}
//			}
			
			for (int s = 0; s <= 2; s++) {
				XSSFRow row1 = outputBook.getSheetAt(s).createRow(i);
				XSSFCell cell1 = row1.createCell(0);
				XSSFCell cell2 = row1.createCell(1);
				cell1.setCellValue(eventID);
				cell2.setCellValue(event.getName());
				cell1.setCellStyle(cellStyle);
				cell2.setCellStyle(cellStyle);
			}

			for (int k = 2; k <= 12; k++) {
				if (k == 8) {
					continue;
				}
				if (row.getCell(k) != null) {
					row.getCell(k).setCellType(Cell.CELL_TYPE_STRING);
				}
			}

			if (row.getCell(2) != null && row.getCell(2).getStringCellValue().length() != 0) {
				StringBuilder datetime = new StringBuilder(row.getCell(2).getStringCellValue());
				event.setStartTime(LocalDateTime.parse(datetime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
//				(Instant.ofEpochMilli(row.getCell(2).getDateCellValue().getTime()).atZone(ZoneId.systemDefault())
//						.toLocalDateTime());
			}

			if (row.getCell(3) != null && row.getCell(3).getStringCellValue().length() != 0) {
				StringBuilder datetime = new StringBuilder(row.getCell(3).getStringCellValue());
				event.setEndTime(LocalDateTime.parse(datetime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
//				event.setEndTime(Instant.ofEpochMilli(row.getCell(3).getDateCellValue().getTime()).atZone(ZoneId.systemDefault())
//						.toLocalDateTime());
			}

			if (row.getCell(4) != null && row.getCell(4).getStringCellValue().length() != 0) {
				event.setPlace(row.getCell(4).getStringCellValue());
			}

			if (row.getCell(5) != null && row.getCell(5).getStringCellValue().length() != 0) {
				event.setCounty(row.getCell(5).getStringCellValue());
			}

			if (row.getCell(6) != null && row.getCell(6).getStringCellValue().length() != 0) {
				event.setAddress(row.getCell(6).getStringCellValue());
			}

			if (row.getCell(7) != null && row.getCell(7).getStringCellValue().length() != 0) {
				event.setHost(row.getCell(7).getStringCellValue());
			}

//			if (row.getCell(8) != null) {
//				event.setFee((int) row.getCell(8).getNumericCellValue());
//			}
			
			if (row.getCell(8) != null) {
				event.setFee(row.getCell(8).getStringCellValue());
			}

			if (row.getCell(9) != null && row.getCell(9).getStringCellValue().length() != 0) {
				event.setIntro(row.getCell(9).getStringCellValue());
			}

			if (row.getCell(10) != null && row.getCell(10).getStringCellValue().length() != 0) {
				String visibility = row.getCell(10).getStringCellValue();
				List<EventVisibilityDto> list = getVisibilityList();
				for (EventVisibilityDto dto : list) {
					if (dto.getVisibility().equalsIgnoreCase(visibility)) {
						event.setVisibilitySN(dto.getVisibilitySN());
						break;
					}
				}
			}

			if (row.getCell(11) != null && row.getCell(11).getStringCellValue().length() != 0) {
				event.setSignupUrl(row.getCell(11).getStringCellValue());
			}

			if (row.getCell(12) != null && row.getCell(12).getStringCellValue().length() != 0) {
				event.setLiveUrl(row.getCell(12).getStringCellValue());
			}
			event.setApply(this.getApplyStatusList().get(1));
			event.setMemberID("後台新增");

			// Google Map回傳所在Lat, Lng
			if (event.getCounty() != null && event.getAddress() != null) {
				LatLng location = gpsService.getSimpleLocationFromAddress(event.getCounty() + event.getAddress());
				if (location != null) {
					event.setLat(BigDecimal.valueOf(location.lat));
					event.setLng(BigDecimal.valueOf(location.lng));
				}
			}
			eventImportList.add(event);
		}

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> tempModel = eventDAO.setEvent(eventImportList);
		if (tempModel.getCode() == 201) {

			EventCreateBatchResultInfoDto dto = new EventCreateBatchResultInfoDto();
			try {
				String outputTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss"));
				amazonService.setFolderName(this.updateExcelFolder);
				File outputFile = new File("EventBatchUpdate_" + outputTime + ".xlsx");
				outputFile.createNewFile();
				OutputStream o = new FileOutputStream(outputFile);
				outputBook.write(o);
				o.flush();
				o.close();
				outputBook.close();
				workbook.close();
				AmazonDto bean = amazonService.uploadFileNotEncrypted(outputFile);
				dto.setBatchUpdateUrl(bean.getUrl());
				dto.setBatchUpdateObjectKey(bean.getObjectKey());
				f.delete();
				outputFile.delete();
			} catch (IOException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
				transactionManager.rollback(status);
				return new ResponseModel<EventCreateBatchResultInfoDto>(500, e.getMessage());
			}
			transactionManager.commit(status);
			return new ResponseModel<EventCreateBatchResultInfoDto>(201, dto);
		} else {
			outputBook.close();
			workbook.close();
			transactionManager.rollback(status);
			return new ResponseModel<EventCreateBatchResultInfoDto>(tempModel.getCode(), tempModel.getErrMsg());
		}
	}

	/**
	 * Event batch update from previous creation result template
	 * 
	 * @param file
	 * @return
	 */
	public ResponseModel<Void> updateEventTag_Master_ImageByImportExcel(MultipartFile file) {

		XSSFWorkbook workbook = null;
		try {
			workbook = new XSSFWorkbook(file.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseModel<>(500, e.getMessage());
		}

		XSSFSheet tagSheet = workbook.getSheetAt(0);
		XSSFSheet recommendSheet = workbook.getSheetAt(1);
		XSSFSheet imageSheet = workbook.getSheetAt(2);

		List<String> eventIDList = new ArrayList<String>();
		List<EventTag> tagList = new ArrayList<EventTag>();
		for (int i = 1; i < tagSheet.getPhysicalNumberOfRows(); i++) {
			XSSFRow row = tagSheet.getRow(i);
			for (int c = 2; c < row.getPhysicalNumberOfCells(); c++) {
				if (row.getCell(c) != null && row.getCell(c).getStringCellValue().length() != 0) {
					EventTag tag = new EventTag();
					tag.setTagID(UUIDGenerator.generateType4UUID().toString());
					eventIDList.add(row.getCell(0).getStringCellValue());
					tag.setEventID(row.getCell(0).getStringCellValue());
					tag.setTag(row.getCell(c).getStringCellValue());
					tagList.add(tag);
				} else {
					continue;
				}
			}
		}

		List<EventRecommend> recommentList = new ArrayList<EventRecommend>();
		for (int i = 1; i < recommendSheet.getPhysicalNumberOfRows(); i++) {
			XSSFRow row = recommendSheet.getRow(i);
			for (int c = 2; c < row.getPhysicalNumberOfCells(); c++) {
				if (row.getCell(c) != null && row.getCell(c).getStringCellValue().length() != 0) {
					EventRecommend recommend = new EventRecommend();
					recommend.setRecommendID(UUIDGenerator.generateType4UUID().toString());
					recommend.setEventID(row.getCell(0).getStringCellValue());
					recommend.setMasterID(row.getCell(c).getStringCellValue());
					recommentList.add(recommend);
				} else {
					continue;
				}
			}
		}

		List<EventImageDto> imageList = new ArrayList<EventImageDto>();
		for (int i = 1; i < imageSheet.getPhysicalNumberOfRows(); i++) {
			XSSFRow row = imageSheet.getRow(i);
			for (int c = 2; c < row.getPhysicalNumberOfCells(); c++) {
				if (row.getCell(c) != null && row.getCell(c).getStringCellValue().length() != 0) {
					EventImageDto image = new EventImageDto();
					image.setImageID(UUIDGenerator.generateType4UUID().toString());
					image.setEventID(row.getCell(0).getStringCellValue());
					image.setImg(row.getCell(c).getStringCellValue());
					if (c == 2) {
						image.setIsMain(true);
					} else {
						image.setIsMain(false);
					}
					imageList.add(image);
				} else {
					continue;
				}
			}
		}

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> resultModel = null;

		try {
			resultModel = eventDAO.setEventTag(tagList);
			if (resultModel.getCode() != 201) {
				workbook.close();
				throw new Exception();
			}

			resultModel = eventDAO.setEventRecommend(recommentList);
			if (resultModel.getCode() != 201) {
				workbook.close();
				throw new Exception();
			}

			resultModel = eventDAO.updateEventImageAllNotMain(eventIDList);
			if (resultModel.getCode() != 204) {
				workbook.close();
				throw new Exception();
			}

			resultModel = eventDAO.setEventImage(imageList);
			if (resultModel.getCode() != 201) {
				workbook.close();
				throw new Exception();
			}

			transactionManager.commit(status);
			workbook.close();
			return new ResponseModel<Void>(201, "");
		} catch (Exception e) {
			transactionManager.rollback(status);
			return resultModel;
		}
	}

	/**
	 * Set event with photos
	 * 
	 * @param event
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws UnsupportedEncodingException
	 */
	public ResponseModel<Void> setEvent(EventCreateDto event) throws NoSuchAlgorithmException, UnsupportedEncodingException {

		List<String> tagList = event.getTagList();
		List<String> masterIDList = event.getMasterIDList();
		List<MultipartFile> files = event.getFiles();

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> resultModel = null;

		try {
			// Set eventID by uuid, insert event info
			String eventID = UUIDGenerator.generateType4UUID().toString();
			event.setEventID(eventID);

			// Google Map回傳所在LAT, LNG
			if (event.getCounty() != null && event.getAddress() != null) {
				LatLng location = gpsService.getSimpleLocationFromAddress(event.getCounty() + event.getAddress());
				if (location != null) {
					event.setLat(BigDecimal.valueOf(location.lat));
					event.setLng(BigDecimal.valueOf(location.lng));
				}
			}

			resultModel = eventDAO.setEvent(event);
			if (resultModel.getCode() != 201) {
				throw new Exception();
			}

			// Set Event Tag Dto list and insert.
			if (tagList != null && tagList.size() != 0) {
				List<EventTag> tagInsertList = new ArrayList<EventTag>();
				for (String t : tagList) {
					EventTag tag = new EventTag();
					tag.setEventID(event.getEventID());
					tag.setTag(t);
					tag.setTagID(UUIDGenerator.generateType4UUID().toString());
					tagInsertList.add(tag);
				}
				resultModel = eventDAO.setEventTag(tagInsertList);
				if (resultModel.getCode() != 201) {
					throw new Exception();
				}
			}

			// Set Event Recommend list and insert.
			if (masterIDList != null && masterIDList.size() != 0) {
				List<EventRecommend> reommendList = new ArrayList<EventRecommend>();
				for (String master : masterIDList) {
					EventRecommend recommend = new EventRecommend();
					recommend.setEventID(event.getEventID());
					recommend.setMasterID(master);
					String uuid = UUIDGenerator.generateType4UUID().toString();
					recommend.setRecommendID(uuid);
					reommendList.add(recommend);
				}
				resultModel = eventDAO.setEventRecommend(reommendList);
				if (resultModel.getCode() != 201) {
					throw new Exception();
				}
			}

			// Upload event image list to aws, and set Event Image Bean then insert it.
			if (files != null && files.size() != 0) {

				amazonService.setFolderName(new StringBuffer().append("Event").append("/").append(event.getEventID()).toString());
				try {
					List<AmazonDto> amazonList = amazonService.uploadFile(files);
					List<EventImageDto> eventImageList = new ArrayList<EventImageDto>();
					int index = 0;
					for (AmazonDto aws : amazonList) {
						EventImageDto img = new EventImageDto();
						img.setEventID(event.getEventID());
						img.setImg(aws.getUrl());
						img.setObjectKey(aws.getObjectKey());
						if (index == 0) {
							img.setIsMain(true);
						} else {
							img.setIsMain(false);
						}
						String uuid = UUIDGenerator.generateType4UUID().toString();
						img.setImageID(uuid);
						eventImageList.add(img);
						index++;
					}
					resultModel = eventDAO.setEventImage(eventImageList);
					if (resultModel.getCode() != 201) {
						throw new Exception();
					}
				} catch (IOException e) {
					resultModel = new ResponseModel<>(500, e.getMessage());
					throw new Exception();
				}
			}
			transactionManager.commit(status);
			return new ResponseModel<Void>(201, "");
		} catch (Exception e) {
			e.printStackTrace();
			transactionManager.rollback(status);
			return resultModel;
		}
	}

	/**
	 * Update event without images by eventID
	 * 
	 * @param eventID
	 * @param event
	 * @return
	 */
	public ResponseModel<Void> updateEvent(String eventID, EventUpdateDto event) {
		Boolean editableFlag = false;
		ResponseModel<Void> resultModel = null;
		List<String> tagList = event.getTagList();
		List<String> masterIDList = event.getMasterIDList();
		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		EventUpdateDto originEvent = eventDAO.getEventForUpdate(eventID);
		String origApply = originEvent.getApply();

		if (!event.getMemberID().equals("後台新增")) {
			if (origApply.equals(this.getApplyStatusList().get(0))) {
				LocalDateTime currentTime = LocalDateTime.now(ZoneId.of("Asia/Taipei"));
				boolean end = originEvent.getEndTime().isBefore(currentTime);
				if (end) {
					return new ResponseModel<Void>(500, "Event has expired.");
				} else {
					editableFlag = true;
				}
			} else if (origApply.equals(this.getApplyStatusList().get(1)) || origApply.equals(this.getApplyStatusList().get(2))) {
				editableFlag = true;
			} else if (origApply.equals(this.getApplyStatusList().get(3))) {
				return new ResponseModel<Void>(500, "Event apply has canceled by member.");
			}
		}

		if (event.getMemberID().equals("後台新增") || (originEvent.getMemberID().equals(event.getMemberID()) && editableFlag == true)) {
			try {
				utility.updateByDto(event, originEvent);
				resultModel = eventDAO.updateEvent(originEvent);
				if (resultModel.getCode() != 201) {
					throw new Exception();
				}

				if (tagList != null) {
					resultModel = eventDAO.deleteEventTagByEventID(eventID);
					if (resultModel.getCode() != 201) {
						throw new Exception();
					}

					List<EventTag> tagDtoList = new ArrayList<EventTag>();
					for (String tag : tagList) {
						EventTag tagBean = new EventTag();
						tagBean.setTag(tag);
						tagBean.setEventID(eventID);
						String uuid = UUIDGenerator.generateType4UUID().toString();
						tagBean.setTagID(uuid);
						tagDtoList.add(tagBean);
					}
					resultModel = eventDAO.setEventTag(tagDtoList);
					if (resultModel.getCode() != 201) {
						throw new Exception();
					}
				}

				if (masterIDList != null) {
					resultModel = eventDAO.deleteEventRecommendByEventID(eventID);
					if (resultModel.getCode() != 201) {
						throw new Exception();
					}
					List<EventRecommend> recBeanList = new ArrayList<EventRecommend>();
					for (String matser : masterIDList) {
						EventRecommend recBean = new EventRecommend();
						recBean.setMasterID(matser);
						recBean.setEventID(eventID);
						String uuid = UUIDGenerator.generateType4UUID().toString();
						recBean.setRecommendID(uuid);
						recBeanList.add(recBean);
					}
					resultModel = eventDAO.setEventRecommend(recBeanList);
					if (resultModel.getCode() != 201) {
						throw new Exception();
					}
				}
				transactionManager.commit(status);
				return new ResponseModel<Void>(204, "");
			} catch (Exception e) {
				e.printStackTrace();
				transactionManager.rollback(status);
				return resultModel;
			}
		} else {
			return new ResponseModel<Void>(500, ApiError.UNAUTHORIZED);
		}
	}

	/**
	 * Update event images by eventID
	 * 
	 * @param eventID
	 * @param changeUuidList
	 * @param files
	 * @return
	 */
	public ResponseModel<Void> updateEventImage(String eventID, List<String> changeUuidList, List<MultipartFile> files) {

		// if there is no files for updating, return 0;
		if (files == null || files != null && files.size() == 0) {
			return new ResponseModel<>(401, ApiError.PARARMETER_NOT_FOUND);
		}

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> resultModel = null;

		try {

			// if thers is no images need to update.
			if (changeUuidList == null || changeUuidList != null && changeUuidList.size() == 0) {

				List<EventImageDto> imgList = eventDAO.getEventImage(eventID);
				// check if image list under event exits.
				// if not exists, set 1st image as main.
				if (imgList == null) {

					amazonService.setFolderName(new StringBuffer().append("Event").append("/").append(eventID).toString());
					try {
						List<AmazonDto> amazonList = amazonService.uploadFile(files);
						List<EventImageDto> eventImageList = new ArrayList<EventImageDto>();
						int index = 0;
						for (AmazonDto aws : amazonList) {

							EventImageDto img = new EventImageDto();
							img.setEventID(eventID);
							img.setImg(aws.getUrl());
							img.setObjectKey(aws.getObjectKey());
							if (index == 0) {
								img.setIsMain(true);
							} else {
								img.setIsMain(false);
							}
							String uuid = UUIDGenerator.generateType4UUID().toString();
							img.setImageID(uuid);
							eventImageList.add(img);
							index++;
						}
						resultModel = eventDAO.setEventImage(eventImageList);
						if (resultModel.getCode() != 201) {
							throw new Exception();
						}
					} catch (IOException e) {
						resultModel = new ResponseModel<Void>(500, e.getMessage());
						throw new Exception();
					}

				}
				// if exists, for all file newly upadte set 'isMain' attribute as false
				else {

					amazonService.setFolderName(new StringBuffer().append("Event").append("/").append(eventID).toString());
					try {
						List<AmazonDto> amazonList = amazonService.uploadFile(files);
						List<EventImageDto> eventImageList = new ArrayList<EventImageDto>();
						for (AmazonDto aws : amazonList) {

							EventImageDto img = new EventImageDto();
							img.setEventID(eventID);
							img.setImg(aws.getUrl());
							img.setObjectKey(aws.getObjectKey());
							img.setIsMain(false);
							String uuid = UUIDGenerator.generateType4UUID().toString();
							img.setImageID(uuid);
							eventImageList.add(img);
						}
						resultModel = eventDAO.setEventImage(eventImageList);
						if (resultModel.getCode() != 201) {
							throw new Exception();
						}
					} catch (IOException e) {
						resultModel = new ResponseModel<Void>(500, e.getMessage());
						throw new Exception();
					}

				}
			}
			// if it needs update images.
			else {
				List<EventImageDto> imgList = eventDAO.getEventImage(changeUuidList);
				// delete first.
				resultModel = eventDAO.deleteEventImage(changeUuidList);
				if (resultModel.getCode() != 201) {
					throw new Exception();
				}

				// setting upload folder.
				amazonService.setFolderName(new StringBuffer().append("Event").append("/").append(eventID).toString());
				try {
					List<AmazonDto> amazonList = amazonService.uploadFile(files);
					List<EventImageDto> eventImageList = new ArrayList<EventImageDto>();
					int index = 0;
					for (AmazonDto aws : amazonList) {

						EventImageDto img = new EventImageDto();
						img.setEventID(eventID);
						img.setImg(aws.getUrl());
						img.setObjectKey(aws.getObjectKey());
						// check if what it should be changed has main image.
						// if there is main page, it should be match the 1st image og incoming file
						// list.
						if (index == 0 && imgList.get(0).getIsMain().toString().equalsIgnoreCase("true")) {
							img.setIsMain(true);
						} else {
							img.setIsMain(false);
						}
						String uuid = UUIDGenerator.generateType4UUID().toString();
						img.setImageID(uuid);
						eventImageList.add(img);
						index++;
					}
					resultModel = eventDAO.setEventImage(eventImageList);
					if (resultModel.getCode() != 201) {
						throw new Exception();
					}
				} catch (IOException e) {
					resultModel = new ResponseModel<Void>(500, e.getMessage());
					throw new Exception();
				}
			}

			transactionManager.commit(status);
			return new ResponseModel<Void>(201, "");
		} catch (Exception e) {
			e.printStackTrace();
			transactionManager.rollback(status);
			return resultModel;
		}
	}

	/**
	 * Upload single event image by eventID
	 * 
	 * @param eventID
	 * @param image
	 * @return
	 */
	public ResponseModel<Void> setEventImageByEventID(String eventID, EventImageItemCreateDto image) {
		image.setEventID(eventID);
		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> resultModel = null;

		// setting upload folder.
		amazonService.setFolderName(new StringBuffer().append("Event").append("/").append(eventID).toString());
		try {
			AmazonDto aws = amazonService.uploadFile(image.getFile());
			image.setImg(aws.getUrl());
			image.setObjectKey(aws.getObjectKey());
		} catch (IOException e1) {
			e1.printStackTrace();
			return new ResponseModel<>(500, e1.getMessage());
		}

		try {
			image.setImageID(UUIDGenerator.generateType4UUID().toString());
			resultModel = eventDAO.setEventImageByEventID(image);
			if (resultModel.getCode() != 201) {
				throw new Exception();
			}

			transactionManager.commit(status);
			return new ResponseModel<Void>(201, "");
		} catch (Exception e) {
			e.printStackTrace();
			transactionManager.rollback(status);
			return resultModel;
		}
	}

	/**
	 * Set event image as main photo by imageID
	 * 
	 * @param imageID
	 * @return
	 */
	public ResponseModel<Void> setEventImageAsMainByImageID(String imageID) {

		String eventID = eventDAO.getEventIDByImageID(imageID);
		if (eventID == null) {
			return new ResponseModel<>(404, ApiError.DATA_NOT_FOUND);
		}

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> resultModel = null;

		try {
			resultModel = eventDAO.setEventImageAllAsNotMainByEventID(eventID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			resultModel = eventDAO.setEventImageAsMainByImageID(imageID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			transactionManager.commit(status);
			return new ResponseModel<Void>(204, "");
		} catch (Exception e) {
			e.printStackTrace();
			transactionManager.rollback(status);
			return resultModel;
		}

	}

	/**
	 * Delete event image by imageID
	 * 
	 * @param imageID
	 * @return
	 */
	public ResponseModel<Void> deleteEventImgByImageID(String imageID) {

		EventImageDto image = eventDAO.getEventImageByImageID(imageID);
		if (image == null) {
			return new ResponseModel<>(404, ApiError.DATA_NOT_FOUND);
		}

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> resultModel = null;
		String originObjectKey = image.getObjectKey();

		try {

			resultModel = eventDAO.deleteEventImgByImageID(imageID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			resultModel = amazonService.deleteFileReturnResponse(originObjectKey);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			transactionManager.commit(status);
			return new ResponseModel<Void>(204, "");
		} catch (Exception e) {
			e.printStackTrace();
			transactionManager.rollback(status);
			return resultModel;
		}
	}

	/**
	 * Update event apply status by eventID
	 * 
	 * @param argsDto
	 * @return
	 */
	public ResponseModel<Void> updateEventApplyStatus(String eventID, EventApplyDto argsDto) {
		argsDto.setEventID(eventID);
		if (argsDto.getIsPass()) {
			argsDto.setApplyStatus(this.getApplyStatusList().get(1));
			argsDto.setRefuseReason(null);
		} else {
			argsDto.setApplyStatus(this.getApplyStatusList().get(2));
		}
		return eventDAO.updateEventApplyStatus(argsDto);
	}

	/**
	 * Update event followed by member
	 * 
	 * @param follow
	 * @return Message of Doing result;
	 */
	public ResponseModel<Void> updateEventFollowByMemberID(EventFollowedDto follow) {

		Boolean ifFollowed = eventDAO.getEventIfMemberFollowed(follow);

		if (ifFollowed == true && follow.getWannaFollow() == true) {
			return new ResponseModel<Void>(500, ApiError.EVENT_HAS_FOLLOWED);
		} else if (ifFollowed == false && follow.getWannaFollow() == false) {
			return new ResponseModel<Void>(500, ApiError.EVENT_HAS_NOT_FOLLOWED);
		} else if (ifFollowed == null) {
			return new ResponseModel<Void>(500, ApiError.DATA_ACCESS_ERROR);
		}

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> resultModel = null;

		try {

			resultModel = eventDAO.updateEventFollowed(follow.getEventID(), follow.getWannaFollow());
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			if (follow.getWannaFollow() == true) {
				resultModel = eventDAO.setEventFollowed(follow);
				if (resultModel.getCode() != 201) {
					throw new Exception();
				}
			} else {
				resultModel = eventDAO.deleteEventFollowed(follow);
				if (resultModel.getCode() != 201) {
					throw new Exception();
				}
			}

			transactionManager.commit(status);
			return new ResponseModel<Void>(201, "");
		} catch (Exception e) {
			transactionManager.rollback(status);
			return resultModel;
		}

	}

	/**
	 * Get pinned event type by memberID.
	 * 
	 * @param memberID
	 * @return
	 */
	public List<PinEventTypeInfoDto> getPinEventTypeListByMemberID(String memberID) {
		return eventDAO.getPinEventTypeListByMemberID(memberID);
	}

	/**
	 * Update pinned event type by memberID.
	 * 
	 * @param memberID
	 * @param list
	 * @return
	 */
	public ResponseModel<Void> updatePinEventTypeList(String memberID, List<PinEventTypeUpdateDto> list) {

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> resultModel = null;

		try {

			List<String> uuidList = eventDAO.getPinEventTypeUUIDListByMemberID(memberID);
			if (uuidList != null && uuidList.size() != 0) {
				resultModel = eventDAO.deletePinEventTypeList(uuidList);
				if (resultModel.getCode() != 201) {
					throw new Exception();
				}
			}

			for (PinEventTypeUpdateDto dto : list) {
				String uuid = UUIDGenerator.generateType4UUID().toString();
				dto.setUuid(uuid);
				dto.setMemberID(memberID);
			}

			resultModel = eventDAO.setPinEventTypeList(list);
			if (resultModel.getCode() != 201) {
				throw new Exception();
			}

			transactionManager.commit(status);
			return new ResponseModel<Void>(204, "");
		} catch (Exception e) {
			transactionManager.rollback(status);
			return resultModel;
		}
	}

	/**
	 * Delete event all information cascade with other related table by eventID
	 * 
	 * @param eventID
	 * @return
	 */
	public ResponseModel<Void> deleteEventByEventID(String eventID) {

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> resultModel = null;
		// Retrieve all object keys belongs to this event.
		List<String> objectKeyList = eventDAO.getEventImageObjectkey(eventID);
		try {
			resultModel = eventDAO.deleteEventFollowedByEventID(eventID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			resultModel = eventDAO.deleteEventRecommendByEventID(eventID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			resultModel = eventDAO.deleteEventTagByEventID(eventID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			resultModel = eventDAO.deleteEventImgByEventID(eventID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			resultModel = eventDAO.deleteEventByEventID(eventID);
			if (resultModel.getCode() != 204) {
				throw new Exception();
			}

			transactionManager.commit(status);
			// Delete event images from AWS S3
			if (objectKeyList != null && objectKeyList.size() != 0) {
				amazonService.deleteFile(objectKeyList);
			}
			return new ResponseModel<Void>(204, "");
		} catch (Exception e) {
			transactionManager.rollback(status);
			return resultModel;
		}
	}

	/**
	 * Set member applied event cancel
	 * 
	 * @param eventID
	 * @param memberID
	 * @return
	 */
	public ResponseModel<Void> setEventCancelByMemberID(String eventID, String memberID) {
		ResponseModel<Void> resultModel = eventDAO.setEventCancelByMemberID(eventID, memberID);
		if (resultModel.getCode() != 204) {
			return new ResponseModel<Void>(resultModel.getCode(), resultModel.getErrMsg());
		}
		return resultModel;
	}

	public EventSearchAppliedInfoDto getEventApplyStatus(String eventID, String memberID) {
		EventSearchAppliedInfoDto event = eventDAO.getEventApplyStatus(eventID, memberID);
		return setEventStatus(event);
	}

	public List<EventSearchAppliedInfoDto> getAppliedEventListByMemberID(String memberID) {
		List<EventSearchAppliedInfoDto> dto = eventDAO.getAppliedEventListByMemberID(memberID);
		if (dto != null && dto.size() > 0) {
			for (EventSearchAppliedInfoDto event : dto) {
				setEventStatus(event);
			}
			return dto;
		} else {
			return null;
		}
	}

//	列表資訊
	public EventSearchAppliedInfoDto setEventStatus(EventSearchAppliedInfoDto dto) {
		if (dto != null) {
			LocalDateTime currentTime = LocalDateTime.now(ZoneId.of("Asia/Taipei"));
			String apply = dto.getApply();
			if (apply.equals(this.getApplyStatusList().get(0))) {
				boolean end = dto.getEndTime().isBefore(currentTime);
				if (end) {
					dto.setApply(this.getApplyStatusList().get(4));
				} else {
					dto.setCancelable(true);
				}
			} else if (apply.equals(this.getApplyStatusList().get(1))) {
				dto.setCancelable(true);
			}
		}
		return dto;
	}

//	詳細資訊
	public EventAdminDto setEventStatus(EventAdminDto dto) {
		if (dto != null) {
			LocalDateTime currentTime = LocalDateTime.now(ZoneId.of("Asia/Taipei"));
			String apply = dto.getApply();
			if (apply.equals(this.getApplyStatusList().get(0))) {
				boolean end = dto.getEndTime().isBefore(currentTime);
				if (end) {
					dto.setApply(this.getApplyStatusList().get(4));
				} else {
					dto.setEditable(true);
					dto.setInsertTime(currentTime);
				}
			} else if (apply.equals(this.getApplyStatusList().get(1)) || apply.equals(this.getApplyStatusList().get(2))) {
				dto.setEditable(true);
				dto.setApply(this.getApplyStatusList().get(0));
				dto.setInsertTime(currentTime);
			}
		}
		return dto;
	}

	private EventSearchInfoDto calculator(BigDecimal user_lat, BigDecimal user_lng, EventSearchInfoDto event) {
		Double distance = utility.calculateDistance(user_lat.doubleValue(), user_lng.doubleValue(), event.getLat().doubleValue(),
				event.getLng().doubleValue());
		event.setDistance(distance);
		return event;
	}

}
