package tw.com.reinbach.Atenore.Swagger;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.context.annotation.Primary;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import io.swagger.annotations.ApiImplicitParam;
import springfox.documentation.service.Parameter;
import springfox.documentation.swagger2.mappers.ServiceModelToSwagger2MapperImpl;

/** Set if @ApiImplicitParam attribute hidden effect.
 * 
 * @author Sora
 *
 */
@Primary 
@Order(Ordered.HIGHEST_PRECEDENCE)
@Component("ServiceModelToSwagger2Mapper")
public class CustomModelToSwaggerMapper extends ServiceModelToSwagger2MapperImpl{
	
	@Override
	protected List<io.swagger.models.parameters.Parameter> parameterListToParameterList(List<Parameter> list){
		list = list.stream().filter(p->
		!"hidden".equals(p.getParamAccess())
		).collect(Collectors.toList());
		return super.parameterListToParameterList(list);
	}
	
}
