package tw.com.reinbach.Atenore.Model.marketing.query.search;

import java.time.LocalDateTime;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;

public class AdsSearchArgsDto {

	@ApiModelProperty(notes = "ads type")
	private String type;
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime startTime;
	
	@ApiModelProperty(notes = "alias keywors")
	private String fuzzyFilter;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public String getFuzzyFilter() {
		return fuzzyFilter;
	}

	public void setFuzzyFilter(String fuzzyFilter) {
		this.fuzzyFilter = fuzzyFilter;
	}
}
