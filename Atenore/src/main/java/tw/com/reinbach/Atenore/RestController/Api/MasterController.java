package tw.com.reinbach.Atenore.RestController.Api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import tw.com.reinbach.Atenore.Model.category.CategoryService;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterTypeSencondaryCascadeDto;
import tw.com.reinbach.Atenore.Model.master.MasterService;
import tw.com.reinbach.Atenore.Model.master.component.MasterDto;
import tw.com.reinbach.Atenore.Model.master.component.MasterRecommend;
import tw.com.reinbach.Atenore.Model.master.component.MasterTag;
import tw.com.reinbach.Atenore.Model.master.query.MasterInfoDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterAliveSearchArgsDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterAliveSearchInfoDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterDeadSearchArgsDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterDeadSearchInfoDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterHitSearchArgsDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterHitSearchInfoDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterSearchArgsDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterSearchInfoDto;
import tw.com.reinbach.Atenore.Model.master.update.MasterFollowedDto;
import tw.com.reinbach.Atenore.Model.search.SearchPreferDto;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;

@RestController
public class MasterController {

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private MasterService masterService;

	/**
	 * Get master list by searching filter
	 * 
	 * @param argsDto
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:9", value = "Get master list by searching filter")
	@RequestMapping(value = { "/web/master/list", "/app/master/list" }, method = { RequestMethod.GET })
	@ApiImplicitParam(name = "Authorization", required = false, paramType = "header", dataType = "String", access = "hidden")
	public ResponseModel<List<MasterSearchInfoDto>> getMasterListWithFilter(MasterSearchArgsDto argsDto) {

		List<MasterSearchInfoDto> list = masterService.getMasterListWithFilter(argsDto);
		if (list == null || list.size() == 0) {
			return new ResponseModel<List<MasterSearchInfoDto>>(404, ApiError.DATA_NOT_FOUND);
		}
		return new ResponseModel<List<MasterSearchInfoDto>>(200, list);
	}

	/**
	 * Get master hit list by hit prefer SN, order by distance between user and
	 * master
	 * 
	 * @param argsDto
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:10", value = "Get master hit list by hit prefer SN, order by distance between user and master")
	@RequestMapping(value = { "/web/master/hit/list", "/app/matser/hit/list" }, method = RequestMethod.GET)
	@ApiImplicitParam(name = "Authorization", required = false, paramType = "header", dataType = "String", access = "hidden")
	public ResponseModel<List<MasterHitSearchInfoDto>> getMasterListByHit(MasterHitSearchArgsDto argsDto) {
		List<MasterHitSearchInfoDto> list = masterService.getMasterListByHitSearchPreferSN(argsDto);
		if (list == null || list.size() == 0) {
			return new ResponseModel<List<MasterHitSearchInfoDto>>(404, ApiError.DATA_NOT_FOUND);
		}
		return new ResponseModel<List<MasterHitSearchInfoDto>>(200, list);
	}

	/**
	 * Get master hit searching prefer list
	 * 
	 * @return
	 */
	@ApiImplicitParam(name = "Authorization", required = false, paramType = "header", dataType = "String", access = "hidden")
	@ApiOperation(notes = "ApiNo:10-1", value = "Get master hit searching prefer list")
	@RequestMapping(value = { "/web/master/hit/search/prefer", "/app/master/hit/search/prefer" }, method = RequestMethod.GET)
	public ResponseModel<List<SearchPreferDto>> getMasterHitSearcingPreferList() {
		return new ResponseModel<List<SearchPreferDto>>(200, masterService.getMasterHitSearcingPreferList());
	}

	/**
	 * Get master alive list
	 * 
	 * @param argsDto
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:11", value = "Get master alive list.")
	@RequestMapping(value = { "/web/master/elite", "/app/master/elite" }, method = RequestMethod.GET)
	@ApiImplicitParam(name = "Authorization", required = false, paramType = "header", dataType = "String", access = "hidden")
	public ResponseModel<List<MasterAliveSearchInfoDto>> getMasterAliveListWithFilter(MasterAliveSearchArgsDto argsDto) {
		List<MasterAliveSearchInfoDto> list = masterService.getMasterAliveListWithFilter(argsDto);
		if (list == null || list.size() == 0) {
			return new ResponseModel<List<MasterAliveSearchInfoDto>>(404, ApiError.DATA_NOT_FOUND);
		}
		return new ResponseModel<List<MasterAliveSearchInfoDto>>(200, list);
	}

	/**
	 * "Get master dead list
	 * 
	 * @param argsDto
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:12", value = "Get master dead list.")
	@RequestMapping(value = { "/web/master/legend", "/app/master/legend" }, method = RequestMethod.GET)
	@ApiImplicitParam(name = "Authorization", required = false, paramType = "header", dataType = "String", access = "hidden")
	public ResponseModel<List<MasterDeadSearchInfoDto>> getMasterDeadListWithFilter(MasterDeadSearchArgsDto argsDto) {
		List<MasterDeadSearchInfoDto> list = masterService.getMasterDeadListWithFilter(argsDto);
		if (list == null || list.size() == 0) {
			return new ResponseModel<List<MasterDeadSearchInfoDto>>(404, ApiError.DATA_NOT_FOUND);
		}
		return new ResponseModel<List<MasterDeadSearchInfoDto>>(200, list);
	}

	/**
	 * Get master type cascade list
	 * 
	 * @return
	 */
	@ApiImplicitParam(name = "Authorization", required = false, paramType = "header", dataType = "String", access = "hidden")
	@ApiOperation(notes = "ApiNo:13", value = "Get master type cascade list")
	@RequestMapping(value = { "/web/master/type", "/app/matser/type" }, method = RequestMethod.GET)
	public ResponseModel<List<MasterTypeSencondaryCascadeDto>> getMasterTypeCascadeWithMainType() {
		List<MasterTypeSencondaryCascadeDto> list = categoryService.getMasterTypeCascadeWithMainType();
		if (list == null || list.size() == 0) {
			return new ResponseModel<List<MasterTypeSencondaryCascadeDto>>(404, ApiError.DATA_NOT_FOUND);
		}
		return new ResponseModel<List<MasterTypeSencondaryCascadeDto>>(200, list);
	}

	/**
	 * Get master info alive
	 * 
	 * @param masterID
	 * @param memberID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:14", value = "Get master info alive.")
	@RequestMapping(value = { "/web/master/elite/{masterID}", "/app/master/elite/{masterID}" }, method = RequestMethod.GET)
	@ApiImplicitParam(name = "Authorization", required = false, paramType = "header", dataType = "String", access = "hidden")
	public ResponseModel<MasterInfoDto> getMasterInfoAlive(
			@ApiParam(required = true, example = "75c6538b-facd-461b-b143-919c0913029e") @PathVariable(required = true) String masterID,
			@ApiParam(required = false, example = "75c6538b-facd-461b-b143-919c0913028c") @RequestParam(required = false) String memberID) {
		MasterDto masterInfo = masterService.getMaster(masterID, true, memberID);
		if (masterInfo == null) {
			return new ResponseModel<MasterInfoDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			List<MasterTag> masterTagList = masterService.getMasterTagListByMasterID(masterID);
			List<MasterRecommend> recommendList = masterService.getMasterRecommendListByMasterID(masterID);
			MasterInfoDto master = new MasterInfoDto();
			master.setMaster(masterInfo);
			master.setTagList(masterTagList);
			master.setRecommendList(recommendList);
			return new ResponseModel<MasterInfoDto>(200, master);
		}
	}

	/**
	 * "Get master info dead
	 * 
	 * @param masterID
	 * @param memberID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:15", value = "Get master info dead.")
	@RequestMapping(value = { "/web/master/legend/{masterID}", "/app/master/legend/{masterID}" }, method = RequestMethod.GET)
	@ApiImplicitParam(name = "Authorization", required = false, paramType = "header", dataType = "String", access = "hidden")
	public ResponseModel<MasterInfoDto> getMasterInfoDead(
			@ApiParam(required = true, example = "90c6538b-facd-461b-b143-919c09130761") @PathVariable(required = true) String masterID,
			@ApiParam(required = false, example = "75c6538b-facd-461b-b143-919c0913028c") @RequestParam(required = false) String memberID) {
		MasterDto masterInfo = masterService.getMaster(masterID, false, memberID);
		if (masterInfo == null) {
			return new ResponseModel<MasterInfoDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			List<MasterTag> masterTagList = masterService.getMasterTagListByMasterID(masterID);
			List<MasterRecommend> recommendList = masterService.getMasterRecommendListByMasterID(masterID);
			MasterInfoDto master = new MasterInfoDto();
			master.setMaster(masterInfo);
			master.setTagList(masterTagList);
			master.setRecommendList(recommendList);
			return new ResponseModel<MasterInfoDto>(200, master);
		}
	}

	@ApiOperation(notes = "ApiNo:W1", value = "Get master info whatever alive.")
	@RequestMapping(value = { "/web/master/{masterID}" }, method = RequestMethod.GET)
	public ResponseModel<MasterInfoDto> getMasterInfo(
			@ApiParam(required = true, example = "75c6538b-facd-461b-b143-919c0913029e") 
			@PathVariable(required = true) String masterID,
			@RequestParam(required = false)String memberID) {
		MasterInfoDto master = masterService.getMasterFullyByMasterID(masterID, memberID);
		if (master == null) {
			return new ResponseModel<MasterInfoDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<MasterInfoDto>(200, master);
		}
	}

	/**
	 * Master followed by Member
	 * 
	 * @param follow
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:16", value = "Set master followed by member.")
	@RequestMapping(value = { "/web/master/follow", "/app/master/follow" }, method = { RequestMethod.POST })
	public ResponseModel<Void> setEventFollowed(MasterFollowedDto follow) {
		follow.setWannaFollow(true);
		ResponseModel<Void> resultModel = masterService.updateMasterFollowByMemberID(follow);
		if (resultModel.getCode() == 201) {
			return new ResponseModel<Void>(204, "Master followed success.");
		} else {
			return resultModel;
		}
	}

	/**
	 * Master cancel followed by Member
	 * 
	 * @param follow
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:17", value = "Delete master followed by member.")
	@RequestMapping(value = { "/web/master/follow", "/app/master/follow" }, method = { RequestMethod.DELETE })
	public ResponseModel<Void> deleteEventFollowed(MasterFollowedDto follow) {
		follow.setWannaFollow(false);
		ResponseModel<Void> resultModel = masterService.updateMasterFollowByMemberID(follow);
		if (resultModel.getCode() == 201) {
			return new ResponseModel<Void>(204, "Master cancel followed success.");
		} else {
			return resultModel;
		}
	}

}
