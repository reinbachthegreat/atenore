package tw.com.reinbach.Atenore.Model.event;

import java.util.List;

import tw.com.reinbach.Atenore.Model.event.component.EventAdminDto;
import tw.com.reinbach.Atenore.Model.event.component.EventDto;
import tw.com.reinbach.Atenore.Model.event.component.EventImageDto;
import tw.com.reinbach.Atenore.Model.event.component.EventRecommend;
import tw.com.reinbach.Atenore.Model.event.component.EventTag;
import tw.com.reinbach.Atenore.Model.event.component.PinEventTypeInfoDto;
import tw.com.reinbach.Atenore.Model.event.create.EventCreateDto;
import tw.com.reinbach.Atenore.Model.event.create.EventImageItemCreateDto;
import tw.com.reinbach.Atenore.Model.event.query.EventNotReviewCountDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventNearBySearchArgsDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchAppliedInfoDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchArgsAdminDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchArgsDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchInfoAdminDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchInfoDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchReviewArgsAdminDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchReviewInfoAdminDto;
import tw.com.reinbach.Atenore.Model.event.update.EventApplyDto;
import tw.com.reinbach.Atenore.Model.event.update.EventFollowedDto;
import tw.com.reinbach.Atenore.Model.event.update.EventUpdateDto;
import tw.com.reinbach.Atenore.Model.event.update.PinEventTypeUpdateDto;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;

public interface EventDAO {

	public List<EventDto> getEventList();

	public List<EventSearchInfoDto> getEventListWithFilter(EventSearchArgsDto argsDto);
	
	public List<EventSearchInfoDto> getEventListNearBy(EventNearBySearchArgsDto argsDto);

//	public List<EventDto> getEventListWithFilter(String eventFilter, LocalDateTime startTime, LocalDateTime endTime);

	public EventDto getEvent(String eventID);

	public EventUpdateDto getEventForUpdate(String eventID);

	public EventAdminDto getEventAll(String eventID);

	public EventDto getEventWithMemberID(String eventID, String memberID);

//	public List<EventDto> getEventListNotReview(String notReviewedString);
//	
//	public List<EventDto> getEventListNotReview(String eventFilter, LocalDateTime startTime, LocalDateTime endTime, String notReviewedString);

	public List<EventTag> getEventTag(String eventID);

	public List<EventTag> getEventTagWithoutEventID(String eventID);

	public List<EventRecommend> getEventRecommend(String eventID);

	public List<EventRecommend> getEventRecommendWithoutEventID(String eventID);

	public List<EventImageDto> getEventImage(String eventID);

	public List<EventImageDto> getEventImage(List<String> uuidList);

	public EventImageDto getEventImageByImageID(String imageID);

	public List<String> getEventImageObjectkey(String eventID);

	public ResponseModel<Void> setEvent(EventCreateDto event);

	public ResponseModel<Void> setEvent(List<EventCreateDto> list);

	public ResponseModel<Void> setEventTag(List<EventTag> list);

	public ResponseModel<Void> setEventRecommend(List<EventRecommend> list);

	public ResponseModel<Void> setEventImage(List<EventImageDto> list);

//	public ResponseModel<Void> updateEvent(EventDto event);

	public ResponseModel<Void> updateEvent(EventUpdateDto event);

	public ResponseModel<Void> deleteEventImage(List<String> uuidList);

	public Boolean getEventIfMemberFollowed(EventFollowedDto follow);

	public ResponseModel<Void> setEventFollowed(EventFollowedDto follow);

	public ResponseModel<Void> deleteEventFollowed(EventFollowedDto follow);

	public ResponseModel<Void> updateEventFollowed(String eventID, Boolean wannaFollow);

	public List<PinEventTypeInfoDto> getPinEventTypeListByMemberID(String memberID);

	public List<String> getPinEventTypeUUIDListByMemberID(String memberID);

	public ResponseModel<Void> deletePinEventTypeList(List<String> uuidList);

	public ResponseModel<Void> setPinEventTypeList(List<PinEventTypeUpdateDto> list);

	public ResponseModel<Void> updateEventImageAllNotMain(String eventID);

	public ResponseModel<Void> updateEventImageAllNotMain(List<String> eventIDList);

	public ResponseModel<Void> deleteEventFollowedByEventID(String eventID);

	public ResponseModel<Void> deleteEventTagByEventID(String eventID);

	public ResponseModel<Void> deleteEventRecommendByEventID(String eventID);

	public ResponseModel<Void> deleteEventImgByEventID(String eventID);

	public ResponseModel<Void> deleteEventByEventID(String eventID);

	public List<EventSearchAppliedInfoDto> getAppliedEventListByMemberID(String memberID);

	public EventSearchAppliedInfoDto getEventApplyStatus(String eventID, String memberID);

	public ResponseModel<Void> setEventCancelByMemberID(String eventID, String memberID);

	public ResponseModel<Void> updateEventClick(String eventID);

	/**
	 * ADMIN
	 * 
	 */
	public List<EventSearchInfoAdminDto> getEventListWithFilter(EventSearchArgsAdminDto argsDto);

	public ResponseModel<Void> updateEventApplyStatus(EventApplyDto argsDto);

	public EventNotReviewCountDto getEventListNotReviewCount(EventSearchReviewArgsAdminDto argsDto);

	public List<EventSearchReviewInfoAdminDto> getEventListNotReview(EventSearchReviewArgsAdminDto argsDto);

	public ResponseModel<Void> deleteEventImgByImageID(String imageID);

	public ResponseModel<Void> setEventImageByEventID(EventImageItemCreateDto image);

	public ResponseModel<Void> setEventImageAllAsNotMainByEventID(String eventID);

	public ResponseModel<Void> setEventImageAsMainByImageID(String imageID);

	public String getEventIDByImageID(String imageID);
}
