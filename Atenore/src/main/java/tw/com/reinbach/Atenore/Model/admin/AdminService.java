package tw.com.reinbach.Atenore.Model.admin;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import javassist.tools.framedump;
import tw.com.reinbach.Atenore.Model.admin.component.FeatureLimitDto;
import tw.com.reinbach.Atenore.Model.admin.component.LimitDto;
import tw.com.reinbach.Atenore.Model.admin.create.AdminCreateDto;
import tw.com.reinbach.Atenore.Model.admin.create.FeatureCreateDto;
import tw.com.reinbach.Atenore.Model.admin.query.AdminInfoDto;
import tw.com.reinbach.Atenore.Model.admin.query.FeatureInfoDto;
import tw.com.reinbach.Atenore.Model.admin.query.FeatureListInfoDto;
import tw.com.reinbach.Atenore.Model.admin.query.LimitListInfoDto;
import tw.com.reinbach.Atenore.Model.admin.query.search.AdminSearchInfoDto;
import tw.com.reinbach.Atenore.Model.admin.query.search.AdminSearchListInfoDto;
import tw.com.reinbach.Atenore.Model.admin.update.AdminUpdateDto;
import tw.com.reinbach.Atenore.Model.admin.update.FeatureUpdateDto;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;
import tw.com.reinbach.Atenore.Utility.CipherUtility;
import tw.com.reinbach.Atenore.Utility.UUIDGenerator;
import tw.com.reinbach.Atenore.Utility.Utility;

@Service
public class AdminService {

	@Autowired
	private AdminDAO adminDAO;
	
	@Autowired
	private Utility utility;
	
	@Autowired
	private PlatformTransactionManager transactionManager;

	/** Get ADMIN list by featureID filter
	 * 
	 * @param featureID
	 * @return
	 */
	public AdminSearchListInfoDto getAdminByFeatureID(String featureID) {
		List<AdminSearchInfoDto> list = adminDAO.getAdminByFeatureID(featureID);
		if(list!=null && list.size()!=0) {
			AdminSearchListInfoDto dto = new AdminSearchListInfoDto();
			dto.setAdminList(list);
			return dto;
		}else {
			return null;
		}
	}
	
	/** Get ADMIN info by adminID
	 * 
	 * @param adminID
	 * @return
	 */
	public AdminInfoDto getAdminByAdminID(String adminID) {
		return adminDAO.getAdminByAdminID(adminID);
	}
	
	/** Create ADMIN info
	 * 
	 * @param admin
	 * @return
	 * @throws Throwable 
	 */
	public ResponseModel<Void> createAdmin(AdminCreateDto admin) throws Throwable{
		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> resultModel = null;
		
		String adminID = UUIDGenerator.generateType4UUID().toString(); 
		admin.setAdminID(adminID);
		String pwd = CipherUtility.encryptStringCommonKey(admin.getAccount(), admin.getPwd()); 
		admin.setPwd(pwd);
		
		try {
			resultModel = adminDAO.setAdmin(admin);
			if(resultModel.getCode()!=201) {
				throw new Exception();
			}
			
			transactionManager.commit(status);
			return new ResponseModel<Void>(201, "");
		}catch(Exception e) {
			transactionManager.rollback(status);
			return resultModel;
		}
	}
	
	/** Update ADMIN info
	 * 
	 * @param adminID
	 * @param formAdmin
	 * @return
	 * @throws Throwable 
	 */
	public ResponseModel<Void> updateAdmin(String adminID, AdminUpdateDto formAdmin) throws Throwable{
		
		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> resultModel = null;
		
		AdminUpdateDto originalAdmin = adminDAO.getAdminForUpdateByAdminID(adminID);
		formAdmin.setAdminID(adminID);
		if(formAdmin.getPwd()!=null) {
			String pwd = CipherUtility.encryptStringCommonKey(formAdmin.getAccount(), formAdmin.getPwd()); 
			formAdmin.setPwd(pwd);			
		}
		
		utility.updateByDto(formAdmin, originalAdmin);
		
		try {
			resultModel = adminDAO.updateAdmin(originalAdmin);
			if(resultModel.getCode()!=204) {
				throw new Exception();
			}
			
			transactionManager.commit(status);
			return new ResponseModel<Void>(204, "");
		}catch(Exception e) {
			transactionManager.rollback(status);
			return resultModel;
		}
	}
	
	/** Delete ADMIN by adminID
	 * 
	 * @param adminID
	 * @return
	 */
	public ResponseModel<Void> deleteAdminByAdminID(String adminID){
		return adminDAO.deleteAdminByAdminID(adminID);
	}
	
	/** Get ADMIN feature list
	 * 
	 * @return
	 */
	public FeatureListInfoDto getFeatureList(){
		List<FeatureInfoDto> list = adminDAO.getFeatureList();
		if(list!=null) {
			FeatureListInfoDto dto = new FeatureListInfoDto();
			dto.setFeatureList(list);
			return dto;
		}else {
			return null;
		}
	}

	/** Get feature with limit list by featurID
	 * 
	 * @param featureID
	 * @return
	 */
	public FeatureInfoDto getFeatureWithLimitListByFeaturID(String featureID) {
		FeatureInfoDto dto = adminDAO.getFeatureByFeaturID(featureID);
		if(dto!=null) {
			List<LimitDto> list = adminDAO.getLimitListByFeatureID(featureID);
			if(list!=null&& list.size()!=0) {
				dto.setLimitList(list);
			}
			return dto;
		}else {
			return null;
		}
	}
	
	/** Create feature with limit list
	 * 
	 * @param feature
	 * @param limitIDList
	 * @return
	 */
	public ResponseModel<Void> createFeatureWithLimitList(FeatureCreateDto feature){
		
		String featureID = UUIDGenerator.generateType4UUID().toString();
		feature.setFeatureID(featureID);
		feature.setTopFeature(false);
		List<FeatureLimitDto> list = new ArrayList<FeatureLimitDto>();
		
		if(feature.getLimitIDList()!=null && feature.getLimitIDList().size()!=0) {
			for(String limitID : feature.getLimitIDList()){
				FeatureLimitDto dto = new FeatureLimitDto();
				dto.setFeatureID(featureID);
				dto.setLimitValue(true);
				dto.setLimitID(limitID);
				list.add(dto);
			}			
		}
		
		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> resultModel = null;
		
		try {
			resultModel = adminDAO.setFeature(feature);
			if(resultModel.getCode()!=201) {
				throw new Exception();
			}
			
			resultModel = adminDAO.setFeatureLimitList(list);
			if(resultModel.getCode()!=201) {
				throw new Exception();
			}			
			
			transactionManager.commit(status);
			return new ResponseModel<Void>(201, "");
		}catch(Exception e) {
			transactionManager.rollback(status);
			return resultModel;
		}
	}
	
	/** Get limit list
	 * 
	 * @return
	 */
	public LimitListInfoDto getLimitList(){
		List<LimitDto> list = adminDAO.getLimitList();
		if(list!=null&&list.size()!=0) {
			LimitListInfoDto dto = new LimitListInfoDto();
			dto.setLimitList(list);
			return dto;
		}else{
			return null;
		}
	}
	
	/** Update feature with limit list by featureID
	 * 
	 * @param featureID
	 * @param formFeature
	 * @param limitList
	 * @return
	 */
	public ResponseModel<Void> updateFeatureByFeatureID(String featureID, FeatureUpdateDto formFeature){
		
		formFeature.setFeatureID(featureID);
		FeatureUpdateDto originalFeature = adminDAO.getFeatureForUpdateByFeaturID(featureID);
		utility.updateByDto(formFeature, originalFeature);
		
		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> resultModel = null;
		
		try {
			resultModel = adminDAO.updateFeature(originalFeature);
			if(resultModel.getCode()!=204) {
				throw new Exception();
			}
			
			resultModel = adminDAO.deleteLimitListByFeatureID(featureID);
			if(resultModel.getCode()!=204) {
				throw new Exception();
			}	
			
			if(formFeature.getLimitIDList()!=null&&formFeature.getLimitIDList().size()!=0) {
				List<FeatureLimitDto> limitList =  new ArrayList<FeatureLimitDto>();
				for(String limitID : formFeature.getLimitIDList()){
					FeatureLimitDto limit = new FeatureLimitDto();
					limit.setFeatureID(featureID);
					limit.setLimitValue(true);
					limit.setLimitID(limitID);
					limitList.add(limit);
				}
				
				resultModel = adminDAO.setFeatureLimitList(limitList);
				if(resultModel.getCode()!=201) {
					throw new Exception();
				}				
			}
			
			transactionManager.commit(status);
			return new ResponseModel<Void>(204, "");
		}catch(Exception e) {
			transactionManager.rollback(status);
			return resultModel;
		}
	}
	
	/** Delete feature by featureID
	 *  
	 * @param featureID
	 * @return
	 */
	public ResponseModel<Void> deleteFeatureByFeatureID(String featureID){
		
		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> resultModel = null;
		
		try {
			resultModel = adminDAO.deleteLimitListByFeatureID(featureID);
			if(resultModel.getCode()!=204) {
				throw new Exception();
			}
			
			resultModel = adminDAO.deleteFeatureByFeatureID(featureID);
			if(resultModel.getCode()!=204) {
				throw new Exception();
			}
			
			transactionManager.commit(status);
			return new ResponseModel<Void>(204, "");
		}catch(Exception e) {
			transactionManager.rollback(status);
			return resultModel;
		}
	}
	
	
	
//	// 取得使用者密碼
//	public String getUserPassword(String account) {
//		Map<String, Object> userDataMap = adminDAO.getUserData(account);
//		return userDataMap.get("pwd").toString();
//	}
//
//	// 取得權限名稱
//	public String getFeatureName(String account) {
//		Map<String, Object> userDataMap = adminDAO.getUserData(account);
//		return userDataMap.get("featureName").toString();
//	}
//
////	取得權限資料
//	public List<Map<String, Object>> getLimitValue(String limitName) {
//		return adminDAO.getLimitValue(limitName);
//	}
////	// 核對帳號密碼並取得帳號資料
////	public List<Map<String, Object>> checkAccountAndPwd(AdminBean bean) throws Throwable {
////
////		String account = bean.getAccount().toLowerCase();
////		String pwd = bean.getPwd();
////		// 加密密碼：先AES加密在MD5加密
////		bean.setPwd(CipherUtility.getMD5Encoding(CipherUtility.encryptStringCommonKey(account, pwd)));
////
////		return adminDAO.checkAccountAndPwd(bean);
////	};
//
//	// 查詢單筆帳號by adminID
//	public List<Map<String, Object>> findByID(String adminID) {
//		return adminDAO.findByID(adminID);
//	};
//
//	// 查詢單筆帳號by account
//	public Map<String, Object> findByAccountAll(String account) {
//		return adminDAO.findByAccountAll(account);
//	};
//
//	// 查詢單筆帳號by account
//	public List<Map<String, Object>> findByAccount(String account) {
//		return adminDAO.findByAccount(account);
//	};
//
//	// 顯示管理者帳號清單
//	public List<Map<String, Object>> getALLAdminAcount(int begin, int end, String featureID, String order) {
//		return adminDAO.getALLAdminAcount(begin, end, featureID, order);
//	};
//
//	// 刪除管理者
//	public int deleteAdmin(String adminID) {
//		return adminDAO.deleteAdmin(adminID);
//	};
//
////	重置密碼
//	public int resetPwd(String id) throws Throwable {
////		查詢帳號
//		String account = adminDAO.findByID(id).get(0).get("account").toString();
//		String pwd = CipherUtility.encryptStringPasswordOnly(account);
//		return adminDAO.updatePwd(account, pwd);
//	};
//
////	修改密碼
//	public int updatePwd(String account, String pwd) throws Throwable {
//		// 查詢帳號
////		String account = adminDAO.findByID(id).get(0).get("account").toString();
////		System.out.println("account:" + account);
//
//		String newPwd = CipherUtility.encryptStringPasswordOnly(pwd);
//		return adminDAO.updatePwd(account, newPwd);
//	};
//
////	新增管理者
//	@Transactional(propagation = Propagation.REQUIRED)
//	public int insertAdmin(AdminDto bean) throws Throwable {
//		String newID = idGenerator.getIDGenerator("AdminID", "admin", 2);
//		String adminID = "A" + newID.substring(newID.length() - 9);
//		bean.setPwd(CipherUtility.encryptStringPasswordOnly(bean.getAccount()));
//		return adminDAO.insertAdmin(bean, adminID);
//	};
//
//	// 變更管理者資料
//	public int updateAdminInfo(AdminDto bean) {
//		return adminDAO.updateAdminInfo(bean);
//	};
//
//	// 變更管理者權限
//	public int updateAdminLimit(String id, String featureID) {
//		return adminDAO.updateAdminLimit(id, featureID);
//	};
//
////	-------------------------------------------------------------------------------------------------------------------
//	// 顯示所有管理者身分別
//	public List<Map<String, Object>> getALLFeature() {
//		return adminDAO.getALLFeature();
//	};
//
//	// 顯示所有管理者身分別 (有排序)
//	public List<Map<String, Object>> getALLFeature(int begin, int end, String order) {
//		return adminDAO.getALLFeature(begin, end, order);
//	};
//
//	// 查詢身分別權限
//	public List<Map<String, Object>> findLimitByFeatureID(String featureID) {
//		return adminDAO.findLimitByFeatureID(featureID);
//	};
//
//	// 新增身分別 + 新增身分別權限
//	@Transactional(propagation = Propagation.REQUIRED)
//	public int insertFeature(String name, List<String> limitIDList) {
//
//		String newID = idGenerator.getIDGenerator("FeatureID", "feature", 2);
//		String featureID = "F" + newID.substring(newID.length() - 9);// 前面串上N後面取9位數
//		// 新增身分別
//		adminDAO.insertFeature(featureID, name);
//		// 新增身分別權限
//		if (limitIDList != null) {
//			// 新增身分別權限
//			insertFeatureLimit(featureID, limitIDList);
//		}
//		return 1;
//	};
//
//	// 編輯管理者身分別
//	@Transactional(propagation = Propagation.REQUIRED)
//	public int updateFeature(String featureID, String featureName, List<String> limitIDList) {
//		// 修改身分別
//		adminDAO.updateFeature(featureID, featureName);
//		if (limitIDList != null) {
//			// 刪除身分別權限
//			adminDAO.deleteFeatureLimitByFeatureID(featureID);
//			// 新增身分別權限
//			insertFeatureLimit(featureID, limitIDList);
//		}
//		return 1;
//	};
//
//	// 刪除管理者身分別
//	@Transactional(propagation = Propagation.REQUIRED)
//	public int deleteFeature(String featureID) {
//		// 刪除身分別權限
//		adminDAO.deleteFeatureLimitByFeatureID(featureID);
//		// 刪除身分別
//		return adminDAO.deleteFeature(featureID);
//	};
//
//	// 查詢管理者權限
//	public List<String> getFeaturelimitByID(String id) {
//		List<Map<String, Object>> autList = adminDAO.getFeaturelimitByID(id);
//
//		List<String> list = new ArrayList<String>();
//		for (Map<String, Object> map : autList) {
//			list.add(map.get("LimitID").toString());
//		}
//		return list;
//	};
//
////	-------------------------------------------------------------------------------------------------------------------
////	新增身分別權限 (勾選的為true，其他為false)
//	public void insertFeatureLimit(String featureID, List<String> limitIDList) {
//		// 查詢所有權限
//		// [limit01, limit03, limit05]
//		List<Map<String, Object>> allLimitList = adminDAO.getAllLimitID();
//
////		將選取的帳號權限與所有權限功能比對,將具有權限功能設定為True
////		(在所有權限清單內，符合身分別權限的LimitValue:true，不符合的給false)
//		for (Map<String, Object> m : allLimitList) {
//			m.put("limitValue", false); // DB預設給false
//			for (String limitID : limitIDList) {
//				if (m.get("limitID").toString().equalsIgnoreCase(limitID)) {
//					m.put("limitValue", true);
//					break;
//				}
//			}
//		}
//		adminDAO.insertFeatureLimit(featureID, allLimitList);
//	}

}
