package tw.com.reinbach.Atenore.Model.category.event.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class EventListSearchByMainTypeInfoDto {

	private String typeName;
	
	private String mainTypeName;
	
	private List<EventSearchByMainTypeInfoDto> eventList;

	public List<EventSearchByMainTypeInfoDto> getEventList() {
		return eventList;
	}

	public void setEventList(List<EventSearchByMainTypeInfoDto> eventList) {
		this.eventList = eventList;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getMainTypeName() {
		return mainTypeName;
	}

	public void setMainTypeName(String mainTypeName) {
		this.mainTypeName = mainTypeName;
	}
	
	
	
}
