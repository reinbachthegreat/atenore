package tw.com.reinbach.Atenore.Model.marketing.query;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AdsTypeListInfoDto {

	private List<String> adsTypeList;

	public List<String> getAdsTypeList() {
		return adsTypeList;
	}

	public void setAdsTypeList(List<String> adsTypeList) {
		this.adsTypeList = adsTypeList;
	}
	
	
	
}
