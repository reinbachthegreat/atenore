package tw.com.reinbach.Atenore.Model.category.master;

import java.util.List;

import tw.com.reinbach.Atenore.Model.category.master.dto.MasterListSearchByMainTypeArgsDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterListSearchByMiddleTypeArgsDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterListSearchByTypeArgsDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterSearchByMainTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterSearchByMiddleTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.MasterSearchByTypeInfoDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.component.MasterMainTypeAdminDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.component.MasterMainTypeDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.component.MasterMiddleTypeAdminDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.component.MasterMiddleTypeDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.component.MasterTypeAdminDto;
import tw.com.reinbach.Atenore.Model.category.master.dto.component.MasterTypeDto;

public interface MasterTypeDAO {

	public List<MasterMainTypeDto> getMasterMainTypeList();
	
	public List<MasterMiddleTypeDto> getMasterMiddleTypeList();
	
	public List<MasterMiddleTypeDto> getMasterMiddleTypeListByMainType(Integer mainID);
	
	public List<MasterTypeDto> getMasterTypeList();
	
	public List<MasterTypeDto> getMasterTypeListByMiddleType(Integer midID);

	public List<MasterMainTypeAdminDto> getMasterMainTypeListWithFilter(String fuzzyFilter);
	
	public List<MasterSearchByMainTypeInfoDto> getMasterListByMainTypeWithFilter(MasterListSearchByMainTypeArgsDto argsDto);
	
	public List<MasterMiddleTypeAdminDto> getMasterMiddleTypeListByMainTypeWithFilter(Integer mainID, String fuzzyFilter);
	
	public List<MasterSearchByMiddleTypeInfoDto> getMasterListByMiddleTypeWithFilter(MasterListSearchByMiddleTypeArgsDto argsDto);
	
	public List<MasterTypeAdminDto> getMasterTypeListByMiddleTypeWithFilter(Integer midID, String fuzzyFilter);
	
	public List<MasterSearchByTypeInfoDto> getMasterListByTypeWithFilter(MasterListSearchByTypeArgsDto argsDto);
	
}
