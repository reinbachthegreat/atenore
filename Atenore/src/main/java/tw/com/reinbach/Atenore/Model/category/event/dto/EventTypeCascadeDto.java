package tw.com.reinbach.Atenore.Model.category.event.dto;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class EventTypeCascadeDto {

	@ApiModelProperty(example = "1")
	private Integer mainID;
	
	@ApiModelProperty(example = "醫療")
	private String name;
	
	private List<EventTypeDto> minorTypeList;

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<EventTypeDto> getMinorTypeList() {
		return minorTypeList;
	}

	public void setMinorTypeList(List<EventTypeDto> minorTypeList) {
		this.minorTypeList = minorTypeList;
	}
	
	public void addMinorTypeList(EventTypeDto eventTypeDto){
		this.minorTypeList.add(eventTypeDto);
	}
	
	
	
}
