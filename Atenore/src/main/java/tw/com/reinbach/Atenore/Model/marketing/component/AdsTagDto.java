package tw.com.reinbach.Atenore.Model.marketing.component;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AdsTagDto {
	
	private String tagID;
	
	@ApiModelProperty(hidden = true)
	private String adsID;
	
	private String tag;

	public String getTagID() {
		return tagID;
	}

	public void setTagID(String tagID) {
		this.tagID = tagID;
	}

	public String getAdsID() {
		return adsID;
	}

	public void setAdsID(String adsID) {
		this.adsID = adsID;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}
	
	

}
