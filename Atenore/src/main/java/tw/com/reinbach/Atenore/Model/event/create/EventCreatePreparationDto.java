package tw.com.reinbach.Atenore.Model.event.create;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import tw.com.reinbach.Atenore.Model.category.event.dto.EventTypeCascadeDto;
import tw.com.reinbach.Atenore.Model.event.component.EventVisibilityDto;

public class EventCreatePreparationDto {

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<EventTypeCascadeDto> eventTypeCascadeList;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<EventVisibilityDto> eventVisibilityList;

	public List<EventTypeCascadeDto> getEventTypeCascadeList() {
		return eventTypeCascadeList;
	}

	public void setEventTypeCascadeList(List<EventTypeCascadeDto> eventTypeCascadeList) {
		this.eventTypeCascadeList = eventTypeCascadeList;
	}

	public List<EventVisibilityDto> getEventVisibilityList() {
		return eventVisibilityList;
	}

	public void setEventVisibilityList(List<EventVisibilityDto> eventVisibilityList) {
		this.eventVisibilityList = eventVisibilityList;
	}
	
	
	
	
}
