package tw.com.reinbach.Atenore.Model.place;

import java.util.List;

import tw.com.reinbach.Atenore.Model.place.component.PinPlaceTypeInfoDto;
import tw.com.reinbach.Atenore.Model.place.component.PlaceDto;
import tw.com.reinbach.Atenore.Model.place.component.PlaceImageDto;
import tw.com.reinbach.Atenore.Model.place.component.PlaceLiaisonDto;
import tw.com.reinbach.Atenore.Model.place.component.PlaceTag;
import tw.com.reinbach.Atenore.Model.place.create.PlaceCreateDto;
import tw.com.reinbach.Atenore.Model.place.create.PlaceLiaisonCreateDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceEditSearchArgsAdminDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceEditSearchInfoAdminDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceEditSearchInfoMemberDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceNearBySearchArgsDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceSearchArgsAdminDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceSearchArgsDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceSearchArgsMemberEditedDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceSearchInfoAdminDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceSearchInfoDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceSearchSimpleInfoDto;
import tw.com.reinbach.Atenore.Model.place.update.PinPlaceTypeUpdateDto;
import tw.com.reinbach.Atenore.Model.place.update.PlaceFollowedDto;
import tw.com.reinbach.Atenore.Model.place.update.PlaceUpdateDto;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;

public interface PlaceDAO {

	public List<PlaceDto> getPlaceList();

	public List<PlaceSearchInfoDto> getPlaceListWithFilter(PlaceSearchArgsDto argsDto);
	
	public List<PlaceSearchInfoDto> getPlaceListNearBy(PlaceNearBySearchArgsDto argsDto);

	public List<PlaceDto> getPlaceListWithFilter(String placeFilter, String county);

	public PlaceDto getPlace(String placeID);

	public PlaceDto getPlaceWithMemberID(String placeID, String memberID);

	public List<PlaceTag> getPlaceTag(String placeID);

	public List<PlaceTag> getPlaceTagWithoutPlaceID(String placeID);

	public List<PlaceLiaisonDto> getPlaceLiaison(String placeID);

	public List<PlaceImageDto> getPlaceImage(String placeID);

	public List<PlaceImg> getPlaceImage(List<String> uuidList);

	public List<String> getPlaceImageObjectkey(String placeID);

	public List<String> getPlaceEditID(String placeID);

	public PlaceUpdateDto getPlaceOriginalByPlaceID(String placeID);

	public List<PlaceEditSearchInfoAdminDto> getPlaceEditListByPlaceID(PlaceEditSearchArgsAdminDto argsDto);

	public List<PlaceEditSearchInfoMemberDto> getPlaceMemberEditedListWithFilter(PlaceSearchArgsMemberEditedDto argsDto);

	public List<PlaceSearchSimpleInfoDto> getPlaceSimpleByName(String name);

	public int deletePlaceTag(String placeID);

	public int deletePlaceLiason(String placeID);

	public int deletePlaceImage(List<String> uuidList);

	public Boolean getPlaceIfMemberFollowed(PlaceFollowedDto follow);

	public ResponseModel<Void> setPlaceFollowed(PlaceFollowedDto follow);

	public ResponseModel<Void> deletePlaceFollowed(PlaceFollowedDto follow);
	
	public ResponseModel<Void> updatePlaceFollowed(String placeID, Boolean wannaFollow);

	/*
	 * Place Edit Version
	 * Series=======================================================================
	 * ================
	 */

	public PlaceDto getPlaceEdit(String editID);

	public PlaceDto getPlaceMemberEdit(String editID, String memberID);

	public List<PlaceDto> getPlaceEditList(String placeID);

	public List<PlaceTag> getPlaceTagListByEditID(String editID);

	public List<PlaceImageDto> getPlaceImgListByEditID(String editID);

	public List<PlaceLiaisonDto> getPlaceLiaisonListByEditID(String editID);

	public List<PlaceLiaison> getPlaceLiaisonEdit(String editID);

	public ResponseModel<Void> setPlaceEdit(List<PlaceCreateDto> list);

	public ResponseModel<Void> setPlaceEdit(PlaceCreateDto place);

	public ResponseModel<Void> setPlaceEdit(PlaceUpdateDto place);

	public ResponseModel<Void> setPlaceEditInUsedFalse(String placeID);

	public ResponseModel<Void> setPlaceEditInUsedTrue(String editID);

	public ResponseModel<Void> setPlace(List<PlaceCreateDto> list);

	public ResponseModel<Void> setPlace(PlaceCreateDto place);

	public ResponseModel<Void> updatePlace(PlaceUpdateDto place);

	public ResponseModel<Void> updatePlace(String placeID, String editID);

	public ResponseModel<Void> editPlaceLiaison(String placeID, String editID);

	public ResponseModel<Void> editPlaceTag(String placeID, String editID);

	public ResponseModel<Void> editPlaceImage(String placeID, String editID);

	public ResponseModel<Void> setPlaceLiaisonEdit(List<PlaceLiaisonCreateDto> editList);

	public ResponseModel<Void> setPlaceLiaison(List<PlaceLiaisonCreateDto> list);

	public ResponseModel<Void> setPlaceTagEdit(List<PlaceTag> editList);

	public ResponseModel<Void> setPlaceTag(List<PlaceTag> list);

	public ResponseModel<Void> setPlaceImageEdit(List<PlaceImg> list);

	public ResponseModel<Void> setPlaceImage(List<PlaceImg> list);

	public List<PinPlaceTypeInfoDto> getPinPlaceTypeListByMemberID(String memberID);

	public List<String> getPinPlaceTypeUUIDListByMemberID(String memberID);

	public ResponseModel<Void> deletePinPlaceTypeList(List<String> uuidList);

	public ResponseModel<Void> setPinPlaceTypeList(List<PinPlaceTypeUpdateDto> list);

	public ResponseModel<Void> deletePlaceFollowedByPlaceID(String placeID);

	public ResponseModel<Void> deletePlaceTagEditByEditID(List<String> editIDList);

	public ResponseModel<Void> deletePlaceTagByPlaceID(String placeID);

	public ResponseModel<Void> deletePlaceImgEditByEditID(List<String> editIDList);

	public ResponseModel<Void> deletePlaceImgByPlaceID(String placeID);

	public ResponseModel<Void> deletePlaceLiaisonEditByEditID(List<String> editIDList);

	public ResponseModel<Void> deletePlaceLiaisonByPlaceID(String placeID);

	public ResponseModel<Void> deletePlaceEditByEditID(List<String> editIDList);

	public ResponseModel<Void> deletePlaceByPlaceID(String placeID);
	
	public ResponseModel<Void> updatePlaceClick(String placeID);

	/**
	 * ADMIN
	 * 
	 */
	public List<PlaceSearchInfoAdminDto> getPlaceListWithFilter(PlaceSearchArgsAdminDto argsDto);

//	public List<PlaceSearchReviewInfoAdminDto> getPlaceListNotReview(PlaceSearchReviewArgsAdminDto argsDto);

}
