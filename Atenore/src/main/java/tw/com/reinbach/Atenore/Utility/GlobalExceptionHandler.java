package tw.com.reinbach.Atenore.Utility;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;

@ControllerAdvice
@RestController
public class GlobalExceptionHandler {

	/**
	 * Controller 裡標注 @RequestBody 的變數在 validate fail 時會丟出
	 * MethodArgumentNotValidException。 這個 method專門處理此類 exception
	 *
	 * @param req
	 * @param e
	 *
	 * @return
	 */
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseModel<Void> handleMethodArgumentNotValidException(HttpServletRequest req, MethodArgumentNotValidException e) {
		List<String> errorMessages = e.getBindingResult().getFieldErrors().stream()
				.map(fieldError -> fieldError.getField() + " " + fieldError.getDefaultMessage()) // 記錄 "fieldName +
																									// validateFailMessage"
				.collect(Collectors.toList());
		return new ResponseModel<Void>(400, errorMessages.toString());
	}

	@ExceptionHandler(BindException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseModel<Void> handleBindException(HttpServletRequest req, BindException e) {
		List<String> errorMessages = e.getBindingResult().getFieldErrors().stream()
				.map(fieldError -> fieldError.getField() + " " + fieldError.getDefaultMessage()) // 記錄 "fieldName +
																									// validateFailMessage"
				.collect(Collectors.toList());
		return new ResponseModel<Void>(400, errorMessages.toString());
	}

	/**
	 * Controller 裡標注 @RequestParam 的變數在 validate fail 時會丟出
	 * ConstraintViolationException。 這個 method專門處理此類 exception
	 *
	 * @param req
	 * @param e
	 *
	 * @return
	 */
	@ExceptionHandler(ConstraintViolationException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseModel<Void> handleConstraintViolationException(HttpServletRequest req, ConstraintViolationException e) {
		// "@NotBlank @RequestParam String myArg" 這樣的 validate 寫法在 validate fail 時無法得知
		// "哪個輸入參數名稱" 驗証失敗，這是 java reflection 本身的限制。
		// 用這類語法時要改寫成 "@NotBlank(myArg must not be blank) @RequestParam String
		// myArg"，程式裡的 validate annotation 要寫出 "完整出錯明細"，
		// 不然在處理 ConstraintViolationException 時只會知道驗証失敗的原因，卻不知道是哪個輸入參數名稱驗証失敗。

		List<String> errorMessages = e.getConstraintViolations().stream().map(ConstraintViolation::getMessage)
				.collect(Collectors.toList());
		return new ResponseModel<Void>(400, errorMessages.toString());
	}
}
