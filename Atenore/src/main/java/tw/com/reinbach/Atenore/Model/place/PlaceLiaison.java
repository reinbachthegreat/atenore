package tw.com.reinbach.Atenore.Model.place;

import org.springframework.stereotype.Component;

@Component
public class PlaceLiaison {

	private String uuid;

	private String placeID;

	private String name;

	private String phone;

	private String isMain;

	/*
	 * Extend attribute, for Place Edit Table. By controlling version.
	 * 
	 */
	private String editID;

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getPlaceID() {
		return placeID;
	}

	public void setPlaceID(String placeID) {
		this.placeID = placeID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getIsMain() {
		return isMain;
	}

	public void setIsMain(String isMain) {
		this.isMain = isMain;
	}

	public String getEditID() {
		return editID;
	}

	public void setEditID(String editID) {
		this.editID = editID;
	}

}
