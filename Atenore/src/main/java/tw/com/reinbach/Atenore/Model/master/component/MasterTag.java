package tw.com.reinbach.Atenore.Model.master.component;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MasterTag {
	
	@ApiModelProperty(example = "123e4567-e89b-42d3-a456-556642440000", dataType = "String", hidden = true)
	private String tagID;
	
	@ApiModelProperty(example = "123e4567-e89b-42d3-a456-556642440000", dataType = "String", hidden = true)
	private String masterID;
	
	@ApiModelProperty(notes = "alias professional tag",example = "國際談判專長", dataType = "String")
	private String tag;

	public String getTagID() {
		return tagID;
	}

	public void setTagID(String tagID) {
		this.tagID = tagID;
	}

	public String getMasterID() {
		return masterID;
	}

	public void setMasterID(String masterID) {
		this.masterID = masterID;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}
	
}
