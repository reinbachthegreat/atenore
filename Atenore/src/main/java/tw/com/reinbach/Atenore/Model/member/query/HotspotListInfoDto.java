package tw.com.reinbach.Atenore.Model.member.query;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import tw.com.reinbach.Atenore.Model.member.component.HotspotDto;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class HotspotListInfoDto {

	private String memberID;
	
	private List<HotspotDto> hotspotList;

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public List<HotspotDto> getHotspotList() {
		return hotspotList;
	}

	public void setHotspotList(List<HotspotDto> hotspotList) {
		this.hotspotList = hotspotList;
	}

	
	
}
