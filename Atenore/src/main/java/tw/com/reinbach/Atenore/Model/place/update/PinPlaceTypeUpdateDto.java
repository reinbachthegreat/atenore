package tw.com.reinbach.Atenore.Model.place.update;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PinPlaceTypeUpdateDto {

	@ApiModelProperty(hidden = true)
	private String memberID;
	
	@ApiModelProperty(hidden = true)
	private String uuid;
	
	@ApiModelProperty(example = "1", notes = "活動小分類編號")
	private Integer typeID;
	
	@ApiModelProperty(example = "1", notes = "活動大分類編號")
	private Integer mainID;
	
//	@ApiModelProperty(hidden = true, example = "2020-08-05 19:50:00")
//	private LocalDateTime insertTime;

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Integer getTypeID() {
		return typeID;
	}

	public void setTypeID(Integer typeID) {
		this.typeID = typeID;
	}

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

//	public LocalDateTime getInsertTime() {
//		return insertTime;
//	}
//
//	public void setInsertTime(LocalDateTime insertTime) {
//		this.insertTime = insertTime;
//	}

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	
	
}
