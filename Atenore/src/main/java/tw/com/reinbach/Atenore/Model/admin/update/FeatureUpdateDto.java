package tw.com.reinbach.Atenore.Model.admin.update;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;
import tw.com.reinbach.Atenore.Model.admin.component.FeatureLimitDto;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class FeatureUpdateDto {

	@ApiModelProperty(hidden = true)
	private String featureID;
	
	private String name;
	
	private List<String> limitIDList;

	public String getFeatureID() {
		return featureID;
	}

	public void setFeatureID(String featureID) {
		this.featureID = featureID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getLimitIDList() {
		return limitIDList;
	}

	public void setLimitIDList(List<String> limitIDList) {
		this.limitIDList = limitIDList;
	}

}
