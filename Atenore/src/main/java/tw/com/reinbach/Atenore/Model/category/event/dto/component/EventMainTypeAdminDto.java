package tw.com.reinbach.Atenore.Model.category.event.dto.component;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class EventMainTypeAdminDto {

	@ApiModelProperty(name = "mainID", value = "大分類編號", example = "1", dataType = "Integer")
	private Integer mainID;
	
	@ApiModelProperty(name = "name", value = "大分類名稱", example = "1", dataType = "String")
	private String name;
	
	@ApiModelProperty(name = "inprogressQty", value = "大分類下正在進行活動數量", example = "10", dataType = "Integer")
	private Integer inprogressQty;

	@ApiModelProperty(name = "expiredQty", value = "大分類下過期活動數量", example = "100", dataType = "Integer")
	private Integer expiredQty;
	
	@ApiModelProperty(name = "totalQty", value = "大分類下所有活動數量", example = "131", dataType = "Integer")
	private Integer totalQty;
	
//	private List<EventTypeAdminDto> eventTypeList;
	

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

//	public List<EventTypeAdminDto> getEventTypeList() {
//		return eventTypeList;
//	}
//
//	public void setEventTypeList(List<EventTypeAdminDto> eventTypeList) {
//		this.eventTypeList = eventTypeList;
//	}

	public Integer getInprogressQty() {
		return inprogressQty;
	}

	public void setInprogressQty(Integer inprogressQty) {
		this.inprogressQty = inprogressQty;
	}

	public Integer getExpiredQty() {
		return expiredQty;
	}

	public void setExpiredQty(Integer expiredQty) {
		this.expiredQty = expiredQty;
	}

	public Integer getTotalQty() {
		return totalQty;
	}

	public void setTotalQty(Integer totalQty) {
		this.totalQty = totalQty;
	}

	
	
}
