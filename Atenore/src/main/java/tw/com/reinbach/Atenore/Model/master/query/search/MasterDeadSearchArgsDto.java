package tw.com.reinbach.Atenore.Model.master.query.search;

import io.swagger.annotations.ApiModelProperty;

public class MasterDeadSearchArgsDto {

	@ApiModelProperty(hidden = true)
	private String memberID;
	
	/* 分類屬性
	 * 
	 */
	@ApiModelProperty(name = "typeID", value = "master type(minor)", example = "1", dataType = "Integer")
	private Integer typeID;

	@ApiModelProperty(name = "midID", value = "master middle type(middle)", example = "1", dataType = "Integer")
	private Integer midID;
	
	@ApiModelProperty(name = "mainID", value = "master main type(main)", example = "1", dataType = "Integer")
	private Integer mainID;
	
	@ApiModelProperty(value = "alias area, base on Taiwan's county.", example = "新北市")
	private String county;

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public Integer getTypeID() {
		return typeID;
	}

	public void setTypeID(Integer typeID) {
		this.typeID = typeID;
	}

	public Integer getMidID() {
		return midID;
	}

	public void setMidID(Integer midID) {
		this.midID = midID;
	}

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}
	
	
	
}
