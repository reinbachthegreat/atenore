package tw.com.reinbach.Atenore.RestController.Api.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiOperation;
import tw.com.reinbach.Atenore.Model.marketing.AdsService;
import tw.com.reinbach.Atenore.Model.marketing.create.AdsCreateDto;
import tw.com.reinbach.Atenore.Model.marketing.query.AdsInfoDto;
import tw.com.reinbach.Atenore.Model.marketing.query.AdsTypeListInfoDto;
import tw.com.reinbach.Atenore.Model.marketing.query.search.AdsSearchArgsDto;
import tw.com.reinbach.Atenore.Model.marketing.query.search.AdsSearchListInfoDto;
import tw.com.reinbach.Atenore.Model.marketing.update.AdsUpdateDto;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;

@RestController
public class MarketingController {

	@Autowired
	private AdsService adsSerivce;
		
	/** Get ads list by filters
	 * 
	 * @param argsDto
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A52", value = "Get ads list by filters")
	@RequestMapping(value = {"/admin/ads", "/web/ads"}, method = {RequestMethod.GET})
	public ResponseModel<AdsSearchListInfoDto> getAdsListByFilters(AdsSearchArgsDto argsDto) {
		AdsSearchListInfoDto dto = adsSerivce.getAdsListByFilters(argsDto);
		if(dto==null) {
			return new ResponseModel<AdsSearchListInfoDto>(404, ApiError.DATA_NOT_FOUND);
		}else {
			return new ResponseModel<AdsSearchListInfoDto>(200, dto);
		}
	}
	
	/** Get ads type list
	 * 
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A53", value = "Get ads type list")
	@RequestMapping(value = {"/admin/adstypelist"}, method = {RequestMethod.GET})	
	public AdsTypeListInfoDto getAdsTypeList() {
		return adsSerivce.getAdsTypeList();
	}
	
	/** Create ads
	 * 
	 * @param ads
	 * @param adsTagList
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A54", value = "Create ads")
	@RequestMapping(value = {"/admin/ads"}, method = {RequestMethod.POST})	
	public ResponseModel<Void> createAds(AdsCreateDto ads){
		return adsSerivce.createAds(ads);
	}
	
	/** Get ads with tag list by adsID
	 * 
	 * @param adsID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A55", value = "Get ads with tag list by adsID")
	@RequestMapping(value = {"/admin/ads/{adsID}", "/web/ads/{adsID}"}, method = {RequestMethod.GET})	
	public ResponseModel<AdsInfoDto> getAdsByAdsID(@PathVariable(required = true) String adsID) {
		AdsInfoDto dto = adsSerivce.getAdsByAdsID(adsID);
		if(dto==null) {
			return new ResponseModel<AdsInfoDto>(404, ApiError.DATA_NOT_FOUND);
		}else {
			return new ResponseModel<AdsInfoDto>(200, dto);
		}
	}

	/** Update Ads info and tag list by adsID
	 * 
	 * @param adsID
	 * @param formAds
	 * @param tagList
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A56", value = "Update Ads info and tag list by adsID")
	@RequestMapping(value = {"/admin/ads/{adsID}"}, method = {RequestMethod.PUT})	
	public ResponseModel<Void> updateAdsByAdsID(
			@PathVariable(required = true) String adsID, AdsUpdateDto formAds) {
		return adsSerivce.updateAdsByAdsID(adsID, formAds);
	}
	
	/** Delete Ads info and tag list by adsID
	 * 
	 * @param adsID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A57", value = "Delete Ads info and tag list by adsID")
	@RequestMapping(value = {"/admin/ads/{adsID}"}, method = {RequestMethod.DELETE})	
	public ResponseModel<Void> deleteAdsByAdsID(@PathVariable(required = true) String adsID){
		return adsSerivce.deleteAdsByAdsID(adsID);
	}
	
	
}
