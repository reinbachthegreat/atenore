package tw.com.reinbach.Atenore.Utility;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class PhotoUploadAndDelete {

	/**
	 * UploadSinglePhoto
	 * 
	 * @param MultipartFile file
	 * 
	 * @return String imgUrl
	 */
	public String UploadSinglePhoto(MultipartFile file, String folderName, String fileName) {

		String imgUrl = null;

		// 上傳照片存入資料夾
		try {
			byte[] bytes = file.getBytes();

			// Creating the directory to store file
			// 路徑：apache-tomcat-8.5.16\webapps\wonderful\WEB-INF\classes\static\image
			String rootPath = System.getProperty("catalina.home") 
							+ File.separator + "webapps" 
							+ File.separator + "CarbonAsset" 
							+ File.separator + "WEB-INF" 
							+ File.separator + "classes" 
							+ File.separator + "static" 
							+ File.separator + "image";
			File dir = new File(rootPath + File.separator + folderName);
			/************************************************/
			// 測試用，正式上線後要拿掉這行改用上面兩行
//			File dir = new File("C:\\" + folderName);

			if (!dir.exists())
				dir.mkdirs();

			File serverFile = new File(dir.getAbsolutePath() + File.separator + fileName);
//			System.out.println("絕對路徑：" + dir.getAbsolutePath() + File.separator + file.getOriginalFilename());
//			System.out.println("相對路徑：" + dir.getCanonicalPath() + File.separator + file.getOriginalFilename());
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));

			stream.write(bytes);
			stream.close();

			// 取得ImgUrl,以便寫入DB
			imgUrl = "image" + File.separator + folderName + File.separator + fileName;
			/*********************************/
			// 測試用，正式上線後要拿掉這行改用上面那行
//			imgUrl = "C:\\" + folderName + File.separator + file.getOriginalFilename(); 

		} catch (Exception e) {
			e.printStackTrace();
		}
		return imgUrl;
	}

	/**
	 * DeleteSinglePhoto
	 * 
	 * @param Map<String,Object> imgURL
	 */
	public void DeleteSinglePhoto(String getImgURL, String folderName) {

		String rootPath = System.getProperty("catalina.home") 
						+ File.separator + "webapps" 
						+ File.separator + "CarbonAsset" 
						+ File.separator + "WEB-INF" 
						+ File.separator + "classes" 
						+ File.separator + "static";

		/******** 判斷ImgURL是否存在 ***************/
		if (getImgURL == null || getImgURL.trim().length() == 0) {
			System.out.println("getImgURL:路徑不存在");
		} else {
			/******** 刪除資料夾中的照片 ***************/
			File myDelFile = new File(rootPath + File.separator + getImgURL);
			// 測試用
//			File myDelFile = new File("C:\\" + folderName  + File.separator + getImgURL); 

			if (myDelFile.exists())
				myDelFile.delete();
		}
	}

	// 取得副檔名
	public String findFileExtension(String fileName) {

		File file = new File(fileName);

		// 取出副檔名方法
//	    System.out.println("小數點是 = " + (char) 46); // 小數點是 = .
		int startIndex = file.getName().lastIndexOf(46) + 1; // file.getName() ->取檔案名稱
		int endIndex = file.getName().length();

		return file.getName().substring(startIndex, endIndex);
	}

}
