package tw.com.reinbach.Atenore.Model.category.place.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PlaceListSearchByTypeInfoDto {

	private Integer mainID;
	
	private Integer typeID;
	
	private String mainTypeName;
	
	private String typeName;
	
	private List<PlaceSearchByTypeInfoDto> placeList;

	public List<PlaceSearchByTypeInfoDto> getPlaceList() {
		return placeList;
	}

	public void setPlaceList(List<PlaceSearchByTypeInfoDto> placeList) {
		this.placeList = placeList;
	}

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public Integer getTypeID() {
		return typeID;
	}

	public void setTypeID(Integer typeID) {
		this.typeID = typeID;
	}

	public String getMainTypeName() {
		return mainTypeName;
	}

	public void setMainTypeName(String mainTypeName) {
		this.mainTypeName = mainTypeName;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	
}
