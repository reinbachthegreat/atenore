package tw.com.reinbach.Atenore.Model.marketing;

import java.util.List;

import tw.com.reinbach.Atenore.Model.marketing.component.AdsDto;
import tw.com.reinbach.Atenore.Model.marketing.component.AdsTagDto;
import tw.com.reinbach.Atenore.Model.marketing.create.AdsCreateDto;
import tw.com.reinbach.Atenore.Model.marketing.query.search.AdsSearchArgsDto;
import tw.com.reinbach.Atenore.Model.marketing.query.search.AdsSearchInfoDto;
import tw.com.reinbach.Atenore.Model.marketing.update.AdsUpdateDto;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;

public interface AdsDAO {

	public List<AdsSearchInfoDto> getAdsListByFilters(AdsSearchArgsDto argsDto);
	
	public AdsDto getAdsByAdsID(String adsID);
	
	public AdsUpdateDto getAdsForUpdateByAdsID(String adsID);
	
	public List<AdsTagDto> getAdsTagByAdsID(String adsID);
	
	public ResponseModel<Void> setAds(AdsCreateDto ads);
	
	public ResponseModel<Void> setAdsTag(List<AdsTagDto> tagList);
	
	public ResponseModel<Void> updateAds(AdsUpdateDto ads);
	
	public ResponseModel<Void> deleteAdsTagByAdsID(String adsID);
	
	public ResponseModel<Void> deleteAdsByAdsID(String adsID);
	
}
