package tw.com.reinbach.Atenore.Model.event.query.search;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class EventSearchReviewListInfoAdminDto {

	private List<EventSearchReviewInfoAdminDto> eventList;

	public List<EventSearchReviewInfoAdminDto> getEventList() {
		return eventList;
	}

	public void setEventList(List<EventSearchReviewInfoAdminDto> eventList) {
		this.eventList = eventList;
	}
}
