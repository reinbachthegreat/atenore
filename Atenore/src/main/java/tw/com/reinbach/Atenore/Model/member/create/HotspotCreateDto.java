package tw.com.reinbach.Atenore.Model.member.create;

import java.math.BigDecimal;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;

public class HotspotCreateDto {

	@ApiModelProperty(required = true)
	private String memberID;
	
	@ApiModelProperty(hidden = true)
	private String hotspotID;
	
	@NotEmpty(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@Size(max=20, message = ApiError.INPUT_LENGTH_MISMATCHED)
	@ApiModelProperty(required = true)
	private String name;
	
	@NotEmpty(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@ApiModelProperty(required = true)
	private String county;
	
	@NotEmpty(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@ApiModelProperty(required = true)
	private String address1;
	
	@NotEmpty(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@ApiModelProperty(required = true)
	private String address2;
	
	@ApiModelProperty(hidden = true)
	private BigDecimal lat;
	
	@ApiModelProperty(hidden = true)
	private BigDecimal lng;

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public String getHotspotID() {
		return hotspotID;
	}

	public void setHotspotID(String hotspotID) {
		this.hotspotID = hotspotID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public BigDecimal getLat() {
		return lat;
	}

	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}

	public BigDecimal getLng() {
		return lng;
	}

	public void setLng(BigDecimal lng) {
		this.lng = lng;
	} 
	
	

}
