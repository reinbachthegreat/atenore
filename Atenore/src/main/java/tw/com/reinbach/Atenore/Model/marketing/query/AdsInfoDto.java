package tw.com.reinbach.Atenore.Model.marketing.query;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import tw.com.reinbach.Atenore.Model.marketing.component.AdsDto;
import tw.com.reinbach.Atenore.Model.marketing.component.AdsTagDto;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AdsInfoDto {

	private AdsDto ads;
	
	private List<AdsTagDto> adsTagList;

	public AdsDto getAds() {
		return ads;
	}

	public void setAds(AdsDto ads) {
		this.ads = ads;
	}

	public List<AdsTagDto> getAdsTagList() {
		return adsTagList;
	}

	public void setAdsTagList(List<AdsTagDto> adsTagList) {
		this.adsTagList = adsTagList;
	}
	
}
