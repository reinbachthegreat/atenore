package tw.com.reinbach.Atenore.Model.event.update;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import tw.com.reinbach.Atenore.Model.category.event.dto.EventTypeCascadeDto;
import tw.com.reinbach.Atenore.Model.event.component.EventDto;
import tw.com.reinbach.Atenore.Model.event.component.EventImageDto;
import tw.com.reinbach.Atenore.Model.event.component.EventRecommend;
import tw.com.reinbach.Atenore.Model.event.component.EventTag;
import tw.com.reinbach.Atenore.Model.event.component.EventVisibilityDto;

public class EventUpdatePreparationDto {

	private EventDto eventInfo;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<EventTag> eventTagList;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<EventRecommend> eventRecommendList;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<EventImageDto> eventImgList;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<EventTypeCascadeDto> eventTypeCascadeList;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<EventVisibilityDto> eventVisibilityList;

	public EventDto getEventInfo() {
		return eventInfo;
	}

	public void setEventInfo(EventDto eventInfo) {
		this.eventInfo = eventInfo;
	}

	public List<EventTag> getEventTagList() {
		return eventTagList;
	}

	public void setEventTagList(List<EventTag> eventTagList) {
		this.eventTagList = eventTagList;
	}

	public List<EventRecommend> getEventRecommendList() {
		return eventRecommendList;
	}

	public void setEventRecommendList(List<EventRecommend> eventRecommendList) {
		this.eventRecommendList = eventRecommendList;
	}

	public List<EventImageDto> getEventImgList() {
		return eventImgList;
	}

	public void setEventImgList(List<EventImageDto> eventImgList) {
		this.eventImgList = eventImgList;
	}

	public List<EventTypeCascadeDto> getEventTypeCascadeList() {
		return eventTypeCascadeList;
	}

	public void setEventTypeCascadeList(List<EventTypeCascadeDto> eventTypeCascadeList) {
		this.eventTypeCascadeList = eventTypeCascadeList;
	}

	public List<EventVisibilityDto> getEventVisibilityList() {
		return eventVisibilityList;
	}

	public void setEventVisibilityList(List<EventVisibilityDto> eventVisibilityList) {
		this.eventVisibilityList = eventVisibilityList;
	}
	
}
