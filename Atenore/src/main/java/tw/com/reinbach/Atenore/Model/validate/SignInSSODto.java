package tw.com.reinbach.Atenore.Model.validate;

import javax.validation.constraints.NotEmpty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;

@ApiModel
public class SignInSSODto {

	@NotEmpty(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@ApiModelProperty(required = true, value = "token from third party")
	private String id_token;
	
	@NotEmpty(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@ApiModelProperty(required = true, value = "pass sso vendor, eg:facebbok, google", example = "google")
	private String sso_type;
	
	private String apple_id;

	public String getId_token() {
		return id_token;
	}

	public void setId_token(String id_token) {
		this.id_token = id_token;
	}

	public String getSso_type() {
		return sso_type;
	}

	public void setSso_type(String sso_type) {
		this.sso_type = sso_type;
	}

	public String getApple_id() {
		return apple_id;
	}

	public void setApple_id(String apple_id) {
		this.apple_id = apple_id;
	}
	
	
	
}
