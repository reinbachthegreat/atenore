package tw.com.reinbach.Atenore.Model.category.place.dto.component;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PlaceMainTypeAdminDto {

	@ApiModelProperty(value = "大分類編號")
	private Integer mainID;

	@ApiModelProperty(value = "大分類名稱")
	private String name;

	@ApiModelProperty(value = "大分類下所有場所數量")
	private Integer totalQty;

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getTotalQty() {
		return totalQty;
	}

	public void setTotalQty(Integer totalQty) {
		this.totalQty = totalQty;
	}

}
