package tw.com.reinbach.Atenore.RestController.Api.admin;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiOperation;
import tw.com.reinbach.Atenore.Model.event.EventService;
import tw.com.reinbach.Atenore.Model.event.create.EventCreateBatchResultInfoDto;
import tw.com.reinbach.Atenore.Model.event.create.EventCreateDto;
import tw.com.reinbach.Atenore.Model.event.create.EventImageItemCreateDto;
import tw.com.reinbach.Atenore.Model.event.query.EventInfoAllDto;
import tw.com.reinbach.Atenore.Model.event.query.EventNotReviewCountDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchArgsAdminDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchListInfoAdminDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchReviewArgsAdminDto;
import tw.com.reinbach.Atenore.Model.event.query.search.EventSearchReviewListInfoAdminDto;
import tw.com.reinbach.Atenore.Model.event.update.EventApplyDto;
import tw.com.reinbach.Atenore.Model.event.update.EventUpdateDto;
import tw.com.reinbach.Atenore.Model.master.MasterService;
import tw.com.reinbach.Atenore.Model.master.create.MasterCreateBatchResultInfoDto;
import tw.com.reinbach.Atenore.Model.master.create.MasterCreateDto;
import tw.com.reinbach.Atenore.Model.master.query.MasterEditInfoDto;
import tw.com.reinbach.Atenore.Model.master.query.MasterInfoDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterEditSearchArgsAdminDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterEditSearchListInfoAdminDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterSearchArgsAdminDto;
import tw.com.reinbach.Atenore.Model.master.query.search.MasterSearchListInfoAdminDto;
import tw.com.reinbach.Atenore.Model.master.update.MasterUpdateDto;
import tw.com.reinbach.Atenore.Model.place.PlaceService;
import tw.com.reinbach.Atenore.Model.place.create.PlaceCreateBatchResultInfoDto;
import tw.com.reinbach.Atenore.Model.place.create.PlaceCreateDto;
import tw.com.reinbach.Atenore.Model.place.query.PlaceInfoDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceEditSearchArgsAdminDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceEditSearchListInfoAdminDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceSearchArgsAdminDto;
import tw.com.reinbach.Atenore.Model.place.query.search.PlaceSearchListInfoAdminDto;
import tw.com.reinbach.Atenore.Model.place.update.PlaceUpdateDto;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;

@RestController
public class ContentController {

	@Autowired
	private EventService eventService;

	@Autowired
	private PlaceService placeService;

	@Autowired
	private MasterService masterService;

	@InitBinder
	public void initBinder(WebDataBinder webDataBinder) {
		webDataBinder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}

	/* Event content management */

	/**
	 * Get event fully information by event ID
	 * 
	 * @param eventID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A15", value = "Get event fully information by event ID")
	@RequestMapping(value = { "/admin/event/{eventID}" }, method = { RequestMethod.GET })
	public ResponseModel<EventInfoAllDto> getEventFullyByEventID(@PathVariable(required = true) String eventID) {
		EventInfoAllDto dto = eventService.getEventFullyByEventID(eventID);
		if (dto == null) {
			return new ResponseModel<EventInfoAllDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<EventInfoAllDto>(200, dto);
		}
	}

	/**
	 * Get event list by filters
	 * 
	 * @param argsDto
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A18", value = "Get event list by filters")
	@RequestMapping(value = { "/admin/event" }, method = { RequestMethod.GET })
	public ResponseModel<EventSearchListInfoAdminDto> getEventListWithFilter(EventSearchArgsAdminDto argsDto) {
		EventSearchListInfoAdminDto dto = eventService.getEventListWithFilter(argsDto);
		if (dto == null) {
			return new ResponseModel<EventSearchListInfoAdminDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<EventSearchListInfoAdminDto>(200, dto);
		}
	}

	/**
	 * Event creation
	 * 
	 * @param event
	 * @param request
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws UnsupportedEncodingException
	 */
	@ApiOperation(notes = "ApiNo:A19", value = "Event creation")
	@RequestMapping(value = { "/admin/event" }, method = { RequestMethod.POST })
	public ResponseModel<Void> setEventByAdmin(EventCreateDto event, HttpServletRequest request)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		if (request.getRequestURI().equalsIgnoreCase("/admin/event")) {
			event.setMemberID("後台新增");
			event.setApply(eventService.getApplyStatusList().get(1));
		}
		return eventService.setEvent(event);
	}

	/**
	 * 
	 * @param eventID
	 * @param event
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A20", value = "Update event without images by eventID")
	@RequestMapping(value = { "/admin/event/{eventID}" }, method = { RequestMethod.PUT })
	public ResponseModel<Void> setEventUpdate(@PathVariable(required = true) String eventID, EventUpdateDto event) {
		event.setMemberID("後台新增");
		return eventService.updateEvent(eventID, event);
	}

	/**
	 * Set event image as main photo by imageID
	 * 
	 * @param imageID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A20-1", value = "Set event image as main photo by imageID")
	@RequestMapping(value = { "/admin/event/image/setMain/{imageID}" }, method = { RequestMethod.PUT })
	public ResponseModel<Void> setEventImageAsMainByImageID(@PathVariable(value = "imageID", required = true) String imageID) {
		return eventService.setEventImageAsMainByImageID(imageID);
	}

	/**
	 * Upload single event image by eventID
	 * 
	 * @param eventID
	 * @param image
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A20-2", value = "Upload single event image by eventID")
	@RequestMapping(value = { "/admin/event/image/item/{eventID}" }, method = { RequestMethod.PUT })
	public ResponseModel<Void> setEventImageByEventID(@PathVariable(value = "eventID", required = true) String eventID,
			EventImageItemCreateDto image) {
		return eventService.setEventImageByEventID(eventID, image);
	}

	/**
	 * Delete event image by imageID
	 * 
	 * @param imageID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A20-3", value = "Delete event image by imageID")
	@RequestMapping(value = { "/admin/event/image/item/{imageID}" }, method = { RequestMethod.DELETE })
	public ResponseModel<Void> deleteEventImageByImageID(@PathVariable(required = true) String imageID) {
		return eventService.deleteEventImgByImageID(imageID);
	}

	/**
	 * Get event not reviewed list by filters
	 * 
	 * @param argsDto
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A21", value = "Get event not reviewed list by filters")
	@RequestMapping(value = { "/admin/event/review" }, method = { RequestMethod.GET })
	public ResponseModel<EventSearchReviewListInfoAdminDto> getEventListNotReviewedWithFilter(EventSearchReviewArgsAdminDto argsDto) {
		EventSearchReviewListInfoAdminDto dto = eventService.getEventListNotReview(argsDto);
		if (dto == null) {
			return new ResponseModel<EventSearchReviewListInfoAdminDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<EventSearchReviewListInfoAdminDto>(200, dto);
		}
	}

	/**
	 * Get event not reviewed count
	 * 
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A21-1", value = "Get event not reviewed count")
	@RequestMapping(value = { "/admin/event/notreview/count" }, method = { RequestMethod.GET })
	public ResponseModel<EventNotReviewCountDto> getEventNotReviewedCount() {
		EventNotReviewCountDto dto = eventService.getEventListNotReviewCount();
		if (dto == null) {
			return new ResponseModel<EventNotReviewCountDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<EventNotReviewCountDto>(200, dto);
		}
	}

	/**
	 * Update event apply status by eventID
	 * 
	 * @param eventID
	 * @param argsDto
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A22", value = "Update event apply status by eventID")
	@RequestMapping(value = { "/admin/event/review/{eventID}" }, method = { RequestMethod.PUT })
	public ResponseModel<Void> updateEventApplyStatus(@PathVariable(required = true) String eventID, EventApplyDto argsDto) {
		if (eventID == null) {
			return new ResponseModel<Void>(400, ApiError.PARARMETER_NOT_FOUND);
		}
		return eventService.updateEventApplyStatus(eventID, argsDto);
	}

	/**
	 * Event batch creation by import from formal excel
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	@ApiOperation(notes = "ApiNO:A23", value = "Event batch creation by importing from formal excel")
	@RequestMapping(value = { "/admin/event/batch/create" }, method = { RequestMethod.POST })
	public ResponseModel<EventCreateBatchResultInfoDto> setEventByImportExcel(@RequestParam(required = true) MultipartFile file)
			throws IOException {
		try {
			ResponseModel<EventCreateBatchResultInfoDto> resultModel = eventService.setEventByImportExcel(file);
			return resultModel;
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseModel<EventCreateBatchResultInfoDto>(500, e.getMessage());
		}
	}

	/**
	 * Event batch update by import from formal excel
	 * 
	 * @param file
	 * @return
	 */
	@ApiOperation(notes = "ApiNO:A24", value = "Event batch update by importing from formal excel")
	@RequestMapping(value = { "/admin/event/batch/update" }, method = { RequestMethod.POST })
	public ResponseModel<Void> updateEventTag_Master_ImageByImportExcel(@RequestParam(required = true) MultipartFile file) {
		return eventService.updateEventTag_Master_ImageByImportExcel(file);
	}

	/**
	 * Delete event all information cascade with other related table by eventID
	 * 
	 * @param eventID
	 * @return
	 */
	@ApiOperation(notes = "ApiNO:A25", value = "Delete event all information cascade with other related table by eventID")
	@RequestMapping(value = { "/admin/event/{eventID}" }, method = { RequestMethod.DELETE })
	public ResponseModel<Void> deleteEventByEventID(@PathVariable(required = true) String eventID) {
		return eventService.deleteEventByEventID(eventID);
	}

	/* Place content management */

	/**
	 * Get place fully information by placeID
	 * 
	 * @param placeID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A17", value = "Get place fully information by placeID")
	@GetMapping(value = "/admin/place/{placeID}")
	public ResponseModel<PlaceInfoDto> getPlaceFullyByPlaceID(@PathVariable(required = true) String placeID) {
		PlaceInfoDto dto = placeService.getPlaceFullyByPlaceID(placeID);
		if (dto == null) {
			return new ResponseModel<PlaceInfoDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<PlaceInfoDto>(200, dto);
		}
	}

	/**
	 * Get place list by filters
	 * 
	 * @param argsDto
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A26", value = "Get place list by filters")
	@GetMapping(value = "/admin/place")
	public ResponseModel<PlaceSearchListInfoAdminDto> getPlaceListWithFilter(PlaceSearchArgsAdminDto argsDto) {
		PlaceSearchListInfoAdminDto dto = placeService.getPlaceListWithFilter(argsDto);
		if (dto == null) {
			return new ResponseModel<PlaceSearchListInfoAdminDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<PlaceSearchListInfoAdminDto>(200, dto);
		}
	}

	/**
	 * Place creation
	 * 
	 * @param place
	 * @param request
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws UnsupportedEncodingException
	 */
	@ApiOperation(notes = "ApiNo:A27", value = "Place creation")
	@PostMapping(value = "/admin/place")
	public ResponseModel<Void> setPlaceByAdmin(@ModelAttribute @Valid PlaceCreateDto place, HttpServletRequest request)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		if (request.getRequestURI().equalsIgnoreCase("/admin/place")) {
			place.setMemberID("後台新增");
		}
		return placeService.setPlace(place);
	}

	/**
	 * Place batch creation by import from formal excel
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	@ApiOperation(notes = "ApiNO:A32", value = "Place batch creation by importing from formal excel")
	@PostMapping(value = "/admin/place/batch/create")
	public ResponseModel<PlaceCreateBatchResultInfoDto> setPlaceByImportExcel(@RequestParam(required = true) MultipartFile file)
			throws IOException {
		try {
			ResponseModel<PlaceCreateBatchResultInfoDto> resultModel = placeService.setPlaceByImportExcel(file);
			return resultModel;
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseModel<PlaceCreateBatchResultInfoDto>(500, e.getMessage());
		}
	}

	/**
	 * Place batch update by import from formal excel
	 * 
	 * @param file
	 * @return
	 */
	@ApiOperation(notes = "ApiNO:A33", value = "Place batch update by importing from formal excel")
	@PostMapping(value = "/admin/place/batch/update")
	public ResponseModel<Void> updatePlaceTag_ImageByImportExcel(@RequestParam(required = true) MultipartFile file) {
		return placeService.updatePlaceTag_ImageByImportExcel(file);
	}

	/**
	 * Get place edit list
	 * 
	 * @param placeID
	 * @param argsDto
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A29", value = "Get place edit list")
	@GetMapping(value = "/admin/place/editlist/{placeID}")
	public ResponseModel<PlaceEditSearchListInfoAdminDto> getPlaceEditListByPlaceID(@PathVariable(required = true) String placeID,
			PlaceEditSearchArgsAdminDto argsDto) {
		PlaceEditSearchListInfoAdminDto dto = placeService.getPlaceEditListByPlaceID(placeID, argsDto);
		if (dto == null) {
			return new ResponseModel<PlaceEditSearchListInfoAdminDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<PlaceEditSearchListInfoAdminDto>(200, dto);
		}
	}

	/**
	 * Get place edit single recored
	 * 
	 * @param editID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A30", value = "Get place edit single recored")
	@GetMapping(value = "/admin/place/edit/{editID}")
	public ResponseModel<PlaceInfoDto> getPlaceEditByEditID(@PathVariable(required = true) String editID) {
		PlaceInfoDto dto = placeService.getPlaceEditByEditID(editID);
		if (dto == null) {
			return new ResponseModel<PlaceInfoDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<PlaceInfoDto>(200, dto);
		}
	}

	/**
	 * Update place edit version by editID in used
	 * 
	 * @param editID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A31", value = "Update place edit version by editID in used")
	@PutMapping(value = "/admin/place/edit/{editID}")
	public ResponseModel<Void> updatePlaceEditApplyInUsed(@PathVariable(required = true) String editID) {
		return placeService.updatePlaceEditApplyInUsed(editID);
	}

	/**
	 * Update place by placeID
	 * 
	 * @param placeID
	 * @param place
	 * @param tagIDList
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A28", value = "Update place placeID")
	@PutMapping(value = "/admin/place/{placeID}")
	public ResponseModel<Void> setPlaceUpdate(@PathVariable(required = true) String placeID, PlaceUpdateDto place,
			@RequestParam(required = false) List<String> imageIDList, HttpServletRequest request)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		if (request.getRequestURI().equalsIgnoreCase("/admin/place")) {
			place.setMemberID("後台新增");
		}
		return placeService.updatePlace(placeID, place, imageIDList);
	}

	/**
	 * Delete place all information cascade with other related table by placeID
	 * 
	 * @param placeID
	 * @return
	 */
	@ApiOperation(notes = "ApiNO:A34", value = "Delete place all information cascade with other related table by placeID")
	@DeleteMapping(value = "/admin/place/{placeID}")
	public ResponseModel<Void> deletePlaceByPlaceID(@PathVariable(required = true) String placeID) {
		return placeService.deletePlaceByPlaceID(placeID);
	}

	/**
	 * Place edit delete. Delete place edit version cascade with other related data
	 * by editID
	 * 
	 * @param editID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A35", value = "Place edit delete. Delete place edit version cascade with other related data by editID")
	@DeleteMapping(value = "/admin/place/edit/{editID}")
	public ResponseModel<Void> deletePlaceEditByEditID(@PathVariable(required = true) String editID) {
		return placeService.deletePlaceEditByEditID(editID);
	}

	/* Master content management */

	/**
	 * Get master fully information by master ID
	 * 
	 * @param masterID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A16", value = "Get master fully information by master ID")
	@RequestMapping(value = { "/admin/master/{masterID}" }, method = { RequestMethod.GET })
	public ResponseModel<MasterInfoDto> getMasterFullyByMasterID(@PathVariable(required = true) String masterID) {
		MasterInfoDto dto = masterService.getMasterFullyByMasterID(masterID);
		if (dto == null) {
			return new ResponseModel<MasterInfoDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<MasterInfoDto>(200, dto);
		}
	}

	/**
	 * Get master list by filters
	 * 
	 * @param argsDto
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A36", value = "Get master list by filters")
	@RequestMapping(value = { "/admin/master" }, method = { RequestMethod.GET })
	public ResponseModel<MasterSearchListInfoAdminDto> getMasterListWithFilter(MasterSearchArgsAdminDto argsDto) {
		MasterSearchListInfoAdminDto dto = masterService.getMasterListWithFilter(argsDto);
		if (dto == null) {
			return new ResponseModel<MasterSearchListInfoAdminDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<MasterSearchListInfoAdminDto>(200, dto);
		}
	}

	/**
	 * Master creation
	 * 
	 * @param master
	 * @param request
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A37", value = "Master creation")
	@RequestMapping(value = { "/admin/master" }, method = { RequestMethod.POST })
	public ResponseModel<Void> setMasterByAdmin(MasterCreateDto master, HttpServletRequest request) {
		if (request.getRequestURI().equalsIgnoreCase("/admin/master")) {
			master.setMemberID("後台新增");
		}
		return masterService.setMaster(master);
	}

	/**
	 * Master update. On the sequence, to create a new version of edit record then
	 * update current information by using the latest edit version. In short, this
	 * mechanism let everyone to edit, information correct or not depends on 'speech
	 * market' theory.
	 * 
	 * @param masterID
	 * @param master
	 * @param request
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A38", value = "Master update. On the sequence, to create a new version of edit record then update current information by using the latest edit version")
	@RequestMapping(value = { "/admin/master/{masterID}" }, method = { RequestMethod.PUT })
	public ResponseModel<Void> updateMaster(@PathVariable(required = true) String masterID, MasterUpdateDto master,
			HttpServletRequest request) {
		if (request.getRequestURI().equalsIgnoreCase("/admin/master/" + masterID)) {
			master.setMemberID("後台管理");
		}
		return masterService.updateMaster(masterID, master);
	}

	/**
	 * Get master edit list
	 * 
	 * @param masterID
	 * @param argsDto
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A39", value = "Get master edit list")
	@RequestMapping(value = { "/admin/master/editlist/{masterID}" }, method = { RequestMethod.GET })
	public ResponseModel<MasterEditSearchListInfoAdminDto> getMaserEditListByMasterID(@PathVariable(required = true) String masterID,
			MasterEditSearchArgsAdminDto argsDto) {
		MasterEditSearchListInfoAdminDto dto = masterService.getMaserEditListByMasterID(masterID, argsDto);
		if (dto == null) {
			return new ResponseModel<MasterEditSearchListInfoAdminDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<MasterEditSearchListInfoAdminDto>(200, dto);
		}
	}

	/**
	 * Get master edit single recored
	 * 
	 * @param editID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A40", value = "Get master edit single recored")
	@RequestMapping(value = { "/admin/master/edit/{editID}" }, method = { RequestMethod.GET })
	public ResponseModel<MasterEditInfoDto> getMasterEditByEditID(@PathVariable(required = true) String editID) {
		MasterEditInfoDto dto = masterService.getMasterEditByEditID(editID);
		if (dto == null) {
			return new ResponseModel<MasterEditInfoDto>(404, ApiError.DATA_NOT_FOUND);
		} else {
			return new ResponseModel<MasterEditInfoDto>(200, dto);
		}
	}

	/**
	 * Update master edit version by editID in used
	 * 
	 * @param editID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A41", value = "Update master edit version by editID in used")
	@RequestMapping(value = { "/admin/master/edit/{editID}" }, method = { RequestMethod.PUT })
	public ResponseModel<Void> updateMasterEditApplyInUsed(@PathVariable(required = true) String editID) {
		return masterService.updateMasterEditApplyInUsed(editID);
	}

	/**
	 * Master batch creation by importing from formal excel
	 * 
	 * @param file
	 * @return
	 */
	@ApiOperation(notes = "ApiNO:A42", value = "Master batch creation by importing from formal excel")
	@RequestMapping(value = { "/admin/master/batch/create" }, method = { RequestMethod.POST })
	public ResponseModel<MasterCreateBatchResultInfoDto> setMasterByImportExcel(MultipartFile file) {
		try {
			ResponseModel<MasterCreateBatchResultInfoDto> resultModel = masterService.setMasterByImportExcel(file);
			return resultModel;
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseModel<MasterCreateBatchResultInfoDto>(500, e.getMessage());
		}
	}

	/**
	 * Master batch update by importing from formal excel
	 * 
	 * @param file
	 * @return
	 */
	@ApiOperation(notes = "ApiNO:A43", value = "Master batch update by importing from formal excel")
	@RequestMapping(value = { "/admin/master/batch/update" }, method = { RequestMethod.POST })
	public ResponseModel<Void> updateMasterTagByImportExcel(@RequestParam(required = true) MultipartFile file) {
		return masterService.updateMasterTagByImportExcel(file);
	}

	/**
	 * Master delete. Delete master cascade with other related data by masterID
	 * 
	 * @param masterID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A44", value = "Master delete. Delete master cascade with other related data by masterID")
	@RequestMapping(value = { "/admin/master/{masterID}" }, method = { RequestMethod.DELETE })
	public ResponseModel<Void> deleteMasterByMasterID(@PathVariable(required = true) String masterID) {
		return masterService.deleteMasterByMasterID(masterID);
	}

	/**
	 * Master edit delete. Delete master edit version cascade with other related
	 * data by editID
	 * 
	 * @param editID
	 * @return
	 */
	@ApiOperation(notes = "ApiNo:A45", value = "Master edit delete. Delete master edit version cascade with other related data by editID")
	@RequestMapping(value = { "/admin/master/edit/{editID}" }, method = { RequestMethod.DELETE })
	public ResponseModel<Void> deleteMasterEditByEditID(@PathVariable(required = true) String editID) {
		return masterService.deleteMasterEditByEditID(editID);
	}

}
