package tw.com.reinbach.Atenore.Model.place.component;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import io.swagger.annotations.ApiModelProperty;

public class PlaceDto {

	@ApiModelProperty(value = "場所編號")
	private String placeID;

//	分類屬性
	@ApiModelProperty(value = "場所分類編號")
	private Integer typeID;
	@ApiModelProperty(value = "場所分類名稱")
	private String typeName;
	@ApiModelProperty(value = "場所大分類編號")
	private Integer mainID;
	@ApiModelProperty(value = "場所大分類名稱")
	private String mainTypeName;

	@ApiModelProperty(value = "場所名稱")
	private String name;

	@ApiModelProperty(value = "場所最大容留人數")
	private Integer max;

	@ApiModelProperty(value = "場所最大容留人數名稱")
	private String maxName;

	@ApiModelProperty(value = "場所面積", example = "50.5坪")
	private String area;

	@ApiModelProperty(value = "場所縣市", example = "新北市")
	private String county;

	@ApiModelProperty(value = "場所地址(排除縣市)", example = "板橋區縣民大道55號2巷158號6樓")
	private String address;

	@ApiModelProperty(value = "場所聯絡資訊")
	private List<PlaceLiaisonDto> liaison;

	@ApiModelProperty(value = "場所相關單位")
	private String unit;

	@ApiModelProperty(value = "場所email", example = "aaa@gmail.com")
	private String mail;

	@ApiModelProperty(value = "場所FB", example = "https://www.facebook.com/tsaiingwen/")
	private String facebook;

	@ApiModelProperty(value = "場所服務項目", example = "會議討論、設備租借")
	private String service;

	@ApiModelProperty(value = "場所簡介")
	private String intro;

	@ApiModelProperty(value = "場所網站URL")
	private String website;

	@ApiModelProperty(value = "場所累計點閱數")
	private Integer click;

	@ApiModelProperty(value = "場所累計追蹤數")
	private Integer follow;

	@ApiModelProperty(value = "GPS, Lat", example = "25.05788")
	private BigDecimal lat;

	@ApiModelProperty(value = "GPS, Lng", example = "24.55855")
	private BigDecimal lng;

	@ApiModelProperty(value = "資訊創立時間", example = "2020-01-10 10:00:00", hidden = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime insertTime;

	@ApiModelProperty(value = "場所資訊是否開放編輯", example = "true/false")
	private String editable;

	@ApiModelProperty(value = "場所發布者編號(現行版本或各歷史編輯版本)")
	private String memberID;

	/*
	 * Extend attribute, for Place Edit Table. By controlling version.
	 * 
	 */
	@ApiModelProperty(value = "場所資訊歷史編輯編號")
	private String editID;

	@ApiModelProperty(value = "場所資訊歷史編輯版本是否使用中", example = "true/false")
	private String inUsed;

	@ApiModelProperty(value = "default value is false, if user has login and followed, would be true.", example = "false")
	private Boolean isFollowed = false;

	public String getPlaceID() {
		return placeID;
	}

	public void setPlaceID(String placeID) {
		this.placeID = placeID;
	}

	public Integer getTypeID() {
		return typeID;
	}

	public void setTypeID(Integer typeID) {
		this.typeID = typeID;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public Integer getMainID() {
		return mainID;
	}

	public void setMainID(Integer mainID) {
		this.mainID = mainID;
	}

	public String getMainTypeName() {
		return mainTypeName;
	}

	public void setMainTypeName(String mainTypeName) {
		this.mainTypeName = mainTypeName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getMax() {
		return max;
	}

	public void setMax(Integer max) {
		this.max = max;
	}

	public String getMaxName() {
		return maxName;
	}

	public void setMaxName(String maxName) {
		this.maxName = maxName;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public List<PlaceLiaisonDto> getLiaison() {
		return liaison;
	}

	public void setLiaison(List<PlaceLiaisonDto> liaison) {
		this.liaison = liaison;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public Integer getClick() {
		return click;
	}

	public void setClick(Integer click) {
		this.click = click;
	}

	public Integer getFollow() {
		return follow;
	}

	public void setFollow(Integer follow) {
		this.follow = follow;
	}

	public BigDecimal getLat() {
		return lat;
	}

	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}

	public BigDecimal getLng() {
		return lng;
	}

	public void setLng(BigDecimal lng) {
		this.lng = lng;
	}

	public LocalDateTime getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(LocalDateTime insertTime) {
		this.insertTime = insertTime;
	}

	public String getEditable() {
		return editable;
	}

	public void setEditable(String editable) {
		this.editable = editable;
	}

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public String getEditID() {
		return editID;
	}

	public void setEditID(String editID) {
		this.editID = editID;
	}

	public String getInUsed() {
		return inUsed;
	}

	public void setInUsed(String inUsed) {
		this.inUsed = inUsed;
	}

	public Boolean getIsFollowed() {
		return isFollowed;
	}

	public void setIsFollowed(Boolean isFollowed) {
		this.isFollowed = isFollowed;
	}

}
