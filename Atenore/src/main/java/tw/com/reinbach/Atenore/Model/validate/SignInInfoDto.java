package tw.com.reinbach.Atenore.Model.validate;

import org.springframework.security.oauth2.common.OAuth2AccessToken;

public class SignInInfoDto {

	private String memberID;

	private OAuth2AccessToken oauth2AccessToken;

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public OAuth2AccessToken getOauth2AccessToken() {
		return oauth2AccessToken;
	}

	public void setOauth2AccessToken(OAuth2AccessToken oauth2AccessToken) {
		this.oauth2AccessToken = oauth2AccessToken;
	}
	
	
	
}
