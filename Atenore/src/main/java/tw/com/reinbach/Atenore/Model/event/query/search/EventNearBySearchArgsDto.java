package tw.com.reinbach.Atenore.Model.event.query.search;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.validation.constraints.NotEmpty;

import org.springframework.format.annotation.DateTimeFormat;

import io.swagger.annotations.ApiModelProperty;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;

public class EventNearBySearchArgsDto {

	@ApiModelProperty(hidden = true)
	private String memberID;

	@ApiModelProperty(value = "user's lat", example = "25.0639368", required = true)
	private BigDecimal lat;

	@ApiModelProperty(value = "user's lng", example = "121.5471534", required = true)
	private BigDecimal lng;
	
	@ApiModelProperty(value = "data filter by today only or not, default is false" , required = true, example = "false")
	private Boolean todayOnly = false; 
	
	@NotEmpty(message = ApiError.REQUIRED_INPUT_NOT_FONUD)
	@ApiModelProperty(required = true)
	private EventSearchPreferArgDto searchPreferArg;
	
	@ApiModelProperty(hidden = true)
	private BigDecimal minLat;
	
	@ApiModelProperty(hidden = true)
	private BigDecimal maxLat;
	
	@ApiModelProperty(hidden = true)
	private BigDecimal minLng;
	
	@ApiModelProperty(hidden = true)
	private BigDecimal maxLng;

	@ApiModelProperty(name = "startTime", example = "2020-01-10 10:00:00", dataType = "LocalDateTime")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime startTime;

	@ApiModelProperty(name = "endTime", example = "2020-01-10 10:00:00", dataType = "LocalDateTime")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime endTime;

	@ApiModelProperty(hidden = true)
	private String apply;

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public BigDecimal getLat() {
		return lat;
	}

	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}

	public BigDecimal getLng() {
		return lng;
	}

	public void setLng(BigDecimal lng) {
		this.lng = lng;
	}

	public EventSearchPreferArgDto getSearchPreferArg() {
		return searchPreferArg;
	}

	public void setSearchPreferArg(EventSearchPreferArgDto searchPreferArg) {
		this.searchPreferArg = searchPreferArg;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}

	public String getApply() {
		return apply;
	}

	public void setApply(String apply) {
		this.apply = apply;
	}

	public Boolean getTodayOnly() {
		return todayOnly;
	}

	public void setTodayOnly(Boolean todayOnly) {
		this.todayOnly = todayOnly;
	}

	public BigDecimal getMinLat() {
		return minLat;
	}

	public void setMinLat(BigDecimal minLat) {
		this.minLat = minLat;
	}

	public BigDecimal getMaxLat() {
		return maxLat;
	}

	public void setMaxLat(BigDecimal maxLat) {
		this.maxLat = maxLat;
	}

	public BigDecimal getMinLng() {
		return minLng;
	}

	public void setMinLng(BigDecimal minLng) {
		this.minLng = minLng;
	}

	public BigDecimal getMaxLng() {
		return maxLng;
	}

	public void setMaxLng(BigDecimal maxLng) {
		this.maxLng = maxLng;
	}
	
	
	
}
