package tw.com.reinbach.Atenore.Model.place.query.search;

import io.swagger.annotations.ApiModelProperty;

public class PlaceSearchSimpleInfoDto {

	@ApiModelProperty(example = "123e4567-e89b-42d3-a456-556642440000")
	private String placeID;

	@ApiModelProperty(example = "診所名稱")
	private String name;

	public String getPlaceID() {
		return placeID;
	}

	public void setPlaceID(String placeID) {
		this.placeID = placeID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
