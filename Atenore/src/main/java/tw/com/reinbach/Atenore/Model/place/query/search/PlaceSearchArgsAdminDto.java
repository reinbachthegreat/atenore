package tw.com.reinbach.Atenore.Model.place.query.search;

import io.swagger.annotations.ApiModelProperty;

public class PlaceSearchArgsAdminDto {

	@ApiModelProperty(value = "fuzzy searching by name keywords")
	private String fuzzyFilter;

	@ApiModelProperty(value = "區域縣市")
	private String county;

	@ApiModelProperty(value = "場所0 / 場地1 類型", example = "1")
	private Integer mainTypePreferArg;

	public String getFuzzyFilter() {
		return fuzzyFilter;
	}

	public void setFuzzyFilter(String fuzzyFilter) {
		this.fuzzyFilter = fuzzyFilter;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public Integer getMainTypePreferArg() {
		return mainTypePreferArg;
	}

	public void setMainTypePreferArg(Integer mainTypePreferArg) {
		this.mainTypePreferArg = mainTypePreferArg;
	}

}
