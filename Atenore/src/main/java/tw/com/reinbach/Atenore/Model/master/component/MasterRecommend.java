package tw.com.reinbach.Atenore.Model.master.component;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "Master recommender actually means this record's history editor.")
public class MasterRecommend {
	
	@ApiModelProperty(hidden = true)
	private String masterID;
	
	@ApiModelProperty(notes = "Alias editor's id. To be editor, must be member or administrator.", example = "123e4567-e89b-42d3-a456-556642440000")
	private String memberID;
	
	@ApiModelProperty(notes = "Alias editor's name. To be editor, must be member or administrator.", example = "Akane Lee")
	private String memberName;

	public String getMasterID() {
		return masterID;
	}

	public void setMasterID(String masterID) {
		this.masterID = masterID;
	}

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	
	
	
}
