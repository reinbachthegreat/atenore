package tw.com.reinbach.Atenore.Model.marketing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.multipart.MultipartFile;

import tw.com.reinbach.Atenore.Model.marketing.component.AdsDto;
import tw.com.reinbach.Atenore.Model.marketing.component.AdsTagDto;
import tw.com.reinbach.Atenore.Model.marketing.create.AdsCreateDto;
import tw.com.reinbach.Atenore.Model.marketing.query.AdsInfoDto;
import tw.com.reinbach.Atenore.Model.marketing.query.AdsTypeListInfoDto;
import tw.com.reinbach.Atenore.Model.marketing.query.search.AdsSearchArgsDto;
import tw.com.reinbach.Atenore.Model.marketing.query.search.AdsSearchInfoDto;
import tw.com.reinbach.Atenore.Model.marketing.query.search.AdsSearchListInfoDto;
import tw.com.reinbach.Atenore.Model.marketing.update.AdsUpdateDto;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ApiError;
import tw.com.reinbach.Atenore.RestController.ResponseModel.ResponseModel;
import tw.com.reinbach.Atenore.Utility.UUIDGenerator;
import tw.com.reinbach.Atenore.Utility.Utility;
import tw.com.reinbach.Atenore.Utility.aws.AmazonDto;
import tw.com.reinbach.Atenore.Utility.aws.AmazonService;

@Service
public class AdsService {

	@Autowired
	private AdsDAO adsDAO;
	
	@Autowired
	private Utility utility;
	
	@Autowired
	private AmazonService amazonService;
	
	private String[] adsType = {"活動", "場所", "達人"};
	
	@Autowired
	private PlatformTransactionManager transactionManager;
	
	/** Get ads type list
	 * 
	 * @return
	 */
	public AdsTypeListInfoDto getAdsTypeList(){
		AdsTypeListInfoDto dto = new AdsTypeListInfoDto();
		dto.setAdsTypeList(new ArrayList<String>(Arrays.asList(this.adsType)));
		return dto;
	}

	/** Get ads list by filters
	 * 
	 * @param argsDto
	 * @return
	 */
	public AdsSearchListInfoDto getAdsListByFilters(AdsSearchArgsDto argsDto) {
		List<AdsSearchInfoDto> list = adsDAO.getAdsListByFilters(argsDto);
		if(list!=null && list.size()!=0) {
			AdsSearchListInfoDto dto = new AdsSearchListInfoDto();
			dto.setAdsList(list);
			return dto;
		}else {
			return null;
		}
	}
	
	/** Create ads
	 * 
	 * @param ads
	 * @param adsTagList
	 * @return
	 */
	public ResponseModel<Void> createAds(AdsCreateDto ads){
		
		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> resultModel = null;
		
		
		String adsID = UUIDGenerator.generateType4UUID().toString(); 
		ads.setAdsID(adsID);
		List<String> adsTagList = ads.getAdsTagList(); 
		MultipartFile file = ads.getFile();
		
		if(file!=null && file.getSize()!=0) {
			amazonService.setFolderName(new StringBuffer().append("Ads").append("/").append(adsID).toString());
			try {
				AmazonDto dto = amazonService.uploadFile(file);
				ads.setImg(dto.getUrl());
				ads.setObjectKey(dto.getObjectKey());
			} catch (IOException e) {
				return new ResponseModel<>(500, ApiError.AWS_FILE_UPLOAD_FAILED);
			}
		}
		
		try {
			resultModel = adsDAO.setAds(ads);
			if(resultModel.getCode()!=201) {
				throw new Exception();
			}
			
			List<AdsTagDto> tagList = new ArrayList<AdsTagDto>();
			for(String tag : adsTagList) {
				AdsTagDto dto = new AdsTagDto();
				dto.setTagID(UUIDGenerator.generateType4UUID().toString());
				dto.setAdsID(adsID);
				dto.setTag(tag);
				tagList.add(dto);
			}
			
			resultModel = adsDAO.setAdsTag(tagList);
			if(resultModel.getCode()!=201) {
				throw new Exception();
			}
			
			transactionManager.commit(status);
			return new ResponseModel<Void>(201, "");
		}catch(Exception e) {
			transactionManager.rollback(status);
			return resultModel;
		}
	}
	
	/** Get ads with tag list by adsID
	 * 
	 * @param adsID
	 * @return
	 */
	public AdsInfoDto getAdsByAdsID(String adsID) {
		AdsDto ads = adsDAO.getAdsByAdsID(adsID);
		if(ads==null) {
			return null;
		}
		AdsInfoDto dto = new AdsInfoDto();
		dto.setAds(ads);
		dto.setAdsTagList(adsDAO.getAdsTagByAdsID(adsID));
		return dto;
	}
	
	/** Update Ads info and tag list by adsID
	 * 
	 * @param adsID
	 * @param formAds
	 * @param tagList
	 * @return
	 */
	public ResponseModel<Void> updateAdsByAdsID(String adsID, AdsUpdateDto formAds){
		
		formAds.setAdsID(adsID);
		List<String> tagList = formAds.getTagList();
		MultipartFile file = formAds.getFile();
		
		AdsUpdateDto orginalAds = adsDAO.getAdsForUpdateByAdsID(adsID);
		utility.updateByDto(formAds, orginalAds);
		if(file!=null && file.getSize()!=0) {
			String originalObjectKey = orginalAds.getObjectKey();
			amazonService.setFolderName(new StringBuffer().append("Ads").append("/").append(adsID).toString());
			try {
				AmazonDto dto = amazonService.uploadFile(file);
				orginalAds.setImg(dto.getUrl());
				orginalAds.setObjectKey(dto.getObjectKey());
			} catch (IOException e) {
				return new ResponseModel<>(500, ApiError.AWS_FILE_UPLOAD_FAILED);
			}
			
			if(originalObjectKey!=null) {
				amazonService.deleteFileReturnResponse(originalObjectKey);
			}
		}
		
		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> resultModel = null;
		
		try {
			resultModel = adsDAO.updateAds(orginalAds);
			if(resultModel.getCode()!=201 && resultModel.getCode()!=204) {
				throw new Exception();
			}
			
			resultModel = adsDAO.deleteAdsTagByAdsID(adsID);
			if(resultModel.getCode()!=201 && resultModel.getCode()!=204) {
				throw new Exception();
			}
			
			List<AdsTagDto> list = new ArrayList<>();
			for(String tag : tagList) {
				AdsTagDto dto = new AdsTagDto();
				dto.setTagID(UUIDGenerator.generateType4UUID().toString());
				dto.setAdsID(adsID);
				dto.setTag(tag);
				list.add(dto);
			}
			resultModel = adsDAO.setAdsTag(list);
			if(resultModel.getCode()!=201 && resultModel.getCode()!=204) {
				throw new Exception();
			}			
			
			transactionManager.commit(status);
			return new ResponseModel<Void>(204, "");
		}catch(Exception e) {
			transactionManager.rollback(status);
			return resultModel;
		}
	}
	
	/** Delete Ads info and tag list by adsID
	 * 
	 * @param adsID
	 * @return
	 */
	public ResponseModel<Void> deleteAdsByAdsID(String adsID){
		
		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		ResponseModel<Void> resultModel = null;		
		
		try {
			AdsDto original = adsDAO.getAdsByAdsID(adsID);
			
			resultModel = adsDAO.deleteAdsTagByAdsID(adsID);
			if(resultModel.getCode()!=204) {
				throw new Exception();
			}
			
			resultModel = adsDAO.deleteAdsByAdsID(adsID);
			if(resultModel.getCode()!=204) {
				throw new Exception();
			}			
			
			transactionManager.commit(status);
			if(original.getObjectKey()!=null) {
				amazonService.deleteFileReturnResponse(original.getObjectKey());
			}
			
			return new ResponseModel<Void>(204, "");
		}catch(Exception e) {
			transactionManager.rollback(status);
			return resultModel;
		}
	}
	
}
