var allowGeoPosition = ({
    data: {
        hasPermissionGetGeo: "",
        position: {}
    },
    methods: {
        disallowGeoAlert: function () {
            this.hasPermissionGetGeo = "disallow";
            sessionStorage.setItem("hasPermissionGetGeo", "disallow");
            this.toggleGeoAlert();
        },
        allowGetPosition: function () {
            this.hasPermissionGetGeo = "allow";
            sessionStorage.setItem("hasPermissionGetGeo", "allow");
            this.toggleGeoAlert();
            this.getPosition();
        },
        toggleGeoAlert: function () {
            if (this.hasPermissionGetGeo != "allow") {
                $("#geolocation-alert").fadeToggle(300);
            } else {
                $("#geolocation-alert").fadeOut(300);
            }
        },
        // 定位到目前位置
        getPosition: function (callback) {
            var vm = this,
                callback = callback;
            
        	if(vm.position.lat!=null && vm.position.lat!="" && vm.position.lng!=null&vm.position.lng!=""){
                if (typeof callback === "function") {
                    callback();
                }        		
        	} else if (this.hasPermissionGetGeo == "allow") {
            	if (navigator.geolocation) {
                    // 取得位置
                    navigator.geolocation.getCurrentPosition(function (position) {
                        if (position.coords.latitude && position.coords.longitude) {
                            vm.position = { lat: position.coords.latitude, lng: position.coords.longitude };
                            if (typeof callback === "function") {
                                callback();
                            }
                        } else {
                            vm.toggleGeoAlert();
                        }
                    });
                }
            } else {
                this.toggleGeoAlert();
            }
        },
        // 設定map
        locationPlace: function () {
            var vm = this;
            vm.getPosition(function () {
                if (map) {
                    map.setCenter({ lat: parseFloat(vm.position.lat), lng: parseFloat(vm.position.lng) });
                } else {
                    map = new google.maps.Map(document.getElementById("map"), {
                        center: { lat: parseFloat(vm.position.lat), lng: parseFloat(vm.position.lng) },
                        zoom: 15,
                        disableDefaultUI: true
                    });
                }
            });
        }
    },
    mounted: function () {
        if (sessionStorage.getItem("hasPermissionGetGeo")) {
            this.hasPermissionGetGeo = sessionStorage.getItem("hasPermissionGetGeo");
        }
    }
})