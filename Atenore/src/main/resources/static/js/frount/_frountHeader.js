$(document).ready(function () {
    $('header #menu-toggle:not(.show)').click(function (event) {
        $('.side-collapse').addClass('nav-mobile-show');
        $('.modal-backdrop').css("pointer-events", "auto").addClass('show');
        $('.modal-backdrop::after').css("content", "\f00d");
    });
    $('header .modal-backdrop').click(function (event) {
        $('.dropdown-menu').removeClass('show');
        $('.side-collapse').removeClass('nav-mobile-show');
        $('.modal-backdrop').css("pointer-events", "none").removeClass('show');
        $('.modal-backdrop::after').css("content", "");
    });
    if (location.href.includes("login")) {
        $("#header-logo").addClass("invisible");
        $("#header-login").addClass("invisible");
        $("header").removeClass("header-box-shadow");
    }
});