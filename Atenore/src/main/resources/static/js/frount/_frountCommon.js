// hover換圖片
$(".pic-slider-img").hover(function () {
    var hoverSrc = $(this).attr("src");
    $("#big-pic").attr("src", hoverSrc);
    $(".pic-slider-img.hover").removeClass("hover");
    $(this).addClass("hover");
})

// 大網圖片左右slider
$("#pic-slider-arrow-wrap-right").click(function () {
    if ($(".pic-slider").length > 1) {
        $(".pic-slider.show").animate({
            left: "-500px",
            right: "500px"
        }, 1000);
        $(".pic-slider:not(.show)").css("left", "500px").css("right", "-500px").animate({
            left: "0px",
            right: "0px"
        }, 1000);
        $(".pic-slider").toggleClass("show");
    }
})

$("#pic-slider-arrow-wrap-left").click(function () {
    if ($(".pic-slider").length > 1) {
        $(".pic-slider.show").animate({
            left: "500px",
            right: "-500px"
        }, 1000);
        $(".pic-slider:not(.show)").css("left", "-500px").css("right", "500px").animate({
            left: "0px",
            right: "0px"
        }, 1000);
        $(".pic-slider").toggleClass("show");
    }
})

// 閱讀全文
$(".detail-article-readmore").click(function () {
    var Height = $(this).siblings(".detail-article").get(0).scrollHeight;
    $(this).siblings(".detail-article").animate({ height: Height }, 1000).addClass("show");
    $(this).animate({ "height": "0px", "margin-top": "0" }, 1000).removeClass("show");
})

// input顯示標題
$(".form-control").focus(function () {
    var field = $(this).attr("data-field");
    if ($("[data-field=" + field + "-title]")) {
        $("[data-field=" + field + "-title]").animate({ opacity: 1 }, 100);
    }
}).blur(function () {
    var field = $(this).attr("data-field");
    if ($("[data-field=" + field + "-title]")) {
        $("[data-field=" + field + "-title]").animate({ opacity: 0 }, 100);
    }
})

// 會員選單
var nowPage = location.pathname.split("/")[2];
$(".member .nav-link").each(function () {
    if ($(this).attr("data-active-page") == nowPage) {
        var newSrc = $(this).children("img").attr("src").replace("grey", "accent");
        $(this).children("img").attr("src", newSrc);
        $(this).addClass("active");
    }
})

$('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
    $("#breadcrumb-current-page").text($(e.target).attr("data-tab-name"));
})

// 密碼顯現
$(".show_password_icon").on("click", function () {
    var ImgSrc = $(this).attr("src");
    if (ImgSrc.includes("show")) {
        $(this).attr("src", ImgSrc.replace("show", "hide"));
        $(this).parents().children("input").attr("type", "password");
    } else {
        $(this).attr("src", ImgSrc.replace("hide", "show"));
        $(this).parents().children("input").attr("type", "text");
    }
})

// 加入收藏
$(".add-to-favorite-button").hover(function () {
    var imgSrc = $(this).children("img").prop("src");
    $(this).children("img").prop("src", imgSrc.replace("accent-hollow", "white"))
}, function () {
    var imgSrc = $(this).children("img").prop("src");
    $(this).children("img").prop("src", imgSrc.replace("white", "accent-hollow"))	
})

//驗證欄位
function validate(inputs) {
    var validate = true;

    inputs.each(function (e, index) {
        var field = $(this).attr("data-field"),
            valu = $(this).val(),
            alertOpacity = $(this).val() == "" ? 1 : 0;
        $("[data-field=" + field + "-alert]").animate({ opacity: 0 }, 100);

        switch (field) {
            case "email":
                // const EmailPatt = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                if (!valu.includes("@")) {
                    $("[data-field=" + field + "-alert]").animate({ opacity: 1 }, 100);
                    validate = false;
                }
                break;
            case "phone":
                if (valu != '' && Number(valu) != valu) {
                    $("[data-field=" + field + "-alert]").animate({ opacity: 1 }, 100);
                    validate = false;
                }
                break;
            case "cell":
                if (valu == '' || Number(valu) != valu) {
                    $("[data-field=" + field + "-alert]").animate({ opacity: 1 }, 100);
                    validate = false;
                }
                break;
            case "name":
            case "county":
            case "address1":
            case "address2":
            case "lastName":
            case "firstName":
            case "oldPassword":
                if (!valu || valu == '') {
                    $("[data-field=" + field + "-alert]").animate({ opacity: 1 }, 100);
                    validate = false;
                }
                break;
            case "password":
            case "newPassword":
            case "passwordCheck":
                const passwordPatt = /\d.*[A-Z]|[A-Z].*\d/,
                    password = $("[data-field=password]").val(),
                    passwordCheck = $("[data-field=passwordCheck]").val();
                if (!passwordPatt.test(valu)) {
                    $("[data-field=" + field + "-alert]").animate({ opacity: 1 }, 100);
                    validate = false;
                }
                if (password != passwordCheck) {
                    $("[data-field=passwordCheck-alert]").animate({ opacity: 1 }, 100);
                    validate = false;
                }
                if (valu == '') {
                    $("[data-field=passwordCheck-alert]").animate({ opacity: 1 }, 100);
                    validate = false;
                }
                break;
        }
    })
    return validate
}

function getStorageItem(arguments) {
    var returnObj = {};
    if (sessionStorage.getItem(arguments)) {
        var arr = sessionStorage.getItem(arguments).split(";");
        arr.forEach(function (item) {
            returnObj[item.split("=")[0]] = item.split("=")[1] || "";
        })
    }
    return returnObj
}

function setStorageItem(key, valObj) {
    var cookieStr = "";
    for (var i = 0; i < Object.keys(valObj).length; i++) {
        if (i != 0) {
            cookieStr += ";";
        }
        cookieStr += Object.keys(valObj)[i] + "=" + Object.values(valObj)[i];
    }
    sessionStorage.setItem(key, cookieStr);
}