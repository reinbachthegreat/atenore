var dataTable = new Vue({
    el: '#data-wrap',
    data: {
        limit: {
            read: checkLimit([], "1"),
            edit: checkLimit([], "2"),
            delete: checkLimit([], "3")
        },
        dataList: [], //回傳資料
        page: {
            dataCount: 0,
            current: 1
        },
        initSet: { // 初始查詢設定
            begin: 1,
            end: 20,
            order: "DESC"
        },
        getDataParm:{
            search: "",
            filter: "All",
            date: "",
        },
        emptyMsg: "查無符合條件之分類",
        cateLevel: [{name:"美食",cID:"C01"}, {name:"非主食類",cID:"CM01"},{name:"甜點",cID:"CS01"}] //[0]大分類,[1]中分類,[2]小分類
    },
    computed: {
        emptyData: function () {
            return this.dataList.length == 0;
        },
        showPages: function () {
            return this.page.dataCount > this.initSet.end
        },
        showData:function(){
            let begin = (this.page.current-1)*this.initSet.end;
            let end = (this.page.current)*this.initSet.end;
            
            return this.dataList.slice(begin,end);
        }
    },
    created: function () { //call ajax
        this.renewDataRow();
    },
    mounted: function () {
        this.$refs.listWrap.classList.remove('d-none');
        let pageWrap = this.$refs.pagination;
        let self = this;
        $(pageWrap).pagination({
            items: 0,
            itemsOnPage: this.initSet.end,
            cssStyle: 'light-theme',
            hrefTextPrefix: "#",
            onPageClick: function (pageNumber) {
                self.page.current = pageNumber;
                self.getDataList();
            }
        });
    },
    methods: {
        getDataList: function () { //get admin account list data
            return axios.get(getDataListUrl, this.getDataParm);
        },
        dataListCallBack: function (res) {
        	console.log(res);
            if(this.initSet.order=="DESC"){
                this.dataList = [...res.data.data].reverse();
            }else{
                this.dataList = [...res.data.data]
            }
            console.log(this.dataList);
            this.page.dataCount = this.dataList.length;
            let pageWrap = this.$refs.pagination;
            $(pageWrap).pagination('drawPage', 1);
            $(pageWrap).pagination('updateItems', this.page.dataCount);
        },
        renewDataRow: function () { //renew admin account list data
            this.initSet.begin = 1;
            this.page.current=1;
            this.getDataList().then((res) => {
                this.dataListCallBack(res);
            }).catch(error => {
                console.error(error);
            });
        },
        computeSn(i) {
            return getItemNum(i, this.initSet.order, this.initSet.end, this.page.current, this.page.dataCount);
        },
        changeOrder: function () {
            this.initSet.order = (this.initSet.order == "DESC") ? "ASC" : "DESC";
            this.dataList.reverse();
        }
    }
});