// check if is mobile
var isMobileCheck = {
    Android: function () {
        return navigator.userAgent.match(/Android/i) ? true : false;
    },
    BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i) ? true : false;
    },
    iOS: function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i) ? true : false;
    },
    Windows: function () {
        return navigator.userAgent.match(/IEMobile/i) ? true : false;
    },
    any: function () {
        return (isMobileCheck.Android() || isMobileCheck.BlackBerry() || isMobileCheck.iOS() || isMobileCheck.Windows());
    }
};

//個位數補零
function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

// check overflow
function isOver(scrollWidth, innerWidth) {
    // js scrollWidth會取四捨誤入整數
    let innerW = Math.round(innerWidth)
    return scrollWidth > innerW;
}

//小數四則運算
// 加法
function accAdd(num1, num2) {
    let r1, r2, m;
    try {
        r1 = num1.toString().split('.')[1].length;
    } catch (e) {
        r1 = 0;
    }
    try {
        r2 = num2.toString().split('.')[1].length;
    } catch (e) {
        r2 = 0;
    }
    m = Math.pow(10, Math.max(r1, r2));
    return Math.round(num1 * m + num2 * m) / m;
}

// 減法
function accSub(num1, num2) {
    let r1, r2, m, n;
    try {
        r1 = num1.toString().split('.')[1].length;
    } catch (e) {
        r1 = 0;
    }
    try {
        r2 = num2.toString().split('.')[1].length;
    } catch (e) {
        r2 = 0;
    }
    m = Math.pow(10, Math.max(r1, r2));
    n = (r1 >= r2) ? r1 : r2;
    return (Math.round(num1 * m + num2 * m) / m).toFixed(n);
}
// 乘法
function accMul(num1, num2) {
    let m = 0, r1, r2;
    let s1 = num1.toString();
    let s2 = num2.toString();
    try {
        m += s1.split(".")[1].length;
    } catch (e) { };
    try {
        m += s2.split(".")[1].length;
    } catch (e) { };
    r1 = Number(num1.toString().replace(".", ""));
    r2 = Number(num2.toString().replace(".", ""));
    return r1 * r2 / Math.pow(10, m);
}

// 除法
function accDiv(num1, num2) {
    let t1, t2, r1, r2;
    try {
        t1 = num1.toString().split('.')[1].length;
    } catch (e) {
        t1 = 0;
    }
    try {
        t2 = num2.toString().split('.')[1].length;
    } catch (e) {
        t2 = 0;
    }
    r1 = Number(num1.toString().replace(".", ""));
    r2 = Number(num2.toString().replace(".", ""));
    return (r1 / r2) * Math.pow(10, (t2 - t1));
}

function floatN(x, n) {
    return Number.parseFloat(x).toFixed(n);
}

// number add commas
function numberWithCommas(x) {
    if (x) {
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    }

}

//dateTime slice
function sliceToDate(dateTimeStr, str) {
    let dt = new Date(dateTimeStr);
    return dt.getFullYear() + str + (dt.getMonth() + 1) + str + dt.getDate();
}

function sliceToMin(dateTimeStr, str) {
    let dt = new Date(dateTimeStr);
    return dt.getFullYear() + str + (dt.getMonth() + 1) + str + dt.getDate() + ' ' + addZero(dt.getHours()) + ':' + addZero(dt.getMinutes());
}

//img lazy loading
function onEnterView(entries, observer) {
    for (let entry of entries) {
        if (entry.isIntersecting) {
            // 監視目標進入畫面
            const img = entry.target
            img.setAttribute('src', img.dataset.src) // 把值塞回 src
            img.removeAttribute('data-src')
            observer.unobserve(img) // 取消監視
        }
    }
}

// img lazy loading
const watcher = new IntersectionObserver(onEnterView)
const lazyImages = document.querySelectorAll('img.lazyImg')
for (let image of lazyImages) {
    watcher.observe(image) // 開始監視
}

// ---------------------------------------------------
// 所有form post用表單內儲存用以阻擋重複發送的檢核變數
function formSetSubmitting() {
    let allForm = document.querySelectorAll('form');
    allForm.forEach(element => {
        element.submitting = false;
    });
}

// admin nav
function adminShowNavCurrent($nowNav) {
    $nowNav.addClass('active');
    $nowNav.parents('.list-item-main').find('.nav-title').addClass('active')
}

// showMsg
function showErrorMsg(str) {
    $('#errorMsg').text(str);
    $('#errorPopUp').modal('show');
}

function showSuccessMsg(str) {
    $('#successMsg').text(str);
    $('#successPopUp').modal('show');
}

function showConfirmMsg(str) {
    $('#confirmInfo').html(str);
    $('#confirmPopUp').modal('show');
}

function showSuccessPopup(str) {
    $('#successMsg').text(str);
    $('#successPopUp').animate({
        "top": "0"
    },500).delay(1000).animate({
        "top": "-100px"
    },500)
}

function showSuccessTopPopup(str) {
    $('#successTopMsg').text(str);
    $('#successTopPopUp').animate({
        "top": "0"
    },500).delay(1000).animate({
        "top": "-100px"
    },500)
}

// checkLimit
function checkLimit(limitArr, limitID) {
    // TODO:check limit
    return true
}

//get item sn
function getItemNum(i, order, perPage, pageNow, totalItem) {
    let itemNum = 1;
    switch (order) {
        case "ASC":
            itemNum = perPage * (pageNow - 1) + 1 + i;
            break;
        case "DESC":
            itemNum = totalItem - perPage * (pageNow - 1) - i;
            break;
        default:
            return false;
    }
    return itemNum;
}

// setAll & setNull
function setAll(obj, val) {
    /* Duplicated with @Maksim Kalmykov
        for(index in obj) if(obj.hasOwnProperty(index))
            obj[index] = val;
    */
    Object.keys(obj).forEach(function (index) {
        obj[index] = val
    });
}
function setNull(obj) {
    setAll(obj, null);
}

/** Set local storage item
 * 
 * @param key
 * @param value
 * @param expire
 * @returns
 */
function setLocalStoreItem(key, value, expire){
	let obj = {
			time:new Date().getTime(),
			value:value,
			expire:expire
	}
	
	let objStr = JSON.stringify(obj);
	localStorage.setItem(key, objStr);
}

/** validation of local storage if expired
 * 
 * @param name
 * @param effect, for effect function
 * @returns
 */
function validateLocalStoreItem(name, effect){
	let timer = setInterval(function () {
	    if(localStorage.getItem(name)){
	        let valueStr = localStorage.getItem(name);
	        var valueObj = JSON.parse(valueStr);
//	        console.log(new Date().getTime() - valueObj.time);
	        if(new Date().getTime() - valueObj.time >= valueObj.expire){
	            localStorage.removeItem(name)
	        }
	    }else{
	        alert('localStorage:' + name + '已失效');
	        effect();
//	        location.replace("/admin/login");
	        clearInterval(timer);
	    }
	},1000) 
	
}

/** Convert object to query string
 * 
 * @param obj
 * @returns
 */
function convertObjectToQueryString(obj){
	var str = "";
	for (var key in obj) {
	    if (str != "") {
	        str += "&";
	    }
	    str += key + "=" + encodeURIComponent(obj[key]);
	}
	return str;
}

/**
 * 
 * @returns
 */
function convertQueryStringToObject(){
	let search = location.search.substring(1);
	return JSON.parse('{"' + decodeURI(search).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}');
}

